###############################################################################
# gr ccl run method
###############################################################################


    def run(self):
        """@fn run
            Hilo encargado de la adquisicion de datos, decodificacion entre
            otras cosas en el modulo de comunicacion
        """
        acq_encoder = -1
        current_time2 = 0
        pause_time = 0
        change_inittime = False
        last_encoder = 0
        direccion = 0
        desborde = 0
        delta = 0
        depthabsolute = 0
        depthabsolute_ant = 0
        depthabsolute_filter = 0
        contdir = 0
        self.data_acum = []

        #create tool speed counter object
        tsc = tool_speed_counter()
        #create structure for data reading and organizing
        dataStructure = [
            {
                'header': (2,0),
                'value': (3,6),
                'sensor': 1,
                'calibrator': self.ccl.get_value},
            {
                'header': (6,1),
                'value': (7,10),
                'sensor': 2,
                'calibrator': self.gr.get_value},
            {
                'header': (10,2),
                'value': (11,14),
                'sensor': 3,
                'calibrator': self.ten.get_value},
            {
                'header': (14,3),
                'value': (15,19),
                'sensor': 4,
                'calibrator': None}
            ]
        ds = dataStructure
        #acquisition manager to determine if save or not based on mode
        if not self.kind:
            acq_mode = 'time_drive'
        elif self.kind:
            acq_mode = 'depth_drive'
            if self.Down:
                acq_mode += '_down'
            else:
                acq_mode += '_up'

        acq_manager = acquisition_mode_manager(
            acq_mode,
            self.save_data,
            self.sample_filter
            )
        #max range of encoder data
        overflowing = 65536
        #create encoder for time or depth drive
        enc = encoder(initial_depth=self.global_buffer['depth_reference'],
                      initial_encoder=0,
                      overflowing=overflowing,
                      calibration_function=self.depth.get_value)
        if debugging:
            #debugging
            debugFile = open('real_data', 'w+')
            debugHead = 'time:{mode[t]}, depth: {mode[d]}, up: {mode[up]}, down: {mode[down]}\n'
            op = {
                't': config.TRACKING_BY_TIME,
                'd': config.TRACKING_BY_DEPTH,
                'up': not self.Down,
                'down': self.Down
                }
            debugFile.write(debugHead.format(mode=op))
            debugTmpl = '{timestamp}: {data} // {raw}\n'
        #======================================================================
        # INITIALIZE FILTERS INFO
        # to do: take this configs from gui
        #======================================================================
        self.sync_ccl_gain(65)
        self.sync_gr_gain(105)
        self.sync_hpf(10)
        self.sync_lpf(30)
        #======================================================================
        # MAIN LOOP
        #======================================================================
        while not self.quit:
            #==================================================================
            # IF NOT PAUSE
            #==================================================================
            if self.Pause is False:

                if change_inittime is True:
                    self.init_time2 = time.time()
                    change_inittime = False
                #IF NO DATA FOUNDED
                if self.nodata:
                    if self.kind == config.TRACKING_BY_TIME:
                        #current_time = round(time.time()-self.init_time,2) #Usado para el modo de tiempo continuo
                        current_time2 = round(time.time() - self.init_time2, 2)  # Usado para el modo de pausa pausando el tiempo
                        if current_time2 != 0:
                            current_time2 = current_time2 + pause_time
                        if self.t_sensors[0] == 0 or current_time2 - self.t_sensors[0] >= self.sample_filter:
                            self.t_sensors[0] = round(current_time2, 2)
                            self.callback(self.t_sensors)
                #==============================================================
                # DATA ACQUISITION
                #==============================================================
                if self.connection is not None and self.connection.isOpen() and self.playing:
                    #try reading port
                    try:
                        raw_result = self.connection.read(1)
                        print raw_result
                    except serial.serialutil.SerialException:
                        #otherwise try to reconnect serial tool
                        worked = self.restaure_connection()
                        print 'worked? ' + str(worked)
                        if worked:
                            self.connect(
                                port=self.port,
                                baudrate=230400,
                                bytesize=8,
                                parity='N',
                                stopbits=1,
                                timeout=5,
                                xonxoff=0)
                            print 're-connected'
                            raw_result = self.connection.read(1)
                            print 'data read'
                            self.nodata = False
                            continue
                        else:
                            # cuando se desconecta
                            self.nodata = True  # ojo
                            continue
                    if len(raw_result) == 1:
                        self.nodata = False  # ojo
                        data = self.decode_string(raw_result)
                        if data[0] == 165:
                            raw_result = self.connection.read(18)
                            data = self.decode_string(raw_result)
                            if data[-1] == 165 and data[0] == 0:
                                data.insert(0, 165)
                                data.pop()
                            if len(data) != 18:
                                continue
                            self.idletime = None
                            data.insert(0, 165)
                            if data[0] == 165 and data[1] == 165:
                                if self.show_messages:
                                    self.show_messages([time.asctime(), 'Sync pass'])
                                self.sync_attempts = 0
                        #------------------------------------------------------
                        # get info from sensors
                        #------------------------------------------------------
                                for s in ds:
                                    if data[s['header'][0]] == s['header'][1]:
                                        if self.checksum(data[s['value'][0]:s['value'][1]]):
                                            if s['sensor'] == 4:
                                                self.t_sensors[s['sensor']] = self.list_to_hex(data[s['value'][0]:s['value'][1]-2])
                                                direccion = data[17]
                                            else:
                                                self.t_sensors[s['sensor']] = s['calibrator'](self.list_to_hex(data[s['value'][0]:s['value'][1]-1]))
                                        else:
                                            self.t_sensors[s['sensor']] = self.data_error
                                    else:
                                        self.t_sensors[s['sensor']] = self.data_error
                                if debugging:
                                    debugFile.write(
                                        debugTmpl.format(timestamp=time.time(),
                                        data=self.t_sensors,
                                        raw=data)
                                        )
                        #------------------------------------------------------
                                if self.show_messages:
                                    self.show_messages(
                                        [time.asctime(), 'All trames pass']
                                        )

                                if self.get_avggr:
                                    self.avggr.set_new_value(self.t_sensors[2])
                                    self.t_sensors[5] = self.avggr.get_value()

                                self.normal_trame = copy(self.t_sensors)

                        #------------------------------------------------------
                                current_time2 = round(
                                    time.time() - self.init_time2,
                                    2
                                    )#SAME
                                #will be the filter updated?
                                if self.updatefilter:
                                    self.sample_filter = copy(
                                        self.depthtimefilter
                                        )
                                    self.updatefilter = False
                                #update time before a pause
                                if current_time2 != 0:
                                    current_time2 = current_time2 + pause_time
                                #get the acquired data of encoder
                                acq_encoder = self.t_sensors[4]
                                #if no current depth, asign the last one
                                if acq_encoder == -1:
                                    acq_encoder = last_encoder
                        #------------------------------------------------------
                                #update encoder to calculate the new depth
                                depthabsolute = enc.update(
                                    acq_encoder,
                                    direccion
                                    )
                                self.global_buffer['depth'] = copy(
                                    depthabsolute
                                    )
                                #depend on the mode, save data
                                acq_manager.update(
                                    depthabsolute,
                                    depthabsolute_ant,
                                    self.normal_trame,
                                    self.t_sensors,
                                    current_time2
                                    )
                                self.callback(self.normal_trame)
                        #------------------------------------------------------
                                depthabsolute_ant = copy(depthabsolute)
                                # CALCULATE SPEED OF TOOL
                                # depthabsolute is on feet(FT)
                                # current_time2 is on seconds(sec)
                                tool_speed = tsc.update(current_time2, depthabsolute)
                                tool_speed = round(tool_speed, 4)
                                self.global_buffer['speed'] = tool_speed
                            else:
                                self.nodata = True  # ojo
                                print ("Sync Failed")
                                continue
                    else:
                        self.nodata = True  # ojo
                        print ("No Inco Data")
                        continue
                #==============================================================
                # IF TOO MUCH DATA IN SERIAL BUFFER, CLEAR BUFFER
                #==============================================================
                if self.connection is not None:
                    try:
                        if self.connection.inWaiting() >= (4 * 10) * 4:
                            self.connection.flushInput()
                    except serial.serialutil.SerialException:
                        pass
                # WAIT CERTAIN TIME TO READ DATA AGAIN
                time.sleep(self.reading_delay)
            #==================================================================
            # IF PAUSE
            #==================================================================
            else:
                pause_time = current_time2
                change_inittime = True
        print 'salio'


###############################################################################
# pt flowmeter depth calculation
###############################################################################


            #CALCULATE DEPTH
            if invalid is False:
                if len(data) > 3:
                    #if tracking by time
                    if self.kind == config.TRACKING_BY_TIME:
                        current_time2 = round(time() - self.init_time2, 2)
                        if self.updatefilter:
                            self.filter = copy(self.depthtimefilter)
                            self.updatefilter = False
                        if current_time2 != 0:
                            current_time2 = current_time2 + self.pause_time
                        if self.t_sensors[0] == 0 or current_time2 - self.t_sensors[0] >= self.filter:
                            self.t_sensors[0] = round(current_time2, 2)
                            ###################################################
                            #DEPTH CALCULATION
                            ###################################################
                            self.current_depth = self.t_sensors[12]

                            #will be the filter updated?
                            if self.updatefilter:
                                self.sample_filter = copy(
                                    self.depthtimefilter
                                    )
                                self.updatefilter = False
                            #update time before a pause
                            if current_time2 != 0:
                                current_time2 = current_time2 + pause_time
                            #get the acquired data of encoder
                            acq_encoder = self.t_sensors[4]
                            #if no current depth, asign the last one
                            if acq_encoder == -1:
                                acq_encoder = last_encoder
                    #------------------------------------------------------
                            #update encoder to calculate the new depth
                            depthabsolute = enc.update(
                                acq_encoder,
                                direccion
                                )
                            self.global_buffer['depth'] = copy(
                                depthabsolute
                                )
                            #depend on the mode, save data
                            acq_manager.update(
                                depthabsolute,
                                depthabsolute_ant,
                                self.normal_trame,
                                self.t_sensors,
                                current_time2
                                )
                            #show data in GUI
                            self.callback(self.normal_trame)
                    #------------------------------------------------------
                            depthabsolute_ant = copy(depthabsolute)
                            # CALCULATE SPEED OF TOOL
                            # depthabsolute is on feet(FT)
                            # current_time2 is on seconds(sec)
                            tool_speed = tsc.update(
                                current_time2,
                                depthabsolute
                                )
                            tool_speed = round(tool_speed, 4)
                            self.global_buffer['speed'] = tool_speed




###############################################################################
# NOT ANY MORE!!!!
###############################################################################



                            if self.update_reference:
                                self.depthabsolute = copy(self.depth_absolute)
                                self.update_reference = False
                            if self.current_depth == -1:
                                self.current_depth = self.lastdepth
                            # self.direccion == 1 means depth increases
                            if self.direccion == 1:
                                if self.contdir == 0:
                                    self.contdir = 1
                                else:
                                    self.delta = self.lastdepth - self.current_depth
                                    self.delta = self.depth.get_value(self.delta)
                                if self.delta > ceil(encoSpan/2):
                                    self.delta = ((encoSpan - self.lastdepth) + self.current_depth)
                                    self.delta = self.depth.get_value(self.delta)
                                    self.depthabsolute = self.depthabsolute + self.delta
                                elif self.delta > 0:
                                    self.delta = self.delta * -1
                                    if self.current_depth < encoSpan and self.lastdepth < encoSpan:
                                        self.depthabsolute = self.depthabsolute + (-1 * self.delta)
                                else:
                                    if self.current_depth < encoSpan and self.lastdepth < encoSpan:
                                        self.depthabsolute = self.depthabsolute + (-1 * self.delta)

                            # self.direccion == 2 means depth decreases
                            elif self.direccion == 2:
                                if self.contdir != 0:
                                    self.contdir = 0
                                else:
                                    self.delta = self.lastdepth - self.current_depth
                                    self.delta = self.depth.get_value(self.delta)
                                if self.delta < -ceil(encoSpan/2):
                                    if self.lastdepth <= encoSpan:
                                        self.delta = ((encoSpan - self.current_depth) + self.lastdepth)
                                        self.delta = self.depth.get_value(self.delta)
                                elif self.delta < 0:
                                    self.delta = self.delta * -1
                                    self.depthabsolute = self.depthabsolute - self.delta
                                else:
                                    self.depthabsolute = self.depthabsolute - self.delta
                                if self.depthabsolute < 0:
                                    self.depthabsolute = 0
                            self.lastdepth = self.current_depth
                            self.t_sensors[12] = round(self.depthabsolute, 2)

                            #if good data, save and show in graphic interface
                            if self.memory_data_incoming is False:
                                self.normal_trame = copy(self.t_sensors)
                                #show in graphic interface
                                self.callback(self.t_sensors)
                                #save data in the respective db format
                                if self.Down:
                                    if self.database:
                                        self.database(self.t_sensors)
                                else:
                                    if self.databaseUp:
                                        self.databaseUp(self.t_sensors)
                    #if tracking by depth
                    else:
                        current_time2 = round(time() - self.init_time2, 3)
                        if current_time2 != 0:
                            current_time2 = current_time2 + self.pause_time
                        self.current_depth = self.t_sensors[12]
                        ###################################################
                        #DEPTH CALCULATION
                        ###################################################
                        # self.direccion == -1 means stays the same
                        if self.current_depth == -1:
                            self.current_depth = self.lastdepth
                        if self.update_reference:
                            self.depthabsolute = copy(self.depth_absolute)
                            self.depthabsolute_ant = copy(self.depth_absolute)
                            self.lastdepth = self.current_depth
                            self.update_reference = False
                        if self.updatefilter:
                            self.filter = copy(self.depthtimefilter)
                            self.updatefilter = False
                        # self.direccion == 1 means depth increases
                        if self.direccion == 1:
                            if self.contdir == 0:
                                self.contdir = 1
                            else:
                                self.delta = self.lastdepth - self.current_depth
                                self.delta = self.depth.get_value(self.delta)
                                if self.delta > 0:
                                    self.delta = ((encoSpan - self.lastdepth) + self.current_depth)
                                    self.delta = self.depth.get_value(self.delta)
                                    self.depthabsolute = self.depthabsolute + self.delta
                                else:
                                    if self.current_depth < encoSpan and self.lastdepth < encoSpan:
                                        self.depthabsolute = self.depthabsolute + (-1 * self.delta)
                        # self.direccion == 2 means depth decreases
                        if self.direccion == 2:
                            if self.contdir != 0:
                                self.contdir = 0
                            else:
                                self.delta = self.lastdepth - self.current_depth
                                self.delta = self.depth.get_value(self.delta)
                                if self.delta < 0:
                                    if self.lastdepth <= encoSpan:
                                        self.delta = ((encoSpan - self.current_depth) + self.lastdepth)
                                        self.delta = self.depth.get_value(self.delta)
                                        self.depthabsolute = self.depthabsolute - self.delta
                                else:
                                    self.depthabsolute = self.depthabsolute - self.delta
                                if self.depthabsolute < 0:
                                    self.depthabsolute = 0
                        self.lastdepth = self.current_depth
                        self.t_sensors[0] = self.depthabsolute
                        if self.depthabsolute > self.depthabsolute_ant:
                            if self.delta < 0:
                                self.depthabsolute_filter += (-1 * self.delta)
                            else:
                                self.depthabsolute_filter += self.delta

                            if self.depthabsolute_filter < self.filter:
                                self.set_values_prom(self.t_sensors)
                            else:
                                self.depthabsolute_filter = 0
                                if len(self.data_acum) > 0:
                                    self.normal_trame = self.get_values_prom()
                                    self.normal_trame[0] = round(self.depthabsolute, 2)
                                    self.normal_trame[12] = current_time2
                                    if self.memory_data_incoming is False:
                                        self.callback(self.normal_trame)
                                        self.depthabsolute_filter = 0
                                        if self.Down:
                                            if self.database:
                                                self.database(self.normal_trame)
                                        else:
                                            if self.databaseUp:
                                                self.databaseUp(self.normal_trame)
                        if self.depthabsolute < self.depthabsolute_ant:
                            if self.delta < 0:
                                self.depthabsolute_filter += (-1 * self.delta)
                            else:
                                self.depthabsolute_filter += self.delta
                            if self.depthabsolute_filter < self.filter:
                                self.set_values_prom(self.t_sensors)
                            else:
                                self.depthabsolute_filter = 0
                                if len(self.data_acum) > 0:
                                    self.normal_trame = self.get_values_prom()
                                    self.normal_trame[0] = round(self.depthabsolute, 2)
                                    self.normal_trame[12] = current_time2
                                    #if data is good, show on GUI and save
                                    if self.memory_data_incoming is False:
                                        #show data on GUI
                                        self.callback(self.normal_trame)
                                        self.depthabsolute_filter = 0
                                        #save data in respective db
                                        if self.Down:
                                            if self.database:
                                                self.database(self.normal_trame)
                                        else:
                                            if self.databaseUp:
                                                self.databaseUp(self.normal_trame)
                        self.depthabsolute_ant = self.depthabsolute
#                else:
#                    self.asignTo('normal', 4)
#                    return