#BLAS_TNJ - Main Documentation File(EN)

##1 GENERAL

This folder contains documentation for users and developers.

##2 FILES IN DOCUMENTATION

/EN: Documentation in english.

##3 PROGRAM EXCECUTION

This software can be executed in any linux-based OS (tested on debian-based distributions)
run mainwindow.py to run the software in linux

Software can be excetuted on win X plataform (Win7 tested) without previous dependences.


##4 DEVELOPMENT ENVIROMENT

Software is currently developed in Spyder 2.2.5, mandatory dependencies are:

* windows:
    * Python 2.7  (Python 2.7.6)
    * GTK 2.24.8 Runtime (gtk2-runtime-2.24.8-2011-12-03-ash)
    * PYGTK 2.24.2 (pygtk-all-in-one-2.24.2.win32-py2.7)
    * PySerial (pyserial-2.7)
    * SetupTools (setuptools-0.6c11.win32-py2.7)
    * py2exe (py2exe-0.6.9.win32-py2.7)

* linux (debian repository or trough Pip):
    * Python 2.7  (--by default)
    * GTK 2.24.8 Runtime (libgtk2.0-dev)
    * PYGTK 2.24.2 (python-gtk2-dev)
    * PySerial (python-serial)
    * SetupTools (python-setuptools)
    * py2exe (--only windows)

