import sys
from cx_Freeze import setup, Executable

options = {
    'build_exe': {
        'includes': [
            'pkg_resources',
            'pyserial',
            'connectionManagement',
            'serial_connection',
            'virtualInstrument',
            'dialogs',
            'gtkbuilderwindow',
            'tools',
            'tracks',
            'utils',
            'database',
            'exportFile',
            'las',
            'las_templates',
            'config',
            'calibration_sensors',
            'settings',
            'tracks_configs',
            'utilities',
            'os',
            're',
            'time',
            'datetime',
            'logging',
            'random',
            'math',
            'pygtk',
            'threading'
        ],
        'excludes': ['IPython', 'matplotlib', 'numpy'],
        'path': sys.path + [
            'communication',
            'gui',
            'playback']
    }
}

executables = [
    Executable('pt.py'),
    Executable('chart_debugger.py')
]

setup(name='pt-flowmeter',
      version='0.0.10',
      description='read flowmeter data',
      options=options,
      executables=executables
      )