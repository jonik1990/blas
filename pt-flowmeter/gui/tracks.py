#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Emdyp.me
Revisado
Script: Tipo B

    Este modulo contiene las clases que permiten graficar los Tracks con los
    datos
    obtenidos desde los sensores, y se puede configurar para mostrar la
    cantidad de
    Tracks deseados

    Clases contenidas:
    I{Line,Track,LineLabel,TextTrack,TrackWidget,ComposeTrackWidget}
"""
from utilities import jpt_logger

import gtk
import gtk.gdk
import cairo
import pango
import threading
import gobject
import time

default_line_options = {
    'width': 2,
    'color': (0.7, 0, 0),
    'dashed': False,
    'dash': (3, 2),
    'last_value': False,
    'span': (0, 400),
    'wrapper': 0,
    'w_color': (0, 0, 0),
    'down': True,
    'erase_data': False
    }

"""
    Constantes - (default_line_options)
    ===================================
        Define las opciones por defecto para la clase I{Line}
            - width (int): define la anchura de la linea trazada en el Track
            - color (I{tuple(float red,float green,float blue)}): lista de 3
            flotantes para definir el color de la linea en modelo RGB
            - dashed (boolean): Indica si la linea seria continua o discontinua
            - dash (I{double *array)}): Se utiliza para establecer el patrÃ³n
            de trazos
            - max_data (int): Define el valor maximo de datos por tupla
            - last_value (boolean): Determina si se muestra el valor numerico
            del dato al final de la linea
            - span (I{tuple(int,int)}): Define el salto ...
            - wrapper (int): Define el envoltorio ...
            - w_color (I{tuple(float red,float green,float blue)}): Lista de 3
            flotantes para definir el color del wrapper en modelo RGB
            - down (boolean): Define la direcciÃ³n de la linea
            - erase_data (boolean): Define si se borran los datos previos
"""


class Line():
    """
        @b Clase @b que @b representa @b una @b linea @b en @b un @b Track
    """
    def __init__(self):
        """
            Constructor de la clase
        """
        self.set_options(default_line_options)
        self.data = []
        self.drawable = False
        self.len_data = 0
        #adding logging captabilities
        self.lg = jpt_logger(name='Line', mode='quiet')
        self.log = self.lg.cursor
        self.lg.disable()

    def set_options(self, options):
        """ @fn set_options
            Fija un nuevo conjunto de opciones para una linea
            TODO: Hay que cambiar la forma de hacer esto!!
            @param options contiene todos los parametros como lo define default
            _line_options (default_line_options)
        """
        if options is None:
            return
        try:
            self.width = options['width']
        except KeyError:
            pass
        try:
            self.color = options['color']
        except KeyError:
            pass
        try:
            self.dashed = options['dashed']
        except KeyError:
            pass
        try:
            self.dash = options['dash']
        except KeyError:
            pass
        try:
            self.last_value = options['last_value']
        except KeyError:
            pass
        try:
            self.span = options['span']
        except KeyError:
            pass
        try:
            self.wrapper = options['wrapper']
        except KeyError:
            pass
        try:
            self.w_color = options['w_color']
        except KeyError:
            pass
        try:
            self.down = options['down']
        except KeyError:
            pass
        try:
            self.erase_data = options['erase_data']
        except KeyError:
            pass
        self.abs_span = self.span[1] - self.span[0]

    def dump_data(self, new_data):
        """@fn dump_data
            Pone nuevos datos en una variable temporal para posterior
            depuracion de datos
            @param new_data par de datos (x,y) (TUPLE(INT, INT))
        """
        self.data = new_data
        self.len_data = len(self.data)
        self.drawable = True

    def clear_data(self):
        """@fn clear_data
            Se encarga de limpiar la data de la linea
        """
        self.data = []
        self.drawable = False
        self.len_data = 0

    def set_data(self, raw_data):
        """@fn set_data
            añade una nueva tupla de datos al conjunto de datos de la linea,
            puede estar configurado para borrar los datos anteriores o por el
            contrario guardarlos
            @param new_data nuevo par de datos (x,y) para la linea
            (TUPLE(INT, INT))
        """
        self.log.debug(
            'set data: self.down?: %s, self.drawable?: %s, self.data: %s',
            self.down,
            self.drawable,
            self.data
            )
        self.log.debug(
            'set data: self.len_data: %s, raw_data: %s',
            self.len_data,
            raw_data
            )
        if self.drawable:
            if self.erase_data:
                if self.down:
                    while self.data[-1][1] >= raw_data[1]:
                        self.data.pop()
                        self.len_data -= 1
                        if len(self.data) == 0:
                            break
                else:
                    while self.data[-1][1] <= raw_data[1]:
                        self.data.pop()
                        self.len_data -= 1
                        if self.len(self.data) == 0:
                            break
            else:
                if self.down:
                    if self.data[-1][1] >= raw_data[1]:
                        return
                else:
                    if self.data[-1][1] <= raw_data[1]:
                        return
            self.len_data += 1
            self.data.append(raw_data)

        else:
            self.data.append(raw_data)
            self.len_data += 1
            if len(self.data) >= 2:
                self.drawable = True


default_track_options = {
    'bg_color': '#ffffff',
    'size_request': (-1, -1),
    'width_scale': 100,
    'height_scale': 100,
    'h_grids': 5,  # numero de lineas horizontales
    'v_grids': 10,  # numero de lineas verticales
    'grid_line_width': 1,
    'grid_dashed': False,
    'strong_grid': 10,  # cada cuanto se hace una linea vertical fuerte
    'grid_color': (0.9, 0.9, 0.9),
    'strong_grid_color': (0, 0, 0),
    'begin_scrolling': 40,  # cuanndo empieza a hacer scroll
    'pointer_label': False,
    'autoscroll': True}

"""
    Constantes - (default_track_options)
    ===================================
        Define las opciones por defecto para la clase I{Track}
            - bg_color (string): Define el color de fondo del Track en formato hexadecimal
            - size_request (I{tuple(int,int)}): Para definir el tamaÃ±o minimo (ancho, alto) del widget
            - width_scale (int): Fija la escala a lo ancho para el track
            - height_scale (int): Fija la escala a lo alto para el track
            - h_grids (int): Define cada cuanto se dibujan lineas horizontales
            - v_grids (int): Define cada cuanto se dibujan lineas verticales
            - grid_line_width (int): Determina el ancho de las lineas de la rejilla
            - grid_dashed (boolean): si es True, las lineas de la rejilla seran punteadas
            - strong_grid (int): Determina cada cuanto se dibujan lienas verticales gruesas en la rejilla
            - grid_color (I{tuple(float red,float green,float blue)}): Lista de 3 flotantes para definir el color de la rejilla en modelo RGB
            - strong_grid_color (I{tuple(float red,float green,float blue)}): Lista de 3 flotantes para definir el color de las lineas gruesas de la rejilla en modelo RGB
            - begin_scrolling (int): A que distancia se inicia a hacer scroll
            - pointer_label (boolean): si es True, muestra las coordenadas (x, y) del cursor sobre la linea "actualmente no se usa"
            - autoscroll (boolean): si es True, se hace scroll automaticamente paraver el final de la imagen
"""


class Track(gtk.DrawingArea):
    """
        @b Clase @b que @b representa @b un @b Track
    """
    def __init__(self, father):
        """
            Constructor de la clase Track
            @param father Una referencia al contenedor o ventana padre del
            Track (gtk.ScrolledWindow)
        """
        gtk.DrawingArea.__init__(self)

        self.handle_expose = self.connect(
            "expose_event", self.expose
            )
        self.handle_motion = self.connect(
            "motion_notify_event", self.motion_notify
            )
        self.handle_notify = self.connect(
            "leave_notify_event", self.leave_notify
            )
        self.father = father

        self.lines = {}
        self.p = False
        self.h_scale = None
        self.w_scale = None
        self.height = None
        self.width = None
        self.current_height = None
        self.father_height = None
        self.pointer = None  # coordenadas x, y del puntero cuando esta
                            #sobre el widget
        self.layout = self.create_pango_layout('')
        desc = pango.FontDescription("mono 8")
        desc.set_weight(pango.WEIGHT_BOLD)
        self.layout.set_font_description(desc)

        self.set_options(default_track_options)
        self.Init = True

    def add_line(self, line_id, line):
        """@fn add_line
            añade una nueva linea a este Track
            @param line_id el id de la nueva linea (INT)
            @param line un objeto de clase Linea (Line)
        """
        self.lines[line_id] = line

    def pr(self):
        """@fn pr
            programa un redibujado
        """
        self.p = True
        self.queue_draw()

    def set_options(self, options):
        """@fn set_options
            Fija un nuevo conjunto de opciones para un Track
            @param options: contiene todos los parametros como lo define
            default_track_options (default_track_options)
        """
        if options is None:
            return
        try:
            self.bg_color = gtk.gdk.color_parse(options['bg_color'])  # @UndefinedVariable
            self.modify_bg(gtk.STATE_NORMAL, self.bg_color)
        except KeyError:
            pass
        try:
            self.size_request = options['size_request']
            self.set_size_request(*self.size_request)
        except KeyError:
            pass
        try:
            self.width_scale = options['width_scale']
        except KeyError:
            pass
        try:
            self.height_scale = options['height_scale']
        except KeyError:
            pass
        try:
            self.h_grids = options['h_grids']
        except KeyError:
            pass
        try:
            self.v_grids = options['v_grids']
        except KeyError:
            pass
        try:
            self.grid_line_width = options['grid_line_width']
        except KeyError:
            pass
        try:
            self.strong_grid = options['strong_grid']
        except KeyError:
            pass
        try:
            self.grid_color = options['grid_color']
        except KeyError:
            pass
        try:
            self.strong_grid_color = options['strong_grid_color']
        except KeyError:
            pass
        try:
            self.grid_dashed = options['grid_dashed']
        except KeyError:
            pass
        try:
            self.begin_scrolling = options['begin_scrolling']
        except KeyError:
            pass
        try:
            self.pointer_label = options['pointer_label']
            if self.pointer_label:
                self.add_events(
                    gtk.gdk.POINTER_MOTION_MASK | gtk.gdk.LEAVE_NOTIFY_MASK  # @UndefinedVariable
                    | gtk.gdk.BUTTON_PRESS_MASK | gtk.gdk.BUTTON1_MOTION_MASK)  # @UndefinedVariable
        except KeyError:
            pass
        try:
            self.autoscroll = options['autoscroll']
        except KeyError:
            pass

        self.queue_draw()

    def leave_notify(self, widget, event):
        """@fn leave_notify
            Callback que redibuja cuando el evento leave notify event ocurre
            @param widget el widget que recibio la señal (gtk.Widget)
            @param event el evento que desencadeno la señal (gtk.gdk.Event)
        """
        #self.pointer = None
        #self.queue_draw()
        pass

    def motion_notify(self, widget, event):
        """@fn motion_notify
            Callback que obtiene la ubicacion del puntero y redibuja cuando
            el evento motion notify event ocurre
            @param widget el widget que recibio la señal (gtk.Widget)
            @param event el evento que desencadeno la señal (gtk.gdk.Event)
        """
#        NO IMPLEMENTADO
        #self.pointer = [event.x, event.y]
        #self.queue_draw()
        pass

    def expose(self, widget, event):
        """@fn expose
            Callback que inicializa el contexto grafico y llama al proceso de
            dibujo para el Track
            @param widget: el widget que recibe la señal y sobre el cual se
            dibujara (gtk.Widget)
            @param event: el evento que desencadeno la señal (gtk.gdk.Event)
        """
        self.context = widget.window.cairo_create()
        self.context.rectangle(event.area.x, event.area.y,
                event.area.width, event.area.height)
        self.context.clip()

        self.height = self.get_allocation().height
        self.width = self.get_allocation().width

        if self.father_height is None:
            self.h_scale = self.height / float(self.height_scale)
        else:
            self.h_scale = self.father_height / float(self.height_scale)

        self.draw_grill()
        new_height = None
        for line in self.lines.values():
            if not line.drawable:
                continue
            self.w_scale = self.width / float(line.abs_span)

            """
            Se quito esta funcionalidad que permite saber las coordenadas x,y
            con poner el mouse encima de la grafica debido a que ahora las
            lineas son independientes
            if self.pointer_label:
                if self.pointer is not None:
                    self.pointer[1] = self.get_pointer()[1]
                    self.draw_mouse_label()
            """
            if line.down:
                if(len(line.data)) > 0:
                    self.current_height = int(round(line.data[-1][1] * self.h_scale)) + self.begin_scrolling

                    adj = self.father.get_vadjustment()

                    if self.current_height > adj.upper:  # agrandar el DrawingArea conforme hay mas datos
                        self.set_size_request(self.size_request[0], self.current_height + int(self.father_height / 1.5))

                    if self.autoscroll:
                        adj.set_value(adj.upper - adj.page_size)
            else:
                if(len(line.data)) > 0:
                    self.current_height = int(round(line.data[-1][1] * self.h_scale)) + self.begin_scrolling
                    new_height = int(round(line.data[0][1] * self.h_scale))
                    adj = self.father.get_vadjustment()
                    # self.set_size_request(300, new_height)
                    self.set_size_request(self.size_request[0], new_height)

                    """if self.Init:
                        #print "Datos: ",self.current_height, new_height
                        adj.set_value(new_height - 450)
                        if self.current_height - adj.get_value() <= 100:
                            self.Init = False
                    """

                    if self.autoscroll:
                        if self.current_height - adj.get_value() <= 100:
                            adj.set_value(self.current_height - int(self.father_height / 1.5))

                    """self.current_height = int(round(line.data[-1][1]*self.h_scale))+self.begin_scrolling
                    new_height = int(round(line.data[0][-1]*self.h_scale))
                    adj = self.father.get_vadjustment()
                    self.set_size_request(self.size_request[0], new_height)
                    if self.autoscroll:
                        #adj = self.father.get_vadjustment()
                        if line.data[0][1]-line.data[-1][1]>=self.height_scale:
                            adj.set_value(int(round(line.data[-1][1]*self.h_scale)))
                        else:
                            adj.set_value(adj.upper-adj.page_size)
                    """

            if(len(line.data)) > 0:
                self.draw_line(line)

            if self.p:
                surface = self.context.get_target()
                #print type(surface)
                surface.write_to_png('img.png')

    def do_image(self):
        """@fn do_image
            Volcar los contenidos graficos a una imagen
        """
        surface = self.context.get_target()
        surface.write_to_png('img.png')
        """
        adj = self.father.get_vadjustment()
        count = 0
        while count * adj.page_size <= adj.upper:
            adj.set_value(count * adj.page_size)

            break
        """

    def draw_mouse_label(self):
        """#fn draw_mouse_label
            Dibuja la posición del puntero del mouse
        """
        #self.context.set_source_rgb(*self.lines_color)
        lbl = '(' + str(int(round(self.pointer[0] / self.w_scale))) \
            + ',' + str(int(round(self.pointer[1] / self.h_scale))) + ')'
        self.layout.set_text(lbl)
        layout_height = self.layout.get_pixel_size()[1]
        layout_width = self.layout.get_pixel_size()[0]
        if self.pointer[0] + (layout_width / 2) - self.width > 0:
            self.context.move_to(
                self.width - layout_width,
                self.pointer[1] - layout_height)
        elif self.pointer[0] - (layout_width / 2) < 0:
            self.context.move_to(
                0,
                self.pointer[1] - layout_height
                )
        else:
            self.context.move_to(
                (self.pointer[0]) - (layout_width / 2),
                self.pointer[1] - layout_height
                )
        self.context.show_layout(self.layout)

    def draw_grill(self):
        """@fn draw_grill
            Dibuja la rejilla del Track
        """
        adj = self.father.get_vadjustment()
        y = adj.get_value()
        yp = adj.page_size + y
        #print y, yp

        self.context.set_source_rgb(*self.grid_color)
        self.context.set_line_width(self.grid_line_width)
        if self.grid_dashed:
            self.context.set_dash((2, 3))  # params: tamaÃ±o del segmento, separacion
        else:
            self.context.set_dash((1, 0))

        #lineas verticales
        for i in range(int(self.v_grids) + 1):
            x = i * self.width / self.v_grids
            self.context.move_to(x + 0.5, y)  # fixed para cairo
            self.context.line_to(x + 0.5, yp)
        self.context.stroke()

        #lineas horizontales
        line_count = int((adj.get_value() / (self.h_grids * self.h_scale)) - 1)
        h_line = line_count * (self.h_grids * self.h_scale)

        while h_line <= yp:
            line_count += 1
            h_line += self.h_grids * self.h_scale
            if line_count % (self.strong_grid) == 0:
                self.context.fill()
                self.context.set_source_rgb(*self.strong_grid_color)
                self.context.rectangle(0, h_line - 1, self.width, self.grid_line_width * 2)
                self.context.fill()
                self.context.set_source_rgb(*self.grid_color)
            elif line_count % (self.strong_grid / 2) == 0:
                self.context.fill()
                self.context.set_source_rgb(*self.strong_grid_color)
                self.context.rectangle(0, h_line, self.width, self.grid_line_width)
                self.context.fill()
                self.context.set_source_rgb(*self.grid_color)
            else:
                self.context.set_source_rgb(*self.grid_color)
                self.context.move_to(0, h_line + 0.5)  # fixed para cairo
                self.context.line_to(self.width, h_line + 0.5)
                self.context.stroke()

    def draw_line(self, line):
        """@fn draw_line
            Dibuja un objeto Line sobre el Track
            @param line El objeto Line a dibujar (Line)
        """
        #for line in self.lines.values():
            #if not line.drawable:
                #continue

        adj = self.father.get_vadjustment()
        y = adj.get_value() / self.h_scale
        yp = (adj.page_size / self.h_scale) + y

        self.context.set_line_width(line.width)
        if line.dashed:
            self.context.set_dash(line.dash)  # params: tamaÃ±o del segmento, separacion
        else:
            self.context.set_dash((1, 0))
        data = line.data
        offset = -line.span[0]
        abs_span = line.abs_span
        wrapper = line.wrapper
        iterate = range(line.len_data - 1)
        for i in iterate:
            #print data[i-1][1]
            #print data[i+1][1]
            #important: commented the one that dont let graph upside data
            #if data[i + 1][1] < y or data[i - 1][1] > yp:
                #continue

            self.context.set_source_rgb(*line.color)
            x1 = (data[i][0] + offset)
            x2 = (data[i + 1][0] + offset)

            #print line.len_data, len(data)
            self.context.move_to(
                x1 * self.w_scale,
                data[i][1] * self.h_scale
                )
            self.context.line_to(
                x2 * self.w_scale,
                data[i + 1][1] * self.h_scale
                )
            self.context.stroke()

            #draw wrapper
            #se puede juntar los dos ciclos?
            for w in range(wrapper):
                self.context.set_source_rgb(*line.w_color)
                if x1 > abs_span or x2 > abs_span:
                    x1 -= abs_span
                    x2 -= abs_span
                    self.context.move_to(
                        x1 * self.w_scale,
                        data[i][1] * self.h_scale
                        )
                    self.context.line_to(
                        x2 * self.w_scale,
                        data[i + 1][1] * self.h_scale
                        )
                else:
                    break
                self.context.stroke()
            x1 = (data[i][0])
            x2 = (data[i + 1][0])
            for w in range(wrapper):
                if x1 < line.span[0] or x2 < line.span[0]:
                    self.context.set_source_rgb(*line.w_color)
                    x1 += abs_span + offset
                    x2 += abs_span + offset
                    self.context.move_to(
                        x1 * self.w_scale,
                        data[i][1] * self.h_scale
                        )
                    self.context.line_to(
                        x2 * self.w_scale,
                        data[i + 1][1] * self.h_scale
                        )
                else:
                    break
                self.context.stroke()

        if line.last_value:
            if line.down:
                y_offset = 5
            else:
                y_offset = -15  # tamaÃ±o de la fuente + 5
            self.context.set_source_rgb(*line.color)
            ##Esto esta super desordenado
            x = data[-1][0] + offset
            if wrapper >= 1:
                x = data[-1][0]
                if x < line.span[0]:
                    self.context.set_source_rgb(0, 0, 0)
                    x += abs_span + offset
                else:
                    x = data[-1][0] + offset
                    if x > abs_span:  # dice que color es si es wrapper
                        self.context.set_source_rgb(0, 0, 0)
                        x -= abs_span
            x_label = x * self.w_scale
            self.layout.set_text(str(data[-1][0]))
            layout_width = self.layout.get_pixel_size()[0]
            if x_label + (layout_width / 2) > self.width:
                x_label = x_label - layout_width
            elif x_label - (layout_width / 2) > 0:
                x_label = x_label - (layout_width / 2)
            self.context.move_to(x_label, data[-1][1] * self.h_scale + y_offset)
            self.context.show_layout(self.layout)

    def set_father_height(self, height):
        """@fn set_father_height
            establecer la altura obtenida del contenedor del Track
            @param height la altura a fijar (INT)
        """
        self.father_height = height
        if self.current_height:
            self.set_size_request(self.size_request[0], self.current_height)
        self.queue_draw()

    def dump_data(self, line_id, new_data):
        """@fn dump_data
            Pone datos de la linea especifica en una nueva variable para su posterior depuracion
            @param line_id Id del objeto de la clase Line (INT)
            @param new_data par de datos (x,y) (TUPLE(INT, INT))
        """
        line = self.lines[line_id]
        line.dump_data(new_data)

    def set_data(self, line_id, new_data):
        """@fn set_data
            fija datos para una linea especifica de este Track
            @param line_id: el id de la linea en este Track (INT)
            @param new_data: nuevo par de datos (x,y) para la linea (TUPLE(INT, INT))
        """
        line = self.lines[line_id]
        line.set_data(new_data)
        self.queue_draw()

    def clear_data(self, line_id):
        """@fn clear_data
            Se encarga de ejecutar una accion de limpiar los datos de una linea especifica
            @param line_id Id de la linea que se va limpiar
        """
        line = self.lines[line_id]
        line.clear_data()

    def disconnect_signals(self):
        """@fn disconnect_signals
            Se encarga de desconectar los callbacks para que no operen mas, para el modo del protocol checker
        """
        self.disconnect(self.handle_expose)
        self.disconnect(self.handle_motion)
        self.disconnect(self.handle_notify)
        self.Init = False

    def connect_signals(self):
        """@fn connect_signals
            Se encarga de conectar los callbacks para que operen, se djea de usar el modo del protocol checker
        """
        self.handle_expose = self.connect(
            "expose_event", self.expose)
        self.handle_motion = self.connect(
            "motion_notify_event", self.motion_notify)
        self.handle_notify = self.connect(
            "leave_notify_event", self.leave_notify)
        self.Init = True


default_labelgraph_options = {
    'bg_color': "#eee",
    'size_request': (-1, 20),
    'labels': (0, 400),
    'line_name': 'Track',
    'line_name_color': (0, 0, 0),
    'labels_color': (0, 0, 0)}

"""
    Constantes - (default_labelgraph_options)
    ===================================
        Define las opciones por defecto para la clase I{LineLabel}
            - bg_color (string): Define el color de fondo del LineLabel in formato hexadecimal
            - size_request (I{tuple(int,int)}): Lista para definir el tamaÃ±o minimo del LineLabel
            - labels (I{tuple(int,int)}): etiquetas de los valores minimo y maximo
            - line_name (string): El nombre de los datos que representa la lÃ­nea
            - line_name_color (I{tuple(float red,float green,float blue)}): Lista de 3 flotantes para definir el color del nombre de la linea en modelo RGB
            - labels_color (I{tuple(float red,float green,float blue)}): Lista de 3 flotantes para definir el color de las etiquetas numericas en modelo RGB
"""


class LineLabel(gtk.DrawingArea):
    """
        @b Clase @b que @b implementa @b la @b etiqueta @b dibujable @b de @b una @b Linea @b que @b pertenece @b a @b un @b track
    """
    def __init__(self):
        """
            Constructor de la clase
        """
        gtk.DrawingArea.__init__(self)
        self.connect("expose_event", self.expose)

        self.layout = self.create_pango_layout('')
        desc = pango.FontDescription("mono 10")
        desc.set_weight(pango.WEIGHT_BOLD)
        self.layout.set_font_description(desc)

        self.set_options(default_labelgraph_options)
        self.context = None
        self.p = False

    def pr(self):
        """@fn pr
            programa un redibujado
        """
        self.p = True
        self.queue_draw()

    def set_options(self, options):
        """@fn set_options
            Fija un nuevo conjunto de opciones para un LineLabel
            @param options: contiene todos los parametros como lo define
            default_labelgraph_options (default_labelgraph_options)
        """
        if options is None:
            return
        try:
            self.bg_color = gtk.gdk.color_parse(options['bg_color'])  # @UndefinedVariable
            self.modify_bg(gtk.STATE_NORMAL, self.bg_color)
        except KeyError:
            pass
        try:
            self.size_request = options['size_request']
            self.set_size_request(*self.size_request)
        except KeyError:
            pass
        try:
            self.labels = options['labels']
        except KeyError:
            pass
        try:
            self.line_name = options['line_name']
        except KeyError:
            pass
        try:
            self.line_name_color = options['line_name_color']
        except KeyError:
            pass
        try:
            self.labels_color = options['labels_color']
        except KeyError:
            pass
        self.queue_draw()

    def expose(self, widget, event):
        """@fn expose
            Callback que inicializa el contexto grafico y llama al proceso
            de dibujo para el LineLabel
            @param widget el widget que recibe la señal y sobre el cual se
            dibujara (gtk.Widget)
            @param event el evento que desencadeno la señal (gtk.gdk.Event)
        """
        self.context = widget.window.cairo_create()

        self.context.rectangle(event.area.x, event.area.y,
                event.area.width, event.area.height)
        self.context.clip()
        self.width = self.get_allocation().width
        self.height = self.get_allocation().height
        self.draw_text()

        if self.p:
            surface = self.context.get_target()
            #print type(surface)
            surface.write_to_png('img.png')

    def draw_text(self):
        """@fn draw_text
            Dibuja el LineLabel
        """
        self.context.set_source_rgb(*self.line_name_color)
        self.layout.set_text(self.line_name)
        self.layout.set_width(self.width * pango.SCALE)
        self.layout.set_alignment(pango.ALIGN_CENTER)
        self.set_size_request(self.size_request[0],
                              self.layout.get_pixel_size()[1] + 25)
        self.context.move_to(0, self.height - 30)
        self.context.show_layout(self.layout)

        new_h = self.get_allocation().height
        self.context.set_source_rgb(*self.labels_color)

        self.layout.set_width(-1)
        self.context.move_to(5, new_h - 15)
        self.layout.set_text(str(self.labels[0]))
        self.context.show_layout(self.layout)
        self.layout.set_text(str(self.labels[1]))
        self.context.move_to(
            self.width - self.layout.get_pixel_size()[0] - 5, new_h - 15
            )
        self.context.show_layout(self.layout)

        self.context.set_source_rgb(*self.line_name_color)
        self.context.move_to(5, new_h - self.layout.get_pixel_size()[1])
        self.context.line_to(
            self.width - 5, new_h - self.layout.get_pixel_size()[1]
            )
        self.context.stroke()

        self.context.set_source_rgb(0, 0, 0)
        self.context.set_line_width(1)
        self.context.move_to(0, self.height - 0.5)
        self.context.line_to(self.width, self.height - 0.5)
        self.context.stroke()


default_textgraph_options = {
    'bg_color': "#000066",
    'size_request': (40, -1),
    'height_scale': 100,
    'step': 10,
    'text_color': (1, 1, 1)}

"""
    Constantes - (default_textgraph_options)
    ===================================
        Define las opciones por defecto para la clase I{TextTrack}
            - bg_color (string): Define el color de fondo del TextTrack in formato hexadecimal
            - size_request (I{tuple(int,int)}): Lista para definir el tamaÃ±o minimo del TextTrack
            - height_scale (int): Indica la escala a lo alto
            - step (int): Usado para establecer el numero en cada paso de conteo sobre el eje vertical
            - text_color (I{tuple(float red,float green,float blue)}): Lista de 3 flotantes para definir el color de textoen modelo RGB
"""


class TextTrack(gtk.DrawingArea):
    """
        @b Clase @b que @b implementa @b la @b TextTrack, @b como @b un @b eje @b vertical @b para @b los @b datos @b de @b los @b Tracks
    """
    def __init__(self, father):
        """
            Constructor de la clase
        """
        gtk.DrawingArea.__init__(self)
        self.connect("expose_event", self.expose)

        self.father = father

        self.p = False
        self.father_height = None
        self.width = None
        self.height = None
        self.h_scale = None
        self.autoscroll = True

        self.layout = self.create_pango_layout('')
        desc = pango.FontDescription("mono 8")
        desc.set_weight(pango.WEIGHT_BOLD)
        self.layout.set_font_description(desc)
        self.layout.set_width(40)
        #self.layout.set_wrap(pango.WRAP_CHAR) para mostrar verticalmente
        self.layout.set_spacing(-4 * pango.SCALE)

        self.set_options(default_textgraph_options)

    def pr(self):
        """@fn pr
            programa un redibujado
        """
        self.p = True
        self.queue_draw()

    def set_options(self, options):
        """@fn set_options
            fija las opciones del TextTrack
            @param options: la altura a fijar (default_textgraph_options)
        """
        if options is None:
            return
        try:
            self.bg_color = gtk.gdk.color_parse(options['bg_color'])  # @UndefinedVariable
            self.modify_bg(gtk.STATE_NORMAL, self.bg_color)
        except KeyError:
            pass
        try:
            self.size_request = options['size_request']
            self.set_size_request(*self.size_request)
        except KeyError:
            pass
        try:
            self.height_scale = options['height_scale']
        except KeyError:
            pass
        try:
            self.step = options['step']
        except KeyError:
            pass
        try:
            self.text_color = options['text_color']
        except KeyError:
            pass

    def set_father_height(self, height):
        """@fn set_father_height
            establecer la altura obtenida del contenedor del TextTrack
            @param height: la altura a fijar (INT)
        """
        self.father_height = height
        self.queue_draw()

    def expose(self, widget, event):
        """@fn expose
            Callback que inicializa el contexto grafico del TextTrack y
            realiza los calculos de escalado antes de llamar a los procesos
            de dibujado
            @param widget: el widget que recibe la señal y sobre el cual se
            dibujara (gtk.Widget)
            @param event: el evento que desencadeno la señal (gtk.gdk.Event)
        """
        self.context = widget.window.cairo_create()
        self.context.rectangle(event.area.x, event.area.y,
                event.area.width, event.area.height)
        self.context.clip()

        self.height = self.get_allocation().height
        self.width = self.get_allocation().width
        self.h_scale = self.father_height / float(self.height_scale)

        self.draw()
        if self.p:
            surface = self.context.get_target()
            #print type(surface)
            surface.write_to_png('img.png')

    def draw(self):
        """@fn draw
            Dibujar el TextTrack como un eje vertical para los datos de los
            Tracks
        """
        adj = self.father.get_vadjustment()
        y = adj.get_value()
        yp = (adj.page_size) + y

        h_text = 0
        text = 0
        while h_text <= self.height:

            text += self.step
            h_text += self.step * self.h_scale
            if h_text < y or h_text > yp:
                continue

            self.context.set_source_rgb(*self.text_color)
            self.layout.set_text(str(text))
            self.context.move_to(
                (self.width / 2) - (self.layout.get_pixel_size()[0] / 2), h_text - (self.layout.get_pixel_size()[1] / 2))
            self.context.show_layout(self.layout)


class TrackWidget(gtk.VBox):
    """
        @b Clase @b que @b implementa @b la @b Trackwidget, @b como @b un @b
        contenedor @b vertical @b para @b el @b Track
    """
    def __init__(self, father, allow_resize=True):
        """
            Constructor de la clase
            @param father Una referencia al contenedor o ventana padre del
            TrackWidget (gtk.ScrolledWindow)
            @param allow_resize indica si se permite el cambio de tamaño
            (BOOLEAN)
        """
        #add logger to debug stuff
        self.lg = jpt_logger(name='TrackWidget', mode='quiet')
        self.log = self.lg.cursor
        self.lg.disable()
        gtk.VBox.__init__(self)
        self.labels = gtk.VBox()
        self.father = father
        self.allow_resize = allow_resize

        self.track = Track(self.father)
        self.lines = {}

    def add_line(self, line_id):
        """@fn add_line
            añade una nueva linea al Track de este TrackWidget
            @param line_id: el id de la nueva linea (INT)
        """
        self.log.debug('line_id: %s', line_id)
        if self.track is None:
            print "Track is None"
            return
        line = Line()
        self.track.add_line(line_id, line)
        label = LineLabel()
        self.lines[line_id] = (line, label)
        if self.allow_resize:
            self.labels.pack_start(label, True, True)
        else:
            self.labels.pack_start(label, False, False)

    def set_track_options(self, track_options):
        """@fn set_track_options
            fija las nuevas opciones para el Track de este TrackWidget
            @param track_options contiene todos los parametros como se definen
            en default_track_options (default_track_options)
        """
        self.track.set_options(track_options)

    def set_line_options(self, line_id, line_options, label_options):
        """@fn set_line_options
            fija las opciones de linea y label para una linea en el Track de
            este TrackWidget
            @param wtrack_id el id del TrackWidget (INT)
            @param line_id el id de la linea en el TrackWidget (INT)
            @param line_options contiene todos los parametros como se definen
            en default_line_options (default_line_options)
            @param label_options: contiene todos los parametros como se definen
            en default_labelgraph_options (default_labelgraph_options)
        """
        self.lines[line_id][0].set_options(line_options)
        self.lines[line_id][1].set_options(label_options)

    def set_data(self, line_id, data):
        """@fn set_data
            fija datos para una linea especifica de un track de este TrackWidget
            @param line_id el id de la linea en este TrackWidget (INT)
            @param data nuevo par de datos (x,y) para la linea (TUPLE(INT, INT)
        """
        try:
            self.track.set_data(line_id, data)
        except:
            print "Error setting data: Line ", line_id


class ComposeTrackWidget(gtk.VBox):
    """
        Clase que implementa el widget compositor de Tracks para usar en una
        ventana
    """
    def __init__(self):
        """
            Constructor de la clase
        """
        #add logger to debug stuff
        self.lg = jpt_logger(name='ComposeTrackWidget', mode='quiet')
        self.log = self.lg.cursor
        self.lg.disable()
        gtk.VBox.__init__(self)

        self.labels_box = gtk.HBox()
        self.labels_box.set_spacing(2)
        #separator ayuda al espacio ocasionado por la scroll_bar
        separator = gtk.Label()
        separator.set_size_request(18, -1)
        self.labels_box.pack_end(separator, False, False)

        self.scrolled_window = gtk.ScrolledWindow()

        self.scrolled_window.set_policy(gtk.POLICY_NEVER, gtk.POLICY_ALWAYS)
        self.scrolled_window.connect("size-allocate", self.sw_size_allocate)
        self.scrolled_window.connect("scroll-event", self.sw_scroll_child)

        self.wtracks_box = gtk.HBox()
        self.wtracks_box.set_spacing(2)
        self.scrolled_window.add_with_viewport(self.wtracks_box)

        self.pack_start(self.labels_box, False, False)
        self.pack_start(self.scrolled_window, True, True)

        #USAR ESTO

        self.allow_vertical_resize = True

        self.wtracks = {}
        self.ttracks = {}

    def sw_scroll_child(self, *args):
        """@fn sw_scroll_child
            callback para realizar un peticion de dibujado del scroll hijo
            @param ttrack lista de los objetos de la calse TextTrack que han
            sido creados actualmente
        """
        for ttrack in self.ttracks.values():
            ttrack[1].queue_draw()

    def sw_size_allocate(self, widget, event):
        """@fn sw_size_allocate
            callback que informa a los tracks el nuevo tamaño de la ventana que los contiene
            @param widget El widget que recibe la señal (gtk.Widget)
            @param event la nueva asignacion de espacio del widget (gtk.gdk.Event)
        """
        if self.allow_vertical_resize:
            height = self.scrolled_window.get_allocation().height
            for wtrack in self.wtracks.values():
                wtrack.track.set_father_height(height)
            for ttrack in self.ttracks.values():
                ttrack[1].set_father_height(height)
        #self.queue_draw()

    def set_autoscroll(self, autoscroll):
        """@fn set_autoscroll
            Fija el valor de autoscroll de los Tracks en el compositor
            @param autoscroll: indica si los Tracks deben hacer scroll automatico (BOOLEAN)
        """
        for wtrack in self.wtracks.values():
            wtrack.track.autoscroll = autoscroll

    def add_wtrack(self, wtrack_id, allow_resize=True):
        """@fn add_wtrack
            añade una nuevo TrackWidget al compositor
            @param wtrack_id el id del TrackWidget (INT)
            @param allow_resize indica si se permite el cambio de tamaño (BOOLEAN)
        """
        wt = TrackWidget(self.scrolled_window, allow_resize=True)
        self.wtracks[wtrack_id] = wt

        if allow_resize:
            self.wtracks_box.pack_start(wt.track)
            self.labels_box.pack_start(wt.labels)
        else:
            self.wtracks_box.pack_start(wt.track, False, False)
            self.labels_box.pack_start(wt.labels, False, False)

    def add_line(self, wtrack_id, line_id):
        """@fn add_line
            añade una nueva linea a un TrackWidget especifico del compositor
            @param wtrack_id el id del track widget (INT)
            @param line_id el id de la nueva linea (INT)
        """
        self.log.debug('line_id: %s, %s', line_id, type(line_id))
        self.log.debug('wtracks: %s, %s', self.wtracks, type(self.wtracks))
        try:
            self.wtracks[wtrack_id].add_line(line_id)
        except Exception as e:
            self.log.debug('error happened on add_line: %s', e)

    def draw(self):
        """@fn draw
            Para realizar un peticion de dibujo tanto de lineas como de la pista de la escala
        """
        for wtrack in self.wtracks.values():
            wtrack.track.queue_draw()
            #print wtrack.track.lines[0].data

        for ttrack in self.ttracks.values():
            ttrack[1].queue_draw()

    def set_track_options(self, wtrack_id, track_options):
        """@fn set_track_options
            fija las nuevas opciones para un Track especifico del compositor
            @param wtrack_id el id del TrackWidget (INT)
            @param track_options contiene todos los parametros como se definen en default_track_options (default_track_options)
        """
        self.wtracks[wtrack_id].track.set_options(track_options)

    def set_ttrack_options(self, ttrack_id, ttrack_options):
        """@fn set_ttrack_options
            fija nuevas opciones para la escala TextTracl especifico del
            compositor de TextTrack
            @param wtrack_id el id del TrackWidget (INT)
            @param track_options contiene todos los parametros como se definen
            en default_textgraph_options (default_textgraph_options)
        """
        self.ttracks[ttrack_id][1].set_options(ttrack_options)

    def set_line_options(self, wtrack_id, line_id, line_options, label_options):
        """@fn set_line_options
            fija las opciones de linea y label para una linea especifica en un
            TrackWidget especifico del compositor
            @param wtrack_id el id del TrackWidget (INT)
            @param line_id el id de la linea en el TrackWidget (INT)
            @param line_options contiene todos los parametros como se definen
            en default_line_options (default_line_options)
            @param label_options contiene todos los parametros como se definen
            en default_labelgraph_options (default_labelgraph_options)
        """
        try:
            self.wtracks[wtrack_id].lines[line_id][0].set_options(line_options)
        except Exception as e:
            self.log.debug(
                'self.wtracks[wtrack_id].lines: %s, %s',
                self.wtracks[wtrack_id].lines,
                type(self.wtracks[wtrack_id].lines)
                )
            self.log.debug(
                'self.wtracks[wtrack_id].lines[line_id]: %s, %s',
                self.wtracks[wtrack_id].lines[line_id],
                type(self.wtracks[wtrack_id].lines[line_id])
                )
            self.log.debug(
                'self.wtracks[wtrack_id].lines[line_id][0]: %s, %s',
                self.wtracks[wtrack_id].lines[line_id][0],
                type(self.wtracks[wtrack_id].lines[line_id][0])
                )
            self.log.debug('error happened on set_line_options: %s', e)
        try:
            self.wtracks[wtrack_id].lines[line_id][1].set_options(label_options)
        except Exception as e:
            self.log.debug(
                'self.wtracks[wtrack_id].lines: %s, %s',
                self.wtracks[wtrack_id].lines,
                type(self.wtracks[wtrack_id].lines)
                )
            self.log.debug(
                'self.wtracks[wtrack_id].lines[line_id]: %s, %s',
                self.wtracks[wtrack_id].lines[line_id],
                type(self.wtracks[wtrack_id].lines[line_id])
                )
            self.log.debug(
                'self.wtracks[wtrack_id].lines[line_id][0]: %s, %s',
                self.wtracks[wtrack_id].lines[line_id][1],
                type(self.wtracks[wtrack_id].lines[line_id][1])
                )
            self.log.debug('error happened on set_line_options: %s', e)

    def ClearData(self, wtrack_id, line_id):
        self.wtracks[wtrack_id].track.disconnect_signals()
        #time.sleep(0.1)
        self.wtracks[wtrack_id].lines[line_id][0].clear_data()
        self.wtracks[wtrack_id].track.connect_signals()

    def add_ttrack(self, ttrack_id, ttrack_options=None):
        """@fn add_ttrack
            añade un nuevo TextTrack al compositor
            @param ttrack_id: el id del nuevo TextTrack (INT)
            @param ttrack_options: contiene todos los parametros como se
            definen en default_textgraph_options (default_textgraph_options)
        """
        ttrack = TextTrack(self.scrolled_window)
        separator = gtk.Label()
        separator.set_size_request(40, -1)
        if ttrack_options is not None:
            ttrack.set_options(ttrack_options)
        self.ttracks[ttrack_id] = (separator, ttrack)
        self.wtracks_box.pack_start(ttrack, False, False)
        self.labels_box.pack_start(separator, False, False)

    def print_image(self):
        """@fn print_image
            imprime los Tracks, Lineas, Etiquetas y TextTracks (textos de tracks)
        """
        for wtrack in self.wtracks.values():
            wtrack.track.pr()
            for line, label in wtrack.lines.values():
                label.pr()
        for ttrack in self.ttracks.values():
            ttrack[1].pr()

    def dump_data(self, *args):
        """@fn dump_data
            Coloca data de un TrackWidget especifico
        """
        self.wtracks[args[0]].track.dump_data(args[1], args[2])

    def set_data(self, *args):
        """@fn set_data
            fija datos para una linea especifica de un track
            @param wtrack_id: el id del TrackWidget (INT)
            @param line_id: el id de la linea en el TrackWidget (INT)
            @param data: nuevo par de datos (x,y) para la linea (TUPLE(INT, INT))
        """
        #setting data in lines
        self.wtracks[args[0]].track.set_data(args[1], args[2])
        """
        try:
            self.wtracks[args[0]].track.set_data(args[1], args[2])
        except:
            print "Error setting data: Line ", line_id, wtrack_id
        """


