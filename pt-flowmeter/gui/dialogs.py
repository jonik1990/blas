# -*- encoding: utf-8 -*-
'''
JPT
Revisado: Pedro Rivera
Script: Tipo A
(ventanas de dialogos)

This script contains dialogs windows used in software, are clasified:

1. GENERAL DIALOGS
   Dialogs that can appear in all the software, includes only AboutDialog

2. LOGGING MODE DIALOGS
   Dialogs that appears on logging mode, here we can find:

    * SSDialog:  configuration dialog with all options, appears with click
                 on "setup" button.
    * SaveSSDialog:  save current configurations, appears with click on "save"
                     button.
    * OpenSSDialog:  open a .jss file with previous-saved configurations,
                     appears with click on "open" button.
    * ConfigTracksDialog:  configuration dialog that appears during data
                           acquisition by clicking "tracks" button.
    * MonitorModeLoggingDialog:  dialog open immediately when logging mode is
                                 selected

3. PLAYBACK DIALOGS
   Dialogs that appears on playback mode, here we can find:

    * ImportDBDialog:  import a saved .jdb file to plot, appears with click on
                       "open" button

4. PROTOCOL CHECKER DIALOGS
   Dialogs that appears on protocol checker, here we can find:

    * ConnectionCheckerDialog: appears when protocol checker is open

'''
import re
import gtk
import json
import time
import math
import types
import gobject
from gettext import gettext
from datetime import datetime

import config
from gui.utils import *
from tools import Tool
from playback.database import load_db
from communication.serial_connection import DavidConnection
from utilities import jpt_logger

from time import time, localtime

from copy import copy

###############################################################################
# 1. GENERAL DIALOGS
###############################################################################


class AboutDialog(object):
    """
         Clase que representa la ventana de dialogo que muestra informacion
         general  del  software
    """
    builder_file = config.gui_file('AboutDialog.glade')

    def __init__(self):
        """
            Constructor de la clase AboutDialog
        """
        self.builder = gtk.Builder()
        self.builder.add_from_file(self.builder_file)
        self.dialog = self.builder.get_object('dialog')

        self.builder.get_object('lbl_sw_name').set_text(config.SW_NAME)
        self.builder.get_object('lbl_sw_version').set_text(config.VERSION)
        company = 'JPT Consulting & Services and ECOPETROL S.A'
        copyright = '(c) Copyright {:} 2013 - 2016.  All rights reserved.'.format(company)
        self.builder.get_object('lbl_copyright').set_text(copyright)

        email = 'Support and Maintenance: ' + 'JPT Consulting & Services'
        self.builder.get_object('lbl_email').set_text(email)

        self.builder.connect_signals(self)
        self.dialog.show_all()
        self.dialog.run()

    def on_btn_close_clicked(self, button):
        """@fn on_btn_close_clicked
            Callback para cerrar la ventana de dialogo ABOUT
        """
        self.dialog.destroy()


class toolNameDialog(object):
    """dialog to configure the pt tool name"""
    builder_file = config.gui_file('ConfigToolDialog.glade')
    portReg = re.compile('/dev/[.]+|COM[\d]+')

    def __init__(self, communicationManager):
        """toolNameDialog class builder.
           requires a toolNameManager class object (not an instance of
           toolNameManager"""
        self.builder = gtk.Builder()
        #build dialog
        self.builder.add_from_file(self.builder_file)
        #get dialog instance
        self.dialog = self.builder.get_object('name_config_tool')
        #get input instances
        self.setidSb = self.builder.get_object('setidsb')
        self.getidLb = self.builder.get_object('getidlb')
        self.slavesCb = self.builder.get_object('slavescb')
        self.samplerateCb = self.builder.get_object('sampleratecb')
        self.pickportCb = self.builder.get_object('pickportcb')
        self.portslist = self.builder.get_object('portslist')
        #get output bar
        self.statusbar = self.builder.get_object('statusbar')
        #create communication manager
        self.comManager = communicationManager()
        #connect signals
        self.builder.connect_signals(self)
        #show the dialog
        self.dialog.show_all()
        self.dialog.run()

    def on_closebutton_clicked(self, button):
        """close dialog"""
        self.dialog.destroy()

    def pt_suitablePorts(self, button):
        """get suitable ports and fill the combobox asociated with that
           information"""
        suitable_ports = self.comManager.getSuitablePorts()
        if len(suitable_ports) == 0:
            self.showMsj('no ports')
            return
        else:
            self.showMsj(str(suitable_ports))
        #put those ports inside the combobox
        self.portslist.clear()
        for p in suitable_ports:
            self.portslist.append([p])
        self.pickportCb.set_active(0)
        self.pt_changePort(self.pickportCb)
        return

    def pt_changePort(self, combobox):
        """change the port in the comManager so serial communication can be
           established"""
        index = combobox.get_active()
        if index:
            #put the selected port in the comManager
            self.comManager.port = getChoice(combobox)

    def pt_slaves(self, combobox):
        """send to the tool how many slaves will be handled"""
        self.comManager.port = getChoice(self.pickportCb)
        numero = getChoice(self.slavesCb)
        status = self.comManager.perform('asignSlaves', numero)
        if status:
            self.showMsj('success')
        else:
            self.showMsj('failure')

    def pt_samplerate(self, button):
        """send to the tool the desired sample-rate"""
        self.comManager.port = getChoice(self.pickportCb)
        print self.comManager.port
        numero = getChoice(self.samplerateCb)
        status = self.comManager.perform('asignSamplerate', numero)
        if status:
            self.showMsj('success')
        else:
            self.showMsj('failure')

    def pt_setid(self, button):
        """send to the tool the desired id"""
        self.comManager.port = getChoice(self.pickportCb)
        numero = self.setidSb.get_value_as_int()
        status = self.comManager.perform('asignId', numero)
        status = status[0]
        if status:
            self.showMsj('success')
        else:
            self.showMsj('failure')

    def pt_getid(self, button):
        """send to the tool a order to request its name"""
        self.comManager.port = getChoice(self.pickportCb)
        try:
            status, toolId = self.comManager.perform('askId')
            self.getidLb.set_text(str(ord(toolId['ID'])))
            if status:
                self.showMsj('success')
            else:
                self.showMsj('failure')
        except:
            self.showMsj('failure')

    def showMsj(self, message):
        """shows a message in the status bar of the dialog"""
        if message == 'success':
            msj = 'operation performed successfully'
        elif message == 'failure':
            msj = 'operation failed, please try again'
        elif message == 'no ports':
            msj = 'not avilable ports founded, please connect one instrument'
        else:
            msj = message
        self.statusbar.set_text(msj)

###########################################################################
# 2. LOGGING MODE DIALOGS
###########################################################################


class SSDialog(object):
    """
         Clase  que  representa  la  ventana  de  dialogo  de  configuracion
         del  servicio  y  de  parametros de  las  herramientas  de  dichos
         servicios  ademas  configura  opciones  que  el  usuario  coloca
         para  el  registro  de  la  informacion  asi  como  la  visualizacion
    """
    builder_file = config.gui_file('SSDialog.glade')
    calibration_file = config.gui_file('Calibration.glade')
    tracks_file = config.gui_file('Tracks.glade')

    def __init__(self, options={}, *args, **kwargs):
        """
            Constructor de la clase SSDialog
            @param options Diccionario que contiene las opciones de
            configuracion del usuario, este lista es pasada desde la ventana
            principal (MainWindow)
        """
        #add debugging captabilities
        self.lg = jpt_logger(name='SSDialog', mode='quiet')
        self.log = self.lg.cursor
        #setup buffer for communication
        self.buffer = kwargs['external_buffer']
        self.log.debug('building buffer: %s', self.buffer)
        #load options
        self.options = options
        self.postOrders = args[0]
        self.ecpt = args[1]

        self.builder = gtk.Builder()
        self.builder.add_from_file(self.builder_file)
        self.calibration = gtk.Builder()
        self.calibration.add_from_file(self.calibration_file)
        self.tracks = gtk.Builder()
        self.tracks.add_from_file(self.tracks_file)

        self.dialog = self.builder.get_object('dialog')
        self.vp_displays = self.builder.get_object('vp_displays')
        self.vp_hw_setup = self.builder.get_object('vp_hw_setup')
        self.vp_cal_setup = self.builder.get_object('vp_cal_setup')
        self.box_tracks = self.builder.get_object('box_tracks')

        self.box_tool = self.builder.get_object('box_tool_pt')

        self.vp_displays.add(self.builder.get_object('box_pt_displays'))
        self.vp_hw_setup.add(self.builder.get_object('box_hw_setup_pt'))
        self.vp_cal_setup.add(self.builder.get_object('box_calibration_pt'))
        self.box_tracks.add(self.tracks.get_object('box_pt_tracks_d5'))

        self.pt_d5_track_conf(self.tracks.get_object('cb_pt_tracks_d5'))
        self.pt_d6_track_conf(self.tracks.get_object('cb_pt_tracks_d6'))

        self.pt_depth_cal(self.builder.get_object('cb_pt_cal_depth_2'))
        self.pt_gr_cal(self.builder.get_object('cb_pt_cal_gr_2'))
        self.pt_ccl_cal(self.builder.get_object('cb_pt_cal_ccl_2'))
        self.pt_res1s_cal(self.builder.get_object('cb_pt_cal_res1s_2'))
        self.pt_res2s_cal(self.builder.get_object('cb_pt_cal_res2s_2'))
        #### OJO AQUI
#        self.pt_res3s_cal(self.builder.get_object('cb_pt_cal_res2s_3'))
        #### CUIDADO
        self.pt_temp1_cal(self.builder.get_object('cb_pt_cal_temp1_2'))
        self.pt_temp2_cal(self.builder.get_object('cb_pt_cal_temp2_2'))
        self.pt_pres_cal(self.builder.get_object('cb_pt_cal_pres_2'))
        self.pt_ten_cal(self.builder.get_object('cb_pt_cal_ten_2'))
        self.pt_accx_cal(self.builder.get_object('cb_pt_cal_accx_2'))
        self.pt_accy_cal(self.builder.get_object('cb_pt_cal_accy_2'))
        self.pt_accz_cal(self.builder.get_object('cb_pt_cal_accz_2'))
        # -- CONFIGURATIONS OF TOOL OPTIONS --
        # get controls of Tool options
        self.opModeCb = self.builder.get_object('OpModeCb')
        self.numToolsCb = self.builder.get_object('NumToolsCb')
        self.id1Sb = self.builder.get_object('id1Sb')
        self.id2Sb = self.builder.get_object('id2Sb')
        self.id2Lbl = self.builder.get_object('id2Lbl')
        self.OpModeLs = self.builder.get_object('ls_OpMode')
        #initial configs for Tool options(1 Tool RollOver mode)
        self.opModeCb.set_active(0)
        self.numToolsCb.set_active(0)
        self.id2Sb.set_sensitive(False)
        self.id2Lbl.set_label('- -')
          #get reset configuration button
        self.resetBtn = self.builder.get_object('ResetBtn')
          #if there is a connection active, dont allow to use tool options
        try:
            if args[2]:
                m = [self.opModeCb, self.numToolsCb, self.id1Sb, self.id2Sb]
                for i in m:
                    i.set_sensitive(False)
        except:
            pass

        self.tool = Tool()
        self.box_tool.pack_start(self.tool, False, False)

        self.builder.connect_signals(self)
        self.tracks.connect_signals(self)

        if self.options:
            set_new_options(
                self.builder_file, self.builder,
                self.options['config_options'])
            set_new_options(
                self.calibration_file, self.calibration,
                self.options['calibration_options'])
            set_new_options(
                self.tracks_file, self.tracks,
                self.options['tracks_options'])

        self.dialog.show_all()
        self.dialog.run()

    def on_dialog_response(self, dialog, response_id):
        """@fn on_dialog_response
            Callback que se genera cuando la ventana de dialog se ha presionado
            el boton Setup o Cancel y se encarga de obtener las configuraciones
            realizadas por le usuario
            @param dialog Widget que recibe la senial
            @param response_id Retorna la respuesta de la ventana Dialog
        """
        #if a button different of Setup is clicked
        if response_id != gtk.RESPONSE_OK:
            self.dialog.destroy()
        #if Setup button is clicked
        else:
            #set the options from the interface
            self.options['config_options'] = prepare_options(
                self.builder_file,
                self.builder
                )
            self.options['calibration_options'] = prepare_options(
                self.calibration_file,
                self.calibration
                )
            self.options['tracks_options'] = prepare_options(
                self.tracks_file,
                self.tracks
                )
            self.buildECPTconfigs()
            self.set_prospection_info()
            #if options validation is ok, close the dialog
            if self.validate_fields():
                self.dialog.destroy()
                return
            else:
                self.dialog.show_all()
                self.dialog.run()

    def set_prospection_info(self):
        """retreive from gui the """
        #retreive field and well to global buffer
        entries = self.options['config_options']['entries']
        for i in ['well', 'field']:
            #if set field and well replace them, if not, let the ones that works
            if len(entries['ent_' + i]) > 0:
                self.buffer[i] = entries['ent_' + i]
        self.log.debug(
            'buffer content: %s',
            self.buffer
            )

    def validate_fields(self):
        """@fn validate_fields
            Funcion para validar los datos introducidos por el usuario y
            corrobara que todo este correctamente configurado
            en cuanto a rangos de span y otros.
        """
        control = 'spinbuttons'
        configSet = 'config_options'
        sensorTags = ['ccl', 'gr', 'ten', 'res', 'pres', 'temp', 'acc']
        pt_tool = self.options[configSet][control]['sb_pt_rd_tool']
        pt_sensors = [self.options[configSet][control]['sb_pt_rd_' + s]
                      for s in sensorTags]
        if any(pt_tool < sensor for sensor in pt_sensors):
            msg = 'Relative depth misconfigured: Parameter Tool has to be longer'
            self.show_error(msg)
            return False

        def tracks_misconfigured(span1, span2):
            if span1 >= span2:
                msg = 'Tracks misconfigured: span 2 should be greater than span 1'
                self.show_error(msg)
                return False
            return True
        dic = self.options['tracks_options'][control]
        trackModes = ['d5', 'd6']
        sensorLabels = [
            'gr', 'avggr', 'ccl', 'temp1', 'temp2', 'pres', 'res3',
            'res1', 'res2', 'accx', 'accy', 'accz', 'ten'
            ]
        for mode in trackModes:
            for sensor in sensorLabels:
                if mode == 'd5' and sensor == 'avggr':
                    pass
                elif not tracks_misconfigured(
                    dic['sb_pt_' + mode + '_' + sensor + '_span1'],
                    dic['sb_pt_' + mode + '_' + sensor + '_span2']):
                    return False
        return True

    def pt_d5_track_conf(self, widget):
        """@fn pt_d5_track_conf
            Callback que configura los opciones de Tracks Options cuando en
            Display Options se elige el RadioButton de 5 Tracks en el
            servicio de Parameter Tool
            (SSDialog.glade->box_pt_displays->rad_pt_5_tracks)
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        dic = {
            0: self.tracks.get_object('frm_pt_d5_track1'),
            1: self.tracks.get_object('frm_pt_d5_track2'),
            2: self.tracks.get_object('frm_pt_d5_track3'),
            3: self.tracks.get_object('frm_pt_d5_track4'),
            4: self.tracks.get_object('frm_pt_d5_track5')
            }
        replace_widget(self.dialog, box, dic[widget.get_active()],
                       False, False, 2)

    def pt_d6_track_conf(self, widget):
        """@fn pt_d6_track_conf
            Callback que configura los opciones de Tracks Options cuando en
            Display Options se elige el RadioButton de 6 Tracks en el
            servicio de Parameter Tool
            (SSDialog.glade->box_pt_displays->rad_pt_6_tracks)
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        dic = {
            0: self.tracks.get_object('frm_pt_d6_track1'),
            1: self.tracks.get_object('frm_pt_d6_track2'),
            2: self.tracks.get_object('frm_pt_d6_track3'),
            3: self.tracks.get_object('frm_pt_d6_track4'),
            4: self.tracks.get_object('frm_pt_d6_track5')}
        replace_widget(self.dialog, box, dic[widget.get_active()],
                       False, False, 2)

    def pt_depth_cal(self, widget):
        """@fn pt_depth_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Depth cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_depth_2) Cambia, y
            configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_depth_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_gr_cal(self, widget):
        """@fn pt_gr_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Gamma Ray cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_gr_2) Cambia, y
            configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_gr_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_ccl_cal(self, widget):
        """@fn pt_gr_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de CCL cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_ccl_2) Cambia, y
            configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_ccl_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_res1s_cal(self, widget):
        """@fn pt_res1s_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Resistivity 1 State cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_res1s_2) Cambia,
            y configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_res1s_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_res2s_cal(self, widget):
        """@fn pt_res2s_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Resistivity 2 State cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_res2s_2) Cambia,
            y configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_res2s_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_temp1_cal(self, widget):
        """@fn pt_temp1_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Temperature 1 cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_temp1_2) Cambia,
            y configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_temp1_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_temp2_cal(self, widget):
        """@fn pt_temp2_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Temperature 2 cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_temp2_2) Cambia,
            y configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_temp2_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_pres_cal(self, widget):
        """@fn pt_pres_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Pressure cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_pres_2) Cambia,
            y configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_pres_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_ten_cal(self, widget):
        """@fn pt_ten_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Tension cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_ten_2) Cambia,
            y configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_ten_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_accx_cal(self, widget):
        """@fn pt_accx_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Acc X cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_accx_2) Cambia,
            y configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_accx_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_accy_cal(self, widget):
        """@fn pt_accy_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Acc Y cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_accy_2) Cambia,
            y configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_accy_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_accz_cal(self, widget):
        """@fn pt_accz_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Acc Z cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_accz_2) Cambia,
            y configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_accz_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def on_rad_pt_5_tracks(self, rad):
        """@fn on_rad_pt_5_tracks
            Callback del radio button que esta en Display Options de 5 Tracks,
            esto configura la caja de tracks (box_tracks),
            con el modo de 5 tracks cargando desde tracks(Tracks.glade) la caja
            de 5 tracks (box_pt_tracks_d5) y tambien
            deshabilita wl widget de Gamma Ray AVG(box_pt_avggr_slot) que esta
            en la seccion de Calibartion Setup
            servicio de Parameter Tool
            @param rad De quien recibe la senial
        """
        if rad.get_active():
            replace_widget(
                self.dialog, self.box_tracks,
                self.tracks.get_object('box_pt_tracks_d5'))
            replace_widget(
                self.dialog,
                self.builder.get_object('box_pt_avggr_slot'),
                None)

    def on_rad_pt_6_tracks(self, rad):
        """@fn on_rad_pt_6_tracks
            Callback del radio button que esta en Display Options de 6 Tracks,
            esto configura la caja de tracks (box_tracks),
            con el modo de 6 tracks cargando desde tracks(Tracks.glade) la caja
            de 5 tracks (box_pt_tracks_d6) y tambien
            habilita wl widget de Gamma Ray AVG(box_pt_avggr_slot) que esta en
            la seccion de Calibartion Setup servicio de Parameter Tool
            @param rad De quien recibe la senial
        """
        if rad.get_active():
            replace_widget(
                self.dialog, self.box_tracks,
                self.tracks.get_object('box_pt_tracks_d6')
                )
            replace_widget(
                self.dialog,
                self.builder.get_object('box_pt_avggr_slot'),
                self.builder.get_object('box_pt_avggr')
                )

    def on_rad_pt_service(self, rad):
        """@fn on_rad_pt_service
            Callback del radio button para elegir el servicio de Parameteer
            Tool Service, este callback permite actualizar el dialog para que
            carge las configuraciones basicas del servicio de Parameter Tool,
            en caso de que anteriormente haya estado en el servicio de loging
            @param rad De quien recibe la senal
        """
        if rad.get_active():
            replace_widget(
                self.dialog, self.vp_displays,
                self.builder.get_object('box_pt_displays')
                )
            replace_widget(
                self.dialog, self.vp_hw_setup,
                self.builder.get_object('box_hw_setup_pt')
                )
            self.box_tool = self.builder.get_object('box_tool_pt')
            self.tool = Tool()
            replace_widget(self.dialog, self.box_tool, self.tool, False, False)
            replace_widget(
                self.dialog, self.vp_cal_setup,
                self.builder.get_object('box_calibration_pt')
                )
            self.on_rad_pt_5_tracks(self.builder.get_object('rad_pt_5_tracks'))
            self.on_rad_pt_6_tracks(self.builder.get_object('rad_pt_6_tracks'))

    def on_btn_set_rel_depth_pt_clicked(self, button):
        """@fn on_btn_set_rel_depth_pt_clicked
            Callback del boton que esta en Hardware Setup "Preview" este boton
            lo que hace es actualizar el grafico de la herramienta
            que esta en self.box_tool y que contiene un objeto de la clase Tool
            que es el encargado de graficar la herramienta
            este callback aplica cuando esta en servicio de Parameter Tool,
            para configurar la profundidad relativa de los sensores
            @param button De quien recibe la senal
        """
        tool = self.builder.get_object('sb_pt_rd_tool').get_value()
        ccl = self.builder.get_object('sb_pt_rd_ccl').get_value()
        gr = self.builder.get_object('sb_pt_rd_gr').get_value()
        res = self.builder.get_object('sb_pt_rd_res').get_value()
        pres = self.builder.get_object('sb_pt_rd_pres').get_value()
        temp = self.builder.get_object('sb_pt_rd_temp').get_value()
        acc = self.builder.get_object('sb_pt_rd_acc').get_value()
        ten = self.builder.get_object('sb_pt_rd_ten').get_value()
        devices = {
            'tool': tool,
            'ccl': {'value': ccl, 'color': (1, 1, 0)},
            'gr': {'value': gr, 'color': (0, 0, 1)},
            'res': {'value': res, 'color': (1, 1, 1)},
            'pres': {'value': pres, 'color': (0, 1, 0)},
            'temp': {'value': temp, 'color': (1, 0, 0)},
            'acc': {'value': acc, 'color': (1, 0.5, 0)},
            'ten': {'value': ten, 'color': (1, 0.5, 0.5)}
            }
        self.tool.set_devices(devices)

    def on_btn_test_davidconn_clicked(self, button):
        """@fn on_btn_test_davidconn_clicked
            Callback Para realizar testeo de la conexion David y verificar que
            la coneccion con el puerto serial se ha realizado
            de forma correcta
            @param button De quien recibe la senal
        """
        s_connection = DavidConnection()
        p = self.builder.get_object('sb_lg_serial_port').get_value()
        if s_connection.connect(port=int(p - 1)):
            self.show_info("Connection Successful")
        else:
            self.show_error('Could not connect to serial port')

    def show_info(self, message):
        """@fn show_info
            Callback mostrar algun mensaje en un ventana de dialogo
            @param message Mensaje que va ser mostrado en la ventana de dialogo
            (STRING)
        """
        dialog = gtk.MessageDialog(
            None,
            gtk.DIALOG_MODAL |
            gtk.DIALOG_DESTROY_WITH_PARENT,
            gtk.MESSAGE_INFO,
            gtk.BUTTONS_OK
            )

        dialog.set_title(gettext('Information'))
        dialog.set_markup(message)

        dialog.get_position()
        dialog.set_deletable(False)

        response = dialog.run()
        dialog.destroy()
        return response

    def show_error(self, message):
        """@fn show_info
            Callback mostrar algun mensaje en un ventana de dialogo de tipo
            error
            @param message Mensaje que va ser mostrado en la ventana de dialogo
            (STRING)
        """
        dialog = gtk.MessageDialog(
            None,
            gtk.DIALOG_MODAL |
            gtk.DIALOG_DESTROY_WITH_PARENT,
            gtk.MESSAGE_ERROR,
            gtk.BUTTONS_OK
            )

        dialog.set_title(gettext('Error'))
        dialog.set_markup(message)

        dialog.get_position()
        dialog.set_deletable(False)

        response = dialog.run()

        dialog.destroy()
        return response

    def reset_config(self, button):
        """
        resetea la configuracion a la configuracion por defecto cuando se hace
        click en el boton Reset de la parte inferior de la ventana de dialogo
        """
        self.postOrders['reload'] = True

    def numToolsChanged(self, numcombobox):
        """
        si el numero de herramientas es 1 entonces deshabilita la casilla para
        introducir el id de la herramienta 2, si el numero de herramientas es
        2 muestra ambas casillas de seleccion de id de herramientas habilitadas
        y elimina el modo roll Over
        """
        #get how many tools are selected
        numTools = getChoice(numcombobox)
        if numTools == '1':
            #deactivate second id input
            self.id2Sb.set_sensitive(False)
            self.id2Lbl.set_label('- -')
            #restore roll over mode in combobox
            self.opModeCb.insert_text(0, 'Roll Over')
            self.opModeCb.set_active(0)
        elif numTools == '2':
            #activate second id input
            self.id2Sb.set_sensitive(True)
            self.id2Lbl.set_text('FL ID')
            #remove roll over mode from combobox
            self.opModeCb.remove_text(0)
            self.opModeCb.set_active(0)
            self.checkIDvalue(self.id1Sb)

    def buildECPTconfigs(self):
        """
        construye las configuraciones necesarias para el servicio ECPT y las
        guarda en el vector especificado para ello
        """
        #clean previous tool names
        self.ecpt['name'] = []
        #get how many tools will be handled
        numTools = getChoice(self.numToolsCb)
        #asign tool names to ecpt configs
        self.ecpt['name'].append(self.id1Sb.get_value_as_int())
        if numTools == '2':
            self.ecpt['name'].append(self.id2Sb.get_value_as_int())
        #asign session mode to ecpt configs
        self.ecpt['mode'] = getChoice(self.opModeCb)

    def checkIDvalue(self, spinbutton):
        """
        asegura que los valores de id de las herramientas sean siempre
        diferentes
        """
        #get number of tools
        numTools = getChoice(self.numToolsCb)
        #check numbers only if there are two tools
        if numTools == '2':
            id1 = self.id1Sb.get_value_as_int()
            id2 = self.id2Sb.get_value_as_int()
            #if the values of id of booth tools are the same, change one
            if id1 == id2:
                spinbutton.set_value(id1 + 1)


class SaveSSDialog(object):
    """
         Clase que representa la ventana de dialogo de guarda la
         configuracion de loging que el usuario haya configurado
    """
    builder_file = config.gui_file('SaveSSDialog.glade')

    def __init__(self, options):
        """
            Constructor de la clase SaveSSDialog
            @param options Dicionario con las opciones que el usuario configuro
            en el la ventana de dailogo de SETUP
        """
        self.builder = gtk.Builder()
        self.builder.add_from_file(self.builder_file)
        self.dialog = self.builder.get_object('dialog')

        self.options = options

        ft = gtk.FileFilter()
        ft.add_pattern("*.jss")
        self.dialog.set_filter(ft)

        self.builder.connect_signals(self)
        self.dialog.show_all()
        self.dialog.run()

    def on_dialog_response(self, dialog, response_id):
        """@fn on_dialog_response
            Callback que se ejecuta cuando el usuario presiona el boton Cancel
            o Save de la ventana de dialogo
            y dependiendo de ello se guarda o no un archivo .jss (JSON)
            @param dialog De quien recibe la senal
            @param response_id Respuesta del boton presionado
        """
        if response_id != 5:
            self.dialog.destroy()
            return
        else:
            filename = self.dialog.get_filename()
            if not filename:
                self.show_info('Please select a folder and filename')
                self.dialog.show_all()
                self.dialog.run()
            if not filename.endswith('.jss'):
                filename += '.jss'
            fl = open(filename, "w")
            json.dump(self.options, fl)
            fl.close()
            self.dialog.destroy()
            return

    def show_info(self, message):
        """@fn show_info
            Callback mostrar algun mensaje en un ventana de dialogo
            @param message Mensaje que va ser mostrado en la ventana de dialogo
            (STRING)
        """
        dialog = gtk.MessageDialog(
            None,
            gtk.DIALOG_MODAL |
            gtk.DIALOG_DESTROY_WITH_PARENT,
            gtk.MESSAGE_INFO,
            gtk.BUTTONS_OK)

        dialog.set_title(gettext('Information'))
        dialog.set_markup(message)

        dialog.get_position()
        dialog.set_deletable(False)

        response = dialog.run()
        dialog.destroy()
        return response


class OpenSSDialog(object):
    """
         Clase  que  representa  la  ventana  de  dialogo  para  cargar  la
         configuracion  de  loging  que  el usuario  haya  configurado  y
         previamante  habia  guardado  en  un  archivo  de  extension
         .jss (JSON)
    """
    builder_file = config.gui_file('OpenSSDialog.glade')

    def __init__(self, father):
        """
            Constructor de la clase OpenSSDialog
            @param father El widget padre de donde viene
        """
        self.builder = gtk.Builder()
        self.builder.add_from_file(self.builder_file)
        self.dialog = self.builder.get_object('dialog')
        self.father = father

        filter = gtk.FileFilter()
        filter.add_pattern("*.jss")
        self.dialog.set_filter(filter)

        self.builder.connect_signals(self)
        self.dialog.show_all()
        self.dialog.run()

    def on_dialog_response(self, dialog, response_id):
        """@fn on_dialog_response
            Callback que se ejecuta cuando el usuario presiona el boton Cancel
            o Load de la ventana de dialogo
            y dependiendo de ello se cargar o no un archivo .jss (JSON)
            @param dialog De quien recibe la senal
            @param response_id Respuesta del boton presionado
        """
        if response_id != 5:
            self.dialog.destroy()
            return
        else:
            filename = self.dialog.get_filename()
            if not filename:
                self.show_info('Please select a file')
                self.dialog.show_all()
                self.dialog.run()
            else:
                fl = open(filename, "r")
                try:
                    self.father.options = json.load(fl)
                    fl.close()
                    self.dialog.destroy()
                    return
                except ValueError:
                    self.show_info('Not a valid file')
                    self.dialog.show_all()
                    self.dialog.run()

    def show_info(self, message):
        """@fn show_info
            Callback mostrar algun mensaje en un ventana de dialogo
            @param message Mensaje que va ser mostrado en la ventana de dialogo
            (STRING)
        """
        dialog = gtk.MessageDialog(
            None,
            gtk.DIALOG_MODAL |
            gtk.DIALOG_DESTROY_WITH_PARENT,
            gtk.MESSAGE_INFO,
            gtk.BUTTONS_OK)

        dialog.set_title(gettext('Information'))
        dialog.set_markup(message)

        dialog.get_position()
        dialog.set_deletable(False)

        response = dialog.run()
        dialog.destroy()
        return response


class ConfigTracksDialog(object):
    """
         Clase  que  representa  la  ventana  de  dialogo  para  modificar
         durante  el  registro  de  la informacion  las  configuraciones  de
         graficado  de  las  curvas.
    """
    tracks_file = config.gui_file('TracksConfigDialog.glade')

    def __init__(self, options):
        """
            Constructor de la clase ConfigTracksDialog
            @param options Las opciones que actualmente el usuario haya
            configurado en la ventana de SETUP.
        """
        self.options = options
        self.tracks = gtk.Builder()
        self.tracks.add_from_file(self.tracks_file)

        self.dialog = self.tracks.get_object('dialog')
        self.box_config = self.tracks.get_object('box_config')

        self.tracks.connect_signals(self)

        self.get_current_options(
            self.tracks_file, self.tracks,
            self.options['tracks_options'])

        if self.options['config_options']['radiobuttons']['rad_pt_service']:
            if self.options['config_options']['radiobuttons']['rad_pt_5_tracks']:
                self.box_config.add(
                    self.tracks.get_object('box_pt_d5_tracks_config')
                    )
            if self.options['config_options']['radiobuttons']['rad_pt_6_tracks']:
                self.box_config.add(
                    self.tracks.get_object('box_pt_d6_tracks_config')
                    )

        self.dialog.show_all()
        self.dialog.run()

    def validate_fields(self):
        """@fn validate_fields
           Funcion para validar los datos introducidos por el usuario y
           corrobara que todo este correctamente configurado
           en cuanto a rangos de span y otros.
        """
        def tracks_misconfigured(span1, span2):
            if span1 >= span2:
                msg = 'Tracks misconfigured: span 2 should be greater than span 1'
                self.show_error(msg)
                return False
            return True

        dic = self.options['tracks_options']['spinbuttons']
        trackModes = ['d5', 'd6']
        sensorLabels = [
            'gr', 'avggr', 'ccl', 'temp1', 'temp2', 'pres', 'res3',
            'res1', 'res2', 'accx', 'accy', 'accz', 'ten', 'depth']
        for mode in trackModes:
            for sensor in sensorLabels:
                if mode == 'd5' and sensor == 'avggr':
                    pass
                elif not tracks_misconfigured(
                    dic['sb_pt_' + mode + '_' + sensor + '_span1'],
                    dic['sb_pt_' + mode + '_' + sensor + '_span2']):
                    return False
        return True

    def on_dialog_response(self, dialog, response_id):
        """@fn on_dialog_response
            Callback que se ejecuta cuando el usuario presiona el boton Cancel
            o Set de la ventana de dialogo y dependiendo de ello se configurar
            las nuevas opciones que van a configurar el modo de graficado de
            las curvas
            @param dialog De quien recibe la senal
            @param response_id Respuesta del boton presionado
        """
        if response_id != gtk.RESPONSE_OK:
            self.dialog.destroy()
        else:
            self.set_new_options(self.tracks_file, self.tracks)
            if self.validate_fields():
                self.dialog.destroy()
            else:
                self.dialog.show_all()
                self.dialog.run()

    def get_current_options(self, filename, builder, builder_widgets):
        """@fn get_current_options
            Funcion que va retornar las opciones actuales de garficacion de
            datos que el usuario ha configurado para el registro
        """
        for c, v in builder_widgets['spinbuttons'].items():
            sb = builder.get_object(c)
            if sb:
                sb.set_value(v)

    def set_new_options(self, filename, builder):
        """@fn set_new_options
            Funcion para obtener de los valores de cada spinbutton que hay en
            la ventana de dialogo y se almacena en el diccionario
            que contiene la configuracion del usuario, esto hace una
            actualizacion de dicho diccionario
            @param filename Nombre del fichero  TracksConfigDialog.glade
            @param builder Objeto que referencia a constructor del dialogo
        """
        builder_widgets = get_important_widgets(filename)
        for c in builder_widgets['spinbuttons']:
            self.options['tracks_options']['spinbuttons'][c] = builder.get_object(c).get_value()

    def show_error(self, message):
        """@fn show_error
            Callback mostrar algun mensaje en un ventana de dialogo de
            tipo error
            @param message Mensaje que va ser mostrado en la ventana de dialogo
            (STRING)
        """
        dialog = gtk.MessageDialog(
            None,
            gtk.DIALOG_MODAL |
            gtk.DIALOG_DESTROY_WITH_PARENT,
            gtk.MESSAGE_ERROR,
            gtk.BUTTONS_OK)

        dialog.set_title(gettext('Error'))
        dialog.set_markup(message)

        dialog.get_position()
        dialog.set_deletable(False)

        response = dialog.run()

        dialog.destroy()
        return response


class MonitorModeLoggingDialog(object):
    """
         Clase  que  representa  la  ventana  de  dialogo  para  el  Monitor
         Logging  MonitorModeLoggingDialog
    """
    try:
        builder_file = config.gui_file('LoggingModeDialog.glade')
        print 'built ok'
    except Exception as e:
        print e

    def __init__(self, parent, external_buffer=None):
        """
            Constructor de la clase MonitorModeLoggingDialog
            @param parent Recibe un widget padre en este caso del principal
        """
        #logging things
        self.lg = jpt_logger(name='monitor_dialog', mode='quiet')
        self.log = self.lg.cursor
        #build gui
        self.builder = gtk.Builder()
        self.parent = parent
        self.builder.add_from_file(self.builder_file)
        #get widgets
        self.dialog = self.builder.get_object('dialoglogging')
        self.ls_sample_rate = self.builder.get_object('ls_sampling_rate')
        self.label_units = self.builder.get_object('lbl_sampling_rateunits')
        self.combosamplerate = self.builder.get_object('cb_sampling_rate')
        self.labelDepthValue = self.builder.get_object('lbl_depth_actual_value')
        self.labelTensionValue = self.builder.get_object('lbl_tension_actual_value')
        self.valuadj = self.builder.get_object('adj_depth_reference')
        self.radup = self.builder.get_object('rad_up_mode')
        self.raddown = self.builder.get_object('rad_down_mode')
        self.namerecordtext = self.builder.get_object('ent_name_record')
        self.radtime = self.builder.get_object('rad_real_time')
        self.raddepth = self.builder.get_object('rad_real_depth')
        self.buttonset = self.builder.get_object('btn_set')
        #get depth widgets
        self.btn_toogle_dir = self.builder.get_object('btn_toogle_dir')
        self.sb_tension_factor = self.builder.get_object('sb_tension_factor')
        self.sb_depth_factor = self.builder.get_object('sb_depth_factor')
        self.sb_depth_offset = self.builder.get_object('sb_depth_offset')
        self.sb_perimeter = self.builder.get_object('sb_perimeter')
        self.sb_counts_per_revolution = self.builder.get_object(
            'sb_counts_per_revolution')
        self.adj_tension_factor = self.builder.get_object('adj_tension_factor')
        self.adj_depth_factor = self.builder.get_object('adj_depth_factor')
        self.adj_depth_offset = self.builder.get_object('adj_depth_offset')
        self.adj_perimeter = self.builder.get_object('adj_perimeter')
        self.adj_counts_per_rev = self.builder.get_object('adj_counts_per_rev')
        #telemetry indicator
        self.lbl_telemetry = self.builder.get_object('lbl_telemetry')
        #speed indicator
        self.labelSpeedValue = self.builder.get_object('lbl_speed_actual_value')
        #default configs
        self.raddepth.set_active(False)
        self.radtime.set_active(True)
        self.radup.set_active(False)
        self.radup.set_sensitive(False)
        self.raddown.set_sensitive(False)
        self.raddown.set_active(False)
        #connect signals
        self.builder.connect_signals(self)
        self.lastDepth = 0.00
        self.down = True
        #select time drive mode
        self.time_drive = True
        #put external buffer
        self.buffer = external_buffer
        #set initial name
        self.change_file_name()

        self.buttonset.set_sensitive(False)

    def change_file_name(self):
        """change name of filename and filename indicator based on current
        time and current field-well info"""
        tmpl = '{f}_{w}_{m}_{t.tm_year}-{t.tm_mon}-{t.tm_mday}'
        self.localtime = localtime(time())
        self.textInit = tmpl.format(
            t=self.localtime,
            f=self.buffer['field'],
            w=self.buffer['well'],
            m=self.buffer['test_mode']
            )
        self.namerecordtext.set_text(self.textInit)
        self.parent.record_filename = self.textInit
        self.log.debug(
            'file_name: %s',
            self.textInit
            )

    def ShowMonitorDialog(self):
        """@fn ShowMonitorDialog
            Permite visualizacion de la ventana de dialogo
        """
        self.dialog.show_all()

    def SetNewValues(self, Depth, Tension, speed):
        """@fn SetNewValues
            Callback que se ejecuta para actualizar las etiquetas de Tension y
            Profundidad en la ventana de dialogo
            @param Depth Valor de profundidad
            @param Tension Valor de tension
        """
        #print "new values"
        self.lastDepth = copy(Depth)
        #set new depth
        gobject.idle_add(
            self.labelDepthValue.set_text,
            '{:.2f}'.format(Depth)
            )
        #set new tension
        gobject.idle_add(
            self.labelTensionValue.set_text,
            '{:.2f}'.format(Tension)
            )
        #set new speed
        gobject.idle_add(
            self.labelSpeedValue.set_text,
            '{:.3f}'.format(abs(speed))
            )
        #show telemetry quality
        self.lbl_telemetry.set_text(
            '{d[received]} of {d[sent]}'.format(
                d=self.buffer['telemetry']
                )
        )
        #gobject.idle_add(self.valuadj.set_value,Depth)

    def depth_reference_changed(self, rad):
        """updates value of depth reference in buffer to expose the data to
        the entire software"""
        self.buffer['depth_reference'] = rad.get_value()

    #BUTTON HANDLERS

    def on_dialoglogging_destroy(self, event):
        """@fn on_dialoglogging_destroy
            Callback cuando se cierra la ventana de dialogo y aviza al widget
            padre que se cerro
        """
        self.parent.EstateDialogMonitor = False

    def on_btn_set_clicked(self, button):
        """@fn on_btn_set_clicked
            Callback que se ejecuta cuando se presiona el boton Set
            @param button Objeto de quien viene el evento de click
        """
        #down or up mode goes to self.Down variable
        self.parent.Down = copy(self.down)
        #initial depth goes to self.DepthReference
        self.parent.DeptReference = self.valuadj.get_value()
        self.labelDepthValue.set_text(str(self.valuadj.get_value()))
        textactual = self.namerecordtext.get_text()
        if textactual != self.parent.record_filename and self.parent.state_record is True:
            msg = 'The name of the database change, you must first stop the current recording.\n'
            msg = msg + 'Want to stop the current recording?'
            if self.parent.show_question(msg):
                self.parent.btn_record.set_active(False)
                self.parent.record_filename = self.namerecordtext.get_text()
        else:
            self.parent.record_filename = self.namerecordtext.get_text()
        self.parent.new_config_from_monitor()

    def rad_real_depth_clicked_cb(self, rad):
        """@fn rad_real_depth_clicked_cb
            Callback que se ejecuta cuando se cambia a modo de profundidad
            VS Datos
            @param rad Objeto de quien viene el evento de click
        """
        if rad.get_active():
            self.parent.k = config.TRACKING_BY_DEPTH
            self.ls_sample_rate.clear()
            self.ls_sample_rate.append(['10'])
            self.ls_sample_rate.append(['4'])
            self.ls_sample_rate.append(['2'])
            self.label_units.set_text('  Samples/Ft')
            if self.parent.options:
                if 'logingmodedialog_options' in self.parent.options:
                    if self.parent.options['logingmodedialog_options']['comboboxes']['cb_sampling_rate'][1] <= 2:
                        self.combosamplerate.set_active(
                            self.parent.options['logingmodedialog_options']['comboboxes']['cb_sampling_rate'][1]
                            )
                    else:
                        self.combosamplerate.set_active(0)
                else:
                    self.combosamplerate.set_active(0)
            else:
                self.combosamplerate.set_active(0)
            self.radup.set_sensitive(True)
            self.radup.set_active(True)
            self.raddown.set_sensitive(True)
            self.raddown.set_active(True)
            #select time depth mode
            self.time_drive = False
            #update configurations
            self.update_configs()

    def update_configs(self, event=''):
        """update various configurations everytime it is needed"""
        #build time_mode for buffer
        if self.time_drive:
            test_mode = 'time_drive'
        else:
            #build depth_mode for buffer
            if self.down:
                test_mode = 'depth_drive_down'
            else:
                test_mode = 'depth_drive_up'

        self.buffer['test_mode'] = test_mode
        self.log.debug('mode changed to %s', test_mode)

    def rad_real_time_clicked_cb(self, rad):
        """@fn rad_real_time_clicked_cb
            Callback que se ejecuta cuando se cambia a modo de tiempo VS Datos
            @param rad Objeto de quien viene el evento de click
        """
        if rad.get_active():
            self.parent.k = config.TRACKING_BY_TIME
            self.ls_sample_rate.clear()
            self.ls_sample_rate.append(['100'])
            self.ls_sample_rate.append(['200'])
            self.ls_sample_rate.append(['300'])
            self.ls_sample_rate.append(['400'])
            self.ls_sample_rate.append(['500'])
            self.ls_sample_rate.append(['1000'])
            self.label_units.set_text('  ms')
            if self.parent.options:
                if 'logingmodedialog_options' in self.parent.options:
                    self.combosamplerate.set_active(
                        self.parent.options['logingmodedialog_options']['comboboxes']['cb_sampling_rate'][1]
                        )
                else:
                    self.combosamplerate.set_active(0)
            else:
                self.combosamplerate.set_active(0)
            self.radup.set_active(False)
            self.radup.set_sensitive(False)
            self.raddown.set_active(True)
            #select time drive mode
            self.time_drive = True
            #update configurations
            self.update_configs()

    def on_rad_up_mode_clicked(self, rad):
        """@fn on_rad_up_mode_clicked
            Callback que se ejecuta cuando se cambia a modo UP de datos
            @param rad Objeto de quien viene el evento de click
        """
        if rad.get_active():
            self.down = False
            gobject.idle_add(self.valuadj.set_value, self.lastDepth)
            #update configurations
            self.update_configs()

    def on_rad_down_mode_clicked(self, rad):
        """@fn on_rad_down_mode_clicked
            Callback que se ejecuta cuando se cambia a modo Down de datos
            @param rad Objeto de quien viene el evento de click
        """
        if rad.get_active():
            self.down = True
            gobject.idle_add(self.valuadj.set_value, self.lastDepth)
            #update configurations
            self.update_configs()

    def update_encoder_configs(self, rad):
        """update configurations on buffer"""
        if 'encoder' not in self.buffer:
            self.buffer['encoder'] = {}
        encoder_settings = self.buffer['encoder']
        encoder_settings['toogle'] = self.btn_toogle_dir.get_active()
        encoder_settings['tension_factor'] = float(
            self.adj_tension_factor.get_value()
            )
        encoder_settings['depth_factor'] = float(
            self.adj_depth_factor.get_value()
            )
        encoder_settings['perimeter'] = float(
            self.adj_perimeter.get_value()
            )
        encoder_settings['counts_per_rev'] = self.adj_counts_per_rev.get_value()
        encoder_settings['depth_offset'] = self.adj_depth_offset.get_value()
        #print encoder_settings

###########################################################################
# 3. PLAYBACK DIALOGS
###########################################################################


class ImportDBDialog(object):
    """
         Clase  que  representa  la  ventana  de  dialogo  para  importar  un
         registro  anteriormente
         guardado  en  un  archivo  de  extension  .jdb
    """
    builder_file = config.gui_file('ImportDBDialog.glade')

    def __init__(self, father):
        """
            Constructor de la clase ImportDBDialog
            @param father Widget padre de quien recibe
        """
        self.builder = gtk.Builder()
        self.builder.add_from_file(self.builder_file)
        self.dialog = self.builder.get_object('dialog')

        self.father = father

        ft = gtk.FileFilter()
        ft.add_pattern("*.jdb")
        self.dialog.set_filter(ft)

        self.builder.connect_signals(self)
        self.dialog.show_all()
        self.dialog.run()

    def on_dialog_response(self, dialog, response_id):
        """@fn on_dialog_response
            Callback que se ejecuta cuando el usuario presiona el boton Cancel
            o Import Database de la ventana de dialogo y dependiendo de ello va
            cargar o no la base de datos .jdb para su posterior PlayBack
            @param dialog De quien recibe la senal
            @param response_id Respuesta del boton presionado
        """
        if response_id != 5:
            self.dialog.destroy()
        else:
            filename = self.dialog.get_filename()
            if not filename:
                self.show_info('Please select a file')
                self.dialog.show_all()
                self.dialog.run()
            else:
                try:
                    options, data, raw_data = load_db(filename)
                    self.father.db = {
                        'options': options,
                        'data': data,
                        'filename': filename,
                        'raw_data': raw_data
                        }
                    self.dialog.destroy()
                    return
                except ValueError:
                    self.show_info('Not a valid file')
                    self.dialog.show_all()
                    self.dialog.run()

    def show_info(self, message):
        """@fn show_info
            Callback mostrar algun mensaje en un ventana de dialogo
            @param message Mensaje que va ser mostrado en la ventana de dialogo
            (STRING)
        """
        dialog = gtk.MessageDialog(
            None,
            gtk.DIALOG_MODAL |
            gtk.DIALOG_DESTROY_WITH_PARENT,
            gtk.MESSAGE_INFO,
            gtk.BUTTONS_OK)

        dialog.set_title(gettext('Information'))
        dialog.set_markup(message)

        dialog.get_position()
        dialog.set_deletable(False)

        response = dialog.run()
        dialog.destroy()
        return response


############################################################################
# 4. PROTOCOL CHECKER DIALOGS
############################################################################
class ConnectionCheckerDialog(object):
    """
       Clase que representa la ventana de dialogo para el Protocol Checker
    """
    builder_file = config.gui_file('ConnectionCheckerDialog.glade')

    def __init__(self, closed_callback):
        """
            Constructor de la clase ConnectionCheckerDialog
            @param closed_callback Callback para cerrar la ventana de
            connection Checker
        """
        self.builder = gtk.Builder()
        self.builder.add_from_file(self.builder_file)
        self.dialog = self.builder.get_object('dialog')
        self.closed_callback = closed_callback

        self.ls_messages = self.builder.get_object('ls_messages')
        self.sw_messages = self.builder.get_object('sw_messages')
        self.ls_trames = self.builder.get_object('ls_trames')
        self.sw_trames = self.builder.get_object('sw_trames')

        self.builder.connect_signals(self)
        self.dialog.show_all()
        self.dialog.run()

    def on_tw_messages_size_allocate(self, widget, event, data=None):
        """@fn on_tw_messages_size_allocate
            Callback para realizar un scrolleo de la ventana de mensages
            @param widget de quien recibe la sena;
            @param event evento que esta ocurriedno
        """
        adj = self.sw_messages.get_vadjustment()
        adj.set_value(adj.upper - adj.page_size)

    def on_tw_trames_size_allocate(self, widget, event, data=None):
        """@fn on_tw_trames_size_allocate
            Callback para realizar un scrolleo de la ventana de mensages
            @param widget de quien recibe la sena;
            @param event evento que esta ocurriedno
        """
        adj = self.sw_trames.get_vadjustment()
        adj.set_value(adj.upper - adj.page_size)

    def messages_add(self, data):
        """@fn messages_add
            Anade un nuevo mensage en la ventana de visualizacion
        """
        self.ls_messages.append(data)

    def trames_add(self, data):
        """@fn trames_add
            Anade una nueva trama en la ventana de visualizacion
        """
        self.ls_trames.append(data)

    def on_btn_close_clicked(self, *args):
        """@fn on_btn_close_clicked
            Cierra ventana de dialogo
        """
        #self.dialog.destroy()
        self.closed_callback()

###############################################################################
# FUNCIONES AUXILIARES
###############################################################################


def getChoice(combobox):
    """get the selected choice of a combobox"""
    model = combobox.get_model()
    index = combobox.get_active()
    choice = model[index][0]
    return choice

#TESTEO
#if __name__ == '__main__':
#
#    dic = {}
#    window = toolNameDialog()
#    print dic
#    gtk.main()
