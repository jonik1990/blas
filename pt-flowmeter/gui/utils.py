# -*- encoding: utf-8 -*-
'''
Emdyp.me
Revisado
Script: Tipo B
'''
import gtk


def get_important_widgets(filename):
    """
        Funcion que lee un .glade un extrae comboboxes, spinbuttons, radiobuttons, checkbuttons, colorbuttons y entries
        @param filename Nombre del fichero .glade (STRING)
        @return Diccionario con comboboxes, spinbuttons, radiobuttons, checkbuttons, colorbuttons y entries que estan en el .glade
    """
    file = open(filename,'r')
    count = 0
    line = file.readline()
    comboboxes = []
    spinbuttons = []
    radiobuttons = []
    checkbuttons = []
    colorbuttons = []
    entries = []

    while line:
        pos_class = line.find('class')
        if pos_class != -1:
            pos_pc = line.find('"')
            pos_sc = line.find('"', pos_pc + 1)
            widget = line[pos_pc + 1:pos_sc]
            pos_pc1 = line.find('"', pos_sc + 1)
            pos_sc1 = line.find('"', pos_pc1 + 1)
            name = line[pos_pc1 + 1:pos_sc1]
            #print widget, name
            count += 1
            if widget == 'GtkComboBox':
                comboboxes.append(name)
            if widget == 'GtkSpinButton':
                spinbuttons.append(name)
            if widget == 'GtkRadioButton':
                radiobuttons.append(name)
            if widget == 'GtkCheckButton':
                checkbuttons.append(name)
            if widget == 'GtkColorButton':
                colorbuttons.append(name)
            if widget == 'GtkEntry':
                entries.append(name)

        line = file.readline()
    """
    print 'All widgets found: ', count
    print 'Comboboxes found:', len(comboboxes)
    print 'Spinbuttons found:', len(spinbuttons)
    print 'Radiobuttons found:', len(radiobuttons)
    print 'Checkbuttons found:', len(checkbuttons)
    print 'Colorbuttons found:', len(colorbuttons)
    print 'Entries found:', len(entries)
    """
    result = {'comboboxes': comboboxes,
            'spinbuttons': spinbuttons,
            'radiobuttons': radiobuttons,
            'checkbuttons': checkbuttons,
            'colorbuttons': colorbuttons,
            'entries': entries}
    return result


def set_new_options(filename, builder, builder_widgets):
        """
            Funcion que configura nuevas opciones de configuracion en la ventana de dialogo de SETUP
            @param filename Nombre del fichero .glade (STRING)
            @param builder Constructor del widget
            @param builder_widgets Widgets contenidos en el Constructor
        """
        def hex_to_rgb(value):
                value = value.lstrip('#')
                lv = len(value)
                return tuple((int(value[i:i + lv / 3], 16) / 65535.0) for i in range(0, lv, lv / 3))

        for c, v in builder_widgets['comboboxes'].items():
            builder.get_object(c).set_active(v[1])

        for c, v in builder_widgets['spinbuttons'].items():
            builder.get_object(c).set_value(v)

        for c, v in builder_widgets['radiobuttons'].items():
            builder.get_object(c).set_active(v)

        for c, v in builder_widgets['checkbuttons'].items():
            builder.get_object(c).set_active(v)

        for c, v in builder_widgets['colorbuttons'].items():
            color = gtk.gdk.color_parse(v[1])
            builder.get_object(c).set_color(color)

        for c, v in builder_widgets['entries'].items():
            builder.get_object(c).set_text(v)


def prepare_options(filename, builder):
        """
            Funcion que retorna un dicionario con las nuevas configuraciones que realize el usuario en el builder asociado con el el
            .glade de dicho builder
            @param filename Nombre del fichero .glade (STRING)
            @param builder Constructor del widget
            @param builder_widgets Widgets contenidos en el Constructor
        """
        def hex_to_rgb(value):
                value = value.lstrip('#')
                lv = len(value)
                return tuple((int(value[i:i + lv / 3], 16) / 65535.0) for i in range(0, lv, lv / 3))

        all_options = {}
        builder_widgets = get_important_widgets(filename)
        options = {}
        for c in builder_widgets['comboboxes']:
            options[c] = (builder.get_object(c).get_active_text(), builder.get_object(c).get_active())
        all_options['comboboxes'] = options
        options = {}
        for c in builder_widgets['spinbuttons']:
            options[c] = builder.get_object(c).get_value()
        all_options['spinbuttons'] = options
        options = {}
        for c in builder_widgets['radiobuttons']:
            options[c] = builder.get_object(c).get_active()
        all_options['radiobuttons'] = options
        options = {}
        for c in builder_widgets['checkbuttons']:
            options[c] = builder.get_object(c).get_active()
        all_options['checkbuttons'] = options
        options = {}
        for c in builder_widgets['colorbuttons']:
            options[c] = (hex_to_rgb(builder.get_object(c).get_color().to_string()),
                          str(builder.get_object(c).get_color()))
        all_options['colorbuttons'] = options
        options = {}
        for c in builder_widgets['entries']:
            options[c] = builder.get_object(c).get_text()
        all_options['entries'] = options

        return all_options


def replace_widget(window, parent, child, expand=True, fill=True, child_number=0):
    """
        Funcion para remover un widget hijo y poner ya se otro o no
        @param window Ventana de donde provienene
        @param parent widget padre
        @param child widget que se va reemplazar, None para no colocar nada
        @param expand si el widget hijo nuevo se va expandir
        @param fill si el widget hijo nuevo se va llenar
        @param child_number numero del hijo que seva reemplazar
    """
    try:
        old_widget = parent.get_children()[child_number]
    except:
        pass
    else:
        parent.remove(old_widget)
    if child is not None:
        if isinstance(parent, gtk.VBox) or isinstance(parent, gtk.HBox):
            parent.pack_start(child, expand, fill)
        else:
            parent.add(child)
    window.show_all()

#   TESTEO
#if __name__ == '__main__':
#    print get_important_widgets('ConfigProjectDialog.glade')
#    print get_important_widgets('Calibration.glade')
#    print get_important_widgets('Tracks.glade')
