# -*- encoding: utf-8 -*-
'''
Emdyp.me
Revisado: Pedro Rivera
Script: Tipo A
(ventanas de dialogos)
'''
import gtk
import json
import time
import math
import types
import gobject
from gettext import gettext
from datetime import datetime

import config
from utils import *
from tools import Tool
from playback.database import load_db
from communication.serial_connection import DavidConnection

from copy import copy

class ConfigTracksDialog(object):
    """
         Clase  que  representa  la  ventana  de  dialogo  para  modificar
         durante  el  registro  de  la informacion  las  configuraciones  de
         graficado  de  las  curvas.
    """
    tracks_file = config.gui_file('TracksConfigDialog.glade')

    def __init__(self, options):
        """
            Constructor de la clase ConfigTracksDialog
            @param options Las opciones que actualmente el usuario haya
            configurado en la ventana de SETUP.
        """
        self.options = options
        self.tracks = gtk.Builder()
        self.tracks.add_from_file(self.tracks_file)

        self.dialog = self.tracks.get_object('dialog')
        self.box_config = self.tracks.get_object('box_config')

        self.tracks.connect_signals(self)

        self.get_current_options(self.tracks_file, self.tracks,
                             self.options['tracks_options'])

        if self.options['config_options']['radiobuttons']['rad_pt_service']:
            if self.options['config_options']['radiobuttons']['rad_pt_5_tracks']:
                self.box_config.add(self.tracks.get_object('box_pt_d5_tracks_config'))
            if self.options['config_options']['radiobuttons']['rad_pt_6_tracks']:
                self.box_config.add(self.tracks.get_object('box_pt_d6_tracks_config'))

        self.dialog.show_all()
        self.dialog.run()

    def validate_fields(self):
        """@fn validate_fields
           Funcion para validar los datos introducidos por el usuario y
           corrobara que todo este correctamente configurado
           en cuanto a rangos de span y otros.
        """
        def tracks_misconfigured(span1, span2):
            if span1 >= span2:
                msg = 'Tracks misconfigured: span 2 should be greater than span 1'
                self.show_error(msg)
                return False
            return True

        dic = self.options['tracks_options']['spinbuttons']
        trackModes = ['d5', 'd6']
        sensorLabels = ['gr','avggr','ccl','temp1','temp2','pres', 'res3',
                        'res1','res2','accx','accy','accz','ten','depth']
        for mode in trackModes:
            for sensor in sensorLabels:
                if mode == 'd5' and sensor == 'avggr':
                    pass
                elif not tracks_misconfigured(dic['sb_pt_'+mode+'_'+sensor+'_span1'],
                                              dic['sb_pt_'+mode+'_'+sensor+'_span2']):
                    return False
        return True

    def on_dialog_response(self, dialog, response_id):
        """@fn on_dialog_response
            Callback que se ejecuta cuando el usuario presiona el boton Cancel
            o Set de la ventana de dialogo y dependiendo de ello se configurar
            las nuevas opciones que van a configurar el modo de graficado de
            las curvas
            @param dialog De quien recibe la senal
            @param response_id Respuesta del boton presionado
        """
        if response_id != gtk.RESPONSE_OK:
            self.dialog.destroy()
        else:
            self.set_new_options(self.tracks_file, self.tracks)
            if self.validate_fields():
                self.dialog.destroy()
            else:
                self.dialog.show_all()
                self.dialog.run()

    def get_current_options(self, filename, builder, builder_widgets):
        """@fn get_current_options
            Funcion que va retornar las opciones actuales de garficacion de
            datos que el usuario ha configurado para el registro
        """
        for c, v in builder_widgets['spinbuttons'].items():
            sb = builder.get_object(c)
            if sb:
                sb.set_value(v)

    def set_new_options(self, filename, builder):
        """@fn set_new_options
            Funcion para obtener de los valores de cada spinbutton que hay en
            la ventana de dialogo y se almacena en el diccionario
            que contiene la configuracion del usuario, esto hace una
            actualizacion de dicho diccionario
            @param filename Nombre del fichero  TracksConfigDialog.glade
            @param builder Objeto que referencia a constructor del dialogo
        """
        builder_widgets = get_important_widgets(filename)
        for c in builder_widgets['spinbuttons']:
            self.options['tracks_options']['spinbuttons'][c] = builder.get_object(c).get_value()

    def show_error(self, message):
        """@fn show_error
            Callback mostrar algun mensaje en un ventana de dialogo de tipo error
            @param message Mensaje que va ser mostrado en la ventana de dialogo
            (STRING)
        """
        dialog = gtk.MessageDialog(None,
                                   gtk.DIALOG_MODAL |
                                   gtk.DIALOG_DESTROY_WITH_PARENT,
                                   gtk.MESSAGE_ERROR,
                                   gtk.BUTTONS_OK)

        dialog.set_title(gettext('Error'))
        dialog.set_markup(message)

        dialog.get_position()
        dialog.set_deletable(False)

        response = dialog.run()

        dialog.destroy()
        return response
