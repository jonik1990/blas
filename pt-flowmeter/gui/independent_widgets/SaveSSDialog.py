# -*- encoding: utf-8 -*-
'''
Emdyp.me
Revisado: Pedro Rivera
Script: Tipo A
(ventanas de dialogos)
'''
import gtk
import json
import time
import math
import types
import gobject
from gettext import gettext
from datetime import datetime

import config
from utils import *
from tools import Tool
from playback.database import load_db
from communication.serial_connection import DavidConnection

from copy import copy

class SaveSSDialog(object):
    """
         Clase  que  representa  la  ventana  de  dialogo  de  guarda  la
         configuracion  de  loging  que  el usuario  haya  configurado
    """
    builder_file = config.gui_file('SaveSSDialog.glade')

    def __init__(self, options):
        """
            Constructor de la clase SaveSSDialog
            @param options Dicionario con las opciones que el usuario configuro
            en el la ventana de dailogo de SETUP
        """
        self.builder = gtk.Builder()
        self.builder.add_from_file(self.builder_file)
        self.dialog = self.builder.get_object('dialog')

        self.options = options

        filter = gtk.FileFilter()
        filter.add_pattern("*.jss")
        self.dialog.set_filter(filter)

        self.builder.connect_signals(self)
        self.dialog.show_all()
        self.dialog.run()

    def on_dialog_response(self, dialog, response_id):
        """@fn on_dialog_response
            Callback que se ejecuta cuando el usuario presiona el boton Cancel
            o Save de la ventana de dialogo
            y dependiendo de ello se guarda o no un archivo .jss (JSON)
            @param dialog De quien recibe la senal
            @param response_id Respuesta del boton presionado
        """
        if response_id != 5:
            self.dialog.destroy()
            return
        else:
            filename = self.dialog.get_filename()
            if not filename:
                self.show_info('Please select a folder and filename')
                self.dialog.show_all()
                self.dialog.run()
            if not filename.endswith('.jss'):
                filename += '.jss'
            file = open(filename, "w")
            json.dump(self.options, file)
            file.close()
            self.dialog.destroy()
            return

    def show_info(self, message):
        """@fn show_info
            Callback mostrar algun mensaje en un ventana de dialogo
            @param message Mensaje que va ser mostrado en la ventana de dialogo
            (STRING)
        """
        dialog = gtk.MessageDialog(None,
                                   gtk.DIALOG_MODAL |
                                   gtk.DIALOG_DESTROY_WITH_PARENT,
                                   gtk.MESSAGE_INFO,
                                   gtk.BUTTONS_OK)

        dialog.set_title(gettext('Information'))
        dialog.set_markup(message)

        dialog.get_position()
        dialog.set_deletable(False)

        response = dialog.run()
        dialog.destroy()
        return response

