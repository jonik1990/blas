# -*- encoding: utf-8 -*-
'''
Emdyp.me
Revisado: Pedro Rivera
Script: Tipo A
(ventanas de dialogos)
'''
import gtk
import json
import time
import math
import types
import gobject
from gettext import gettext
from datetime import datetime

import config
from utils import *
from tools import Tool
from playback.database import load_db
from communication.serial_connection import DavidConnection

from copy import copy

class MonitorModeLoggingDialog(object):
    """
         Clase  que  representa  la  ventana  de  dialogo  para  el  Monitor  Logging  MonitorModeLoggingDialog
    """
    builder_file = config.gui_file('LoggingModeDialog.glade')

    def __init__(self, parent):
        """
            Constructor de la clase MonitorModeLoggingDialog
            @param parent Recibe un widget padre en este caso del principal
        """
        self.builder = gtk.Builder()
        self.parent = parent
        self.builder.add_from_file(self.builder_file)
        self.dialog = self.builder.get_object('dialoglogging')
        self.ls_sample_rate = self.builder.get_object('ls_sampling_rate')
        self.label_units = self.builder.get_object('lbl_sampling_rateunits')
        self.combosamplerate = self.builder.get_object('cb_sampling_rate')
        self.labelDepthValue = self.builder.get_object('lbl_depth_actual_value')
        self.labelTensionValue = self.builder.get_object('lbl_tension_actual_value')
        self.valuadj = self.builder.get_object('adj_depth_zero')
        self.radup = self.builder.get_object('rad_up_mode')
        self.raddown = self.builder.get_object('rad_down_mode')
        self.namerecordtext = self.builder.get_object('ent_name_record')
        self.radtime = self.builder.get_object('rad_real_time')
        self.raddepth = self.builder.get_object('rad_real_depth')
        self.buttonset = self.builder.get_object('btn_set')
        self.builder.connect_signals(self)
        self.lastDepth = 0.00
        self.down = True

        self.localtime = time.localtime(time.time())
        self.textInit = str(self.localtime.tm_year) + '_' + str(self.localtime.tm_mon) + '_' + str(self.localtime.tm_mday) + '_'
        self.parent.record_filename = self.textInit
        self.textInit = self.textInit + str(self.localtime.tm_hour) + '_' + str(self.localtime.tm_min) + '_' + str(self.localtime.tm_sec)
        self.namerecordtext.set_text(self.textInit)


        self.buttonset.set_sensitive(False)

        #self.dialog.show_all()
        #self.dialog.run()#

    def ShowMonitorDialog(self):
        """@fn ShowMonitorDialog
            Permite visualizacion de la ventana de dialogo
        """
        self.dialog.show_all()

    def on_dialoglogging_destroy(self, event):
        """@fn on_dialoglogging_destroy
            Callback cuando se cierra la ventana de dialogo y aviza al widget padre que se cerro
        """
        self.parent.EstateDialogMonitor = False

    def on_btn_set_clicked(self, button):
        """@fn on_btn_set_clicked
            Callback que se ejecuta cuando se presiona el boton Set
            @param button Objeto de quien viene el evento de click
        """
        #print "Set New"
        self.parent.Down = copy(self.down)
        self.parent.DeptReference = self.valuadj.get_value()
        textactual = self.namerecordtext.get_text()
        if textactual != self.parent.record_filename and self.parent.state_record is True:
            msg = 'The name of the database change, you must first stop the current recording.\n'
            msg = msg + 'Want to stop the current recording?'
            if self.parent.show_question(msg):
                self.parent.btn_record.set_active(False)
                self.parent.record_filename = self.namerecordtext.get_text()
        else:
            self.parent.record_filename = self.namerecordtext.get_text()
        self.parent.new_config_from_monitor()
        #if self.parent.options:
            #self.parent.options['logingmodedialog_options'] = prepare_options(self.builder_file, self.builder)
            #print self.parent.options['logingmodedialog_options']

    def rad_real_depth_clicked_cb(self, rad):
        """@fn rad_real_depth_clicked_cb
            Callback que se ejecuta cuando se cambia a modo de profundidad VS Datos
            @param rad Objeto de quien viene el evento de click
        """
        if rad.get_active():
            self.parent.k = config.TRACKING_BY_DEPTH
            self.ls_sample_rate.clear()
            self.ls_sample_rate.append(['10'])
            self.ls_sample_rate.append(['4'])
            self.ls_sample_rate.append(['2'])
            self.label_units.set_text('  Samples/Ft')
            if self.parent.options:
                if self.parent.options.has_key('logingmodedialog_options'):
                    if self.parent.options['logingmodedialog_options']['comboboxes']['cb_sampling_rate'][1] <= 2:
                        self.combosamplerate.set_active(self.parent.options['logingmodedialog_options']['comboboxes']['cb_sampling_rate'][1])
                    else:
                        self.combosamplerate.set_active(0)
                else:
                    self.combosamplerate.set_active(0)
            else:
                self.combosamplerate.set_active(0)
            self.radup.set_sensitive(True)

    def rad_real_time_clicked_cb(self, rad):
        """@fn rad_real_time_clicked_cb
            Callback que se ejecuta cuando se cambia a modo de tiempo VS Datos
            @param rad Objeto de quien viene el evento de click
        """
        if rad.get_active():
            self.parent.k = config.TRACKING_BY_TIME
            self.ls_sample_rate.clear()
            self.ls_sample_rate.append(['100'])
            self.ls_sample_rate.append(['200'])
            self.ls_sample_rate.append(['300'])
            self.ls_sample_rate.append(['400'])
            self.ls_sample_rate.append(['500'])
            self.ls_sample_rate.append(['1000'])
            self.label_units.set_text('  ms')
            if self.parent.options:
                if self.parent.options.has_key('logingmodedialog_options'):
                    self.combosamplerate.set_active(self.parent.options['logingmodedialog_options']['comboboxes']['cb_sampling_rate'][1])
                else:
                    self.combosamplerate.set_active(0)
            else:
                self.combosamplerate.set_active(0)
            self.radup.set_active(False)
            self.radup.set_sensitive(False)
            self.raddown.set_active(True)

    def on_rad_up_mode_clicked(self, rad):
        """@fn on_rad_up_mode_clicked
            Callback que se ejecuta cuando se cambia a modo UP de datos
            @param rad Objeto de quien viene el evento de click
        """
        if rad.get_active():
            self.down = False
            gobject.idle_add(self.valuadj.set_value, self.lastDepth)

    def on_rad_down_mode_clicked(self, rad):
        """@fn on_rad_down_mode_clicked
            Callback que se ejecuta cuando se cambia a modo Down de datos
            @param rad Objeto de quien viene el evento de click
        """
        if rad.get_active():
            self.down = True
            gobject.idle_add(self.valuadj.set_value, self.lastDepth)

    def SetNewValues(self, Depth, Tension):
        """@fn SetNewValues
            Callback que se ejecuta para actualizar las etiquetas de Tension y
            Profundidad en la ventana de dialogo
            @param Depth Valor de profundidad
            @param Tension Valor de tension
        """
        #print "new values"
        self.lastDepth = copy(Depth)
        gobject.idle_add(self.labelDepthValue.set_text, str(Depth) + " Ft")
        gobject.idle_add(self.labelTensionValue.set_text, str(Tension) + " N")
        #gobject.idle_add(self.valuadj.set_value,Depth)