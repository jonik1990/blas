# -*- encoding: utf-8 -*-
'''
Emdyp.me
Revisado: Pedro Rivera
Script: Tipo A
(ventanas de dialogos)
'''
import gtk
import json
import time
import math
import types
import gobject
from gettext import gettext
from datetime import datetime

import config
from utils import *
from tools import Tool
from playback.database import load_db
from communication.serial_connection import DavidConnection

from copy import copy


class AboutDialog(object):
    """
         Clase que representa la ventana de dialogo que muestra informacion
         general  del  software
    """
    builder_file = config.gui_file('AboutDialog.glade')

    def __init__(self):
        """
            Constructor de la clase AboutDialog
        """
        self.builder = gtk.Builder()
        self.builder.add_from_file(self.builder_file)
        self.dialog = self.builder.get_object('dialog')

        self.builder.get_object('lbl_sw_name').set_text(config.SW_NAME)
        self.builder.get_object('lbl_sw_version').set_text(config.VERSION)

        #copyright = '(c) Copyright' + config.SW_NAME + '2013.  All rights reserved.'
        copyright = '(c) Copyright JPT Consulting & Services 2013 - 2015.  All rights reserved.'
        self.builder.get_object('lbl_copyright').set_text(copyright)

        email = 'Support and Maintenance: ' + 'JPT Consulting & Services'
        self.builder.get_object('lbl_email').set_text(email)

        self.builder.connect_signals(self)
        self.dialog.show_all()
        self.dialog.run()

    def on_btn_close_clicked(self, button):
        """@fn on_btn_close_clicked
            Callback para cerrar la ventana de dialogo ABOUT
        """
        self.dialog.destroy()
