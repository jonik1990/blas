# -*- encoding: utf-8 -*-
'''
Emdyp.me
Revisado: Pedro Rivera
Script: Tipo A
(ventanas de dialogos)
'''
import gtk
import json
import time
import math
import types
import gobject
from gettext import gettext
from datetime import datetime

import config
from utils import *
from tools import Tool
from playback.database import load_db
from communication.serial_connection import DavidConnection

from copy import copy


class SSDialog(object):
    """
         Clase  que  representa  la  ventana  de  dialogo  de  configuracion
         del  servicio  y  de  parametros de  las  herramientas  de  dichos
         servicios  ademas  configura  opciones  que  el  usuario  coloca
         para  el  registro  de  la  informacion  asi  como  la  visualizacion
    """
    builder_file = config.gui_file('SSDialog.glade')
    calibration_file = config.gui_file('Calibration.glade')
    tracks_file = config.gui_file('Tracks.glade')

    def __init__(self, options={}):
        """
            Constructor de la clase SSDialog
            @param options Diccionario que contiene las opciones de
            configuracion del usuario, este lista es pasada desde la ventana
            principal (MainWindow)
        """
        self.options = options

        self.builder = gtk.Builder()
        self.builder.add_from_file(self.builder_file)
        self.calibration = gtk.Builder()
        self.calibration.add_from_file(self.calibration_file)
        self.tracks = gtk.Builder()
        self.tracks.add_from_file(self.tracks_file)

        self.dialog = self.builder.get_object('dialog')
        self.vp_displays = self.builder.get_object('vp_displays')
        self.vp_hw_setup = self.builder.get_object('vp_hw_setup')
        self.vp_cal_setup = self.builder.get_object('vp_cal_setup')
        self.box_tracks = self.builder.get_object('box_tracks')

        self.box_tool = self.builder.get_object('box_tool_pt')

        self.vp_displays.add(self.builder.get_object('box_pt_displays'))
        self.vp_hw_setup.add(self.builder.get_object('box_hw_setup_pt'))
        self.vp_cal_setup.add(self.builder.get_object('box_calibration_pt'))
        self.box_tracks.add(self.tracks.get_object('box_pt_tracks_d5'))

        self.pt_d5_track_conf(self.tracks.get_object('cb_pt_tracks_d5'))
        self.pt_d6_track_conf(self.tracks.get_object('cb_pt_tracks_d6'))

        self.pt_depth_cal(self.builder.get_object('cb_pt_cal_depth_2'))
        self.pt_gr_cal(self.builder.get_object('cb_pt_cal_gr_2'))
        self.pt_ccl_cal(self.builder.get_object('cb_pt_cal_ccl_2'))
        self.pt_res1s_cal(self.builder.get_object('cb_pt_cal_res1s_2'))
        self.pt_res2s_cal(self.builder.get_object('cb_pt_cal_res2s_2'))
        #### OJO AQUI
#        self.pt_res3s_cal(self.builder.get_object('cb_pt_cal_res2s_3'))
        #### CUIDADO
        self.pt_temp1_cal(self.builder.get_object('cb_pt_cal_temp1_2'))
        self.pt_temp2_cal(self.builder.get_object('cb_pt_cal_temp2_2'))
        self.pt_pres_cal(self.builder.get_object('cb_pt_cal_pres_2'))
        self.pt_ten_cal(self.builder.get_object('cb_pt_cal_ten_2'))
        self.pt_accx_cal(self.builder.get_object('cb_pt_cal_accx_2'))
        self.pt_accy_cal(self.builder.get_object('cb_pt_cal_accy_2'))
        self.pt_accz_cal(self.builder.get_object('cb_pt_cal_accz_2'))

        self.tool = Tool()
        self.box_tool.pack_start(self.tool, False, False)

        self.builder.connect_signals(self)
        self.tracks.connect_signals(self)

        if self.options:
            set_new_options(self.builder_file, self.builder,
                                 self.options['config_options'])
            set_new_options(self.calibration_file, self.calibration,
                                 self.options['calibration_options'])
            set_new_options(self.tracks_file, self.tracks,
                                 self.options['tracks_options'])

        self.dialog.show_all()
        self.dialog.run()

    #funcion de prueba de acciones, se puede borrar
    def test(self, widget):
        pass

    def on_dialog_response(self, dialog, response_id):
        """@fn on_dialog_response
            Callback que se genera cuando la ventana de dialog se ha presionado el boton Setup o Cancel
            y se encarga de obtener las configuraciones realizadas por le usuario
            @param dialog Widget que recibe la senial
            @param response_id Retorna la respuesta de la ventana Dialog
        """
        if response_id != gtk.RESPONSE_OK:
            self.dialog.destroy()
        else:
            self.options['config_options'] = prepare_options(self.builder_file, self.builder)
            self.options['calibration_options'] = prepare_options(self.calibration_file, self.calibration)
            self.options['tracks_options'] = prepare_options(self.tracks_file, self.tracks)
            if self.validate_fields():
                self.dialog.destroy()
                return
            else:
                self.dialog.show_all()
                self.dialog.run()

    def validate_fields(self):
        """@fn validate_fields
            Funcion para validar los datos introducidos por el usuario y
            corrobara que todo este correctamente configurado
            en cuanto a rangos de span y otros.
        """
        pt_tool = self.options['config_options']['spinbuttons']['sb_pt_rd_tool']
        pt_sensors = [self.options['config_options']['spinbuttons']['sb_pt_rd_ccl'],
                      self.options['config_options']['spinbuttons']['sb_pt_rd_gr'],
                      self.options['config_options']['spinbuttons']['sb_pt_rd_ten'],
                      self.options['config_options']['spinbuttons']['sb_pt_rd_res'],
                      self.options['config_options']['spinbuttons']['sb_pt_rd_pres'],
                      self.options['config_options']['spinbuttons']['sb_pt_rd_temp'],
                      self.options['config_options']['spinbuttons']['sb_pt_rd_acc']]
        if any(pt_tool < sensor for sensor in pt_sensors):
            msg = 'Relative depth misconfigured: Parameter Tool has to be longer'
            self.show_error(msg)
            return False

        def tracks_misconfigured(span1, span2):
            if span1 >= span2:
                msg = 'Tracks misconfigured: span 2 should be greater than span 1'
                self.show_error(msg)
                return False
            return True
        dic = self.options['tracks_options']['spinbuttons']
        trackModes = ['d5', 'd6']
        sensorLabels = ['gr','avggr','ccl','temp1','temp2','pres', 'res3',
                        'res1','res2','accx','accy','accz','ten','depth']
        for mode in trackModes:
            for sensor in sensorLabels:
                if mode == 'd5' and sensor == 'avggr':
                    pass
                elif not tracks_misconfigured(dic['sb_pt_'+mode+'_'+sensor+'_span1'],
                                              dic['sb_pt_'+mode+'_'+sensor+'_span2']):
                    return False
        return True

    def pt_d5_track_conf(self, widget):
        """@fn pt_d5_track_conf
            Callback que configura los opciones de Tracks Options cuando en
            Display Options se elige el RadioButton de 5 Tracks en el
            servicio de Parameter Tool (SSDialog.glade->box_pt_displays->rad_pt_5_tracks)
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        dic = {0: self.tracks.get_object('frm_pt_d5_track1'),
               1: self.tracks.get_object('frm_pt_d5_track2'),
               2: self.tracks.get_object('frm_pt_d5_track3'),
               3: self.tracks.get_object('frm_pt_d5_track4'),
               4: self.tracks.get_object('frm_pt_d5_track5')}
        replace_widget(self.dialog, box, dic[widget.get_active()], False, False, 2)

    def pt_d6_track_conf(self, widget):
        """@fn pt_d6_track_conf
            Callback que configura los opciones de Tracks Options cuando en
            Display Options se elige el RadioButton de 6 Tracks en el
            servicio de Parameter Tool (SSDialog.glade->box_pt_displays->rad_pt_6_tracks)
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        dic = {0: self.tracks.get_object('frm_pt_d6_track1'),
               1: self.tracks.get_object('frm_pt_d6_track2'),
               2: self.tracks.get_object('frm_pt_d6_track3'),
               3: self.tracks.get_object('frm_pt_d6_track4'),
               4: self.tracks.get_object('frm_pt_d6_track5')}
        replace_widget(self.dialog, box, dic[widget.get_active()], False, False, 2)

    def pt_depth_cal(self, widget):
        """@fn pt_depth_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Depth cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_depth_2) Cambia, y
            configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_depth_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_gr_cal(self, widget):
        """@fn pt_gr_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Gamma Ray cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_gr_2) Cambia, y
            configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_gr_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_ccl_cal(self, widget):
        """@fn pt_gr_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de CCL cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_ccl_2) Cambia, y
            configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_ccl_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_res1s_cal(self, widget):
        """@fn pt_res1s_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Resistivity 1 State cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_res1s_2) Cambia,
            y configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_res1s_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_res2s_cal(self, widget):
        """@fn pt_res2s_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Resistivity 2 State cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_res2s_2) Cambia,
            y configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_res2s_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_temp1_cal(self, widget):
        """@fn pt_temp1_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Temperature 1 cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_temp1_2) Cambia,
            y configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_temp1_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_temp2_cal(self, widget):
        """@fn pt_temp2_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Temperature 2 cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_temp2_2) Cambia,
            y configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_temp2_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_pres_cal(self, widget):
        """@fn pt_pres_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Pressure cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_pres_2) Cambia,
            y configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_pres_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_ten_cal(self, widget):
        """@fn pt_ten_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Tension cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_ten_2) Cambia,
            y configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_ten_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_accx_cal(self, widget):
        """@fn pt_accx_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Acc X cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_accx_2) Cambia,
            y configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_accx_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_accy_cal(self, widget):
        """@fn pt_accy_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Acc Y cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_accy_2) Cambia,
            y configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_accy_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def pt_accz_cal(self, widget):
        """@fn pt_accz_cal
            Callback que configura los opciones de Calibrate Options en el
            ComboBox de Acc Z cuando la eleccion de calibracion
            ((SSDialog.glade->box_calibration_pt->cb_pt_cal_accz_2) Cambia,
            y configura el nuevo widget segun el tipo que se haya elegido
            y lo carga desde el fichero "Calibration.glade". servicio de
            Parameter Tool
            @param widget De quien recibe la senial
        """
        box = widget.get_parent().get_parent()
        text = widget.get_active_text().lower()
        new_widget = self.calibration.get_object('box_pt_accz_' + text)
        replace_widget(self.dialog, box, new_widget, False, False, 1)

    def on_rad_pt_5_tracks(self, rad):
        """@fn on_rad_pt_5_tracks
            Callback del radio button que esta en Display Options de 5 Tracks,
            esto configura la caja de tracks (box_tracks),
            con el modo de 5 tracks cargando desde tracks(Tracks.glade) la caja
            de 5 tracks (box_pt_tracks_d5) y tambien
            deshabilita wl widget de Gamma Ray AVG(box_pt_avggr_slot) que esta
            en la seccion de Calibartion Setup
            servicio de Parameter Tool
            @param rad De quien recibe la senial
        """
        if rad.get_active():
            replace_widget(self.dialog, self.box_tracks,
                                 self.tracks.get_object('box_pt_tracks_d5'))
            replace_widget(self.dialog,
                           self.builder.get_object('box_pt_avggr_slot'),
                           None)

    def on_rad_pt_6_tracks(self, rad):
        """@fn on_rad_pt_6_tracks
            Callback del radio button que esta en Display Options de 6 Tracks,
            esto configura la caja de tracks (box_tracks),
            con el modo de 6 tracks cargando desde tracks(Tracks.glade) la caja
            de 5 tracks (box_pt_tracks_d6) y tambien
            habilita wl widget de Gamma Ray AVG(box_pt_avggr_slot) que esta en la seccion de Calibartion Setup
            servicio de Parameter Tool
            @param rad De quien recibe la senial
        """
        if rad.get_active():
            replace_widget(self.dialog, self.box_tracks,
                                 self.tracks.get_object('box_pt_tracks_d6'))
            replace_widget(self.dialog,
                           self.builder.get_object('box_pt_avggr_slot'),
                           self.builder.get_object('box_pt_avggr'))

    def on_rad_pt_service(self, rad):
        """@fn on_rad_pt_service
            Callback del radio button para elegir el servicio de Parameteer Tool
            Service, este callback permite actualizar el dialog para que carge
            las configuraciones basicas del servicio de Parameter Tool, en caso
            de que anteriormente haya estado en el servicio de loging
            @param rad De quien recibe la senal
        """
        if rad.get_active():
            replace_widget(self.dialog, self.vp_displays,
                                 self.builder.get_object('box_pt_displays'))
            replace_widget(self.dialog, self.vp_hw_setup,
                                 self.builder.get_object('box_hw_setup_pt'))
            self.box_tool = self.builder.get_object('box_tool_pt')
            self.tool = Tool()
            replace_widget(self.dialog, self.box_tool, self.tool, False, False)
            replace_widget(self.dialog, self.vp_cal_setup,
                                 self.builder.get_object('box_calibration_pt'))
            self.on_rad_pt_5_tracks(self.builder.get_object('rad_pt_5_tracks'))
            self.on_rad_pt_6_tracks(self.builder.get_object('rad_pt_6_tracks'))

    def on_btn_set_rel_depth_pt_clicked(self, button):
        """@fn on_btn_set_rel_depth_pt_clicked
            Callback del boton que esta en Hardware Setup "Preview" este boton
            lo que hace es actualizar el grafico de la herramienta
            que esta en self.box_tool y que contiene un objeto de la clase Tool
            que es el encargado de graficar la herramienta
            este callback aplica cuando esta en servicio de Parameter Tool,
            para configurar la profundidad relativa de los sensores
            @param button De quien recibe la senal
        """
        tool = self.builder.get_object('sb_pt_rd_tool').get_value()
        ccl = self.builder.get_object('sb_pt_rd_ccl').get_value()
        gr = self.builder.get_object('sb_pt_rd_gr').get_value()
        res = self.builder.get_object('sb_pt_rd_res').get_value()
        pres = self.builder.get_object('sb_pt_rd_pres').get_value()
        temp = self.builder.get_object('sb_pt_rd_temp').get_value()
        acc = self.builder.get_object('sb_pt_rd_acc').get_value()
        ten = self.builder.get_object('sb_pt_rd_ten').get_value()
        devices = {'tool': tool,
                   'ccl': {'value': ccl, 'color': (1, 1, 0)},
                   'gr': {'value': gr, 'color': (0, 0, 1)},
                   'res': {'value': res, 'color': (1, 1, 1)},
                   'pres': {'value': pres, 'color': (0, 1, 0)},
                   'temp': {'value': temp, 'color': (1, 0, 0)},
                   'acc': {'value': acc, 'color': (1, 0.5, 0)},
                   'ten': {'value': ten, 'color': (1, 0.5, 0.5)}
                    }
        self.tool.set_devices(devices)

    def on_btn_test_davidconn_clicked(self, button):
        """@fn on_btn_test_davidconn_clicked
            Callback Para realizar testeo de la conexion David y verificar que
            la coneccion con el puerto serial se ha realizado
            de forma correcta
            @param button De quien recibe la senal
        """
        s_connection = DavidConnection()
        p = self.builder.get_object('sb_lg_serial_port').get_value()
        if s_connection.connect(port=int(p - 1)):
            self.show_info("Connection Successful")
        else:
            self.show_error('Could not connect to serial port')

    def show_info(self, message):
        """@fn show_info
            Callback mostrar algun mensaje en un ventana de dialogo
            @param message Mensaje que va ser mostrado en la ventana de dialogo (STRING)
        """
        dialog = gtk.MessageDialog(None,
                                   gtk.DIALOG_MODAL |
                                   gtk.DIALOG_DESTROY_WITH_PARENT,
                                   gtk.MESSAGE_INFO,
                                   gtk.BUTTONS_OK)

        dialog.set_title(gettext('Information'))
        dialog.set_markup(message)

        dialog.get_position()
        dialog.set_deletable(False)

        response = dialog.run()
        dialog.destroy()
        return response

    def show_error(self, message):
        """@fn show_info
            Callback mostrar algun mensaje en un ventana de dialogo de tipo
            error
            @param message Mensaje que va ser mostrado en la ventana de dialogo
            (STRING)
        """
        dialog = gtk.MessageDialog(None,
                                   gtk.DIALOG_MODAL |
                                   gtk.DIALOG_DESTROY_WITH_PARENT,
                                   gtk.MESSAGE_ERROR,
                                   gtk.BUTTONS_OK)

        dialog.set_title(gettext('Error'))
        dialog.set_markup(message)

        dialog.get_position()
        dialog.set_deletable(False)

        response = dialog.run()

        dialog.destroy()
        return response

