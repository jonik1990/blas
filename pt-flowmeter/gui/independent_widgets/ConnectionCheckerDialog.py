# -*- encoding: utf-8 -*-
'''
Emdyp.me
Revisado: Pedro Rivera
Script: Tipo A
(ventanas de dialogos)
'''
import gtk
import json
import time
import math
import types
import gobject
from gettext import gettext
from datetime import datetime

import config
from utils import *
from tools import Tool
from playback.database import load_db
from communication.serial_connection import DavidConnection

from copy import copy

class ConnectionCheckerDialog(object):
    """
       Clase que representa la ventana de dialogo para el Protocol Checker
    """
    builder_file = config.gui_file('ConnectionCheckerDialog.glade')

    def __init__(self, closed_callback):
        """
            Constructor de la clase ConnectionCheckerDialog
            @param closed_callback Callback para cerrar la ventana de
            connection Checker
        """
        self.builder = gtk.Builder()
        self.builder.add_from_file(self.builder_file)
        self.dialog = self.builder.get_object('dialog')
        self.closed_callback = closed_callback

        self.ls_messages = self.builder.get_object('ls_messages')
        self.sw_messages = self.builder.get_object('sw_messages')
        self.ls_trames = self.builder.get_object('ls_trames')
        self.sw_trames = self.builder.get_object('sw_trames')

        self.builder.connect_signals(self)
        self.dialog.show_all()
        self.dialog.run()

    def on_tw_messages_size_allocate(self, widget, event, data=None):
        """@fn on_tw_messages_size_allocate
            Callback para realizar un scrolleo de la ventana de mensages
            @param widget de quien recibe la sena;
            @param event evento que esta ocurriedno
        """
        adj = self.sw_messages.get_vadjustment()
        adj.set_value(adj.upper - adj.page_size)

    def on_tw_trames_size_allocate(self, widget, event, data=None):
        """@fn on_tw_trames_size_allocate
            Callback para realizar un scrolleo de la ventana de mensages
            @param widget de quien recibe la sena;
            @param event evento que esta ocurriedno
        """
        adj = self.sw_trames.get_vadjustment()
        adj.set_value(adj.upper - adj.page_size)

    def messages_add(self, data):
        """@fn messages_add
            Anade un nuevo mensage en la ventana de visualizacion
        """
        self.ls_messages.append(data)

    def trames_add(self, data):
        """@fn trames_add
            Anade una nueva trama en la ventana de visualizacion
        """
        self.ls_trames.append(data)

    def on_btn_close_clicked(self, *args):
        """@fn on_btn_close_clicked
            Cierra ventana de dialogo
        """
        #self.dialog.destroy()
        self.closed_callback()
