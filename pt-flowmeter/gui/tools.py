# -*- encoding: utf-8 -*-
'''
Emdyp.me
Revisado
Script: Tipo B
(heramientas de dibujo)
'''

import gtk
import cairo
import pango


class Tool(gtk.DrawingArea):
    """
         Clase  que  representa  a  Tool  y  se  encarga  de  realizar  un  grafico  de  la  profundidad
         relativa  de  los  sensores  que  se  encuentran  en  la  herramienta.
    """
    def __init__(self):
        """
            Constructor de la clase Tool
        """
        gtk.DrawingArea.__init__(self)
        self.connect("expose_event", self.expose)

        self.layout = self.create_pango_layout('')
        desc = pango.FontDescription("mono 10")
        desc.set_weight(pango.WEIGHT_BOLD)
        self.layout.set_font_description(desc)
        self.context = None
        self.set_size_request(400, 150)
        self.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse('#bbb'))
        self.devices = {}

    def set_devices(self, devices):
        """@fn set_devices
            Funcion que actualiza el diccionario de la posicion relativa de los sensores en la herramienta
            @param devices Diccionario con los parametros de la profundidad relativa
        """
        self.devices = devices
        self.queue_draw()

    def expose(self, widget, event):
        """@fn expose
            Callback para dibujada, cada vez que haya una peticion de redibujado
            @param widget De quien recibe la senal
            @param event evento que llega
        """
        self.context = widget.window.cairo_create()

        self.context.rectangle(event.area.x, event.area.y,
                event.area.width, event.area.height)
        self.context.clip()
        self.width = self.get_allocation().width
        self.height = self.get_allocation().height
        self.draw()
        self.draw_devices()

    def draw_devices(self):
        """@fn draw_devices
            Se encarga de dibujar las profundidades relativas en la herramienta
        """
        #esto podria ser mejor, para datos genericos
        y = 25
        x = 20
        for key, value in self.devices.items():
            if key == 'tool':
                self.context.set_source_rgb(0, 0, 0)
                self.context.move_to(20, 105)
                self.context.line_to(380, 105)
                self.context.stroke()
                self.layout.set_text(str(value))
                self.context.move_to(200 - (self.layout.get_pixel_size()[0] / 2), 110)
                self.context.show_layout(self.layout)
                continue
            self.context.set_source_rgb(*self.devices[key]['color'])
            self.context.move_to(x, y)
            y += 15
            if y > 40:
                y = 25
                x += 100
            self.layout.set_text(str(key) + ':' + str(self.devices[key]['value']))
            self.context.show_layout(self.layout)
            #x es la conversion de cada sensor para poderlo ubicar de acuerdo
            #al largo de la herramienta
            try:
                x_line = int((360.0 / self.devices['tool']) * self.devices[key]['value'])
            except ZeroDivisionError:
                x_line = int(360.0 * self.devices[key]['value'])
            self.context.move_to(380 - x_line, 60)
            self.context.line_to(380 - x_line, 90)
            self.context.stroke()

    def draw(self):
        """@fn draw
            Se encarga de dibujar una representacion de la herramienta
        """
        self.context.set_source_rgb(.3, .3, .3)
        self.context.move_to(20, 65)
        self.context.line_to(20, 85)
        self.context.rel_line_to(310, 0)
        self.context.rel_line_to(50, -10)
        self.context.rel_line_to(-50, -10)
        self.context.close_path()
        self.context.fill()

        self.context.set_source_rgb(.6, 0, .6)
        self.context.rectangle(377, 72, 6, 6)
        self.context.fill()

        self.layout.set_text('Zero')
        self.context.move_to(20, 10)
        self.context.show_layout(self.layout)