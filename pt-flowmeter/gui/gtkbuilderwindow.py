# -*- encoding: utf-8 -*-
'''
Emdyp.me
Revisado: Pedro Rivera
Script: Tipo B
'''
"""
Modulo Framework que tiene funciones básicas para el manejo de ventanas GUI
"""

from gettext import gettext
from gobject import GError
import pygtk
pygtk.require('2.0')
import gtk
from gtk import gdk
import pango


class GtkBuilderWindow(object):
    """
    Super Clase que implementa lo necesario para que se pueda hacer uso de
    interfaces Glade
    """
    builder_file = None
    #builder_file_lmd = None

    def __init__(self):
        """
        Constructor de la Super Clase que toma un fichero .glade y lo usa
        para construir la ventana
        """
        self._builder = gtk.Builder()
        #self._builderlmd = gtk.Builder()

        assert isinstance(self.builder_file, str), 'builder_file not specified'

        try:
            self._builder.add_from_file(self.builder_file)
            #self._builderlmd.add_from_file(self.builder_file_lmd)
        except GError:
            raise IOError('GtkBuilder file not found')

        root_window = self.__class__.__name__
        self._window = self._builder.get_object(root_window)

        if self._window is None:
            error = 'Class name ({0}) should match with object in file {1}'
            error = error.format(root_window, self.builder_file)
            raise NameError(error)

        self._builder.connect_signals(self)

    def __getattr__(self, name):
        """
        Método comodín para seleccionar atributos o widgets dentro del fichero
        .glade

        @param name: Nombre del widget o atributo
        @type name: String
        @return: El valor del atributo o el widget si se encuentra.
        """
        widget = self._builder.get_object(name)

        if widget is not None:
            return widget

        return getattr(self._window, name)

    def show_error(self, message):
        """
        Método para mostrar un Dialogo de Error que contiene un botón.

        @param message: Mensaje a mostrar
        @type message: String
        @return: Código de respuesta.
        """
        dialog = gtk.MessageDialog(self._window,
                                   gtk.DIALOG_MODAL |
                                   gtk.DIALOG_DESTROY_WITH_PARENT,
                                   gtk.MESSAGE_ERROR,
                                   gtk.BUTTONS_OK)

        dialog.set_title(gettext('Error'))
        dialog.set_markup(message)

        dialog.get_position()
        dialog.set_deletable(False)

        response = dialog.run()

        dialog.destroy()
        return response

    def show_question(self, message):
        """
        Método para mostrar un Dialogo de Pregunta que contiene dos botones.

        @param message: Mensaje a mostrar
        @type message: String
        @return: True o False, según responda el usuario.
        """
        dialog = gtk.MessageDialog(self._window,
                                   gtk.DIALOG_MODAL |
                                   gtk.DIALOG_DESTROY_WITH_PARENT,
                                   gtk.MESSAGE_QUESTION,
                                   gtk.BUTTONS_YES_NO)

        dialog.set_title(gettext('Question'))
        dialog.set_markup(message)

        dialog.get_position()
        dialog.set_deletable(False)
        #dialog.resize(500,200)

        response = dialog.run()
        dialog.destroy()
        result = False
        if response == -8:
            result = True
        return result

    def show_info(self, message):
        """
        Método para mostrar un Dialogo de Información que contiene un botón.

        @param message: Mensaje a mostrar
        @type message: String
        @return: Código de respuesta.
        """
        dialog = gtk.MessageDialog(self._window,
                                   gtk.DIALOG_MODAL |
                                   gtk.DIALOG_DESTROY_WITH_PARENT,
                                   gtk.MESSAGE_INFO,
                                   gtk.BUTTONS_OK)

        dialog.set_title(gettext('Information'))
        dialog.set_markup(message)

        dialog.get_position()
        dialog.set_deletable(False)

        response = dialog.run()
        dialog.destroy()
        return response

    def gtk_main_quit(self, *args):
        """
        Método que controla la salida segura a la hora de cerrar la ventana
        """
        gtk.main_quit()

    def on_delete_event(self, *args):
        """
        Método que controla la eliminación de un evento
        """
        self.destroy()

    def get_all_widgets(self):
        """
        Método que retorna todos los widgets del fichero .glade que conforman
        la ventana
        """
        return self._builder.get_objects()

    def get_real_widget(self):
        """
        Método que retorna la ventana Madre
        """
        return self._window
