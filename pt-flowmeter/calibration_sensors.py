# -*- encoding: utf-8 -*-
"""
Emdyp.me
Revisado: Pedro Rivera
Script: Tipo A
(calibracion de sensores, parte final)
"""
import math
import gtk


class Linear():
    """
         Clase  que  representa  una  calibracion  de  tipo  lineal
    """
    def __init__(self, m, b):
        """
            Constructor de la calse Linear (Y = m*X + b)
            @param m Pendiente de la ecuacion lineal
            @param b punto de corte en el eje Y de la ecuacion de la linea
        """
        self.m = m
        self.b = b

    def get_value(self, x):
        """@fn get_value
            Obtiene el valor de la aproximacion lineal
            @param x Valor que va ser aproximado
            @return Aproximacion Lineal
        """
        r = (self.m * x) + self.b
        return round(r, 2)


class DepthCalibration():
    """
         Clase que representa una calibracion basada en la cantidad de cuentas
         del encoder y un offset
    """
    def __init__(self, m, b):
        """
            Constructor de la calse DepthCalibration (Y = 1/CountsRev * OffsetFact)
            @param m Pendiente de la ecuacion lineal
            @param b punto de corte en el eje Y de la ecuacion de la linea
        """
        self.countsrev = m
        self.offset_fact = b
        self.value_cal = (1.0 / self.countsrev) * self.offset_fact

    def get_value(self, x):
        """@fn get_value
            Obtiene el valor de la profundidad basada en la cantidad de cuentas
            del encoder
            @param x Valor que va ser aproximado
            @return Aproximacion profundidad
        """
        r = x * self.value_cal
        return round(r, 8)


class Polynomial():
    """
         Clase  que  representa  una  calibracion  de  tipo  Polynomial
    """
    def __init__(self, a, b, c, d):
        """
            Constructor de la clase Polynomial (Y = a*X^3 + b*X^2 + c*X + d)
            @param a Parametro de orden 3
            @param b Parametro de orden 2
            @param c Parametro de orden 1
            @param d Parametro de ajuste
        """
        self.a = a
        self.b = b
        self.c = c
        self.d = d

    def get_value(self, x):
        """@fn get_value
            Obtiene el valor de la aproximacion polinomila de orden 3
            @param x Valor que va ser aproximado
            @return Aproximacion polinomial
        """
        r = (self.a * (x ** 3)) + (self.b * (x ** 2)) + (self.c * x) + self.d
        return round(r, 2)


class Exponential():
    """
         Clase  que  representa  una  calibracion  de  tipo  Exponencial
    """
    def __init__(self, a, b, c):
        """
            Constructor de la clase Exponential (Y = a*Exp(b*X) + c)
            @param a Parametro Exponencial
            @param b Parametro Exponencial
            @param c Parametro ajuste
        """
        self.a = a
        self.b = b
        self.c = c

    def get_value(self, x):
        """@fn get_value
            Obtiene el valor de la aproximacion Exponencial
            @param x Valor que va ser aproximado
            @return Aproximacion Exponencial
        """
        try:
            r = (self.a * (math.e ** (self.b * x))) + self.c
            return round(r, 2)
        except:
            return 10000


class Sinusoidal():
    """
         Clase  que  representa  una  calibracion  de  tipo  Senosoidal
    """
    def __init__(self, a, b, c, d):
        """
            Constructor de la clase Sinusoidal (Y = a*Sin(b*X + c) + d)
            @param a Parametro de Seno 1
            @param b Parametro de Seno 2
            @param c Parametro de Seno 3
            @param d Parametro de ajuste
        """
        self.a = a
        self.b = b
        self.c = c
        self.d = d

    def get_value(self, x):
        """@fn get_value
            Obtiene el valor de la aproximacion Senosoidal
            @param x Valor que va ser aproximado
            @return Aproximacion Senosoidal
        """
        r = (self.a * math.sin(((self.b * x) + self.c))) + self.d
        return round(r, 2)


class AVG():
    """
         Clase  que  representa  una  calibracion  de  tipo  promedio
    """
    def __init__(self, max_values):
        """
            Constructor de la clase AVG
            @param max_values especifica con cuantos valores se va realizar el promedio
        """
        self.max_values = float(max_values)
        self.values = []
        self.count = 0.0
        self.avg_value = 0

    def set_new_value(self, value):
        """@fn set_new_value
            Pone un nuevo valor para el calculo del promedio
            @param value Valor que va ser considerado para el promedio
        """
        self.values.append(value)
        self.count += 1
        self.avg_value = 0
        if self.count > self.max_values:
            self.values.pop(0)
            for i in self.values:
                self.avg_value += i
            self.avg_value /= self.max_values
        else:
            for i in self.values:
                self.avg_value += i
            self.avg_value /= self.count

    def get_value(self):
        """@fn get_value
            Obtiene el valor promedio
            @return Promedio
        """
        return round(int(self.avg_value), 2)
#   ------------------------------------------
#   RECEPCION DE DATOS DE LA INTERFAZ GRÁFICA
#        Se leen los datos del cuadro de dialogo de calibracion del sensor
# --------------------------------------------


def get_pt_sensors(options):
    """ Funcion para el servicio de Parameter Tool Service retorna el tipo de
        calibracion elegida por el usuario para los sensores.
        @param options Opciones de configuracione del usuario el cual es un
        diccionario
        @return Objetos de las clases Linear, Polynomial, Exponential,
        Sinusoidal y AVG
    """
    control = 'spinbuttons'
    calibrationSet = 'calibration_options'
    sensors = []
    sensorLabels = ['gr', 'ccl', 'temp1', 'temp2', 'pres',
                    'res1s', 'res2s', 'accx', 'accy', 'accz', 'ten', 'depth']
    for sensor in sensorLabels:
        if sensor == 'avggr':
            sensors.append(AVG(options[calibrationSet][control]['sb_pt_avggr']))
            l = Linear(1, 0)
        else:
            modelType = options['config_options']['comboboxes']['cb_pt_cal_'+ sensor +'_2'][0]
            if modelType == 'Linear':
                m = options[calibrationSet][control]['sb_pt_'+ sensor +'_lin_m']
                b = options[calibrationSet][control]['sb_pt_'+ sensor +'_lin_b']
                if sensor == 'depth':
                    l = DepthCalibration(m, b)
                else:
                    l = Linear(m, b)
            elif  modelType == 'Polynomial':
                a = options[calibrationSet][control]['sb_pt_' + sensor + '_pol_a']
                b = options[calibrationSet][control]['sb_pt_' + sensor + '_pol_b']
                c = options[calibrationSet][control]['sb_pt_' + sensor + '_pol_c']
                d = options[calibrationSet][control]['sb_pt_' + sensor + '_pol_d']
                l = Polynomial(a, b, c, d)
            elif  modelType == 'Exponential':
                a = options[calibrationSet][control]['sb_pt_' + sensor + '_exp_a']
                b = options[calibrationSet][control]['sb_pt_' + sensor + '_exp_b']
                c = options[calibrationSet][control]['sb_pt_' + sensor + '_exp_c']
                l = Exponential(a, b, c)
            elif  modelType == 'Sinusoidal':
                a = options[calibrationSet][control]['sb_pt_' + sensor + '_sin_a']
                b = options[calibrationSet][control]['sb_pt_' + sensor + '_sin_b']
                c = options[calibrationSet][control]['sb_pt_' + sensor + '_sin_c']
                d = options[calibrationSet][control]['sb_pt_' + sensor + '_sin_d']
                l = Sinusoidal(a, b, c, d)
        if sensor == 'ccl' and 'avggr' in sensorLabels:
            sensors[-1] = l
        else:
            sensors.append(l)
    return sensors


#testeo

#if __name__ == '__main__':
##    print "punto de revision"
#
#    e = Exponential((6.4128 * (10 ** -46)), 0.007667, 0)
#    print e.get_value(15504)
#
#    avg = AVG(5)
#    avg.set_new_value(6)
#    print avg.get_value()
#    avg.set_new_value(2)
#    print avg.get_value()
#    avg.set_new_value(1)
#    print avg.get_value()
#    avg.set_new_value(1)
#    print avg.get_value()
#    avg.set_new_value(10)
#    print avg.get_value()
#    avg.set_new_value(15)
#    print avg.get_value()
#    avg.set_new_value(15)
#    print avg.get_value()
#