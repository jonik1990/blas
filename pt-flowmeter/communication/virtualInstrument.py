# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 14:45:15 2015

@author: ging

contain classes:
- trameManager
- virtualInstrument

- acquisition mode manager
- tool_speed_counter
- moving_average_filter

- encoder

"""

import re
from random import randrange
import datetime
from math import fsum
from time import time, sleep
from copy import copy
from utilities import jpt_logger

###############################################################################
#  TRAME MANAGER CLASS
###############################################################################
class trameManager():
    """templates and regular expressions for comunication trames.
    this class have:
    - templates to send information to the tool
    - regular expression to recognize the type of trame the data belongs to
    - trame description to retrieve information from acquired data
    also have the methods identify and get data, the first one to recognize
    the type of trame in data or to check if a given data belongs to a specific
    trame, the second one is used extract information from given data

    attributes:
    - self.Trames
    - self.Regexp
    - self.ChecksumCalc

    methods:
    - self.identify
    - self.getData
    """
    def __init__(self):
        self.valid = '\x56'
        self.invalid = '\x49'
        #TRAMES
        #SENT
        setidTrame = ['\x06', 'ID', '\x00']
        getidTrame = ['\x07', '\x00', '\x00']
        samplerateTrame = ['\x09', 'SMPL', '\x00']
        slavesTrame = ['\x0a', 'SLVS', '\x00']
        # mode trames
        listenTrame = ['\x08', '\x01', 'ID']
        rollOverTrame = ['\x08', '\x02', '\x00']
        pinTrame = ['\x08', '\x03', 'ID']
        #RECIVED
        idRespTrame = ['\xd5', 'VCODE', 'ID', '\xaa']
#        getidRespTrame = ['\xd5', 'VCODE', 'ID', '\xaa']
        samplerateRespTrame = ['\xd5', 'VCODE', '\x09', '\xaa']
        slavesRespTrame = ['\xd5', 'VCODE', '\x0a', '\xaa']
        modeRespTrame = ['\xd5', 'VCODE', '\x08', '\xaa']
        # pin trames
        pinvalidTrame = ['\xd5', self.valid, '\x08', '\xaa']
        upholeTrame = [
            '\x03', 'ENCOH', 'ENCOL', 'DIR', 'TENH', 'TENL', 'CHKSMU'
            ]
        dataTrame = [
            '\xa5', '\xa5', 'ID', '\x00',
            'TEMPH', 'TEMPL', 'PRESH', 'PRESL',
            'XH', 'XL', 'YH', 'YL', 'ZH', 'ZL', 'CHKSMP'
            ]
        respPinTrame = dataTrame + upholeTrame

        #REGULAR EXPRESSIONS
        #SENT
        setidExp = '\x06[\x00-\xff]\x00'
        getidExp = '\x06\x00\x00'
        samplerateExp = '\x09[\x01-\x03]\x00'
        slaveExp = '\x0a[\x01-\x03]\x00'
        # mode trames
        listenExp = '\x08\x01[\x00-\xff]'
        rollOverExp = '\x08\x02\x00'
        pinExp = '\x08\x03[\x00-\xff]'
        #RECIVED
        idRespExp = '\xd5[\x56\x49][\x00-\xff]\xaa'
#        getidRespExp = '\xd5[\x56\x49][\x00-\xff]\xaa'
        samplerateRespExp = '\xd5[\x56\x49]\x09\xaa'
        slaveRespExp = '\xd5[\x56\x49]\x0a\xaa'
        modeRespExp = '\xd5[\x56\x49]\x08\xaa'
        # pin trames
        pinvalidExp = '\xd5\x56\x08\xaa'
        dataExp = '\xa5\xa5[\x00-\xff]\x00[\x00-\xff]{11}'
        upholeExp = '\x03.{2}[\x02\x01\x00].{3}'
        pin_respExp = dataExp + upholeExp

        #BUILD TRAMES
        self.Trames = {
            'setid': setidTrame, 'getid': getidTrame,
            'idResp': idRespTrame,
            'samplerate': samplerateTrame,
            'samplerateResp': samplerateRespTrame,
            'slaves': slavesTrame, 'slavesResp': slavesRespTrame,
            'listen': listenTrame, 'rollOver': rollOverTrame,
            'pin': pinTrame,
            'moderesp': modeRespTrame, 'pinvalid': pinvalidTrame,
            'data': dataTrame,
            'uphole': upholeTrame, 'pin_resp': respPinTrame
            }
        #BUILD REGEXP
        expr = {
            'setid': setidExp, 'getid': getidExp,
            'idResp': idRespExp,
            'samplerate': samplerateExp,
            'samplerateResp': samplerateRespExp,
            'slaves': slaveExp, 'slavesResp': slaveRespExp,
            'listen': listenExp, 'rollOver': rollOverExp,
            'pin': pinExp,
            'moderesp': modeRespExp, 'pinvalid': pinvalidExp,
            'data': dataExp,
            'uphole': upholeExp, 'pin_resp': pin_respExp}
        self.Regexp = dict()
        for index in expr:
            self.Regexp[index] = re.compile(expr[index])
        #BUILD TEMPLATES
        self.Templates = dict()
        for trame in self.Trames:
            self.Templates[trame] = ''
            for item in self.Trames[trame]:
                if len(item) > 1:
                    self.Templates[trame] += '{:}'
                else:
                    self.Templates[trame] += item
        #GUIDE FOR CHECKSUM CALCULATION
        self.ChecksumCalc = dict()
        self.ChecksumCalc = {
            'pin_resp': {
                'CHKSMP': [
                    'TEMPH', 'TEMPL', 'PRESH', 'PRESL',
                    'XH', 'XL', 'YH', 'YL', 'ZH', 'ZL'
                    ],
                'CHKSMU': ['ENCOH', 'ENCOL', 'DIR', 'TENH', 'TENL']
                },
            'data': {
                'CHKSMP': [
                    'TEMPH', 'TEMPL', 'PRESH', 'PRESL',
                    'XH', 'XL', 'YH', 'YL', 'ZH', 'ZL']
                },
            'uphole': {
                'CHKSMU': ['ENCOH', 'ENCOL', 'DIR', 'TENH', 'TENL']
                }
            }

    def identify(self, string, **kwargs):
        """check if a string belongs to a certain kind of trame and return
           the type of trame, if use isTrame='trameName', returns True
           if belongs to given trame, otherwise returns false"""
        if 'isTrame' in kwargs:
            trameType = kwargs['isTrame']
            matches = self.Regexp[trameType].findall(string)
            if len(matches) > 0:
                return True
            else:
                return False
        else:
            stringType = None
            #check with regular expresions to find out trype of trame
            for trame in self.Regexp:
                if self.Regexp[trame].match(string):
                    stringType = trame
                    break
            #return type if recognized, failed if dont
            if stringType is None:
                return 'failed'
            else:
                return stringType

    def getData(self, string, **kwargs):
        """get data in a dictionary from a given string, as argument can be
           used trameType=something to select an specific trame type, also can
           be used rep=ascci if you want a string object as response, otherwise
           use rep=hex to get a numer, by default you'll get a string object"""
        #data representation of response
        if 'rep' in kwargs:
            rep = kwargs['rep']
        else:
            rep = 'ascci'
        #identify string
        if 'trameType' in kwargs:
            stringType = kwargs['trameType']
        else:
            stringType = self.identify(string)
        #clean string
        string = self.Regexp[stringType].findall(string)[-1]
        if stringType == 'failed':
            return {}
        else:
            data = dict()
            trame = self.Trames[stringType]
            tramelength = len(trame)
            for item in range(tramelength):
                if len(trame[item]) > 1:
                    if rep == 'hex':
                        data[trame[item]] = ord(string[item])
                    else:
                        data[trame[item]] = string[item]
            return data

###############################################################################
#  VIRTUALINSTRUMENT CLASS
###############################################################################
class virtualInstrument():
    """simulation of an instrument, needs and id given as a number in string
    format"""
    def __init__(self, idstr):
        self.id = idstr
        self.id_hex = chr(int(self.id))
        self.trames = trameManager()

    def genData(self):
        """return ascci random data"""
        return chr(randrange(0, 200, 1))

    def answer(self, request):
        """answer sent request from serial module of blas"""
        sleep(0.05)
        #detect request
        reqType = self.trames.identify(request)
        #answer request
        if reqType == 'failed':
            resp = self.trames.Templates['invalidTr'].format(self.id_hex)
        elif reqType == 'pin':
            resp = self.trames.Templates['pin_resp'].format(
                self.id_hex,
                self.genData(),
                self.genData(),
                self.genData(),
                self.genData(),
                self.genData(),
                self.genData(),
                self.genData(),
                self.genData(),
                self.genData(),
                self.genData(),
                self.genData(),
                self.genData(),
                self.genData(),
                self.genData(),
                '\x01',
                self.genData(),
                self.genData(),
                self.genData(),
                )
        elif reqType == 'getid':
            resp = self.trames.Templates['validTr'].format(self.id_hex)
        else:
            resp = self.trames.Templates['invalidTr'].format(self.id_hex)
        return resp

###############################################################################
#  ACQUISITION MODE MANAGER CLASS
###############################################################################
class acquisition_mode_manager():
    """manage the outcome of data stream based on the acquisition mode
    this mode can be time drive, depth drive up or depth drive down"""

    def __init__(self, mode, save_function, sample_filter):
        """builder of acquisition_mode_manager class, it needs the mode of
        data acquisition that can be "time_drive", "depth_drive_up",
        "depth_drive_down", a save_function, the object of the function
        that will be used to save data and a sample_filter that is a min time
        between records of data"""
        self.lg = jpt_logger(name='acq_manager', mode='quiet')
        self.log = self.lg.cursor
        self.lg.disable()
        #detect daq mode
        if 'depth' in mode:
            self.mode = 'depth'
        elif 'time' in mode:
            self.mode = 'time'
        else:
            self.log.warning('no valid mode, taking time drive as default mode')
            self.mode = 'time'
        self.down = 'down' in mode
        self.save_func = save_function
        self.samp_filt = sample_filter
        #control variables
        self.count = 0
        self.current_acq_time = time()
        self.last_acq_time = time()
        self.head = None
        self.saved_data = False

    def update(self, current_depth, last_depth, data, sensors, current_time):
        """updates the acquisition_mode_manager with the depth measure in order
        to determine if the data is saved or ignored"""
        self.log.debug(
            'mode: %s, save: %s',
            self.mode,
            self.saved_data
            )
        self.log.debug(
            'data: %s, sensors: %s',
            data,
            sensors
            )
        self.log.debug(
            'depth: last_%s current_%s',
            last_depth,
            current_depth
            )
        self.log.debug(
            'time: last_%s current_%s',
            self.last_acq_time,
            current_time
            )

        #initialize head, is the last acquired valid value
        if not self.head:
            self.head = copy(current_depth)
        self.saved_data = False
        #sample filter
        self.current_acq_time = time()
        delta = self.last_acq_time - self.current_acq_time
        if delta > self.samp_filt:
            save = True
            self.last_acq_time = self.current_acq_time
        #save data depends on mode
        #for debugging save = True
        save = True
        increase = current_depth > last_depth
        decrease = current_depth < last_depth
        #copy sensors into data
        #data = copy(sensors)  # this one is dangerous, is a bugfix
        if self.mode == 'depth':
            #check if is a new valid data
            if self.down:
                new_valid = current_depth > self.head
            else:
                new_valid = current_depth < self.head
            self.log.debug(
                'new depth data is valid: %s',
                new_valid
                )
            #organize data of depth drive
            #depth goes in data[0] and time goes on data[4]
            #save depth on first column
            data[0] = round(current_depth, 2)
            #save time on fitht column
            data[4] = copy(current_time)
            sensors[0] = copy(current_depth)
            self.log.debug(
                'data: %s, sensors: %s',
                data,
                sensors
                )
            self.log.debug(
                'depth: %s, increase: %s, save: %s',
                current_depth,
                increase,
                save
                )
            if self.down and increase and save and new_valid:
                self.log.debug('depth down save %s', current_depth)
                self.save_func()
                self.saved_data = True
                self.head = copy(current_depth)
            elif not self.down and decrease and save and new_valid:
                self.save_func()
                self.saved_data = True
                self.log.debug('depth up save %s', current_depth)
                self.head = copy(current_depth)
        elif self.mode == 'time' and save:
            #organize data of time drive
            #time goes in data[0] and encoder goes on data[4]
            #save depth on first column
            data[0] = round(current_time, 2)
            #save time on fitht column
            data[4] = current_depth
            self.save_func()
            self.saved_data = True

    def stop(self):
        """stop log"""
        self.logfile.close()

###############################################################################
#  UNITSCONVERTER CLASS
###############################################################################
class unitsConverter():
    """converter into engineering units"""
    numeratorExp = re.compile('.+/')
    denominarorExp = re.compile('/.+')

    def unitsParser(self, units):
        """parse units in a string"""
        num = self.numeratorExp.findall(units)[0][0:-1]
        den = self.denominarorExp.findall(units)[0][1:]
        return num, den


###############################################################################
#  TOOL_SPEED_COUNTER CLASS
###############################################################################
class tool_speed_counter():
    """counter that counts the speed of a tool"""
    def __init__(self, init_time=0, init_long=0, source_units='ft/s'):
        """builder of the class"""
        #logging captabilities
        self.lg = jpt_logger(name='tsc', mode='only_file')
        self.log = self.lg.cursor

        self.lastTime = init_time
        self.lastLong = init_long
        self.converter = unitsConverter()
        self.longUnit, self.timeUnit = self.converter.unitsParser(source_units)

    def update(self, time, data, mode='abs', units='ft/min'):
        """update the counter and returns the speed of the tool, it has
        two modes of operation: abs when the time given is absolute and diff
        when the time given is a delta between updates"""
        self.log.info('time: %s, data: %s', time, data)
        #get delta time
        if mode == 'diff':
            deltaTime = float(time)
        else:
            deltaTime = float(time) - self.lastTime
        #calculate speed
        deltaLong = data - self.lastLong
        if deltaTime == 0:
            speed = 0
        else:
            speed = deltaLong / deltaTime
            self.log.info(
                'raw_speed: %s, deltaLong: %s, deltaTime: %s',
                speed,
                deltaLong,
                deltaTime
                )
        #if mode abs, save this time to be the next last
        if mode == 'abs':
            self.lastTime = time
        self.lastLong = data
        #put speed in needed engineering units
        lng, tm = self.converter.unitsParser(units)
        if tm == 'min' and self.timeUnit == 's':
            speed = speed * 60.0
        return speed


class moving_average_filter():
    """creates a moving average filter"""
    def __init__(self, moves=3):
        """build moving_average_filter, needs the quantity of moves behind
        to take average, important: DONT USE EVEN NUMBERS, studies of this
        kind of filter denotes even number as a moves behind leads to
        unstable behaviour, perfect values are 3 or 5"""
        self.present_value = 0
        self.moves = moves
        self.steps = [0 for i in range(moves)]
        self.count = 0

    def update(self, acq):
        """update filter with a data acquisition and it returns the value
        filtered"""
        #if count = 0 make all steps take initial value
        if self.count == 0:
            self.steps = [acq for i in range(self.moves)]
            self.present_value = acq
        #if count > 0 update steps buffer
        elif self.count > 0:
            for i in range(self.moves - 1, 0, -1):
                self.steps[i] = self.steps[i - 1]
            self.steps[0] = acq
        #update counter
        self.count += 1
        #calculate average
        self.present_value = fsum(self.steps) / len(self.steps)
        return self.present_value


###############################################################################
#  ENCODER CLASS
###############################################################################
class encoder():
    """encoder instrument
    physical_configs={
        perimeter=number,
        counts_per_revolution=number,
        correction_factor=number,
        }
    refers to physical configurations

    reversal_config=False
    refers to a change of conventions on A-B connections on the encoder
    affects delta calculation on multiplication
    """
    def __init__(self, initial_depth=0,
                 initial_encoder=0,
                 overflowing=655535,
                 calibration_function=None,
                 depth_filter=None,
                 ignore='dir',
                 physical_configs={},
                 reversal_convention=False,
                 enable_filter=False
                 ):
        #decide if ignore depth or dir where they're opposite
        self.ignore = ignore
        #log events in encoder
        self.lg = jpt_logger(name='encoder')
        self.log = self.lg.cursor
        #encoder physical_configs
        self.physical_configs = physical_configs

        #depth of encoder
        self.current = initial_encoder
        self.last = initial_encoder
        self.absolute = initial_depth
        self.lastAbsolute = initial_depth
        #direction magement(True if incremental, False if decremental)
        self.current_direction = True
        self.last_direction = True
        self.buffer_direction = [True] * 8
        #encoder overflowing
        self.overflowing = overflowing
        self.max_delta = overflowing / 2
        #calibration function
        self.cal_function = calibration_function
        #control variables
        self.count = 0
        self.delta = 0
        self.lastDelta = 0
        #moving average filter
        self.enable_filter = enable_filter
        self.m_a = moving_average_filter(moves=3)
        #log initial conditions of encoder
        self.log.debug(
            'initial_encoder_value: %s, overflowing: %s, max_delta: %s',
            initial_encoder,
            self.overflowing,
            self.max_delta
            )
        self.reversal_convention = reversal_convention

    def update_simple(self, measured, direction, toogle_spoler=False):
        """simple way of calculation"""

        self.log.info(
            'measured: %s, direction: %s, toogle_spoler: %s',
            measured,
            direction,
            toogle_spoler
            )

        toogle_spoler = self.physical_configs['toogle']
        counts_per_revolution = self.physical_configs['counts_per_rev']
        perimeter = self.physical_configs['perimeter']
        correction_factor = self.physical_configs['depth_factor']
        depth_offset = self.physical_configs['depth_offset']
        cal = perimeter / (2 * counts_per_revolution)
        #set initial condition on first data acquisition
        if self.count == 0:
            self.current = measured
            self.last = measured
            self.count += 1
            return self.absolute
        #------------------------
        #calculate delta
        #------------------------
        self.current = float(measured)
        #calculate raw delta
        self.delta = self.current - self.last

        self.log.info(
            'current: %s, last: %s, raw_delta: %s',
            self.current,
            self.last,
            self.delta
            )

        #compensation for overflowing pass
        if abs(self.delta) > self.max_delta:
            d0 = abs(self.delta)
            d1 = self.overflowing - d0
            self.log.warning(
                'd0: %s, d1: %s',
                d0,
                d1
                )
            if d1 < 0:
                raise Exception('overflowing transpassed, panel error')
            self.delta = min(d0, d1)
            if self.current > self.max_delta:
                self.log.warning('fliping delta %s', self.delta)
                self.delta *= -1

        #scale delta with pysical settings
        self.delta = (self.delta * cal * correction_factor) + depth_offset

        #if toogle spooler, change sign
        if toogle_spoler:
            self.delta *= -1
        #if filtering enabled, send delta to moving average filter
        if self.enable_filter:
            self.m_a.update(self.delta)

        self.log.info(
            'fixed delta: %s',
            self.delta
            )

        #------------------------
        #calculate absolute depth
        #------------------------
        self.absolute = self.delta + self.lastAbsolute
        self.lastAbsolute = copy(self.absolute)
        self.last = copy(self.current)
        self.count += 1

        self.log.info(
            'final absolute depth: %s',
            self.absolute
            )

        return self.absolute

    def simulate(self, timearray, mode='increase'):
        """simulate data acquired for a enconder offline, returns the time
        and data"""
        pass

    def getDelta(self):
        """return the current delta of the encoder"""
        return self.delta
