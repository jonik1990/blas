# -*- encoding: utf-8 -*-
"""
Emdyp.me
Revisado
Script: Tipo A
(estructura y manejo de datos)

# 1. CONNECTION MANAGEMENT
methods that manages serial connection, the methods are connect,
onlydisconnect, onlyreconnect, disconnect, reconnect and fix_connection

# 2. DATA PROCESSING
methods that process data for internal purposes, methods in this set are
decode_string, calibrate_sensor, checksum, list_to_hex
set_values_prom and get_values_prom

# 3. EXCECUTION MANAGEMENT
methods that manages comunication excecution, methods in this set are
play, pause, stop, record, read_data, write_data and run

# 4. TOOL SETTINGS
methods used to comunicate with bspt to set configurations, these methods are
sync_st, sync_sf, sync_comp, sync_gn, change_reference, change_filter and
change_up_down

# AUXILIAR METHODS
methods that doesnt belong to the class but are used in it, these are
get_suitable_ports and isValidTrame

"""
import gc
from time import sleep, time
from copy import copy


import config
from .virtualInstrument import trameManager, virtualInstrument
#from calibration_sensors import get_pt_sensors, Exponential
from calibration_sensors import get_pt_sensors
from .connectionManagement import ConnectionManager
#from connectionManagement import get_suitable_ports, isValidTrame
from .virtualInstrument import tool_speed_counter, acquisition_mode_manager
from .virtualInstrument import encoder
from utilities import jpt_logger


class DavidConnection(ConnectionManager):
    """
         Clase  que  representa  una DavidConnection  y  ejecucion  de
         decodificacion  de  datos  ademas  de  la  memoria
    """
    trames = trameManager()

    def __init__(self, kind=config.TRACKING_BY_TIME, reading_delay=0.5,
                 callback=None, show_error=None, sample_filter=1,
                 memory_off=None, global_buffer={},
                 memory_on=None, get_avggr=False, show_messages=None,
                 show_trames=None, name="Serial_thread", ecptConfigs=[]):
        """
            Constructor de la clase
            @param kind Para indicar si se va graficar contra tiempo o
            profundidad (INT)
            @param reading_delay Para indicar el retardo de lectura
            serial (FLOAT)
            @param callback Callback (Funcion recibida de MainWindow) que
            envia los datos a otros widgets para ser graficados
            (callback Ver Mainwindow.py)
            @param show_error Para mostrar los mensajes de errores
            (gtkbuilderwindow.show_error)
            @param filter Especifica un submuestreo de los datos (FLOAT)
            @param memory_off Datos de memoria deshablitada (BOOLEAN)
            @param memory_on Datos de memoria habilitada (BOOLEAN)
            @param get_avggr Para habilitar el calculo del promedio de las
            cuentas de Gamma Ray, (BOOLEAN)
            @param show_messages Para mostrar mensajes de sincronizacion,
            Checksum y de Tramas (callback Ver MainWindow.py)
            @param show_trames Para mostrar las tramas que estan entrando
            (callback Ver MainWindow.py)
        """
        super(DavidConnection, self).__init__()
        #create loggers for debugging
        self.lg = jpt_logger(name='davidConnection', mode='only_file')
        self.log = self.lg.cursor
        self.lg.disable()

        self.rw = jpt_logger(name='raw', mode='only_file')
        self.raw = self.rw.cursor

        self.br = jpt_logger(name='brute', mode='only_file')
        self.brute = self.br.cursor

        self.name = name
        self.kind = kind
        self.reading_delay = reading_delay
        self.callback = callback
        self.show_error = show_error
        self.idletime = None
        self.database = None
        self.database_memory = None
        self.databaseUp = None
        #filter to know how many seconds between daq for reading
        self.sample_filter = round(sample_filter, 2)

        self.memory_off = memory_off
        self.memory_on = memory_on

        self.get_avggr = get_avggr

        self.show_messages = show_messages
        self.show_trames = show_trames

        self.memory_data_incoming = False

        self.init_time = 0
        self.init_time2 = 0

        self.data_acum = []

        if self.get_avggr:
            self.t_sensors = [0] * 16
            self.t_sensors2 = [0] * 16
            self.normal_trame = [0] * 16
        else:
            self.t_sensors = [0] * 15
            self.t_sensors2 = [0] * 15
            self.normal_trame = [0] * 15

        self._time = 0

        self.playing = False
        self.connection = None
        self.quit = False

        self.sync_attempts = 0
        self.max_sync_attempts = 100

        self.data_error = -1

        self.gr = None
        self.ccl = None
        self.temp1 = None
        self.temp2 = None
        self.pres = None
        self.res1s = None
        self.res2s = None
        self.accx = None
        self.accy = None
        self.accz = None
        self.ten = None
        self.depth = None
        self.avggr = None

        self.nodata = False
        self.Pause = False
        self.depth_absolute = 0.000
        self.update_reference = False
        self.depthtimefilter = 0.000
        self.updatefilter = False
        self.Down = True
        self.updateDown = False
        #configurations for ECPT Service
        self.ecpt = ecptConfigs
        #global buffer for backwards compatibility
        self.global_buffer = global_buffer
        #buffer for internal stuff
        self.internal_buffer = {
            'last_data': {
                'ZL': 0, 'YH': 0, 'XH': 0, 'PRESH': 0, 'ZH': 0,
                'YL': 0, 'XL': 0, 'PRESL': 0, 'TEMPH': 0,
                'TENH': 0, 'TEMPL': 0, 'CHKSMP': 0, 'ENCOH': 0,
                'CHKSMU': 0, 'ENCOL': 0, 'ID': 1, 'DIR': 0, 'TENL': 0
                }
            }

#==============================================================================
# 2. DATA PROCESSING
#==============================================================================

    def decode_string(self, string):
        """@fn decode_string
            Convierte una cadena de strings en su representacion numerica
            entera, str a int
            @param string Cadena de caracteres que va ser convertido (STRING)
            @return Lista de numeros representantes de los caracteres
            (LIST[INT])
        """
        result = []
        for c in string:
            result.append(ord(c))
        return result

    def calibrate_sensors(self, options):
        """@fn calibrate_sensors
            Crea los objetos para los sensores de Gamma Ray, CCL, profundidad,
            TEN, Gamma Ray Promedio, Presion, Temperatura, Aceleracion segun el
            metodo de calibarcion elegido.
            Los metodos de calibracion son Lineal, Polinomial, Exponencial,
            Promedio y Senosoidal
            @param options Diccionario con las configuraciones del usuario
            (Diccionario)
            @return Objectos de las clases  Linear,  Polynomial,  Exponetial,
            Sinusoidal and  AVG (Ver  calibrations_sensors)
        """
        (self.gr, self.ccl, self.temp1, self.temp2,
         self.pres, self.res1s, self.res2s, self.accx, self.accy,
         self.accz, self.ten, self.depth) = get_pt_sensors(options)

    def checksum(self, chk):
        """@fn checksum
            Verifica el Checksum de las tramas que estan llegando via puerto
            serial
            @param chk Lista de valores que se va comprobar su cheksum
            @return TRUE si el  checksum es valido, de lo contrario FALSE
        """
        suma = 0
        for i in chk:
            suma += i
        _chk = hex(suma)
        # check if number is 0 or 256
        return _chk[-1] == '0' and (_chk[-2] == '0' or _chk[-2] == 'x')

    def list_to_hex(self, numList):
        """@fn list_to_hex
            Convierte dos bytes en un valor entero, es decir, une dos bytes y
            los convierte en uno
            @param lista Lista de enteros de tamanio maximo 2 [LIST[INT]]
            @return Valor convertido(FLOAT)
        """
        if len(numList) >= 2:
            return float((numList[0] * 256) + numList[1])
        else:
            return float(numList[0])

    def set_values_prom(self, data):
        """@fn set_values_prom
            Se encarga de ubicar una lista de datos en una lista acumuladora
            para poder hayar el promedio
            @param data Lista de datos que va ser almacenada para calcular el
            promedio
        """
        data2 = copy(data)
        self.data_acum.append(data2)

    def get_values_prom(self):
        """@fn get_values_prom
            Se encarga de calcular el valor promedio de la lista acumuladora
        """
        data_prom = []
        len_data = len(self.data_acum)
        if len_data == 0:
            len_data = 1.0
        sumList = []
        lenList = []
        # build a list for summed data and a list for number of data summed
        for item in range(16):
            sumList.append(0.0)
            lenList.append(len_data)
        # sum all data and get number of summed numbers
        for data in self.data_acum:
            for var in range(len(sumList)):
                if data[var] != -1:
                    sumList[var] += data[var]
                else:
                    lenList[var] -= 1.0
                    if lenList[var] <= 0:
                        lenList[var] = 1.0
        # calculate media and append to data_prom
        for var in range(len(sumList)):
            data_prom.append(round((sumList[var]) / float(lenList[var]), 2))
        # reset data_acum variable
        self.data_acum = []
        return data_prom

    def save_data(self):
        """save data on jdb file"""
        self.database(self.t_sensors)

#==============================================================================
# 3. EXCECUTION MANAGEMENT
#==============================================================================

    def PinAcqisition(self, tools):
        """
        data acquisition in pin mode
        """
        # acquisition main loop
        #START COMUNICATION
        data = {}
        while not self.quit:
            # if connection keeps alive
            connection_exists = self.connection is not None
            connection_open = self.connection.isOpen()
            if connection_exists and connection_open and self.playing:
                acquisition_rate = self.ecpt['samplerate']
                acq_rate = acquisition_rate - 2 * (self.toolRespTime + 0.0017)
                #MODULE MAIN LOOP
                start_time = time()
                data = copy(self.internal_buffer['last_data'])

                #READ ALL TOOLS
                for tool in tools:
                    self.log.info(
                        'acq_rate: %s, acquisition_rate: %s, resp_time: %s, %s',
                        acq_rate,
                        acquisition_rate,
                        self.toolRespTime,
                        tool
                        )

                    #ASK TOOL STATE and READ TOOL ANSWER
                    acquired = self.pin(tool)
                    #update sent order to tool
                    self.global_buffer['telemetry']['sent'] += 1
                    #log brute response of tool
                    self.brute.info([acquired])
                    #if good response, extract data and show it on gui
                    for t in ['pin_resp', 'uphole']:
                        self.nodata = True
                        if self.trames.identify(acquired, isTrame=t):
                            print 'is {:}'.format(t)
                            data = self.trames.getData(
                                acquired,
                                trameType=t,
                                rep='hex'
                                )

                            if check_data(data):
                                self.serveData(data, t)
                                self.internal_buffer['last_data'] = copy(data)

                            self.nodata = False
                            break
                    #check how to wait to call panel
                    elapsed = time() - start_time
                    if 0.27 - elapsed > 0:
                        sleep(0.27 - elapsed)
                    else:
                        sleep(0.1)

                    #ASK PANEL and READ UPHOLE SHORT DATA
                    panel_raw, status = self.request_panel()
                    self.brute.debug([panel_raw])
                    self.log.debug(
                        'encuesta panel: %s, status %s',
                        [panel_raw],
                        status
                        )
                    try:
                        if self.trames.identify(panel_raw, isTrame='uphole'):
                            panel_data = self.trames.getData(
                                panel_raw,
                                trameType='uphole',
                                rep='hex'
                                )
                            for d in panel_data:
                                data[d] = panel_data[d]
                        self.log.info(
                            'nueva info de panel: %s',
                            data
                            )

                        if check_data(data):
                            self.last_data = data
                            self.serveData(data, 'pin_resp')

                    except Exception as e:
                        print e

                    #check how to wait to start again with loop
                    self.log.warning(
                        'first: %s, second: %s',
                        elapsed,
                        time() - start_time
                        )

                    elapsed = time() - start_time
                    if elapsed < 0.5:
                        sleep(0.5 - elapsed)
                    else:
                        sleep(0.1)

    def RollOverAcqisition(self, tool):
        """
        data acquisition in roll over mode
        """
        waitTime = 0.125
        #send to the tool order for roll over function
        order = self.trames.Templates['rollOver']
        self.log.debug(self.trames.Templates['rollOver'])
        while True:
            try:
                self.connection.flush()
                self.connection.write(order)
                self.log.debug('mande la orden rollover')
                sleep(waitTime)
                break
#                response = self.connection.read(self.connection.inWaiting())
            except:
                break
        #start roll over data acquisition
        cycles = 0
        raw_data = ''
        header = self.trames.Trames['data'][0]
        self.log.debug('starting rollover loop')
        while True:
            #check if cycle must continue
            if self.quit:
                break
            elif self.connection.isOpen() and self.playing:
                self.log.debug('connection is open')
                #read information in port
#                raw = self.connection.read(self.connection.inWaiting())
                raw = self.connection.read(100)  # OJO
                self.log.debug(raw)
                if cycles == 0:
                    raw_data = raw
                    cycles += 1
                else:
                    raw_data += raw
            #check if good data in information
                if raw_data[1] == header and cycles == 0:
                    raw_data = header + raw_data
            #parse and pre-process raw_data
                raw_data = raw_data[raw_data.index(header * 2):]
                packages = raw_data.split(header * 2)
                packages.remove('')
                data = []
                for i in packages:
                    i = header * 2 + i
            #if good info found, extract data, save it and serve it
                    if self.trames.identify(i, isTrame='pin_resp'):
                        data.append(self.trames.getData(i, trameType='pin_resp',
                                                        rep='hex'))
                        self.serveData(data[-1], 'pin_resp')
            #if good info not found, continue
                    else:
                        self.log.debug('invalid data package')
                        if i == packages[-1]:
                            raw_data = i
            #wait some time
                sleep(waitTime)
            else:
                sleep(0.125)
                continue

    def ListenAcqisition(self, tools):
        """
        data acquisition in pin mode
        """
        #not implemented, for now works the same as pin mode
        self.PinAcqisition(tools)

    def run(self):
        """@fn run
            Hilo encargado de la adquisicion de datos, decodificacion entre
            otras cosas en el modulo de comunicacion
        """
        # auxiliar variables
        if not self.kind == config.TRACKING_BY_TIME:
            self.t_sensors[0] = 32640
        self.pause_time = 0
        self.init_depth = 32640
        self.current_depth = -1
        self.depthabsolute = 0
        self.depthabsolute_ant = 0
        self.depthabsolute_filter = 0
        self.contdir = 0
        self.down = True
        self.lastdepth = 0
        self.direccion = 0
        self.delta = 0
        #encoder configurations
        acq_encoder = -1
        last_encoder = 0
        #create tool speed counter object
        tsc = tool_speed_counter()
        #acquisition manager to determine if save or not based on mode
        acq_mode = self.global_buffer['test_mode']

        acq_manager = acquisition_mode_manager(
            acq_mode,
            self.save_data,
            self.sample_filter
            )
        #max range of encoder data
        overflowing = 65536
        #create encoder for time or depth drive
        enc = encoder(
            initial_depth=self.global_buffer['depth_reference'],
            initial_encoder=0,
            overflowing=overflowing,
            calibration_function=self.depth.get_value,
            ignore='dir',
            physical_configs=self.global_buffer['encoder']
            )
        self.vi = {
            'enc': enc,
            'acq_manager': acq_manager,
            'overflowing': overflowing,
            'tsc': tsc,
            'last_encoder': last_encoder,
            'acq_encoder': acq_encoder
            }
        self.internal_buffer = {
            'last_data': {
                'ZL': 0, 'YH': 0, 'XH': 0, 'PRESH': 0, 'ZH': 0,
                'YL': 0, 'XL': 0, 'PRESL': 0, 'TEMPH': 0,
                'TENH': 0, 'TEMPL': 0, 'CHKSMP': 0, 'ENCOH': 0,
                'CHKSMU': 0, 'ENCOL': 0, 'ID': 1, 'DIR': 0, 'TENL': 0
                }
            }
        #configuration for ecpt service
        tools = []
        #PIN MODE SPECIAL CONFIGS
        self.toolRespTime = 0.1233
        for i in self.ecpt['name']:
            tools.append(virtualInstrument(str(i)))
        #detect the software acquisition mode of ecpt service
        self.log.debug(
            'daq mode: %s, kind: %s',
            self.ecpt['mode'],
            self.kind
            )
        if self.ecpt['mode'] == 'Roll Over':
            #start roll over mode
            self.RollOverAcqisition(tools)
        elif self.ecpt['mode'] == 'Pin':
            #start pin mode
            self.PinAcqisition(tools)
        elif self.ecpt['mode'] == 'Listen':
            #start listen mode
            self.ListenAcqisition(tools)

    def serveData(self, data, trameType):
        """save data in t_sensors so all functions can access measured data"""
        self.raw.info(data)
        self.log.debug(
            'serveData input: %s',
            data
            )
        #AUXILIAR VARIABLES

        #GET ITEMS FOR CHECKSUM CALCULATION
        if trameType == 'pin_resp':
            #PIN_RESP
            firstPack = self.trames.ChecksumCalc['pin_resp']
        elif trameType == 'uphole':
            #UPHOLE
            firstPack = self.trames.ChecksumCalc['uphole']
        elif trameType == 'data':
            #DATA
            firstPack = self.trames.ChecksumCalc['data']
        #GET CHECKSUM VALIDATIONS
        validChksm = dict()
        for item in firstPack:
            numberList = [data[item]]
            for i in firstPack[item]:
                numberList.append(data[i])
            validChksm[item] = self.checksum(numberList)
        #asign data if is pin_resp or data type
        if trameType == 'pin_resp' or trameType == 'data':
            if validChksm['CHKSMP']:
                self.log.debug('CHKSMP valid')
                #organize data
                Temp = {'data': [data['TEMPH'], data['TEMPL']],
                        'sensor': [self.gr, self.ccl],
                        'sensorNum': [11, 10]}
                Pres = {'data': [data['PRESH'], data['PRESL']],
                        'sensor': [self.temp2, self.temp1],
                        'sensorNum': [2, 1]}
                Accx = {'data': [data['XH'], data['XL']],
                        'sensor': [self.pres, self.accz],
                        'sensorNum': [3, 9]}
                Accy = {'data': [data['YH'], data['YL']],
                        'sensor': [self.res1s, self.accx],
                        'sensorNum': [7, 4]}
                Accz = {'data': [data['ZH'], data['ZL']],
                        'sensor': [self.res2s, self.accy],
                        'sensorNum': [8, 5]}
                var = [Temp, Pres, Accx, Accy, Accz]
                #asign data to self.t_sensors
                for n in range(len(self.ecpt['name'])):

                    self.log.debug(
                        'data[ID]: %s, ecpt[name]: %s',
                        data['ID'],
                        self.ecpt['name'][n]
                        )

                    if data['ID'] == self.ecpt['name'][n]:
                        for i in var:
                            d = i['sensor'][n].get_value(
                                self.list_to_hex(i['data'])
                                )

                            self.log.debug(
                                'tool: %s, data: %s, n: %s, ecpt_name: %s',
                                data['ID'],
                                d,
                                n,
                                self.ecpt['name']
                                )

                            self.t_sensors[i['sensorNum'][n]] = d
                self.log.debug('[532]t_sensors: %s', self.t_sensors)
            else:
                self.log.debug('CHKSMP invalid')
        #pin_resp and uphole trames have CHKSMU value so is not needed to ask
        #but data trame doesnt have an uphole trame
        self.log.debug('CHKSMU validity: %s', validChksm['CHKSMU'])
        if not trameType == 'data' and validChksm['CHKSMU']:
            #update good trame acquired
            self.global_buffer['telemetry']['received'] += 1
            Encoder = [data['ENCOH'], data['ENCOL']]
            Dir = [data['DIR']]
            Tension = [data['TENH'], data['TENL']]
            self.t_sensors[12] = self.list_to_hex(Encoder)  # encoder val
            self.t_sensors[13] = self.list_to_hex(Dir)  # encoder dir
            self.direccion = self.t_sensors[13]
            self.t_sensors[14] = self.ten.get_value(self.list_to_hex(Tension))
            invalid = False
            #CALCULATE DEPTH
            self.log.debug('[548]t_sensors: %s', self.t_sensors)
            if invalid is False:
                self.log.debug('data_len: %s', len(data))
                if len(data) > 3:

                    current_time2 = round(time() - self.init_time2, 4)
                    if self.updatefilter:
                        self.sample_filter = copy(self.depthtimefilter)
                        self.updatefilter = False
                    if current_time2 != 0:
                        current_time2 = current_time2 + self.pause_time

                    self.log.debug(
                        'first_data: %s, too early sampling: %s',
                        self.t_sensors[0] == 0,
                        current_time2 - self.t_sensors[0] >= self.sample_filter
                        )

                    if self.t_sensors[0] == 0 or current_time2 - self.t_sensors[0] >= self.sample_filter:
                        self.t_sensors[0] = round(current_time2, 4)

                    ###################################################
                    #DEPTH CALCULATION
                    ###################################################
                    self.current_depth = self.t_sensors[12]
                    #will be the filter updated?
                    if self.updatefilter:
                        self.sample_filter = copy(
                            self.depthtimefilter
                            )
                        self.updatefilter = False

                    #get the acquired data of encoder
                    self.vi['acq_encoder'] = self.t_sensors[12]
                    #if no current depth, asign the last one
                    if self.vi['acq_encoder'] == -1:
                        self.vi['acq_encoder'] = self.vi['last_encoder']
            #------------------------------------------------------
                    #update encoder to calculate the new depth
                    self.depthabsolute = self.vi['enc'].update_simple(
                        self.vi['acq_encoder'],
                        Dir
                        )

                    self.log.debug(
                        'final absolute depth: %s',
                        self.depthabsolute
                        )

                    #propagate absolute depth
                    self.global_buffer['depth'] = copy(
                        self.depthabsolute
                        )

                    if 'depth' in self.global_buffer['test_mode']:
                        self.t_sensors[12] = round(current_time2, 4)
                    else:
                        self.t_sensors[12] = self.depthabsolute
                    self.log.debug('[595]t_sensors: %s', self.t_sensors)
                    self.log.debug('[596]normal_trame: %s', self.normal_trame)
                    #depend on the mode, save data
                    self.vi['acq_manager'].update(
                        self.depthabsolute,
                        self.depthabsolute_ant,
                        self.normal_trame,
                        self.t_sensors,
                        current_time2
                        )
                    self.log.debug('[605]t_sensors: %s', self.t_sensors)
                    self.log.debug('[606]normal_trame: %s', self.normal_trame)
                    #show data in GUI
                    #self.callback(self.normal_trame)
                    self.callback(self.t_sensors)  # not what was meant
            #------------------------------------------------------
                    self.depthabsolute_ant = copy(self.depthabsolute)
                    # CALCULATE SPEED OF TOOL
                    # depthabsolute is on feet(FT)
                    # current_time2 is on seconds(sec)
                    tool_speed = self.vi['tsc'].update(
                        current_time2,
                        self.depthabsolute
                        )
                    tool_speed = round(tool_speed, 4)
                    self.log.debug('speed value: %s', tool_speed)
                    self.global_buffer['speed'] = tool_speed
                    gc.collect()

#==============================================================================
# 4. TOOL SETTINGS
#==============================================================================
    def sync_st(self, st):
        """@fn sync_st
            Para ajsutar umbral de sincronizacion
            @param st float
        """
        st = int(st)
        self.write_data(2, st)
        sleep(0.1)

    def sync_sf(self, sf):
        """@fn sync_sf
            Para ajustar frecuencia de sincronizacion
            @param sf float
        """
        sf = int(sf)
        self.write_data(3, sf)
        sleep(0.1)

    def sync_comp(self, comp):
        """@fn sync_comp
            Para ajustar comparadores de voltaje
            @param comp float
        """
        comp = int(comp)
        self.write_data(4, comp)
        sleep(0.1)

    def sync_gn(self, gn):
        """@fn sync_gn
            Para ajustar ganancia
            @param gn float
        """
        gn = int(gn)
        self.write_data(5, gn)
        sleep(0.1)

    def change_reference(self, newdepth):
        """@fn change_reference
            Se encarga de actualizar el valor de profundidad de referencia
            @param newdepth Valor de la nueva referencia
        """
        self.depth_absolute = copy(newdepth)
        self.update_reference = True

    def change_filter(self, newfilter):
        """@fn change_filter
            Se encarga de actualizar el valor de filtro de profundidad y tiempo
            @param newfilter Valor del nuevo valor del filtro de tiempo y
            profundidad
        """
        self.depthtimefilter = copy(newfilter)
        self.updatefilter = True

    def change_up_down(self, newdown):
        """@fn change_up_down
            Permite realizar la actualizacion el modo del registro si es Down
            o Up
            @param newdown Estado de modo de Up o Dowb
        """
        self.Down = copy(newdown)
        self.updateDown = True


def check_data(data):
    """check if data should be sent to serve_data routine"""
    headers = [a for a in data]
    count = 0
    for i in headers:
        count += data[i]
    if count == 1:
        return False
    else:
        return True
