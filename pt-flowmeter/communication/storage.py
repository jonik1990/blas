# -*- coding: utf-8 -*-
"""
implements data storage functions for all jpt tools and services
contains following classes:
    - keeper: does data saving
    - mode_manager: implements logics about how and which data to save
    based on daq modes
watch test_storage_module for further documentation
"""

#general imports
import os
from datetime import datetime
import logging as lg
from random import randrange

#local imports
from utilities import jpt_logger
from settings import lg_settings, jdb_data_layout


###############################################################################
# DATA STORAGE MANAGER
###############################################################################


class keeper():  # COMPLETE
    """save data in jdb format and can optionally show data in console"""

    def __init__(self, save_options=None, service=None, filter_conf=None):
        """builder of class"""
        self.debug = True
        extension = '.jdb'
        self.main_folder = os.getcwd()
        self.records_folder = os.path.join(self.main_folder, 'recordings')
        #build logger cursor
        if type(save_options) is dict and 'name' in save_options:
            self.cursor = lg.getLogger(save_options['name'])
        else:
            self.cursor = lg.getLogger(
                save_options['keeper-{:}'.format(randrange(1, 1000))]
                )
        self.cursor.setLevel(lg.DEBUG)  # DEFINE THE LEVEL TO WORK 24/7

        #build file name and save folder
        if save_options is None:
            #build default file name
            self.filename = self.def_file_name()
            #build default save folder
            self.savefolder = self.def_folder_name()
        else:
            if 'name' not in save_options:
                #build default file name
                self.filename = self.def_file_name()
            else:
                self.filename = save_options['name']
            if 'folder' not in save_options:
                #build default save folder
                self.savefolder = self.def_folder_name()
            else:
                self.savefolder = save_options['folder']
        self.jdb_filepath = os.path.join(
            self.records_folder,
            self.filename + extension
            )

        #create templating for data storage
        self.jdb_template = '%(message)s'
        jdb_formatter = lg.Formatter(fmt=self.jdb_template)
        jdb_console_formatter = lg.Formatter(
            fmt=lg_settings['default']['format'],
            datefmt=lg_settings['default']['datefmt']
            )

        #create handlers for data storage
        ch = lg.StreamHandler()
        fh = lg.FileHandler(self.jdb_filepath)
        fh.setFormatter(jdb_formatter)
        ch.setFormatter(jdb_console_formatter)

        #create logger for data storage
        self.cursor.addHandler(fh)
        if self.debug:
            self.cursor.addHandler(ch)

        #create filters for data saving
        self.service = service
        if not filter_conf:
            filter_conf = ['time_drive', '100ms']
        if not service:
            service = 'gr_ccl'
        filter_manager = mode_manager(
            service=service,
            filter_conf=filter_conf
            )
        self.cursor.addFilter(filter_manager)

    def def_folder_name(self):
        """generate default folder name"""
        if not os.path.exists(self.records_folder):
            os.mkdir(self.records_folder)
        return self.records_folder

    def def_file_name(self):
        """generate default file name"""
        self.cursor.debug('starting default filename generation')
        tmpl_name = 'session_%Y%m%d_%H-%M-%S.jdb'
        self.cursor.debug('default name generated: ')
        return datetime.now().strftime(tmpl_name)

    def add(self, data):
        """add data to register"""
        self.cursor.debug(data)

###############################################################################
# DATA STORAGE FILTERING
###############################################################################


class mode_manager():  # COMPLETE
    """implements logic of specific daq modes, modes are:
        - time_drive
        - depth_drive_up
        - depth_drive_down
    also implements records filtering depending on the current daq mode"""
    def __init__(self, service='gr_ccl', filter_conf=None):
        #creates a logger
        self.logger = jpt_logger(name='mode_manager')
        #get cursor from logger to save information
        self.log = self.logger.cursor

        #build mode manager
        if not filter_conf:
            self.mode = 'time_drive'
            self.daq_filter = '100ms'
        else:
            self.mode = filter_conf[0]
            self.daq_filter = filter_conf[1]
        self.service = service
        self.prev_value = None
        self.last_value = None

        #build criteria for filering
        self.criteria = None
        self.set_criteria(self.daq_filter)

    def set_criteria(self, criteria):
        """define filter criteria, criteria is the value of independent
        variable defined as minimum resolution"""
        #if is time, delete mili-seconds indicator and make it float
        if 'time_drive' == self.mode and 'ms' in self.daq_filter:
            self.criteria = float(self.daq_filter.replace('ms', '')) * 0.001
        #if is depth, delete ft indicator and make it float
        elif 'depth' in self.mode and 'ft' in self.daq_filter:
            self.criteria = float(self.daq_filter.replace('ft', ''))
        #otherwise, units and variable dont match
        else:
            raise ValueError('daq mode and filter units doesnt match')

    def filter(self, recording):
        """implements filtering for keeper module"""
        self.log.debug('starting filtering')

        #check if is the file header
        if 'header' in recording.msg:
            return recording

        #get independent variable
        if self.mode == 'time_drive':
            independent_var = recording.msg['time']
        else:
            independent_var = recording.msg['depth']

        #if first time, define recording value as current -last
        if not self.last_value and not self.prev_value:
            self.last_value = independent_var
            self.prev_value = independent_var
            return self.sort_recording(recording)
        self.last_value = independent_var

        #if up in mode, filter descending, otherwise, ascending
        delta = self.prev_value - self.last_value
        if 'up' in self.mode:
            self.log.info('up mode on')
            self.log.info((delta, self.criteria))
            self.log.info((delta > 0, delta > self.criteria))
            #if value is decreasing and exceeds criteria
            if delta > 0 and delta > self.criteria:
                self.prev_value = self.last_value
                return self.sort_recording(recording)
        else:
            self.log.info('down or time drive mode')
            self.log.info((delta, self.criteria))
            self.log.info((delta < 0, abs(delta) > self.criteria))
            #if value is increasing and exceeds criteria
            if delta < 0 and abs(delta) > self.criteria:
                self.prev_value = self.last_value
                return self.sort_recording(recording)
        self.log.info('detached by filter {:}'.format(recording.msg))
        return None

    def sort_recording(self, recording):
        """sort recording"""
        data = [
            round(recording.msg[i], 4)
            for i in jdb_data_layout[self.mode][self.service]
            ]
        recording.msg = {'Normal': data}
        return recording