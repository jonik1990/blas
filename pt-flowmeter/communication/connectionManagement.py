# -*- encoding: utf-8 -*-
import gc
import re
import time
import serial
import datetime
import threading

from copy import copy
#from math import ceil
import subprocess as sub

import config
#from communication.virtualInstrument import trameManager, virtualInstrument
from communication.virtualInstrument import trameManager
#from calibration_sensors import get_pt_sensors, Exponential
from utilities import get_active_ports, jpt_logger


class ConnectionManager(threading.Thread):
    """
         Clase que representa un administrador de conexión y ejecucion de datos
         ademas del guardado de datos en memoria
    """
    trames = trameManager()

    def __init__(self, kind=config.TRACKING_BY_TIME, reading_delay=0.5,
                 callback=None, show_error=None, sample_filter=1,
                 memory_off=None,
                 memory_on=None, get_avggr=False, show_messages=None,
                 show_trames=None, name="Serial_thread"):
        """
            Constructor de la clase
            @param kind Para indicar si se va graficar contra tiempo o
            profundidad (INT)
            @param reading_delay Para indicar el retardo de lectura
            serial (FLOAT)
            @param callback Callback (Funcion recibida de MainWindow) que
            envia los datos a otros widgets para ser graficados
            (callback Ver Mainwindow.py)
            @param show_error Para mostrar los mensajes de errores
            (gtkbuilderwindow.show_error)
            @param filter Especifica un submuestreo de los datos (FLOAT)
            @param memory_off Datos de memoria deshablitada (BOOLEAN)
            @param memory_on Datos de memoria habilitada (BOOLEAN)
            @param get_avggr Para habilitar el calculo del promedio de las
            cuentas de Gamma Ray, (BOOLEAN)
            @param show_messages Para mostrar mensajes de sincronizacion,
            Checksum y de Tramas (callback Ver MainWindow.py)
            @param show_trames Para mostrar las tramas que estan entrando
            (callback Ver MainWindow.py)
        """
        super(ConnectionManager, self).__init__()
        #create a logger for debugging purposes
        self.lg = jpt_logger(name='ConnectionManager')
        self.log = self.lg.cursor
        self.lg.disable()
        self.name = name
        self.kind = kind
        self.reading_delay = reading_delay
        self.callback = callback
        self.show_error = show_error
        self.idletime = None
        self.database = None
        self.database_memory = None
        self.databaseUp = None

        self.sample_filter = round(sample_filter, 2)

        self.memory_off = memory_off
        self.memory_on = memory_on

        self.get_avggr = get_avggr

        self.show_messages = show_messages
        self.show_trames = show_trames

        self.memory_data_incoming = False

        self.init_time = 0
        self.init_time2 = 0

        self.data_acum = []

        if self.get_avggr:
            self.t_sensors = [0] * 16
            self.t_sensors2 = [0] * 16
            self.normal_trame = [0] * 16
        else:
            self.t_sensors = [0] * 15
            self.t_sensors2 = [0] * 15
            self.normal_trame = [0] * 15

        self._time = 0

        self.playing = False
        self.connection = None
        self.quit = False

        self.sync_attempts = 0
        self.max_sync_attempts = 100

        self.data_error = -1

#==============================================================================
# 1. CONNECTION MANAGEMENT
#==============================================================================

    def connect(self, port=1, baudrate=128000, bytesize=8,
                parity='N', stopbits=1, timeout=0.45, xonxoff=0):
        """@fn connect
            Configura la conexion serial
            @param port Numero del puerto al que se va conectar (INT)
            @param baudrate BaudRate para la comunicacion serial (INT)
            @param bytesize Tamaño en bits del byte (INT)
            @param parity Paridad (STRING)
            @param stopbits Bits de parad (INT)
            @param timeout Time Out de la comunicacion serial(INT)
            @param xonxoff Xon o Xoff (INT)
            @return TRUE si se establecio bien la configuracion, de lo
            contrario FALSE (BOOLEAN)
        """
        self.port = port
        self.baudrate = baudrate
        self.bytesize = bytesize
        self.parity = parity
        self.stopbits = stopbits
        self.xonxoff = xonxoff
        self.timeout = timeout

        if self.port is None:
            return False

        try:
            self.connection = serial.Serial(
                self.port, int(self.baudrate),
                int(self.bytesize),
                str(self.parity),
                int(self.stopbits),
                timeout
                )
            self.log.debug('connection started at %s', self.connection)
            return True
        except serial.SerialException as e:
            self.log.debug('Connection Error: %s', e)
            return False

    def disconnect(self):
        """@fn disconnect
            Deshabilita la conexion serial y ademas hablita la bandera de
            cerrado para terminar el hilo de adquisicion de datos
        """
        self.quit = True
        if self.connection is not None:
            self.connection.close()
            self.connection = None
        self.stop()

    def onlydisconnect(self):
        """@fn onlydisconnect
            Realiza la accion de cerrar la conexion serial si esta abierta
        """
        if self.connection is not None:
            self.connection.close()

    def onlyreconnect(self, **kwargs):
        """@fn onlyreconnect
            Realiza la accion de reconectar la conexion serial
            @return True si la reconexion se establecio correctamente, de lo
            contrario False
        """
        if kwargs['newport']:
            self.port = kwargs['newport']
        if self.port is None:
            return False

        try:
            self.connection = serial.Serial(
                self.port, int(self.baudrate),
                int(self.bytesize),
                str(self.parity),
                int(self.stopbits),
                0.5)
            self.connection.flushInput()
            self.connection.flushOutput()
            return True
        except serial.SerialException as e:
            self.log.debug('Connection Error: %s', e)
            return False

    def reconnect(self):
        """reconnection routine"""
        ports = get_suitable_ports()
        regShort = re.compile('\x03.{2}[\x02\x01].{3}')
        regLong = re.compile('\xa5{2}[\x58\x59]\x00.{7}\x01')
        max_tryDevice = 10
        max_tryFix = 10
        max_tryConnect = 10
        valid_data = False
        tryConnect = 0
        while not valid_data and tryConnect < max_tryConnect and self.playing:
            try:
                s = serial.Serial(port=ports[0], baudrate=self.baudrate)
                s.flushInput()
                s.flushOutput()
                data = s.read(100)
            except:
                data = str()
                found_trame = False
                valid_tool = False
                tryFix = 0
                while not found_trame and tryFix < max_tryFix and self.playing:
                    tryDevice = 0
                    while not valid_tool and tryDevice < max_tryDevice and self.playing:
                        ports = get_suitable_ports(baud=self.baudrate)
                        valid_tool = len(ports) is not 0
                        time.sleep(1)
                        tryDevice += 1
                    found_trame = self.fix_connection(baud=self.baudrate)
                    if found_trame:
                        s = serial.Serial(port=ports[0], baudrate=self.baudrate)
                        s.flushInput()
                        s.flushOutput()
                        data = s.read(100)
                    tryFix += 1
            finally:
                notEmpty = data != ''
                shorttrame = len(regShort.findall(data)) != 0
                longtrame = len(regLong.findall(data)) != 0
                valid_data = notEmpty and (longtrame or shorttrame)
                tryConnect += 1
        return found_trame, ports[0]

    def fix_connection(self, baud=19200):
        """fix connection that sends bad data"""

        good_ports = get_suitable_ports()

        found = False
        notError = True
        loops = 0
        max_loops = 3

        while loops < max_loops and notError and not found and self.playing:
            # try with 8 bytesize communication
            try:
                s = serial.Serial(
                    port=good_ports[0], baudrate=baud,
                    bytesize=8, parity='N', stopbits=1,
                    timeout=3, xonxoff=0, rtscts=0, )
                s.flushInput()
                s.flushOutput()
                time.sleep(5)
                data = s.read(s.inWaiting())
                time.sleep(1)
                del s
            except:
                notError = False
                data = ''
            finally:
                time.sleep(1)
                gc.collect()
                # if not found, try with 7 bytesize communication while
                # \x25 is not detected
            if not isValidTrame('short', data) and not isValidTrame('long', data):
                attempts = 0
                while '\x25' or '\x01' not in data and attempts < max_loops and self.playing:
                    s = serial.Serial(
                        port=good_ports[0], baudrate=baud,
                        bytesize=7, parity='N', stopbits=1,
                        timeout=0, xonxoff=0, rtscts=0)
                    s.flushInput()
                    s.flushOutput()
                    time.sleep(10)
                    data = s.read(s.inWaiting())
                    del s
                    attempts += 1
                # check data with 8 bytes length
                s = serial.Serial(port=good_ports[0], baudrate=baud,
                                  bytesize=8, parity='N', stopbits=1,
                                  timeout=0, xonxoff=0, rtscts=0)
                s.flushInput()
                s.flushOutput()
                time.sleep(2.5)
                data = s.read(s.inWaiting())
                del s
                # check if there is valid data in acquired
                found = isValidTrame('short', data) or isValidTrame('long', data)

            else:
                found = True
                loops = max_loops - 1
            loops += 1
        return found

#==============================================================================
# 3. EXCECUTION MANAGEMENT
#==============================================================================

    def play(self, init_time):
        """@fn play
            Habilita el inicio de la adquisicion de datos
        """
        self.init_time = init_time
        self.init_time2 = init_time
        self.playing = True

    def pause(self, bandera):
        """@fn pause
            Indica si es ha realizado la accion de pausa de sesion
            @param bandera Estado de la pausa
        """
        self.Pause = bandera

    def stop(self):
        """@fn stop
            Detiene la adquisicion de datos
        """
        self.playing = False
        self.update_reference = False
        self.depth_absolute = 0.000
        self.depthtimefilter = 0.000
        self.updatefilter = False
        self.Down = True
        self.updateDown = False

    def record(self, active, db=None, dp_up=None):  # , db_memory=None
        """@fn record
            Para Activar el almacenamiento en la base de datos
            @param active Flag that active store in databse (BOOLEAN)
            @param db Objeto o Callback
            @param db_memory Objeto o Callback
            @param dp_up Objeto o Callback
        """
        if active:
            self.database = db
            #self.database_memory = db_memory
            self.databaseUp = dp_up
        else:
            if self.database:
                self.database = None
            #if self.database_memory:
                #self.database_memory = None
            if self.databaseUp:
                self.databaseUp = None

    def read_data(self, size):
        """@fn read_data
            Funcion que se encarga de leer los datos que llegan de puerto
            serial
            @param size Cantidad de datos a leer via puerto serial
        """
        try:
            raw_result = self.connection.read(size)
        except serial.serialutil.SerialException:
            if self.show_error:
                msg = 'Serial/USB connection failed'
                sub.call(['notify-send', msg, 'check connections'])
            status, portnew = self.reconnect()
            if status:
                self.onlyreconnect(newport=portnew)
                raw_result = self.connection.read(size)
            self.log.debug('out or reconnection')
            # liberar entradas del buffer serial
            self.connection.flushInput()
            # liberar salidas del buffer serial
            self.connection.flushOutput()
        data = self.decode_string(raw_result)
        if self.show_trames:
            self.show_trames([str(data)])
        return data

    def readport(self, readTime):
        """define time to read port, retuns raw string of acquired data,
           in case of errors, returns None"""
        try:
            time.sleep(readTime)
            response = self.connection.read(self.connection.inWaiting())
            return response
        except:
            return None

    def write_data(self, header, data):
        """@fn write_data
            Escribe via puerto serial una cabezera y un dato
            @param header Cabezara que va iniciar lo se va enviar via serial
            (INT)
            @param data Dato que se va enviar via serial (INT)
        """
        hb = data / 256
        lb = data - (hb * 256)
        self.connection.write(chr(header))
        self.connection.write(chr(hb))
        self.connection.write(chr(lb))

    def asignTo(self, asignType, number):
        """asign error or normal data to sensors range"""
        if number == 1:
            span = range(1, 4)
        elif number == 2:
            span = range(4, 10)
        elif number == 3:
            span = range(10, 12)
        elif number == 4:
            span = range(12, 15)
        for i in span:
            if asignType == 'error':
                self.t_sensors[i] = self.data_error
            elif asignType == 'normal':
                self.t_sensors[i] = copy(self.normal_trame[i])
        return

    #def pin(self, tool):
        #"""ask tools for data, retuns a dictionary with data id and acquired
           #strings"""
        ##write pin order in port
        #pinOrder = self.trames.Templates['pin'].format(tool.id_hex)
        #self.log.debug(
            #'the order is: %s, readable: %s, writable: %s',
            #[pinOrder],
            #self.connection.readable(),
            #self.connection.writable()
            #)
        #timeout = False
        #max_delta = datetime.timedelta(0, 0.6)
        #initTime = datetime.datetime.now()
        #notAnswer = True
        #error = False
        ##wait for answer
        #while notAnswer and not timeout and not error:
            #try:
                ##clear connection before send data
                #self.connection.flush()
                ##write order data to device
                #self.connection.write(pinOrder)
                ##get data from tool
                #data = self.readport(self.toolRespTime)
                #reqTime = datetime.datetime.now()
                #notAnswer = True
                #if max_delta > reqTime - initTime:
                    #timeout = True
                #else:
                    #initTime = datetime.datetime.now()
            #except Exception as e:
                #self.log.warning('error during tool pin: %s', e)
                #error = True
                #data = self.trames.Templates['pinvalid'].format(tool.id_hex)
        #return data

    def pin(self, tool):
        """ask tools for data, retuns a dictionary with data id and acquired
           strings"""
        #write pin order in port
        pinOrder = self.trames.Templates['pin'].format(tool.id_hex)
        self.log.debug(
            'the order is: %s',
            [pinOrder],
            )
        timeout = False
        max_delta = datetime.timedelta(0, 0.6)
        initTime = datetime.datetime.now()
        notAnswer = True
        error = False
        #wait for answer
        while notAnswer and not timeout and not error:
            try:
                #clear connection before send data
                try:
                    self.connection.reset_input_buffer()
                    self.connection.reset_output_buffer()
                except:
                    self.connection.flushInput()
                    self.connection.flushOutput()
                #write order data to device
                self.connection.write(pinOrder)
                #get data from tool
                #data = self.readport(self.toolRespTime)
                data = self.connection.read(26)
                self.log.info('pin_order: %s', [pinOrder])
                self.log.info('pinned data: %s', [data])
                reqTime = datetime.datetime.now()
                notAnswer = True
                if max_delta > reqTime - initTime:
                    timeout = True
                else:
                    initTime = datetime.datetime.now()
            except Exception as e:
                self.log.warning('error during tool pin: %s', e)
                error = True
                data = self.trames.Templates['pinvalid'].format(tool.id_hex)
        return data

    def request_panel(self):
        """request uphole data to the panel of flowmeter application"""
        try:
            #clear connection before send data
            try:
                self.connection.reset_input_buffer()
                self.connection.reset_output_buffer()
            except:
                self.connection.flushInput()
                self.connection.flushOutput()
            #send data
            order = '\x0c\x00\x00'
            self.connection.write(order)
            #read response
            raw = self.connection.read(7)
            #return data
            error = False
        except Exception as e:
            self.log.warning('error during panel pin: %s', e)
            raw = b''
            error = True
        return raw, error

    def run(self):
        """main excecution loop"""
        pass

###############################################################################
# TOOL NAME CONFIGURATION
###############################################################################


class toolNameManager(threading.Thread):
    """manages serial sesion during tool rename"""
    trames = trameManager()
    status = 'starting'
    readTime = 0.15
    samplerates = {'250 ms': 1, '500 ms': 2, '1000 ms': 3}
    port = ''
    connection = None

    def __init__(self, name="Name_Serial_thread"):
        """builder method of toolNameManager class"""
        super(toolNameManager, self).__init__()

    def startCom(self, baudrate=115200):
        """starts communication"""
        try:
            self.connection = serial.Serial(self.port, baudrate)
            if not self.connection.isOpen():
                self.connection.open()
            return True
        except serial.SerialException, e:
            print "Connection Error: ", e, self.port
            return False

    def stopCom(self):
        """stop communication"""
        try:
            self.connection.close()
        except:
            self.log.debug("Connection Error: cant stop properly")

    def setPort(self, port):
        """set the port that will be used in the communication"""
        self.port = port

    def getSuitablePorts(self):
        """return a list of suitable ports that can be the connected tool"""
        return get_suitable_ports(baud=11520, mode='quiet')

    def perform(self, action, *args):
        """performs a given action and allows other inputs related to the
           action, it returns a status true if succesful or false if failed
           also can have a second component if the performed action returns
           information for the user"""
        if action == 'asignSlaves':
            assert len(args) == 1, 'only two arguments allowed'
            slaves = chr(int(args[0]))
            order = self.trames.Templates['slaves'].format(slaves)
        elif action == 'asignSamplerate':
            assert len(args) == 1, 'only two arguments allowed'
            samplerate = chr(self.samplerates[args[0]])
            order = self.trames.Templates['samplerate'].format(samplerate)
        elif action == 'asignId':
            assert len(args) == 1, 'only two arguments allowed'
            ID = chr(int(args[0]))
            order = self.trames.Templates['setid'].format(ID)
        elif action == 'askId':
            order = self.trames.Templates['getid']
        status = self.sendOrder(order)
        print status
        return status

    def sendOrder(self, order):
        """send order to tool"""
        cycles = 0
        max_cycles = 10
        status = False
        #create serial session
        successCon = self.startCom()
        #while action is not success
        while True:
            if not successCon:
                return False
        #write order in port
            self.connection.flush()
            self.connection.write(order)
        #catch tool response
            time.sleep(0.125)
            response = self.connection.read(self.connection.inWaiting())
        #verify response
            if self.trames.identify(response, isTrame='slavesResp'):
                self.log.debug('slavesResp')
                if self.trames.valid in response:
                    status = True
                    break
            elif self.trames.identify(response, isTrame='samplerateResp'):
                self.log.debug('samplerateResp')
                if self.trames.valid in response:
                    status = True
                    break
            elif self.trames.identify(response, isTrame='idResp'):
                self.log.debug('idResp')
                if self.trames.valid in response:
                    status = True
                    tool_id = self.trames.getData(response, trameType='idResp')
                    self.stopCom()
                    self.log.debug(
                        'status: %s, tool_id: %s',
                        status,
                        tool_id
                        )
                    return status, tool_id
            if cycles >= max_cycles:
                self.stopCom()
                return False
            else:
                cycles += 1
                continue
        #finish serial session
        self.stopCom()
        #return actoin status true if ok, false if failed
        return status


#==============================================================================
# AUXILIAR METHODS
#==============================================================================

def get_suitable_ports(baud=19200, mode='normal'):
    """list suitable serial ports"""
    sysports = re.compile('(/dev/ttyS[\d]+)|([\d]+portsfound)')
    # Get ports list
    port_list = get_active_ports()
    port_list = [a for a in port_list if not sysports.match(a) and not a == '']
    # Check ports in list
    suitable_ports = list()
    for port in port_list:
        suitable = True
        try:
            j = serial.Serial(port=port, baudrate=baud, timeout=10)
            if mode != 'quiet':
                j.read()
            j.close()
        except Exception as e:
            print (e)
            suitable = False
        if suitable:
            suitable_ports.append(port)
    # Clear serial connections
    if suitable_ports != []:
        del j
    gc.collect()
    return suitable_ports


def isValidTrame(trame, data):
    """check if trame is short or long"""
    regShort = re.compile('\x03.{2}[\x02\x01].{3}')
    regLong = re.compile('\xa5{2}[\x58\x59]\x00.{7}\x01')
    if trame == 'long':
        status = len(regLong.findall(data)) != 0
    elif trame == 'short':
        status = len(regShort.findall(data)) != 0
    else:
        status = False
    return status
