#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
JPT
Revisado: Pedro Rivera
Script: Tipo A
(composicion ventana principal)
"""
import gc
import time
import gtk
import gtk.gdk
#import random
import gobject
#import math
from datetime import datetime, timedelta

from communication.connectionManagement import toolNameManager
from communication.connectionManagement import get_suitable_ports
import gui.dialogs as dialogs
from communication.serial_connection import DavidConnection
from gui.tracks import ComposeTrackWidget
from tracks_configs import *
from gui.utils import *
from playback.database import *
from playback.las import *
import config
from gui.gtkbuilderwindow import GtkBuilderWindow  # UnresolvedImport

from utilities import jpt_logger

global_buffer = {}


class MainWindow(GtkBuilderWindow):
    """
         Clase que representa la Clase Principal del Software y desde la cual
         se hace el llamado a las demas clases
    """
    builder_file = config.gui_file('MainWindow.glade')
    #builder_file_lmd = config.gui_file('LoggingModeDialog.glade')

    #implement a global buffer to communicate some data
    global_buffer = {
        'speed': 0,
        'depth_reference': 0,
        'recording_state': False,
        'test_mode': 'time_drive',
        'well': '01',
        'field': 'field',
        'encoder': {
            'toogle': True,
            'tension_factor': 1,
            'depth_factor': 1,
            'perimeter': 2,
            'counts_per_rev': 300,
            'depth_offset': 0
            },
        'telemetry': {
            'sent': 0,
            'received': 0,
            'success_ratio': 0
            }
        }

    def __init__(self):
        """
            Constructor de la clase MainWindow
        """
        gtk.gdk.threads_init()
        super(MainWindow, self).__init__()
        #log debugging captabilities
        self.lg = jpt_logger(name='mainwindow', mode='quiet')
        self.log = self.lg.cursor
        self.s_connection = None
        self.options = {}
        self.record = None
        self.record_filename = None
        self.db = None
        self.ct_widget = None
        self.start_time = None
        self.show_all()
        self.error = [False, 'Unknown Error']
        self.pausar = False
        self.clock()
        self.debugging = False

        self._window.maximize()
        self.k = config.TRACKING_BY_TIME
        self.spinbuttons = []
        for sb in get_important_widgets(self.builder_file)['spinbuttons']:
            self.spinbuttons.append(self._builder.get_object(sb))

        self._window.set_title(config.SW_NAME + ' - Welcome')

        vp_color = gtk.gdk.color_parse("#f7f7f7")  # UndefinedVariable
        self.vp_expander1.modify_bg(gtk.STATE_NORMAL, vp_color)
        self.vp_expander2.modify_bg(gtk.STATE_NORMAL, vp_color)
        self.vp_expander3.modify_bg(gtk.STATE_NORMAL, vp_color)
        self.vp_expander4.modify_bg(gtk.STATE_NORMAL, vp_color)
        vp_color = gtk.gdk.color_parse("#f0f0f0")  # UndefinedVariable
        self.vp_welcome.modify_bg(gtk.STATE_NORMAL, vp_color)

        self.portcom = None
        self.baudrate = None

        self.dialog_monitor = None
        self.EstateDialogMonitor = None
        self.Down = False
        self.DeptReference = 0.000
        self.state_record = False
        #self.filenamerecord = None

        #special parameters for ECPT services
        self.ecpt = {'port': [], 'name': [], 'mode': '', 'samplerate': 1}
        self.samplingRateSb = self._builder.get_object('samplingRateSb')

    def clock(self):
        """@fn clock
            Funcion para poner en el label lbl_working_time (MainWindow.glade)
            el tiempo transcurrido desde que se inicio el servicio, es decir,
            desde que se dio Play. Cabe destacar que esta es llamada en el
            constructor y no desde la accion
            de presional el boton Play.
        """
        if self.error[0]:
            self.show_error(self.error[1])
            self.error[0] = False
        if self.start_time:
            time_now = datetime.now()
            delta_time = time_now - self.start_time
            self.working_time = timedelta(seconds=delta_time.seconds)
            self.lbl_working_time.set_text(
                'Working Time: ' + str(self.working_time)
                )
        #if self.pausar == False:
        gobject.timeout_add(1000, self.clock)

    def update_spinbuttons(self):
        """@fn update_spinbuttons
            Actualiza cada spinbutton de la interfaz principal
            (MainWindow.glade)
        """
        for sb in self.spinbuttons:
            sb.update()

    def set_options(self, down=True, **kwargs):
        """@fn set_options
            Funcion que carga en la interfaz principal la parte de
            visualizacion de lineas ya sea del servicio de loging o parameter
            tool
            @param down Indica si la linea se va visualizar de arriba hacia
            abajo o lo contrario
        """
        #code for depth drive up compatibility
        if 'set_h' in kwargs:
            h = kwargs['set_h']
        else:
            h = -1
        if self.options:
            #down = self.options['logingmodedialog_options']['radiobuttons']['rad_down_mode']
            down = 'up' not in self.global_buffer['test_mode']
            if self.options['config_options']['radiobuttons']['rad_pt_service']:
                if self.options['config_options']['radiobuttons']['rad_pt_5_tracks']:
                    self.ct_widget = new_pt_d5_composetrack(
                        self.options['tracks_options'],
                        down,
                        h=h
                        )
                    replace_widget(
                        self._window,
                        self.box_pt_value_monitor,
                        None)
                    replace_widget(
                        self._window,
                        self.box_pt_info_monitor,
                        None)
                if self.options['config_options']['radiobuttons']['rad_pt_6_tracks']:
                    self.ct_widget = new_pt_d6_composetrack(
                        self.options['tracks_options'],
                        down,
                        h=h
                        )

                    replace_widget(
                        self._window,
                        self.box_pt_value_monitor,
                        self.lbl_pt_mon_avggr)
                    replace_widget(
                        self._window,
                        self.box_pt_info_monitor,
                        self.lbl_pt_info_mon_avggr)

                replace_widget(
                    self._window,
                    self.box_monitor,
                    self.box_pt_monitor)
                replace_widget(
                    self._window,
                    self.box_filters,
                    self.box_pt_filters)
                ###self.cb_baudrate.set_active(3)  #OJOJOJOJJO
                self.btn_shock_protect.set_visible(False)

                gobject.idle_add(self.ct_widget.draw)

    def playback_mode(self):
        """@fn playback_mode
            Funcion para cargar en la interfaz principal cuando este en modo
            PlayBack los widgtes de ComposeTrackWidget y tambien
            los widgets correspondientes al modo de PlayBack como los botones
            de ImportDB, toPDF y toLAS
        """
        #give the graphic panel the instruction
        #to graph increasing or decreasing
        self.set_options(down=self.Down)
        self.btn_play.set_sensitive(False)
        self.btn_save_ss.set_sensitive(False)
        replace_widget(self, self.box_content, self.box_simulator)
        replace_widget(self._window, self.box_pb_tracks, self.ct_widget)
#        hide pause and record buttons
        self.btn_pause.hide()
        self.btn_record.hide()

    def logging_mode(self):
        """@fn logging_mode
            Funcion para cargar en la interfaz principal cuando este en modo
            LOGING los widgtes de ComposeTrackWidget y tambien los widgets
            correspondientes al modo Loging como los botones de Setup, Save,
            Load, Play, Stop, Record etc.
        """
        #self.exp_filters_setup.set_expanded(True)
        #self.exp_filters_setup.set_expanded(False)
#        hide pause and record buttons
        self.btn_record.hide()
        self.btn_pause.hide()
        self.on_btn_scan_ports_clicked(None)
        self.set_options()
        self.btn_save_ss.set_sensitive(True)
        self.btn_play.set_sensitive(True)
        replace_widget(self, self.box_content, self.box_paned)
        replace_widget(self._window, self.box_tracks, self.ct_widget)
        self.right_panel.set_sensitive(True)
        self.playing(False)
        self.sb_sampling_step.hide()
        if self.options['config_options']['radiobuttons']['rad_pt_service']:
            self.cb_baudrate.set_active(3)
        # self.btn_pause.set_sensitive(True)

    def report_error(self, msg):
        """@fn report_error
            Se encarga de en caso de algun error en la conexion serial,
            desconecta toda comunicacion y ademas detiene el registro en caso
            de que se este ejecutando
            @param msg String con el mensaje que se va mostrar
        """
        self.s_connection.disconnect()
        self.playing(False)
        self.s_connection = None
        self.error[1] = msg
        self.error[0] = True

    def set_status(self, msg):
        """@fn set_status
            Callback que se usa para mostrar mensajes de estado de algun
            proceso. esta se ejecuta cuando el hilo principal de la interfaz
            principal esta en tiempo de descanso para actaulizar el estado de
            algun widget grafico.
            @param msg Mensage que va ser actualizado en un widget de la
            interfaz
        """
        gobject.idle_add(self.lbl_status.set_text, msg)

    def record_warning(self, set):
        """@fn record_warning
            Callback que se usa para indicar en la interfaz grafica el estado
            de grabado de los datos, si se esta guardanado los datos
            se mostrar en la interfaz principal un mensaje indicando que se
            esta guardando la informacion, de lo contrario, se mostrara
            un mensaje en rojo indicando un warning
            @param set Indica si se esta grabando la informacion
        """
        if not set:
            lbl_color = gtk.gdk.color_parse("#33cc33")  # UndefinedVariable
            self.lbl_monitor_warning.modify_fg(gtk.STATE_NORMAL, lbl_color)
            vp_color = gtk.gdk.color_parse("#f0f0f0")  # UndefinedVariable
            self.vp_monitor_warning.modify_bg(gtk.STATE_NORMAL, vp_color)
            self.lbl_monitor_warning.set_text('Recording data')
        else:
            lbl_color = gtk.gdk.color_parse("#ff0000")  # UndefinedVariable
            self.lbl_monitor_warning.modify_fg(gtk.STATE_NORMAL, lbl_color)
            vp_color = gtk.gdk.color_parse("#000000")  # UndefinedVariable
            self.vp_monitor_warning.modify_bg(gtk.STATE_NORMAL, vp_color)
            self.lbl_monitor_warning.set_text('Warning: Data is not being recorded!')

    def playing(self, set):
        """@fn playing
            Funcion que se encarga de incializar en la interfaz principal
            cuando se esta en modo Loging algunos widgets como labes, buttons
            etc, para que se incialicen desde el inicio y tambien para iniciar
            un reloj.
            @param set Booleano que indica si esta inciado el registro de datos
            o detenido
        """
#######################################
# poner valores del filtro por defecto
#######################################
        self.sb_pt_st.set_value(5500)
        self.sb_pt_sf.set_value(13000)
        self.sb_pt_comparator.set_value(5)
        self.sb_pt_gain.set_value(5)
        self.record_warning(True)

        if set:
            self.btn_autoscroll.set_active(True)
            self.start_time = datetime.now()

        else:
            self.lbl_pt_mon_gr.set_text('')
            self.lbl_pt_mon_avggr.set_text('')
            self.lbl_pt_mon_ccl.set_text('')
            self.lbl_pt_mon_pres.set_text('')
            self.lbl_pt_mon_temp1.set_text('')
            self.lbl_pt_mon_temp2.set_text('')
            self.lbl_pt_mon_res1.set_text('')
            self.lbl_pt_mon_res2.set_text('')
            self.lbl_pt_mon_res3.set_text('')
            self.lbl_pt_mon_accx.set_text('')
            self.lbl_pt_mon_accy.set_text('')
            self.lbl_pt_mon_accz.set_text('')
            self.lbl_pt_mon_ten.set_text('')
            self.lbl_pt_mon_depth.set_text('')
            self.lbl_working_time.set_text('')
            # LABELS
            self.set_labels()

            self.start_time = None
            self.lbl_status.set_text('')
            self.lbl_monitor_warning.set_text('')

        self.btn_config_tracks.set_sensitive(set)
        self.btn_autoscroll.set_sensitive(set)
        self.btn_stop.set_sensitive(set)
        #self.btn_record.set_sensitive(set)
        self.btn_record.hide()
        self.btn_play.set_sensitive(not set)
        self.btn_pt_return.set_sensitive(not set)
        self.btn_open_ss.set_sensitive(not set)
        self.btn_save_ss.set_sensitive(not set)
        self.btn_ss.set_sensitive(not set)
        self.btn_import_db.set_sensitive(not set)
        self.btn_connection_checker.set_sensitive(set)
        self.btn_shock_protect.set_sensitive(set)
        self.box_logging_mode.set_sensitive(not set)
        self.box_con_setup.set_sensitive(not set)
        self.box_pt_filters.set_sensitive(set)

        #self.btn_pause.set_sensitive(set)
        self.btn_pause.hide()

        self.exp_connection_setup.set_expanded(not set)
        self.exp_logging_mode.set_expanded(not set)
        #self.exp_filters_setup.set_expanded(set)
        self.exp_monitor.set_expanded(set)

    def _init_connection(self):
        """@fn _init_connection
            Funcion que se encarga de inicializar las configuraciones con el
            modulo de serial_connection y dependiendo de las configuraciones
            del servicio y cantidad de tracks que el usuario haya configurado
            se configura la conexion serial para esto y tambien
            el tipo de calibracion que vaya realizar en los datos que llegan de
            los sensores.
        """
        port = str(self.ls_serial_port.get_value(self.cb_serial_port.get_active_iter(), 0))
        baudrate = int(self.ls_baudrate.get_value(self.cb_baudrate.get_active_iter(), 0))
        #copy to self.ecpt the port information
        self.ecpt['port'] = copy.copy(port)

        #self.portcom = self.cb_serial_port.get_active_text()
        #filter_step = int(self.cb_sampling_step.get_active_text())
        filter_step = int(self.dialog_monitor.combosamplerate.get_active_text())
        if self.dialog_monitor.raddown.get_active():
            self.Down = True

        if self.dialog_monitor.radup.get_active():
            self.Down = False

        #if self.k == config.TRACKING_BY_TIME:
        #    print "Por tiempo"
        #else:
        #    print "Por Profundidad"

        #if self.rad_real_time.get_active():
        if self.k == config.TRACKING_BY_TIME:
            #self.k = config.TRACKING_BY_TIME
            filter_step = float(filter_step) / 1000.0   # a unidades de segundos
            print self.portcom,self.baudrate, filter_step, self.Down, self.DeptReference

        else:
            if self.options['config_options']['radiobuttons']['rad_pt_service']:
                samples_per_ft = self.options['calibration_options']['spinbuttons']['sb_pt_depth_lin_m']
                offset_fact = self.options['calibration_options']['spinbuttons']['sb_pt_depth_lin_b']
                #filter_step = ((1.0/samples_per_ft)*offset_fact)
                filter_step = 1.0 / filter_step

            print self.portcom,self.baudrate, filter_step, samples_per_ft, offset_fact, self.Down, self.DeptReference

        if self.options['config_options']['radiobuttons']['rad_pt_service']:
            if self.options['config_options']['radiobuttons']['rad_pt_5_tracks']:
                avg_gr = False
                callback_function = self.on_pt_d5_data
            else:
                avg_gr = True
                callback_function = self.on_pt_d6_data

            self.s_connection = DavidConnection(
                kind=self.k,
                reading_delay=0.005,
                callback=callback_function,
                show_error=self.report_error,
                sample_filter=filter_step,
                memory_off=self.on_memory_incoming_off,
                memory_on=self.on_memory_incoming_on,
                get_avggr=avg_gr,
                show_messages=self.on_connection_messages,
                show_trames=self.on_connection_trames,
                name="serial_thread",
                ecptConfigs=self.ecpt,
                global_buffer=self.global_buffer
                )

            self.s_connection.calibrate_sensors(self.options)

            self.s_connection.change_reference(self.DeptReference)
            self.s_connection.change_up_down(self.Down)

            if self.s_connection.connect(self.portcom, self.baudrate):
                self.on_btn_pt_set_gn_clicked(None)
                self.on_btn_pt_set_comp_clicked(None)
                self.on_btn_pt_set_sf_clicked(None)
                self.on_btn_pt_set_st_clicked(None)
                self.s_connection.connection.flushInput()
                self.s_connection.connection.flushOutput()
                return True

            else:
                self.show_error('Could not connect to serial port')
                return False

    def on_connection_messages(self, data):
        """@fn on_connection_messages
            Callback usado para mostrar mensages del protocol checker desde
            conexion serial y solo es llamada si el boton
            de Connection en la ventana principal de Loging se ha presionado.
        """
        if self.debugging:
            #gobject.idle_add(self.ls_messages.append, data)
            self.ls_messages.append(data)

    def on_connection_trames(self, data):
        """@fn on_connection_trames
            Callback usado para mostrar tramas del protocol checker desde
            conexion serial y solo es llamada si el boton
            de Connection en la ventana principal de Loging se ha presionado.
        """
        if self.debugging:
            #gobject.idle_add(self.ls_trames.append, data)
            self.ls_trames.append(data)

############################################
#    CONEXION DATOS A GRAFICA
############################################

    def on_pt_d6_data(self, data):
        """@fn on_pt_d6_data
            Callback que se encarga de actualizar informacion en el
            ComposeTrackWidet del servicio de Parameter Tool en modo de
            graficado de 6 tracks
            y tambien de actualizar el label de Gamma Ray AVG
            @param data Lista de datos con informacion de las tramas
        """
        self.on_pt_d5_data(data)

#compose service 6

        #self.lbl_pt_mon_avggr.set_text(str(data[15]))

    def on_pt_d5_data(self, data):
        """@fn on_pt_d5_data
            Callback que se encarga de actualizar informacion en el
            ComposeTrackWidet del servicio de Parameter Tool en modo de
            graficado de 5 tracks
            y tambien de actualizar el labels de Resistivity 1, Resistivity 2,
            Resistivity 2, Temperature 1, Temperature 2, Presion,
            Acc X, Acc Y, Acc Z ,Gamma Ray, CCL, Tension y Depth
            @param data Lista de datos con informacion de las tramas
        """
        ############################################
        # Conexion de datos a Grafico y a ventana de monitor
        # Configuracion del sensor Temperature 1
        gobject.idle_add(self.ct_widget.set_data, 0, 0, (data[11], data[0]))
        gobject.idle_add(self.lbl_pt_mon_gr.set_text, str(data[11]) +" "+"")
        ## Configuracion del sensor Temperature 2
        #gobject.idle_add(self.ct_widget.set_data, 0, 1, (data[10], data[0]))
        gobject.idle_add(self.lbl_pt_mon_ccl.set_text, str(data[10]) +" "+"")
        # Configuracion del sensor Acc X1
        gobject.idle_add(self.ct_widget.set_data, 2, 0, (data[3], data[0]))
        gobject.idle_add(self.lbl_pt_mon_pres.set_text, str(data[3]) +" "+"")
        # Configuracion del sensor Acc Y1
        gobject.idle_add(self.ct_widget.set_data, 2, 1, (data[7], data[0]))
        gobject.idle_add(self.lbl_pt_mon_res1.set_text, str(data[7]) +" "+"")
        # Configuracion del sensor Acc Z1
        gobject.idle_add(self.ct_widget.set_data, 2, 2, (data[8], data[0]))
        gobject.idle_add(self.lbl_pt_mon_res2.set_text, str(data[8]) +" "+"")
        # Configuracion del sensor Acc X2
        gobject.idle_add(self.ct_widget.set_data, 3, 0, (data[9], data[0]))
        gobject.idle_add(self.lbl_pt_mon_res3.set_text, str(data[9]) +" "+"")
        ## Configuracion del sensor Acc Y2
        #gobject.idle_add(self.ct_widget.set_data, 3, 1, (data[4], data[0]))
        gobject.idle_add(self.lbl_pt_mon_accx.set_text, str(data[4]) +" "+"")
        ## Configuracion del sensor Acc Z2
        #gobject.idle_add(self.ct_widget.set_data, 3, 2, (data[5], data[0]))
        gobject.idle_add(self.lbl_pt_mon_accy.set_text, str(data[5]) +" "+"")
        # Configuracion del sensor Pressure 1
        gobject.idle_add(self.ct_widget.set_data, 1, 0, (data[2], data[0]))
        gobject.idle_add(self.lbl_pt_mon_temp2.set_text, str(data[2]) +" "+"")
        ## Configuracion del sensor Pressure 2
        #gobject.idle_add(self.ct_widget.set_data, 1, 1, (data[1], data[0]))
        gobject.idle_add(self.lbl_pt_mon_temp1.set_text, str(data[1]) +" "+"")
        # Configuracion del sensor Tension
        gobject.idle_add(self.ct_widget.set_data, 4, 0, (data[14], data[0]))
        gobject.idle_add(self.lbl_pt_mon_ten.set_text, str(data[14]) +" "+"")

#########################################################################
# mostrar enconder
#########################################################################

        if self.k == config.TRACKING_BY_TIME:
            gobject.idle_add(
                self.lbl_pt_mon_depth.set_text,
                str(data[12])
                )  # 12
            if self.dialog_monitor:
                self.dialog_monitor.SetNewValues(
                    data[12], data[14], self.global_buffer['speed']
                    )
        else:
            gobject.idle_add(
                self.lbl_pt_mon_depth.set_text,
                str(data[12])
                )  # 12 data[0]
            if self.dialog_monitor:
                self.dialog_monitor.SetNewValues(
                    data[0], data[14], self.global_buffer['speed']
                    )

        gobject.idle_add(
            self.lbl_pt_mon_ten.set_text,
            '{:.2f} N'.format(data[14])
            )
        gc.collect()

        #if self.k == config.TRACKING_BY_TIME:
            #if self.dialog_monitor:
                #self.dialog_monitor.SetNewValues(data[12], data[14])
        #else:
            #if self.dialog_monitor:
                #self.dialog_monitor.SetNewValues(data[0], data[14])

    def on_btn_pause_toggled(self, button):
        """@fn on_btn_pause_toggled
            Callback encargado de ejecutar una pausa en tiempo y profundidad
            de la sesion que se esta ejecutando
            @param button Widget de donde viebe el evento
        """
        conexion_fisica = False
        conexion_soft = False
        ports = get_suitable_ports(baud=115200, mode='quiet')
        for p in ports:
            if p == self.portcom:
                conexion_fisica = True
                break

        if conexion_fisica is True:
            if self.s_connection != None:
                if self.btn_pause.get_active():
                    self.pausar = True
                    self.s_connection.pause(True)
                    self.s_connection.onlydisconnect()
                else:
                    self.pausar = False
                    conexion_soft = self.s_connection.onlyreconnect()
                    if conexion_soft:
                        time.sleep(0.1)
                        self.s_connection.pause(False)
                    else:
                        self.btn_pause.set_active(True)
                        self.show_error('Could not connect to serial port')
        else:
            self.btn_pause.set_active(True)
            self.show_error('Could not connect to serial port')

    def on_btn_logging_service_clicked(self, button):
        """@fn on_btn_logging_service_clicked
            Callback que es llamada cuando en la interfaz principal se presiona
            el boton Loging, y este se encarga de cargar en la interfaz
            princpal todo lo que tiene que ver con el loging como Setup, Save,
            Open, Play, Stop, Record, etc.
            @param widget de quien recibe la senal
        """
        replace_widget(self._window, self.box_content, None)
        replace_widget(self._window, self.box_toolbar, self.tb_logging_service)
        self._window.set_title(config.SW_NAME + ' - Logging Service')
        if self.dialog_monitor is None:
            self.dialog_monitor = dialogs.MonitorModeLoggingDialog(
                self,
                external_buffer=self.global_buffer
                )
#            hide pause and record buttons
            self.btn_pause.hide()
            self.btn_record.hide()
            self.dialog_monitor.ShowMonitorDialog()
            self.EstateDialogMonitor = True

    def on_btn_playback_clicked(self, button):
        """@fn on_btn_playback_clicked
            Callback que es llamado cuando en la interfaz principal se presiona
            el boton PlayBack, y este se encarga de cargar en la interfaz
            principal todo lo necesario para el playback, tal como la
            importacion de la base de datos, exportacion a .pdf y .las
            @param widget de quien recibe la senal
        """
        replace_widget(self._window, self.box_content, None)
        replace_widget(self._window, self.box_toolbar, self.tb_playback)
        self._window.set_title(config.SW_NAME + ' - Playback')

    def on_btn_monitor_service_clicked(self, button):
        """@fn on_btn_monitor_service_clicked
            Callback que se encarga de cargar en la interfaz principal la caja
            contenedora de monito service
            @param button de quien recibe la senal
        """
        self.btn_pause.hide()
        self.btn_record.hide()
        replace_widget(self._window, self.box_content, None)
        pass

    def on_btn_about_clicked(self, button):
        """@fn on_btn_about_clicked
            Callback que se genera cuando se presiona el boton About en la
            ventana principal, y este se encarga se mostrar una ventana
            de dialogo con informacion del software
            @param button widget de quien recibe la senal
        """
        dialogs.AboutDialog()

    def toolConfigClicked(self, button):
        """
        Callback que se genera cuando se presiona el boton Tool Configuration
        en la ventana principal, y este se encarga se mostrar una ventana
        de dialogo con controles para saber los puertos seriales del sistema,
        obtener id de la herramienta, asignar id a la herramienta, configurar
        samplerate y el número de esclavos
        """
        dialogs.toolNameDialog(toolNameManager)

    def setSamplingRate(self, button):
        """
        set sampling rate of the interface
        """
        self.ecpt['samplerate'] = self.samplingRateSb.get_value()

    def on_btn_return_clicked(self, button):
        """@fn on_btn_return_clicked
            Callback que se genera cuando se presiona el boton Return en la
            ventana principal, y este se encarga volver el toolbar con
            los botones de Loging, Playback, Quit y About.
            @param button widget de quien recibe la senal
        """
        # STOP primero
        replace_widget(self._window, self.box_toolbar, self.tb_main)
        replace_widget(self._window, self.box_content, self.vp_welcome)
        self._window.set_title(config.SW_NAME + ' - Welcome')
        if self.EstateDialogMonitor:
            self.dialog_monitor.dialog.destroy()
        self.dialog_monitor = None

    def on_btn_scan_ports_clicked(self, button):
        """@fn on_btn_scan_ports_clicked
            Callback que se genera cuando se presiona el boton Scan Ports en la
            ventana principal una vez se haya realizado la configuracion
            del tipo de login que se va realizar, y lo que hace es actualizar
            el combobox con los puertos activos.
            @param button widget de quien recibe la senal
        """
        self.ls_serial_port.clear()
        ports = get_suitable_ports(baud=115200, mode='quiet')
        for p in ports:
            self.ls_serial_port.append([p])
        self.cb_serial_port.set_active(0)

    def on_btn_open_ss_clicked(self, button):
        """@fn on_btn_open_ss_clicked
            Callback que se genera cuando se presiona el boton Open en la
            ventana principal de Loging, este permite llmar a una ventana
            de dialogo que permite al usuario abrir una configuracion previa,
            cargando un archivo .json y ademas cuando se cargar correctamente
            el .json indicado por el usuario este callback establece las
            configuraciones guardadas.
            @param button widget de quien recibe la senal
        """
        dialogs.OpenSSDialog(self)
        if self.options:
            if 'mainwindow_options' in self.options:
                set_new_options(
                    self.builder_file,
                    self._builder,
                    self.options['mainwindow_options']
                    )
            if 'logingmodedialog_options' in self.options:
                set_new_options(
                    self.dialog_monitor.builder_file,
                    self.dialog_monitor.builder,
                    self.options['logingmodedialog_options'])
            #self.btn_set.set_sensitive(True)
            self.dialog_monitor.buttonset.set_sensitive(True)
            self.logging_mode()

    def on_btn_save_ss_clicked(self, button):
        """@fn on_btn_save_ss_clicked
            Callback que se genera cuando se presiona el boton Save en la
            ventana principal de Loging, este permite llamar a una ventana
            de dialogo que permite al usuario guardar un configuracion de
            registro que desea en un archivo .json
            @param button widget de quien recibe la senal
        """
        self.update_spinbuttons()
        self.options['mainwindow_options'] = prepare_options(
            self.builder_file,
            self._builder
            )
        self.options['logingmodedialog_options'] = prepare_options(
            self.dialog_monitor.builder_file,
            self.dialog_monitor.builder
            )
        dialogs.SaveSSDialog(self.options)

    def on_btn_ss_clicked(self, button):
        """@fn on_btn_ss_clicked
            Callback que se genera cuando se presiona el boton Setup en la
            ventana principal de Loging, este permite llamar a una ventana
            de dialogo con todas las heramientas de configuracion que el
            usuario puede configurar para realizar un registro de datos.
            @param button widget de quien recibe la senal
        """
        #array to figure it out if is needed to reload interface
        self.postOrders = {'reload': False}
        #have a backup of options
        currentOptions = copy.copy(self.options)
        while True:
            #call dialog
            try:
                dialogs.SSDialog(
                    self.options,
                    self.postOrders,
                    self.ecpt,
                    self.s_connection.playing,
                    external_buffer=self.global_buffer
                    )
            except:
                dialogs.SSDialog(
                    self.options,
                    self.postOrders,
                    self.ecpt,
                    external_buffer=self.global_buffer
                    )
            if not self.postOrders['reload']:
                break
            #reset options
            self.options = copy.copy(currentOptions)
            self.postOrders['reload'] = False
        if self.options:
            self.logging_mode()
            #hide pause and record buttons
            self.btn_pause.hide()
            self.btn_record.hide()

            self.exp_logging_mode.set_expanded(False)
            self.dialog_monitor.buttonset.set_sensitive(True)
            #update name in name label
            self.dialog_monitor.change_file_name()
        else:
            self.show_info('Please create or load a project')

    def on_btn_play_clicked(self, buttton):
        """@fn on_btn_play_clicked
            Callback que se genera cuando se presiona el boton de Play en la
            ventana principal de loging, este permite ya inciar el registros de
            los datos y visualizacion de las diferentes variables en la
            interfaz principal.
            @param button widget de quien recibe la senal
        """
        indexportcom = self.cb_serial_port.get_active()
        self.portcom = self.cb_serial_port.get_active_text()
        indexbaudrate = self.cb_baudrate.get_active()
        self.baudrate = self.ls_baudrate.get_value(
            self.cb_baudrate.get_active_iter(),
            0
            )

        self.update_spinbuttons()
        self.logging_mode()

        if self.s_connection is None:
            if self._init_connection():
                self.time = time.time()
                self.s_connection.start()
                self.s_connection.play(self.time)
                self.playing(True)
                self.cb_serial_port.set_active(indexportcom)
                self.cb_baudrate.set_active(indexbaudrate)

                self.options['logingmodedialog_options'] = prepare_options(
                    self.dialog_monitor.builder_file,
                    self.dialog_monitor.builder
                    )

                if self.options['config_options']['radiobuttons']['rad_pt_service']:
                    self.log.debug(
                        '756_jdb filename: %s',
                        self.record_filename
                        )
                    self.record = DataBase(self.record_filename, self.options)
                    self.record.write_header()
                    # save data from the start of the session
                    self.s_connection.record(
                        True,
                        self.record.write_data,
                        #self.record.write_memory_data,
                        self.record.write_data2
                        )
                    self.record_warning(False)
                    #  hide pause and record buttons
                    self.btn_pause.hide()
                    self.btn_record.hide()

                if self.dialog_monitor.radtime.get_active():
                    self.dialog_monitor.raddepth.set_sensitive(False)

                if self.dialog_monitor.raddepth.get_active():
                    self.dialog_monitor.radtime.set_sensitive(False)
                #  hide pause and record buttons
                self.btn_pause.hide()
                self.btn_record.hide()

            else:
                self.s_connection = None
                return
        else:
            msg = 'There is already a serial connection'
            self.show_info(msg)

    def on_btn_stop_clicked(self, button):
        """@fn on_btn_stop_clicked
            Callback que se genera cuando se presiona el boton de Stop en la
            ventana principal de loging, este permite detener el registro
            de datos y la visualizaciobn de la infromacion, tambien de detener
            la conexion serial.
            @param button widget de quien recibe la senal
        """
        msg = 'Do you want to stop recording?'
        if self.btn_record.get_active():
            if self.show_question(msg):
                self.btn_record.set_active(False)
                if self.record:
                    self.record = None
                    self.record_filename = None
            else:
                return
        msg = 'Do you want to stop logging?'
        if not self.show_question(msg):
            return
        if self.s_connection:
            self.s_connection.disconnect()
            if self.record:
                self.s_connection.record(False)
                self.record.end_writing()
            self.s_connection = None
        self.playing(False)
        self.btn_pause.set_active(False)
        self.ct_widget.set_autoscroll(False)
        self.state_record = False
        self.exp_logging_mode.set_expanded(False)
        if self.dialog_monitor.radtime.get_active():
            self.dialog_monitor.raddepth.set_sensitive(True)

        if self.dialog_monitor.raddepth.get_active():
            self.dialog_monitor.radtime.set_sensitive(True)
        #  hide pause and record buttons
        self.btn_pause.hide()
        self.btn_record.hide()

        #self.logging_mode()

    def on_btn_record_toggled(self, button):
        """@fn on_btn_record_toggled
            Callback que se genera cuando se presiona el boton de Record en la
            ventana principal de loging, este permite grabar el registro de
            datos en una base de datos .jdb, abriendo una ventana de dialogo la
            cual permite indicarle un nombre a la base de datos.
            @param button widget de quien recibe la senal
        """
        if self.btn_record.get_active():
            #dialogs.RecordDialog(self)  #ojojoj
            if self.record_filename:
                self.btn_config_tracks.set_sensitive(False)
                if self.record is None:
                    if self.options['config_options']['radiobuttons']['rad_pt_service']:
                        self.log.debug(
                            '846_jdb filename: %s',
                            self.record_filename
                            )
                        self.record = DataBase(
                            self.record_filename,
                            self.options
                            )
                        self.record.write_header()  # this is important

                if self.options['config_options']['radiobuttons']['rad_pt_service']:
                    self.s_connection.record(
                        True,
                        self.record.write_data,
                        #self.record.write_memory_data,
                        self.record.write_data2
                        )
                self.record_warning(False)
                self.state_record = True
            else:
                self.btn_record.set_active(False)
                self.state_record = False
        else:
            self.btn_config_tracks.set_sensitive(True)
            self.record_warning(True)
            self.state_record = False
            if self.record:
                self.s_connection.record(False)
                self.record.end_writing()
                self.record = None
                #self.record_filename = None

    def on_btn_config_tracks_clicked(self, button):
        """@fn on_btn_config_tracks_clicked
            Callback que se genera cuando se presiona el boton de Tracks en la
            ventana principal de loging, este permite realizar nuevas
            configuraciones al modo de visualizacion de las graficas de lineas,
            como colores, escala, etc, abriendo en una ventana de dialogo las
            configiraciones basicas para las garficas de datos.
            @param button widget de quien recibe la senal
        """
        if self.options:
            dialogs.ConfigTracksDialog(self.options)

            if self.options['config_options']['radiobuttons']['rad_pt_service']:
                if self.options['config_options']['radiobuttons']['rad_pt_5_tracks']:
                    config_pt_d5_composetrack(self.ct_widget, self.options['tracks_options'])
                if self.options['config_options']['radiobuttons']['rad_pt_6_tracks']:
                    config_pt_d6_composetrack(self.ct_widget, self.options['tracks_options'])

            gobject.idle_add(self.ct_widget.draw)

    def new_config_from_monitor(self):
        """@fn new_config_from_monitor
            Encargado de ajustar configuraciones
        """
        if self.options:
            self.options['logingmodedialog_options'] = prepare_options(self.dialog_monitor.builder_file, self.dialog_monitor.builder)

            if self.options['config_options']['radiobuttons']['rad_pt_service']:
                if self.options['config_options']['radiobuttons']['rad_pt_5_tracks']:
                    if self.k == config.TRACKING_BY_TIME:
                        config_pt_d5_composetrack(self.ct_widget, self.options['tracks_options'])
                    else:
                        if self.Down:
                            config_pt_d5_composetrack(self.ct_widget, self.options['tracks_options'], True)
                            if self.s_connection:
                                self.s_connection.change_reference(self.DeptReference)
                                self.s_connection.change_up_down(self.Down)
                        else:
                            config_pt_d5_composetrack(self.ct_widget, self.options['tracks_options'], False)
                            if self.s_connection:
                                self.s_connection.change_reference(self.DeptReference)
                                self.s_connection.change_up_down(self.Down)

                if self.options['config_options']['radiobuttons']['rad_pt_6_tracks']:
                    if self.k == config.TRACKING_BY_TIME:
                        config_pt_d6_composetrack(self.ct_widget, self.options['tracks_options'])
                    else:
                        if self.Down:
                            config_pt_d6_composetrack(self.ct_widget, self.options['tracks_options'], True)
                            if self.s_connection:
                                self.s_connection.change_reference(self.DeptReference)
                                self.s_connection.change_up_down(self.Down)

                        else:
                            config_pt_d6_composetrack(self.ct_widget, self.options['tracks_options'], False)
                            if self.s_connection:
                                self.s_connection.change_reference(self.DeptReference)
                                self.s_connection.change_up_down(self.Down)

            gobject.idle_add(self.ct_widget.draw)

    def on_btn_autoscroll_toggled(self, button):
        """@fn on_btn_autoscroll_toggled
            Callback que se genera cuando se presiona el boton de AutoScroll en
            la ventana principal de loging, este permite que las graficas de
            las lineas de datos se actualize automaticamente cambiando la
            escala segun lleguen los datos.
            @param button widget de quien recibe la senal
        """
        if self.btn_autoscroll.get_active():
            if self.ct_widget:
                self.ct_widget.set_autoscroll(True)
        else:
            if self.ct_widget:
                self.ct_widget.set_autoscroll(False)

    def on_btn_connection_checker_toggled(self, button):
        """@fn on_btn_connection_checker_toggle
            Callback que se genera cuando se presiona el boton de Connection en
            la ventana principal de loging, cargar en la interfaz principal del
            loging dos widgets de texto donde apareran los mensajes de
            sincronizacion, fallas y tramas que esten llegando via puerto
            serial.
            @param button widget de quien recibe la senal
        """
        #ARREGLAR ESTO, MUY ACOPLADO
        if self.btn_connection_checker.get_active():
            self.debugging = True
            replace_widget(self._window, self.box_debugger_slot, self.box_debugger)
        else:
            self.debugging = False
            replace_widget(self._window, self.box_debugger_slot, None)
        self.btn_pause.hide()
        self.btn_record.hide()

    def on_btn_shock_protect_toggled(self, button):
        """@fn on_btn_shock_protect_toggled
            Callback encargado de realizar operaciones de proteccion al
            protoclo de comunicacion serial, apagndolo
            durante este proceso y reactiavndolo.
            @param button Widget de donde viene el evento
        """
        if self.btn_shock_protect.get_active():
            self.btn_stop.set_sensitive(False)
            self.btn_record.set_sensitive(False)
            self.btn_autoscroll.set_sensitive(False)
            self.btn_config_tracks.set_sensitive(False)
            self.btn_connection_checker.set_sensitive(False)
            if self.s_connection:
                self.s_connection.disconnect()
                self.s_connection = None
        else:
            self.btn_stop.set_sensitive(True)
            self.btn_record.set_sensitive(True)
            self.btn_autoscroll.set_sensitive(True)
            self.btn_config_tracks.set_sensitive(True)
            self.btn_connection_checker.set_sensitive(True)
            if self.s_connection is None:
                if self._init_connection():
                    self.s_connection.start()
                    self.s_connection.play(self.time)
                    if self.btn_record.get_active():
                        if self.record:
                            if self.options['config_options']['radiobuttons']['rad_pt_service']:
                                self.s_connection.record(
                                    True,
                                    self.record.write_data,
                                    #self.record.write_memory_data
                                    )
                else:
                    self.s_connection = None
                    return

    def on_btn_import_db_clicked(self, button):
        """@fn on_btn_import_db_clicked
            Callback que se genera cuando se presiona el boton de ImportDB en
            la ventana principal de Playback, este permite crear una ventana de
            dialogo que permite al usuario abrir una base de datos .jdb con un
            registro previo de datos y los carga para que que usario pueda
            visualizar todos estos datos nuevamente.
            @param button widget de quien recibe la senal
        """
        dialogs.ImportDBDialog(self)
        if self.db:
            #code for depth_drive up compatibility
            self.options = self.db['options']
            max_span = max(self.db['data']['index'])
            if '_up_' in os.path.basename(self.db['filename']):
                canvas_heigth = (5.375 * max_span) + 200
                self.set_options(set_h=canvas_heigth)
            else:
                self.set_options()
            #=======================================
            self.playback_mode()
            msg = 'Playing back: ' + str(self.db['filename']) + '\n'
            msg += 'Datetime: ' + str(self.db['options']['datetime'])
            self.lbl_pb_info.set_text(msg)

            if self.options['config_options']['radiobuttons']['rad_pt_service']:
                if self.options['config_options']['radiobuttons']['rad_pt_5_tracks']:
                    dump_pt_d5_composetrack(self.ct_widget, self.db['data'])
                if self.options['config_options']['radiobuttons']['rad_pt_6_tracks']:
                    dump_pt_d6_composetrack(self.ct_widget, self.db['data'])

                self.ct_widget.set_autoscroll(False)
                self.btn_export_las.set_sensitive(True)
                gobject.idle_add(self.ct_widget.draw)
            else:
                msg = "Playback only load database Parameter Tool Service"
                self.show_error(msg)

    def on_btn_export_las_clicked(self, button):
        """@fn on_btn_export_las_clicked
            Callback que se genera cuando se presiona el boton de toLAS en la
            ventana principal de Playback
            @param button widget de quien recibe la senal
        """
        if not self.db or not self.options:
            return
        if self.options['logingmodedialog_options']['radiobuttons']['rad_real_time']:
            sampling_rate = int(self.options['logingmodedialog_options']['comboboxes']['cb_sampling_rate'][0]) / 1000.0
            index = 'TIME'
        else:
            sampling_rate = 1.0 / int(self.options['logingmodedialog_options']['comboboxes']['cb_sampling_rate'][0])
            index = 'DEPTH'

        if self.options['config_options']['radiobuttons']['rad_pt_service']:
            if self.options['config_options']['radiobuttons']['rad_pt_5_tracks']:
                mode = 'pt5t'
            if self.options['config_options']['radiobuttons']['rad_pt_6_tracks']:
                mode = 'pt6t'
        else:
            msg = "Export to LAS only database Parameter Tool Service"
            self.show_error(msg)
        metadata = {
            'company': self.options['config_options']['entries']['ent_company'],
            'field': self.options['config_options']['entries']['ent_field'],
            'well': self.options['config_options']['entries']['ent_well'],
            'location': self.options['config_options']['entries']['ent_location'],
            'county': self.options['config_options']['entries']['ent_county'],
            'state': self.options['config_options']['entries']['ent_state'],
            'country': self.options['config_options']['entries']['ent_country'],
            'service_company': self.options['config_options']['entries']['ent_service_company'],
            'well_id': self.options['config_options']['entries']['ent_well_id'],
            'ercb': self.options['config_options']['entries']['ent_ercb'],
            'latitude': self.options['config_options']['spinbuttons']['sb_latitude'],
            'longitude': self.options['config_options']['spinbuttons']['sb_longitude'],
            'zero_reference': (self.options['config_options']['comboboxes']['cb_zero_reference'][0],
                               self.options['config_options']['spinbuttons']['sb_zero_reference']),
            'elevation': self.options['config_options']['spinbuttons']['sb_elevation'],
            'index': index,
            'datetime': self.options['datetime'],
            'sampling_rate': sampling_rate,
            'mode': mode,
            'down': self.options['logingmodedialog_options']['radiobuttons']['rad_down_mode'],
            'filename': self.db['filename']}

        ##creacion MUCHO OJO, FALTA
        LasFile(metadata, self.db['raw_data'])

        ##finalizacion

        self.show_info('Export to LAS was successful!')

    def on_btn_export_pdf_clicked(self, button):
        """
        NO IMPLEMENTADO: exportar datos a pdf
        """
        pass

    def on_btn_pt_set_st_clicked(self, button):
        """@fn on_btn_pt_set_st_clicked
            Callback que se genera cuando se presiona el boton de set en Filter
            Setup de Sync Threshold en la ventana principal de Loging, se
            encarga de escribir via puerto serial una valor de Umbral de
            Sincronizacion que se haya configurado en su spinbutton via puerto
            serial. Cuando el servicio que se configuro fue de Parameter Tool
            @param button widget de quien recibe la senal
        """
        self.update_spinbuttons()
        st = self.sb_pt_st.get_value_as_int()
        self.s_connection.sync_st(st)

    def on_btn_pt_set_sf_clicked(self, button):
        """@fn on_btn_pt_set_sf_clicked
            Callback que se genera cuando se presiona el boton de set en Filter
            Setup de Sync Frecuency en la ventana principal de Loging, se
            encarga de escribir via puerto serial una valor de Frecuencia de
            Sincronizacion que se haya configurado en su spinbutton via puerto
            serial. Cuando el servicio que se configuro fue de Parameter Tool
            @param button widget de quien recibe la senal
        """
        self.update_spinbuttons()
        sf = self.sb_pt_sf.get_value_as_int()
        self.s_connection.sync_sf(sf)

    def on_btn_pt_set_comp_clicked(self, button):
        """@fn on_btn_pt_set_comp_clicked
            Callback que se genera cuando se presiona el boton de set en Filter
            Setup de Comparator en la ventana principal de Loging, se encarga
            de escribir via puerto serial una valor del comparador de ventanas
            que se haya configurado en su spinbutton via puerto serial. Cuando
            el servicio que se configuro fue de Parameter Tool
            @param button widget de quien recibe la senal
        """
        self.update_spinbuttons()
        comp = self.sb_pt_comparator.get_value_as_int()
        self.s_connection.sync_comp(comp)

    def on_btn_pt_set_gn_clicked(self, button):
        """@fn on_btn_pt_set_gn_clicked
            Callback que se genera cuando se presiona el boton de set en Filter
            Setup de Gain en la ventana principal de Loging, se encarga de
            escribir via puerto serial una valor de ganancia de amplificadores
            que se haya configurado en su spinbutton via puerto serial. Cuando
            el servicio que se configuro fue de Parameter Tool
            @param button widget de quien recibe la senal
        """
        self.update_spinbuttons()
        gn = self.sb_pt_gain.get_value_as_int()
        self.s_connection.sync_gn(gn)

    def on_memory_incoming_off(self):
        """@fn on_memory_incoming_off
            Callback usado para indicar si ya no estan llegando datos de
            memoria, si no esta llegando datos lo que hace es mediante un
            indicador tipo led se mantiene apagado y esto lo hace cada vez que
            la interfaz principal este en modo de descanso
        """
        gobject.idle_add(self.img_memory_led.set_from_file, 'gui2/off_led.png')

    def on_memory_incoming_on(self):
        """@fn on_memory_incoming_on
            Callback usado para indicar si ya estan llegando datos de memoria,
            si estan llegando datos lo que hace es mediante un indicador tipo
            led lo enciende y esto lo hace cada vez que la interfaz principal
            este en modo de descanso
        """
        gobject.idle_add(self.img_memory_led.set_from_file, 'gui2/on_led.png')

    def on_tw_messages_size_allocate(self, widget, event, data=None):
        """@fn on_tw_messages_size_allocate
            Callback para realizar autoscroll de la ventana de mensages, alli
            es donde llegan mensajes de la conexion serial si el
            boton de Connection se ha activado.
            @param widget De quien recibe la senal
            @param event el tipo de evento que esta ocurriendo
            @param data None
        """
        adj = self.sw_messages.get_vadjustment()
        adj.set_value(adj.upper - adj.page_size)

    def on_tw_trames_size_allocate(self, widget, event, data=None):
        """@fn on_tw_trames_size_allocate
            Callback para realizar autoscroll de la ventana de tramas, alli es
            donde llegan tramas de la conexion serial si el
            boton de Connection se ha activado.
            @param widget De quien recibe la senal
            @param event el tipo de evento que esta ocurriendo
            @param data None
        """
        adj = self.sw_trames.get_vadjustment()
        adj.set_value(adj.upper - adj.page_size)

    def on_window_quit(self, *args):
        """@fn on_window_quit
            Callback que se genera cuando se trata de cerrar la ventana
            principal del software, si se estaba guardando informacion,
            boton Record presionado, el crea una ventan indicado si se
            desea para la el registro, de igual forma si esta el boton Play
            presionado, y finalmente una ventana de dialogo para indicar si
            realmente se desea salir del software.
        """
        if self.btn_record.get_active():
            msg = 'Do you want to stop recording?'
            if self.show_question(msg):
                self.btn_record.set_active(False)
            else:
                return True

        if self.btn_stop.get_sensitive():
            msg = 'Do you want to stop logging?'
            if self.show_question(msg):
                if self.s_connection:
                    self.s_connection.disconnect()
                    self.s_connection = None
            else:
                return True
        msg = "Are you sure you want to exit?"
        if self.show_question(msg):
            gtk.main_quit()
        else:
            return True

    def set_labels(self):
        """set labels of different widgets like the ones in monitor widget
        and in setup """
        # MONITOR WIDGET LABELS

        self.label5.set_text("Temp: ")
        self.label6.set_text("Flow: ")
        self.label7.set_text("")
        self.label8.set_text("Pressure: ")
        self.label9.set_text("Acc X: ")
        self.label10.set_text("Acc Y: ")
        self.label11.set_text("Acc Z: ")
        self.label12.set_text("")
        self.label13.set_text("Dummy: ")
        self.label14.set_text("")
        self.label15.set_text("")
        self.label16.set_text("Tension: ")
        self.label19.set_text("Depth: ")

        # SETUP WIDGET LABELS

if __name__ == '__main__':
    window = MainWindow()
    window.show()
    gtk.main()
