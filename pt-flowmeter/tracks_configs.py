# -*- encoding: utf-8 -*-
'''
Emdyp.me
Revisado
Script: Tipo A
(valores de configuracion)
[Grafico]
'''

from gui.tracks import ComposeTrackWidget

############################################

#    COMPOSICION DE TRACKS

############################################


def new_pt_d5_composetrack(options, down, **kwargs):
    """
        Funcion que realiza la configuracion de las pistas de dibujo de
        graficas para el servicio de Parameter Tool, se encarga de preparar
        todas las pistas segun las configuraciones que lleguen en el
        diccionario options y segun si se se ha configurado
        para que se muestre 5 pistas de graficas en el servicio de Loging.
        @param options Diccionario con las opciones de configuracion del
        usuario (Diccionario)
        @param down Parametro que indica si se va garfica de arriba hacia abajo
        o vivecerza (BOOLEAN)
        @return Objeto de la clase ComposeTrackWidget con la composicion de
        pistas.
    """
    ct_widget = ComposeTrackWidget()

    #compatibility code to fix depth_drive_up plotting
    if 'h' in kwargs:
        cust = kwargs['h']
    else:
        cust = -1

###########################
# TRACK 0
###########################
    ct_widget.add_wtrack(0)

###########################
# SIGNAL Temperature
###########################
    ct_widget.add_line(0, 0)
    line_options = {
        'width': int(options['spinbuttons']['sb_pt_d5_gr_line']),
        'color': options['colorbuttons']['color_pt_d5_gr_line'][0],
        'dashed': options['checkbuttons']['chk_pt_d5_gr_dash_line'],
        'last_value': options['checkbuttons']['chk_pt_d5_gr_last_value'],
        'span': (
            int(options['spinbuttons']['sb_pt_d5_gr_span1']),
            int(options['spinbuttons']['sb_pt_d5_gr_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d5_gr_wrapper']),
        'w_color': (0, 0, 0),'down': down, 'erase_data': False}
    labelgraph_options = {
        'bg_color': '#eee',
        'size_request': (-1, 20),
        'labels': (
            int(options['spinbuttons']['sb_pt_d5_gr_span1']),
            int(options['spinbuttons']['sb_pt_d5_gr_span2'])),
        'line_name': 'Temperature',
        'line_name_color': options['colorbuttons']['color_pt_d5_gr_line'][0],
        'labels_color': (0, 0, 0)}
    ct_widget.set_line_options(0, 0, line_options, labelgraph_options)

    track_options = {
        'bg_color': options['colorbuttons']['color_pt_d5_t1_bg'][1],
        'size_request': (-1, cust),
        'width_scale': 100,
        'height_scale': int(options['spinbuttons']['sb_pt_d5_height_scale']),
        'h_grids': int(options['spinbuttons']['sb_pt_d5_t1_hgrids']),
        'v_grids': int(options['spinbuttons']['sb_pt_d5_t1_vgrids']),
        'grid_line_width': 1,
        'grid_dashed': options['checkbuttons']['chk_pt_d5_t1_grid_dashed'],
        'strong_grid': 10,
        'grid_color': options['colorbuttons']['color_pt_d5_t1_grid'][0],
        'strong_grid_color': options['colorbuttons']['color_pt_d5_t1_strong_grid'][0],
        'begin_scrolling': 40, 'pointer_label': False, 'autoscroll': True}
    ct_widget.set_track_options(0, track_options)

    ct_widget.add_ttrack(0)
    default_textgraph_options = {
        'step': int(options['spinbuttons']['sb_pt_d5_timedepth_step']),
        'height_scale': int(options['spinbuttons']['sb_pt_d5_height_scale'])
        }
    ct_widget.set_ttrack_options(
        0,
        default_textgraph_options
        )

###########################
# TRACK 1
###########################
    ct_widget.add_wtrack(1)

###########################
# SIGNAL Pressure
###########################
    ct_widget.add_line(1, 0)
    line_options = {
        'width': int(options['spinbuttons']['sb_pt_d5_temp2_line']),
        'color': options['colorbuttons']['color_pt_d5_temp2_line'][0],
        'dashed': options['checkbuttons']['chk_pt_d5_temp2_dash_line'],
        'last_value': options['checkbuttons']['chk_pt_d5_temp2_last_value'],
        'span': (
            int(options['spinbuttons']['sb_pt_d5_temp2_span1']),
            int(options['spinbuttons']['sb_pt_d5_temp2_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d5_temp2_wrapper']),
        'w_color': (0, 0, 0), 'down': down, 'erase_data': False}
    labelgraph_options = {
        'bg_color': '#eee',
        'size_request': (-1, 20),
        'labels': (
            int(options['spinbuttons']['sb_pt_d5_temp2_span1']),
            int(options['spinbuttons']['sb_pt_d5_temp2_span2'])),
        'line_name': 'Pressure',
        'line_name_color': options['colorbuttons']['color_pt_d5_temp2_line'][0],
        'labels_color': (0, 0, 0)}
    ct_widget.set_line_options(1, 0, line_options, labelgraph_options)

    track_options = {
        'bg_color': options['colorbuttons']['color_pt_d5_t2_bg'][1],
        'size_request': (-1, cust),
        'width_scale': 100,
        'height_scale': int(options['spinbuttons']['sb_pt_d5_height_scale']),
        'h_grids': int(options['spinbuttons']['sb_pt_d5_t2_hgrids']),
        'v_grids': int(options['spinbuttons']['sb_pt_d5_t2_vgrids']),
        'grid_line_width': 1,
        'grid_dashed': options['checkbuttons']['chk_pt_d5_t2_grid_dashed'],
        'strong_grid': 10,
        'grid_color': options['colorbuttons']['color_pt_d5_t2_grid'][0],
        'strong_grid_color': options['colorbuttons']['color_pt_d5_t2_strong_grid'][0],
        'begin_scrolling': 40, 'pointer_label': False, 'autoscroll': True}
    ct_widget.set_track_options(1, track_options)

###########################
# TRACK 2
###########################
    ct_widget.add_wtrack(2)

###########################
# SIGNAL Acc X
###########################
    ct_widget.add_line(2, 0)
    line_options = {
        'width': int(options['spinbuttons']['sb_pt_d5_pres_line']),
        'color': options['colorbuttons']['color_pt_d5_pres_line'][0],
        'dashed': options['checkbuttons']['chk_pt_d5_pres_dash_line'],
        'last_value': options['checkbuttons']['chk_pt_d5_pres_last_value'],
        'span': (
            int(options['spinbuttons']['sb_pt_d5_pres_span1']),
            int(options['spinbuttons']['sb_pt_d5_pres_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d5_pres_wrapper']),
        'w_color': (0, 0, 0), 'down': down, 'erase_data': False}
    labelgraph_options = {
        'bg_color': '#eee',
        'size_request': (-1, 20),
        'labels': (
            int(options['spinbuttons']['sb_pt_d5_pres_span1']),
            int(options['spinbuttons']['sb_pt_d5_pres_span2'])),
        'line_name': 'Acc X',
        'line_name_color': options['colorbuttons']['color_pt_d5_pres_line'][0],
        'labels_color': (0, 0, 0)}
    ct_widget.set_line_options(2, 0, line_options, labelgraph_options)

###########################
# SIGNAL Acc Y
###########################
    ct_widget.add_line(2, 1)
    line_options = {
        'width': int(options['spinbuttons']['sb_pt_d5_res1_line']),
        'color': options['colorbuttons']['color_pt_d5_res1_line'][0],
        'dashed': options['checkbuttons']['chk_pt_d5_res1_dash_line'],
        'last_value': options['checkbuttons']['chk_pt_d5_res1_last_value'],
        'span': (
            int(options['spinbuttons']['sb_pt_d5_res1_span1']),
            int(options['spinbuttons']['sb_pt_d5_res1_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d5_res1_wrapper']),
        'w_color': (0, 0, 0), 'down': down, 'erase_data': False}
    labelgraph_options = {
        'bg_color': '#eee',
        'size_request': (-1, 20),
        'labels': (
            int(options['spinbuttons']['sb_pt_d5_res1_span1']),
            int(options['spinbuttons']['sb_pt_d5_res1_span2'])),
        'line_name': 'Acc Y',
        'line_name_color': options['colorbuttons']['color_pt_d5_res1_line'][0],
        'labels_color': (0, 0, 0)}
    ct_widget.set_line_options(2, 1, line_options, labelgraph_options)

###########################
# SIGNAL Acc Z
###########################
    ct_widget.add_line(2, 2)
    line_options = {
        'width': int(options['spinbuttons']['sb_pt_d5_res2_line']),
        'color': options['colorbuttons']['color_pt_d5_res2_line'][0],
        'dashed': options['checkbuttons']['chk_pt_d5_res2_dash_line'],
        'last_value': options['checkbuttons']['chk_pt_d5_res2_last_value'],
        'span': (
            int(options['spinbuttons']['sb_pt_d5_res2_span1']),
            int(options['spinbuttons']['sb_pt_d5_res2_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d5_res2_wrapper']),
        'w_color': (0, 0, 0), 'down': down, 'erase_data': False}
    labelgraph_options = {
        'bg_color': '#eee',
        'size_request': (-1, 20),
        'labels': (
            int(options['spinbuttons']['sb_pt_d5_res2_span1']),
            int(options['spinbuttons']['sb_pt_d5_res2_span2'])),
        'line_name': 'Acc Z',
        'line_name_color': options['colorbuttons']['color_pt_d5_res2_line'][0],
        'labels_color': (0, 0, 0)}
    ct_widget.set_line_options(2, 2, line_options, labelgraph_options)

    track_options = {
        'bg_color': options['colorbuttons']['color_pt_d5_t3_bg'][1],
        'size_request': (-1, cust),
        'width_scale': 100,
        'height_scale': int(options['spinbuttons']['sb_pt_d5_height_scale']),
        'h_grids': int(options['spinbuttons']['sb_pt_d5_t3_hgrids']),
        'v_grids': int(options['spinbuttons']['sb_pt_d5_t3_vgrids']),
        'grid_line_width': 1,
        'grid_dashed': options['checkbuttons']['chk_pt_d5_t3_grid_dashed'],
        'strong_grid': 10,
        'grid_color': options['colorbuttons']['color_pt_d5_t3_grid'][0],
        'strong_grid_color': options['colorbuttons']['color_pt_d5_t3_strong_grid'][0],
        'begin_scrolling': 40, 'pointer_label': False, 'autoscroll': True}
    ct_widget.set_track_options(2, track_options)

###########################
# TRACK 3
###########################
    ct_widget.add_wtrack(3)

###########################
# SIGNAL Flow
###########################
    ct_widget.add_line(3, 0)
    line_options = {
        'width': int(options['spinbuttons']['sb_pt_d5_res3_line']),
        'color': options['colorbuttons']['color_pt_d5_res3_line'][0],
        'dashed': options['checkbuttons']['chk_pt_d5_res3_dash_line'],
        'last_value': options['checkbuttons']['chk_pt_d5_res3_last_value'],
        'span': (
            int(options['spinbuttons']['sb_pt_d5_res3_span1']),
            int(options['spinbuttons']['sb_pt_d5_res3_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d5_res3_wrapper']),
        'w_color': (0, 0, 0),'down': down,'erase_data': False}
    labelgraph_options = {
        'bg_color': '#eee',
        'size_request': (-1, 20),
        'labels': (
            int(options['spinbuttons']['sb_pt_d5_res3_span1']),
            int(options['spinbuttons']['sb_pt_d5_res3_span2'])),
        'line_name': 'Flow',
        'line_name_color': options['colorbuttons']['color_pt_d5_res3_line'][0],
        'labels_color': (0, 0, 0)}
    ct_widget.set_line_options(3, 0, line_options, labelgraph_options)

    track_options = {
        'bg_color': options['colorbuttons']['color_pt_d5_t4_bg'][1],
        'size_request': (-1, cust),
        'width_scale': 100,
        'height_scale': int(options['spinbuttons']['sb_pt_d5_height_scale']),
        'h_grids': int(options['spinbuttons']['sb_pt_d5_t4_hgrids']),
        'v_grids': int(options['spinbuttons']['sb_pt_d5_t4_vgrids']),
        'grid_line_width': 1,
        'grid_dashed': options['checkbuttons']['chk_pt_d5_t4_grid_dashed'],
        'strong_grid': 10,
        'grid_color': options['colorbuttons']['color_pt_d5_t4_grid'][0],
        'strong_grid_color': options['colorbuttons']['color_pt_d5_t4_strong_grid'][0],
        'begin_scrolling': 40, 'pointer_label': False, 'autoscroll': True}
    ct_widget.set_track_options(3, track_options)

    ct_widget.add_ttrack(3)
    default_textgraph_options = {
        'step': int(options['spinbuttons']['sb_pt_d5_timedepth_step']),
        'height_scale': int(options['spinbuttons']['sb_pt_d5_height_scale'])
        }
    ct_widget.set_ttrack_options(
        3,
        default_textgraph_options
        )

###########################
# TRACK 4
###########################
    ct_widget.add_wtrack(4)

###########################
# SIGNAL Tension
###########################
    ct_widget.add_line(4, 0)
    line_options = {
        'width': int(options['spinbuttons']['sb_pt_d5_ten_line']),
        'color': options['colorbuttons']['color_pt_d5_ten_line'][0],
        'dashed': options['checkbuttons']['chk_pt_d5_ten_dash_line'],
        'last_value': options['checkbuttons']['chk_pt_d5_ten_last_value'],
        'span': (
            int(options['spinbuttons']['sb_pt_d5_ten_span1']),
            int(options['spinbuttons']['sb_pt_d5_ten_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d5_ten_wrapper']),
        'w_color': (0, 0, 0), 'down': down, 'erase_data': False}
    labelgraph_options = {
        'bg_color': '#eee',
        'size_request': (-1, 20),
        'labels': (
            int(options['spinbuttons']['sb_pt_d5_ten_span1']),
            int(options['spinbuttons']['sb_pt_d5_ten_span2'])),
        'line_name': 'Tension',
        'line_name_color': options['colorbuttons']['color_pt_d5_ten_line'][0],
        'labels_color': (0, 0, 0)}
    ct_widget.set_line_options(4, 0, line_options, labelgraph_options)

    track_options = {
        'bg_color': options['colorbuttons']['color_pt_d5_t5_bg'][1],
        'size_request': (-1, cust),
        'width_scale': 100,
        'height_scale': int(options['spinbuttons']['sb_pt_d5_height_scale']),
        'h_grids': int(options['spinbuttons']['sb_pt_d5_t5_hgrids']),
        'v_grids': int(options['spinbuttons']['sb_pt_d5_t5_vgrids']),
        'grid_line_width': 1,
        'grid_dashed': options['checkbuttons']['chk_pt_d5_t5_grid_dashed'],
        'strong_grid': 10,
        'grid_color': options['colorbuttons']['color_pt_d5_t5_grid'][0],
        'strong_grid_color': options['colorbuttons']['color_pt_d5_t5_strong_grid'][0],
        'begin_scrolling': 40, 'pointer_label': False, 'autoscroll': True}
    ct_widget.set_track_options(4, track_options)

    return ct_widget


def new_pt_d6_composetrack(options, down, **kwargs):
    """
        Funcion que realiza la configuracion de las pistas de dibujo de
        graficas para el servicio de Parameter Tool, se encarga de preparar
        todas las pistas segun las configuraciones que lleguen en el
        diccionario options y segun si se se ha configurado
        para que se muestre 6 pistas de graficas en el servicio de Loging.
        @param options Diccionario con las opciones de configuracion del
        usuario (Diccionario)
        @param down Parametro que indica si se va garfica de arriba hacia
        abajo o vivecerza (BOOLEAN)
        @return Objeto de la clase ComposeTrackWidget con la composicion de
        pistas.
    """
    ct_widget = ComposeTrackWidget()

    #compatibility code to fix depth_drive_up plotting
    if 'h' in kwargs:
        cust = kwargs['h']
    else:
        cust = -1

###########################
# TRACK 0
###########################
    ct_widget.add_wtrack(0)

###########################
# SIGNAL Temperature
###########################
    ct_widget.add_line(0, 0)
    line_options = {
        'width': int(options['spinbuttons']['sb_pt_d6_gr_line']),
        'color': options['colorbuttons']['color_pt_d6_gr_line'][0],
        'dashed': options['checkbuttons']['chk_pt_d6_gr_dash_line'],
        'last_value': options['checkbuttons']['chk_pt_d6_gr_last_value'],
        'span': (
            int(options['spinbuttons']['sb_pt_d6_gr_span1']),
            int(options['spinbuttons']['sb_pt_d6_gr_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d6_gr_wrapper']),
        'w_color': (0, 0, 0), 'down': down, 'erase_data': False}
    labelgraph_options = {
        'bg_color': '#eee',
        'size_request': (-1, 20),
        'labels': (
            int(options['spinbuttons']['sb_pt_d6_gr_span1']),
            int(options['spinbuttons']['sb_pt_d6_gr_span2'])),
        'line_name': 'Temperature',
        'line_name_color': options['colorbuttons']['color_pt_d6_gr_line'][0],
        'labels_color': (0, 0, 0)}
    ct_widget.set_line_options(0, 0, line_options, labelgraph_options)

    track_options = {
        'bg_color': options['colorbuttons']['color_pt_d6_t1_bg'][1],
        'size_request': (-1, cust),  # caution
        'width_scale': 100,
        'height_scale': int(options['spinbuttons']['sb_pt_d6_height_scale']),
        'h_grids': int(options['spinbuttons']['sb_pt_d6_t1_hgrids']),
        'v_grids': int(options['spinbuttons']['sb_pt_d6_t1_vgrids']),
        'grid_line_width': 1,
        'grid_dashed': options['checkbuttons']['chk_pt_d6_t1_grid_dashed'],
        'strong_grid': 10,
        'grid_color': options['colorbuttons']['color_pt_d6_t1_grid'][0],
        'strong_grid_color': options['colorbuttons']['color_pt_d6_t1_strong_grid'][0],
        'begin_scrolling': 40, 'pointer_label': False, 'autoscroll': True}
    ct_widget.set_track_options(0, track_options)

    ct_widget.add_ttrack(0)
    default_textgraph_options = {
        'step': int(options['spinbuttons']['sb_pt_d6_timedepth_step']),
        'height_scale': int(options['spinbuttons']['sb_pt_d6_height_scale'])
        }
    ct_widget.set_ttrack_options(
        0,
        default_textgraph_options
        )

###########################
# TRACK 1
###########################
    ct_widget.add_wtrack(1)

###########################
# SIGNAL Pressure
###########################
    ct_widget.add_line(1, 0)
    line_options = {
        'width': int(options['spinbuttons']['sb_pt_d6_temp2_line']),
        'color': options['colorbuttons']['color_pt_d6_temp2_line'][0],
        'dashed': options['checkbuttons']['chk_pt_d6_temp2_dash_line'],
        'last_value': options['checkbuttons']['chk_pt_d6_temp2_last_value'],
        'span': (
            int(options['spinbuttons']['sb_pt_d6_temp2_span1']),
            int(options['spinbuttons']['sb_pt_d6_temp2_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d6_temp2_wrapper']),
        'w_color': (0, 0, 0), 'down': down, 'erase_data': False}
    labelgraph_options = {
        'bg_color': '#eee',
        'size_request': (-1, 20),
        'labels': (
            int(options['spinbuttons']['sb_pt_d6_temp2_span1']),
            int(options['spinbuttons']['sb_pt_d6_temp2_span2'])),
        'line_name': 'Pressure',
        'line_name_color': options['colorbuttons']['color_pt_d6_temp2_line'][0],
        'labels_color': (0, 0, 0)}
    ct_widget.set_line_options(1, 0, line_options, labelgraph_options)

    track_options = {
        'bg_color': options['colorbuttons']['color_pt_d6_t2_bg'][1],
        'size_request': (-1, cust),  # caution
        'width_scale': 100,
        'height_scale': int(options['spinbuttons']['sb_pt_d6_height_scale']),
        'h_grids': int(options['spinbuttons']['sb_pt_d6_t2_hgrids']),
        'v_grids': int(options['spinbuttons']['sb_pt_d6_t2_vgrids']),
        'grid_line_width': 1,
        'grid_dashed': options['checkbuttons']['chk_pt_d6_t2_grid_dashed'],
        'strong_grid': 10,
        'grid_color': options['colorbuttons']['color_pt_d6_t2_grid'][0],
        'strong_grid_color': options['colorbuttons']['color_pt_d6_t2_strong_grid'][0],
        'begin_scrolling': 40, 'pointer_label': False, 'autoscroll': True}
    ct_widget.set_track_options(1, track_options)

###########################
# TRACK 2
###########################
    ct_widget.add_wtrack(2)

###########################
# SIGNAL Acc X
###########################
    ct_widget.add_line(2, 0)
    line_options = {
        'width': int(options['spinbuttons']['sb_pt_d6_pres_line']),
        'color': options['colorbuttons']['color_pt_d6_pres_line'][0],
        'dashed': options['checkbuttons']['chk_pt_d6_pres_dash_line'],
        'last_value': options['checkbuttons']['chk_pt_d6_pres_last_value'],
        'span': (
            int(options['spinbuttons']['sb_pt_d6_pres_span1']),
            int(options['spinbuttons']['sb_pt_d6_pres_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d6_pres_wrapper']),
        'w_color': (0, 0, 0), 'down': down, 'erase_data': False}
    labelgraph_options = {
        'bg_color': '#eee',
        'size_request': (-1, 20),
        'labels': (
            int(options['spinbuttons']['sb_pt_d6_pres_span1']),
            int(options['spinbuttons']['sb_pt_d6_pres_span2'])),
        'line_name': 'Acc X',
        'line_name_color': options['colorbuttons']['color_pt_d6_pres_line'][0],
        'labels_color': (0, 0, 0)}
    ct_widget.set_line_options(2, 0, line_options, labelgraph_options)



###########################
# SIGNAL Acc Y
###########################
    ct_widget.add_line(2, 1)
    line_options = {
        'width': int(options['spinbuttons']['sb_pt_d6_res1_line']),
        'color': options['colorbuttons']['color_pt_d6_res1_line'][0],
        'dashed': options['checkbuttons']['chk_pt_d6_res1_dash_line'],
        'last_value': options['checkbuttons']['chk_pt_d6_res1_last_value'],
        'span': (
            int(options['spinbuttons']['sb_pt_d6_res1_span1']),
            int(options['spinbuttons']['sb_pt_d6_res1_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d6_res1_wrapper']),
        'w_color': (0, 0, 0), 'down': down, 'erase_data': False}
    labelgraph_options = {
        'bg_color': '#eee',
        'size_request': (-1, 20),
        'labels': (
            int(options['spinbuttons']['sb_pt_d6_res1_span1']),
            int(options['spinbuttons']['sb_pt_d6_res1_span2'])),
        'line_name': 'Acc Y',
        'line_name_color': options['colorbuttons']['color_pt_d6_res1_line'][0],
        'labels_color': (0, 0, 0)}
    ct_widget.set_line_options(2, 1, line_options, labelgraph_options)



###########################
# SIGNAL Acc Z
###########################
    ct_widget.add_line(2, 2)
    line_options = {
        'width': int(options['spinbuttons']['sb_pt_d6_res2_line']),
        'color': options['colorbuttons']['color_pt_d6_res2_line'][0],
        'dashed': options['checkbuttons']['chk_pt_d6_res2_dash_line'],
        'last_value': options['checkbuttons']['chk_pt_d6_res2_last_value'],
        'span': (
            int(options['spinbuttons']['sb_pt_d6_res2_span1']),
            int(options['spinbuttons']['sb_pt_d6_res2_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d6_res2_wrapper']),
        'w_color': (0, 0, 0), 'down': down, 'erase_data': False}
    labelgraph_options = {
        'bg_color': '#eee',
        'size_request': (-1, 20),
        'labels': (
            int(options['spinbuttons']['sb_pt_d6_res2_span1']),
            int(options['spinbuttons']['sb_pt_d6_res2_span2'])),
        'line_name': 'Acc Z',
        'line_name_color': options['colorbuttons']['color_pt_d6_res2_line'][0],
        'labels_color': (0, 0, 0)}
    ct_widget.set_line_options(2, 2, line_options, labelgraph_options)

    track_options = {
        'bg_color': options['colorbuttons']['color_pt_d6_t3_bg'][1],
        'size_request': (-1, cust),  # caution
        'width_scale': 100,
        'height_scale': int(options['spinbuttons']['sb_pt_d6_height_scale']),
        'h_grids': int(options['spinbuttons']['sb_pt_d6_t3_hgrids']),
        'v_grids': int(options['spinbuttons']['sb_pt_d6_t3_vgrids']),
        'grid_line_width': 1,
        'grid_dashed': options['checkbuttons']['chk_pt_d6_t3_grid_dashed'],
        'strong_grid': 10,
        'grid_color': options['colorbuttons']['color_pt_d6_t3_grid'][0],
        'strong_grid_color': options['colorbuttons']['color_pt_d6_t3_strong_grid'][0],
        'begin_scrolling': 40, 'pointer_label': False, 'autoscroll': True}
    ct_widget.set_track_options(2, track_options)

###########################
# TRACK 3
###########################
    ct_widget.add_wtrack(3)

###########################
# SIGNAL Flow
###########################
    ct_widget.add_line(3, 0)
    line_options = {
        'width': int(options['spinbuttons']['sb_pt_d6_res3_line']),
        'color': options['colorbuttons']['color_pt_d6_res3_line'][0],
        'dashed': options['checkbuttons']['chk_pt_d6_res3_dash_line'],
        'last_value': options['checkbuttons']['chk_pt_d6_res3_last_value'],
        'span': (
            int(options['spinbuttons']['sb_pt_d6_res3_span1']),
            int(options['spinbuttons']['sb_pt_d6_res3_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d6_res3_wrapper']),
        'w_color': (0, 0, 0),
        'down': down,
        'erase_data': False}
    labelgraph_options = {
        'bg_color': '#eee',
        'size_request': (-1, 20),
        'labels': (
            int(options['spinbuttons']['sb_pt_d6_res3_span1']),
            int(options['spinbuttons']['sb_pt_d6_res3_span2'])),
        'line_name': 'Flow',
        'line_name_color': options['colorbuttons']['color_pt_d6_res3_line'][0],
        'labels_color': (0, 0, 0)}
    ct_widget.set_line_options(3, 0, line_options, labelgraph_options)

    track_options = {
        'bg_color': options['colorbuttons']['color_pt_d6_t4_bg'][1],
        'size_request': (-1, cust),  # caution
        'width_scale': 100,
        'height_scale': int(options['spinbuttons']['sb_pt_d6_height_scale']),
        'h_grids': int(options['spinbuttons']['sb_pt_d6_t4_hgrids']),
        'v_grids': int(options['spinbuttons']['sb_pt_d6_t4_vgrids']),
        'grid_line_width': 1,
        'grid_dashed': options['checkbuttons']['chk_pt_d6_t4_grid_dashed'],
        'strong_grid': 10,
        'grid_color': options['colorbuttons']['color_pt_d6_t4_grid'][0],
        'strong_grid_color': options['colorbuttons']['color_pt_d6_t4_strong_grid'][0],
        'begin_scrolling': 40, 'pointer_label': False, 'autoscroll': True}
    ct_widget.set_track_options(3, track_options)

    ct_widget.add_ttrack(3)
    default_textgraph_options = {
        'step': int(options['spinbuttons']['sb_pt_d6_timedepth_step']),
        'height_scale': int(options['spinbuttons']['sb_pt_d6_height_scale'])
        }
    ct_widget.set_ttrack_options(
        3,
        default_textgraph_options
        )

###########################
# TRACK 4
###########################
    ct_widget.add_wtrack(4)

###########################
# SIGNAL Tension
###########################
    ct_widget.add_line(4, 0)
    line_options = {
        'width': int(options['spinbuttons']['sb_pt_d6_ten_line']),
        'color': options['colorbuttons']['color_pt_d6_ten_line'][0],
        'dashed': options['checkbuttons']['chk_pt_d6_ten_dash_line'],
        'last_value': options['checkbuttons']['chk_pt_d6_ten_last_value'],
        'span': (
            int(options['spinbuttons']['sb_pt_d6_ten_span1']),
            int(options['spinbuttons']['sb_pt_d6_ten_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d6_ten_wrapper']),
        'w_color': (0, 0, 0), 'down': down, 'erase_data': False}
    labelgraph_options = {
        'bg_color': '#eee',
        'size_request': (-1, 20),
        'labels': (
            int(options['spinbuttons']['sb_pt_d6_ten_span1']),
            int(options['spinbuttons']['sb_pt_d6_ten_span2'])),
        'line_name': 'Tension',
        'line_name_color': options['colorbuttons']['color_pt_d6_ten_line'][0],
        'labels_color': (0, 0, 0)}
    ct_widget.set_line_options(4, 0, line_options, labelgraph_options)

    track_options = {
        'bg_color': options['colorbuttons']['color_pt_d6_t5_bg'][1],
        'size_request': (-1, cust),  # caution
        'width_scale': 100,
        'height_scale': int(options['spinbuttons']['sb_pt_d6_height_scale']),
        'h_grids': int(options['spinbuttons']['sb_pt_d6_t5_hgrids']),
        'v_grids': int(options['spinbuttons']['sb_pt_d6_t5_vgrids']),
        'grid_line_width': 1,
        'grid_dashed': options['checkbuttons']['chk_pt_d6_t5_grid_dashed'],
        'strong_grid': 10,
        'grid_color': options['colorbuttons']['color_pt_d6_t5_grid'][0],
        'strong_grid_color': options['colorbuttons']['color_pt_d6_t5_strong_grid'][0],
        'begin_scrolling': 40, 'pointer_label': False, 'autoscroll': True}
    ct_widget.set_track_options(4, track_options)


    return ct_widget

############################################

#   ACTUALIZACIÓN DE CONFIGURACIONES HECHAS POR EL USUARIO EN VENTANA DE DIALOGO

############################################


def config_pt_d5_composetrack(ct_widget, options, down=True):
    """
        Funcion que es llamada desde la clase MainWindow por la funcion on_btn_
        config_tracks_clicked y se encarga de actualizar
        configuraciones que el usuario haya realizado durante el proceso de
        loging en el servicio de Parameter Tool cuando se optado por
        la opcion de usar 5 pistas de graficado.
        @param ct_widget Objeto de la clase ComposeTrackWidget que contiene
        toda la informacion de las pistas de graficado
        @param options Diccionaario con las nuevas opciones colocadas por el
        usuario.
    """
    line_options = {
        'span': (
            int(options['spinbuttons']['sb_pt_d5_gr_span1']),
            int(options['spinbuttons']['sb_pt_d5_gr_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d5_gr_wrapper']),
        'down': down}
    labelgraph_options = {
        'labels': (
            int(options['spinbuttons']['sb_pt_d5_gr_span1']),
            int(options['spinbuttons']['sb_pt_d5_gr_span2']))}
    ct_widget.set_line_options(0, 0, line_options, labelgraph_options)
    #line_options = {
        #'span': (int(options['spinbuttons']['sb_pt_d5_ccl_span1']), int(options['spinbuttons']['sb_pt_d5_ccl_span2'])),
        #'wrapper': int(options['spinbuttons']['sb_pt_d5_ccl_wrapper']),
        #'down': down}
    #labelgraph_options = {
        #'labels': (int(options['spinbuttons']['sb_pt_d5_ccl_span1']), int(options['spinbuttons']['sb_pt_d5_ccl_span2']))}
    #ct_widget.set_line_options(0, 1, line_options, labelgraph_options)
    line_options = {
        'span': (
            int(options['spinbuttons']['sb_pt_d5_temp1_span1']),
            int(options['spinbuttons']['sb_pt_d5_temp1_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d5_temp1_wrapper']),
        'down': down}
    labelgraph_options = {
        'labels': (
            int(options['spinbuttons']['sb_pt_d5_temp1_span1']),
            int(options['spinbuttons']['sb_pt_d5_temp1_span2']))}
    ct_widget.set_line_options(1, 0, line_options, labelgraph_options)
    #line_options = {
        #'span': (int(options['spinbuttons']['sb_pt_d5_temp2_span1']), int(options['spinbuttons']['sb_pt_d5_temp2_span2'])),
        #'wrapper': int(options['spinbuttons']['sb_pt_d5_temp2_wrapper']),
        #'down': down}
    #labelgraph_options = {
        #'labels': (int(options['spinbuttons']['sb_pt_d5_temp2_span1']), int(options['spinbuttons']['sb_pt_d5_temp2_span2']))}
    #ct_widget.set_line_options(1, 1, line_options, labelgraph_options)
    #line_options = {
        #'span': (int(options['spinbuttons']['sb_pt_d5_pres_span1']), int(options['spinbuttons']['sb_pt_d5_pres_span2'])),
        #'wrapper': int(options['spinbuttons']['sb_pt_d5_pres_wrapper']),
        #'down': down}
    #labelgraph_options = {
        #'labels': (int(options['spinbuttons']['sb_pt_d5_pres_span1']), int(options['spinbuttons']['sb_pt_d5_pres_span2']))}
    #ct_widget.set_line_options(1, 2, line_options, labelgraph_options)
    line_options = {
        'span': (
            int(options['spinbuttons']['sb_pt_d5_res1_span1']),
            int(options['spinbuttons']['sb_pt_d5_res1_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d5_res1_wrapper']),
        'down': down}
    labelgraph_options = {
        'labels': (
            int(options['spinbuttons']['sb_pt_d5_res1_span1']),
            int(options['spinbuttons']['sb_pt_d5_res1_span2']))}
    ct_widget.set_line_options(2, 0, line_options, labelgraph_options)
    line_options = {
        'span': (
            int(options['spinbuttons']['sb_pt_d5_res2_span1']),
            int(options['spinbuttons']['sb_pt_d5_res2_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d5_res2_wrapper']),
        'down': down}
    labelgraph_options = {
        'labels': (
            int(options['spinbuttons']['sb_pt_d5_res2_span1']),
            int(options['spinbuttons']['sb_pt_d5_res2_span2']))}
    ct_widget.set_line_options(2, 1, line_options, labelgraph_options)
    line_options = {
        'span': (
            int(options['spinbuttons']['sb_pt_d5_res3_span1']),
            int(options['spinbuttons']['sb_pt_d5_res3_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d5_res3_wrapper']),
        'down': down}
    labelgraph_options = {
        'labels': (
            int(options['spinbuttons']['sb_pt_d5_res3_span1']),
            int(options['spinbuttons']['sb_pt_d5_res3_span2']))}
    ct_widget.set_line_options(2, 2, line_options, labelgraph_options)
    line_options = {
        'span': (
            int(options['spinbuttons']['sb_pt_d5_accx_span1']),
            int(options['spinbuttons']['sb_pt_d5_accx_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d5_accx_wrapper']),
        'down': down}
    labelgraph_options = {
        'labels': (
            int(options['spinbuttons']['sb_pt_d5_accx_span1']),
            int(options['spinbuttons']['sb_pt_d5_accx_span2']))}
    ct_widget.set_line_options(3, 0, line_options, labelgraph_options)
    #line_options = {
        #'span': (int(options['spinbuttons']['sb_pt_d5_accy_span1']), int(options['spinbuttons']['sb_pt_d5_accy_span2'])),
        #'wrapper': int(options['spinbuttons']['sb_pt_d5_accy_wrapper']),
        #'down': down}
    #labelgraph_options = {
        #'labels': (int(options['spinbuttons']['sb_pt_d5_accy_span1']), int(options['spinbuttons']['sb_pt_d5_accy_span2']))}
    #ct_widget.set_line_options(3, 1, line_options, labelgraph_options)
    #line_options = {
        #'span': (int(options['spinbuttons']['sb_pt_d5_accz_span1']), int(options['spinbuttons']['sb_pt_d5_accz_span2'])),
        #'wrapper': int(options['spinbuttons']['sb_pt_d5_accz_wrapper']),
        #'down': down}
    #labelgraph_options = {
        #'labels': (int(options['spinbuttons']['sb_pt_d5_accz_span1']), int(options['spinbuttons']['sb_pt_d5_accz_span2']))}
    #ct_widget.set_line_options(3, 2, line_options, labelgraph_options)
    line_options = {
        'span': (
            int(options['spinbuttons']['sb_pt_d5_ten_span1']),
            int(options['spinbuttons']['sb_pt_d5_ten_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d5_ten_wrapper']),
        'down': down}
    labelgraph_options = {
        'labels': (
            int(options['spinbuttons']['sb_pt_d5_ten_span1']),
            int(options['spinbuttons']['sb_pt_d5_ten_span2']))}
    ct_widget.set_line_options(4, 0, line_options, labelgraph_options)

    default_textgraph_options = {
        'step': int(options['spinbuttons']['sb_pt_d5_timedepth_step']),
        'height_scale': int(options['spinbuttons']['sb_pt_d5_height_scale'])}
    ct_widget.set_ttrack_options(0, default_textgraph_options)

    #default_textgraph_options = {
        #'step': int(options['spinbuttons']['sb_pt_d5_timedepth_step']),
        #'height_scale': int(options['spinbuttons']['sb_pt_d5_height_scale'])}
    #ct_widget.set_ttrack_options(1, default_textgraph_options)

    track_options = {
        'height_scale': int(options['spinbuttons']['sb_pt_d5_height_scale'])
        }
    ct_widget.set_track_options(0, track_options)
    track_options = {
        'height_scale': int(options['spinbuttons']['sb_pt_d5_height_scale'])
        }
    ct_widget.set_track_options(1, track_options)
    track_options = {
        'height_scale': int(options['spinbuttons']['sb_pt_d5_height_scale'])
        }
    ct_widget.set_track_options(2, track_options)
    track_options = {
        'height_scale': int(options['spinbuttons']['sb_pt_d5_height_scale'])
        }
    ct_widget.set_track_options(3, track_options)
    track_options = {
        'height_scale': int(options['spinbuttons']['sb_pt_d5_height_scale'])
        }
    ct_widget.set_track_options(4, track_options)

    #if down == False:
    ct_widget.ClearData(0, 0)
    #ct_widget.ClearData(0, 1)
    ct_widget.ClearData(1, 0)
    #ct_widget.ClearData(1, 1)
    #ct_widget.ClearData(1, 2)
    ct_widget.ClearData(2, 0)
    ct_widget.ClearData(2, 1)
    ct_widget.ClearData(2, 2)
    ct_widget.ClearData(3, 0)
    #ct_widget.ClearData(3, 1)
    #ct_widget.ClearData(3, 2)
    ct_widget.ClearData(4, 0)


def config_pt_d6_composetrack(ct_widget, options, down=True):
    """
        Funcion que es llamada desde la clase MainWindow por la funcion on_btn_
        config_tracks_clicked y se encarga de actualizar
        configuraciones que el usuario haya realizado durante el proceso de
        loging en el servicio de Parameter Tool cuando se optado por
        la opcion de usar 6 pistas de graficado.
        @param ct_widget Objeto de la clase ComposeTrackWidget que contiene
        toda la informacion de las pistas de graficado
        @param options Diccionaario con las nuevas opciones colocadas por el
        usuario.
    """
    line_options = {
        'span': (
            int(options['spinbuttons']['sb_pt_d6_gr_span1']),
            int(options['spinbuttons']['sb_pt_d6_gr_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d6_gr_wrapper']),
        'down': down}
    labelgraph_options = {
        'labels': (
            int(options['spinbuttons']['sb_pt_d6_gr_span1']),
            int(options['spinbuttons']['sb_pt_d6_gr_span2']))}
    ct_widget.set_line_options(0, 0, line_options, labelgraph_options)
    #line_options = {
        #'span': (int(options['spinbuttons']['sb_pt_d6_ccl_span1']), int(options['spinbuttons']['sb_pt_d6_ccl_span2'])),
        #'wrapper': int(options['spinbuttons']['sb_pt_d6_ccl_wrapper']),
        #'down': down}
    #labelgraph_options = {
        #'labels': (int(options['spinbuttons']['sb_pt_d6_ccl_span1']), int(options['spinbuttons']['sb_pt_d6_ccl_span2']))}
    #ct_widget.set_line_options(0, 1, line_options, labelgraph_options)
    #line_options = {
        #'span': (int(options['spinbuttons']['sb_pt_d6_avggr_span1']), int(options['spinbuttons']['sb_pt_d6_avggr_span2'])),
        #'wrapper': int(options['spinbuttons']['sb_pt_d6_avggr_wrapper']),
        #'down': down}
    #labelgraph_options = {
        #'labels': (int(options['spinbuttons']['sb_pt_d6_avggr_span1']), int(options['spinbuttons']['sb_pt_d6_avggr_span2']))}
    #ct_widget.set_line_options(0, 2, line_options, labelgraph_options)
    line_options = {
        'span': (
            int(options['spinbuttons']['sb_pt_d6_temp1_span1']),
            int(options['spinbuttons']['sb_pt_d6_temp1_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d6_temp1_wrapper']),
        'down': down}
    labelgraph_options = {
        'labels': (
            int(options['spinbuttons']['sb_pt_d6_temp1_span1']),
            int(options['spinbuttons']['sb_pt_d6_temp1_span2']))}
    ct_widget.set_line_options(1, 0, line_options, labelgraph_options)
    #line_options = {
        #'span': (int(options['spinbuttons']['sb_pt_d6_temp2_span1']), int(options['spinbuttons']['sb_pt_d6_temp2_span2'])),
        #'wrapper': int(options['spinbuttons']['sb_pt_d6_temp2_wrapper']),
        #'down': down}
    #labelgraph_options = {'labels': (int(options['spinbuttons']['sb_pt_d6_temp2_span1']), int(options['spinbuttons']['sb_pt_d6_temp2_span2']))}
    #ct_widget.set_line_options(1, 1, line_options, labelgraph_options)
    #line_options = {
        #'span': (int(options['spinbuttons']['sb_pt_d6_pres_span1']), int(options['spinbuttons']['sb_pt_d6_pres_span2'])),
        #'wrapper': int(options['spinbuttons']['sb_pt_d6_pres_wrapper']),
        #'down': down}
    #labelgraph_options = {
        #'labels': (int(options['spinbuttons']['sb_pt_d6_pres_span1']), int(options['spinbuttons']['sb_pt_d6_pres_span2']))}
    #ct_widget.set_line_options(1, 2, line_options, labelgraph_options)
    line_options = {
        'span': (
            int(options['spinbuttons']['sb_pt_d6_res1_span1']),
            int(options['spinbuttons']['sb_pt_d6_res1_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d6_res1_wrapper']),
        'down': down}
    labelgraph_options = {
        'labels': (
            int(options['spinbuttons']['sb_pt_d6_res1_span1']),
            int(options['spinbuttons']['sb_pt_d6_res1_span2']))}
    ct_widget.set_line_options(2, 0, line_options, labelgraph_options)
    line_options = {
        'span': (
            int(options['spinbuttons']['sb_pt_d6_res2_span1']),
            int(options['spinbuttons']['sb_pt_d6_res2_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d6_res2_wrapper']),
        'down': down}
    labelgraph_options = {
        'labels': (
            int(options['spinbuttons']['sb_pt_d6_res2_span1']),
            int(options['spinbuttons']['sb_pt_d6_res2_span2']))}
    ct_widget.set_line_options(2, 1, line_options, labelgraph_options)
    line_options = {
        'span': (
            int(options['spinbuttons']['sb_pt_d6_res3_span1']),
            int(options['spinbuttons']['sb_pt_d6_res3_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d6_res3_wrapper']),
        'down': down}
    labelgraph_options = {
        'labels': (
            int(options['spinbuttons']['sb_pt_d6_res3_span1']),
            int(options['spinbuttons']['sb_pt_d6_res3_span2']))}
    ct_widget.set_line_options(2, 2, line_options, labelgraph_options)
    line_options = {
        'span': (
            int(options['spinbuttons']['sb_pt_d6_accx_span1']),
            int(options['spinbuttons']['sb_pt_d6_accx_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d6_accx_wrapper']),
        'down': down}
    labelgraph_options = {
        'labels': (
            int(options['spinbuttons']['sb_pt_d6_accx_span1']),
            int(options['spinbuttons']['sb_pt_d6_accx_span2']))}
    ct_widget.set_line_options(3, 0, line_options, labelgraph_options)
    #line_options = {
        #'span': (int(options['spinbuttons']['sb_pt_d6_accy_span1']), int(options['spinbuttons']['sb_pt_d6_accy_span2'])),
        #'wrapper': int(options['spinbuttons']['sb_pt_d6_accy_wrapper']),
        #'down': down}
    #labelgraph_options = {
        #'labels': (int(options['spinbuttons']['sb_pt_d6_accy_span1']), int(options['spinbuttons']['sb_pt_d6_accy_span2']))}
    #ct_widget.set_line_options(3, 1, line_options, labelgraph_options)
    #line_options = {
        #'span': (int(options['spinbuttons']['sb_pt_d6_accz_span1']), int(options['spinbuttons']['sb_pt_d6_accz_span2'])),
        #'wrapper': int(options['spinbuttons']['sb_pt_d6_accz_wrapper']),
        #'down': down}
    #labelgraph_options = {
        #'labels': (int(options['spinbuttons']['sb_pt_d6_accz_span1']), int(options['spinbuttons']['sb_pt_d6_accz_span2']))}
    #ct_widget.set_line_options(3, 2, line_options, labelgraph_options)
    line_options = {
        'span': (
            int(options['spinbuttons']['sb_pt_d6_ten_span1']),
            int(options['spinbuttons']['sb_pt_d6_ten_span2'])),
        'wrapper': int(options['spinbuttons']['sb_pt_d6_ten_wrapper']),
        'down': down}
    labelgraph_options = {
        'labels': (
            int(options['spinbuttons']['sb_pt_d6_ten_span1']),
            int(options['spinbuttons']['sb_pt_d6_ten_span2']))}
    ct_widget.set_line_options(4, 0, line_options, labelgraph_options)

    default_textgraph_options = {
        'step': int(options['spinbuttons']['sb_pt_d6_timedepth_step']),
        'height_scale': int(options['spinbuttons']['sb_pt_d6_height_scale'])}
    ct_widget.set_ttrack_options(0, default_textgraph_options)

    #default_textgraph_options = {
        #'step': int(options['spinbuttons']['sb_pt_d6_timedepth_step']),
        #'height_scale': int(options['spinbuttons']['sb_pt_d6_height_scale'])}
    #ct_widget.set_ttrack_options(1, default_textgraph_options)

    track_options = {
        'height_scale': int(options['spinbuttons']['sb_pt_d6_height_scale'])}
    ct_widget.set_track_options(0, track_options)
    track_options = {
        'height_scale': int(options['spinbuttons']['sb_pt_d6_height_scale'])}
    ct_widget.set_track_options(1, track_options)
    track_options = {
        'height_scale': int(options['spinbuttons']['sb_pt_d6_height_scale'])}
    ct_widget.set_track_options(2, track_options)
    track_options = {
        'height_scale': int(options['spinbuttons']['sb_pt_d6_height_scale'])}
    ct_widget.set_track_options(3, track_options)
    track_options = {
        'height_scale': int(options['spinbuttons']['sb_pt_d6_height_scale'])}
    ct_widget.set_track_options(4, track_options)

    #if down == False:
    ct_widget.ClearData(0, 0)
    #ct_widget.ClearData(0, 1)
    #ct_widget.ClearData(0, 2)
    ct_widget.ClearData(1, 0)
    #ct_widget.ClearData(1, 1)
    #ct_widget.ClearData(1, 2)
    ct_widget.ClearData(2, 0)
    ct_widget.ClearData(2, 1)
    ct_widget.ClearData(2, 2)
    ct_widget.ClearData(3, 0)
    #ct_widget.ClearData(3, 1)
    #ct_widget.ClearData(3, 2)
    ct_widget.ClearData(4, 0)


############################################

#   CARGA DE DATOS DESDE UNA BASE DE DATOS

############################################

def dump_pt_d5_composetrack(ct_widget, data):
    """
        Funcion que es llamada desde MainWindow por la funcion
        on_btn_import_db_clicked, se usa para cargar la informacion
        de una base de datos de un resgistro previo y cargar toda la
        informacion de el compositor de pistas, ComposeTRackWidget,
        para su visualizacion cuando se esta en el modo de PlayBack, para
        datos que corresponde al servicio de Parameter Tool de 5 pistas.
        @param ct_widget Objeto de la clase ComposeTrackWidget que contiene
        toda la informacion de las pistas de graficado
        @param data Lista de datos provenientes de la base de datos
    """
    ct_widget.dump_data(0, 0, data['gr'])
    ct_widget.dump_data(0, 1, data['ccl'])  # no
    ct_widget.dump_data(1, 0, data['temp1'])  # no
    ct_widget.dump_data(1, 1, data['temp2'])  # no
    ct_widget.dump_data(2, 0, data['pres'])
    ct_widget.dump_data(2, 1, data['res1'])  # no
    ct_widget.dump_data(2, 2, data['res2'])  # no
    ct_widget.dump_data(3, 0, data['res3'])
    ct_widget.dump_data(3, 1, data['accx'])
    ct_widget.dump_data(3, 2, data['accy'])
    ct_widget.dump_data(3, 2, data['accz'])  # no
    ct_widget.dump_data(4, 0, data['ten'])


def dump_pt_d6_composetrack(ct_widget, data):
    """
        Funcion que es llamada desde MainWindow por la funcion
        on_btn_import_db_clicked, se usa para cargar la informacion
        de una base de datos de un resgistro previo y cargar toda la
        informacion de el compositor de pistas, ComposeTRackWidget,
        para su visualizacion cuando se esta en el modo de PlayBack, para
        datos que corresponde al servicio de Parameter Tool de 6 pistas.
        @param ct_widget Objeto de la clase ComposeTrackWidget que contiene
        toda la informacion de las pistas de graficado
        @param data Lista de datos provenientes de la base de datos
    """
    dump_pt_d5_composetrack(ct_widget, data)
