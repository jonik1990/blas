# -*- encoding: utf-8 -*-
"""
Emdyp.me
Revisado
Script: Tipo A
(valores de configuracion)
"""
import sys
if len(sys.argv) < 2:
    sys.argv.append('py2exe')

import os
import shutil
from distutils.core import setup
import py2exe

import config

print '============================'
print ' ====  build software   ===='
print '============================'

GTK_PATH = r'C:\Program Files\GTK2-Runtime'
MSVC_DLL = r'C:\Python27\msvcr90.dll'
MSVC_MANIFEST = r'c:\Python27\Microsoft.VC90.CRT.manifest'

if os.path.exists('build'):
    shutil.rmtree('build')

if os.path.exists('dist'):
    shutil.rmtree('dist')

if os.path.exists('Output'):
    shutil.rmtree('Output')

setup(
    name=config.SW_NAME,
    version=config.VERSION,
    package_dir={},
    py_modules=[],
    packages=[],
    scripts=['mainwindow.py'],
    windows=[
        {
            'script': 'mainwindow.py',
            'icon_resources': [(1, 'logo.ico')]
        }

    ],
    data_files=[
    ],
    options={
        'py2exe': {
            'packages': ['encodings', 'gtk', 'gtk.keysyms'],
            'includes': ['locale', 'gio', 'cairo', 'pangocairo', 'pango',
                         'atk', 'gobject', 'serial'],
            'excludes': ['ltihooks', 'pywin', 'pywin.debugger',
                         'pywin.debugger.dbgcon', 'pywin.dialogs',
                         'pywin.dialogs.list', 'Tkconstants', 'Tkinter', 'tcl'
                         'doctest', 'macpath', 'pdb', 'cookielib', 'ftplib',
                         'caledar', 'win32wnet', 'unicodedata'],
            'dll_excludes': ['libglade-2.0-0.dll', 'w9xpopen.exe'],
            'optimize': '2',
        }
    },
    zipfile=None
)
print '============================='
print ' ==== copying gtk+ files ===='
print '============================='
for dir in ('lib', 'etc', 'share'):
    shutil.copytree(os.path.join(GTK_PATH, dir), os.path.join('dist', dir))
print 'done'

print 'copying msvc files'
shutil.copy(MSVC_DLL, 'dist')
shutil.copy(MSVC_MANIFEST, 'dist')
print 'done'

print 'deleting unnecesary files:'
for dir in os.listdir(os.path.join('dist', 'share', 'themes')):
    if dir == 'MS-Windows':
        continue
    shutil.rmtree(os.path.join('dist', 'share', 'themes', dir))
    print '\ttheme', dir

print 'copying gui files'
shutil.copytree(config.GUI_PATH, os.path.join('dist', config.GUI_PATH))

os.rename('dist/mainwindow.exe', 'dist/' + config.SW_NAME + '.exe')
os.rename('dist', config.SW_NAME + '_' + config.VERSION)
print 'done, see you later :)'
