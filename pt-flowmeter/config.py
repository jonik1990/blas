# -*- encoding: utf-8 -*-
'''
JPT
Revisado: Pedro Rivera
Script: Tipo A
(nombres de software)
[Service-builder]
'''
import os
from time import time, localtime

SW_NAME = 'BLAS-Flowmeter 0.0.18'
"""@var SW_NAME
    Variable que almacena el nombre del software
"""
VERSION = '0.0.18'
"""@var VERSION
    Variable que almacena la version del software
"""
##version format: general.gui.logical + alpha/betta

GUI_PATH = 'gui_files/'
"""@var GUI_PATH
    Variable que almacena el path del GUI
"""


def gui_file(filename):
    """@fn gui_file
        Funcion que coloca en las variables de entorno de PYTHON la ubicacion
        de la carpeta donde esta los archivos de la interfaz grafica
        @param filename Ubicacion donde estan los archivosde la interfaz
        grafica, *.glade, *.ico, *.png
        @return La concatenacion de GUI_PATH='gui/' con filename
    """
    return os.path.join(GUI_PATH, filename)

DB_PATH = 'recordings'
"""@var DB_PATH
    Variable que almacena el path del recordings
"""


def db_file(filename):
    """@fn db_file
        Funcion que verifica o crea un directorio para grabar los registros
        @param filename Nombre del archivo donde se va guardar la informacion
        del registro
    """
    if not os.path.exists(DB_PATH):
        os.mkdir(DB_PATH)
    filename += '_{t.tm_hour}-{t.tm_min}-{t.tm_sec}.jdb'.format(
        t=localtime(time())
        )
    return os.path.join(DB_PATH, filename)

LAS_PATH = 'las'
"""@var LAS_PATH
    Variable que almacena el path de la exportacion a LAS
"""


def las_file(filename):
    """@fn las_file
        Funcion que verifica o crea un directorio para grabar los registros
        @param filename Nombre del archivo donde se va guardar la informacion
        del registro LAS
    """
    if not os.path.exists(LAS_PATH):
        os.mkdir(LAS_PATH)
    filename += '.las'
    return os.path.join(LAS_PATH, filename)


TRACKING_BY_TIME = 0
"""@var TRACKING_BY_TIME
    Configuracion para Tracking por tiempo
"""
TRACKING_BY_DEPTH = 1
"""@var TRACKING_BY_DEPTH
    Configuracion para Tracking por profundidad
"""
