#!/usr/bin/env python
# -*- encoding: utf-8 -*-
#AUTHOR: CARLOS EDUARDO CAMPO M.


class las():

    def __init__(self):
        super(las, self).__init__()
        self.vasl = []
        self.mainHeader = []
        self.titles = []

    def makingHeader(self, metadata):
        """make a header for las files, las file header have a consistency
           Unity, Data and Description. also a rigth separation between values
           is exactly 10 white spaces"""

        #with metadata info, extracts information

        #metadata = {'index': 'TIME',
                    #'sampling_rate': '12',
                    #'company': '',
                    #'well': '',
                    #'field': '',
                    #'location': '',
                    #'county': '',
                    #'state': '',
                    #'country': '',
                    #'service_company': '',
                    #'datetime': '',
                    #'well_id': '',
                    #'ercb': ''}

        if metadata['index'] == "TIME":
            units = 'SECONDS'
            noindex = "DEPTH"
        if metadata['index'] == "DEPTH":
            units = 'FEET'
            noindex = "TIME"

        #minim and maxim are gived
        minim = str(self.vals[0])
        maxim = str(self.vals[1])
        nTracks = self.tracks

        #packed information item [unity, data, description]
        #mainHeader encode information of main header with list index as below
        #[version, wrap, start, stop, step, null, company, well, field,
        #location, province, county, state, country, company, service, date,
        #ID well, license number]

        self.mainHeader = [
                ['~VERSION metadata', '', ''],
                ['VERS.', '2.0', ':CWLS LOG ASCII STANDARD -VERSION 2.0'],
                ['WRAP.', 'NO', ':ONE LINE PER DEPTH STEP'],
                ['~WELL', '', ''],
                ['#MNEM.UNIT', 'DATA', 'DESCRIPTION'],
                ['#---- ----', '----', '-----------'],
                ['STRT.{:}'.format(units), str(minim),
                                                 ':START ' + metadata['index']],
                ['STOP.{:}'.format(units), str(maxim),
                                                  ':STOP ' + metadata['index']],
                ['STEP.{:}'.format(units), metadata['sampling_rate'], ':STEP'],
                ['NULL.', '-999.25', ':NULL VALUE'],
                ['COMP.', metadata['company'], ':COMPANY'],
                ['WELL.', metadata['well'], ':WELL'],
                ['FLD.', metadata['field'], ':FIELD'],
                ['LOC.', metadata['location'], ':LOCATION'],
                ['PROV.', '', 'PROVINCE'],
                ['CNT.', metadata['county'], ':COUNTY'],
                ['STAT.', metadata['state'], ':STATE'],
                ['CNTR.', metadata['country'], ':COUNTRY'],
                ['SRVC.', metadata['service_company'], ':SERVICE COMPANY'],
                ['DATE.', metadata['datetime'], ':LOG DATE'],
                ['UWI.', metadata['well_id'], ':UNIQUE WELL ID'],
                ['LIC.', metadata['ercb'], ':ERCB LICENCE NUMB'],
                ['~CURVE', '', '']
                     ]
        self.curvHeader = [
                          ['#MNEM.UNIT', 'API CODES', 'CURVE DESCRIPTION'],
                          ['#---- ----', '---------', '-----------------']
                         ]
        self.paramHeader = [
                            ['~PARAMETER', '', ''],
                            ['#MNEM.UNIT', 'VALUE', 'DESCRIPTION'],
                            ['#---- ----', '-----', '-----------'],
                            ['~OTHER', '', ''],
                            ['#', '', ''],
                            ['#', '', ''],
                            ['~A', '', '']
                           ]

        #separator

        #It could be decoded in json  file gived by database, code below just
        #create a single line with parameter values, to write
        if nTracks == 4:
            self.titles = [
                           [units, metadata['index'], metadata['index']],
                           ['', 'CCL', 'CCL'],
                           ['', 'GAMMA_RAY', 'GAMMA_RAY'],
                           ['N', 'TENSION', 'TENSION'],
                           ['FT', 'DEPTH', 'DEPTH']
                          ]

        if nTracks == 3:
            self.titles = [
                           [units, metadata['index'], metadata['index']],
                           ['', 'GAMMA_RAY', 'GAMMA_RAY'],
                           ['', 'CCL', 'CCL'],
                           ['N', 'TENSION', 'TENSION'],
                           ['FT', noindex, noindex],
                           ['', 'AVG_GR', 'AVG_GAMMA_RAY']
                          ]

        if nTracks == 5:
            self.titles = [
                            [units, metadata['index'], metadata['index']],
                            ['C', 'TEMP1', 'TEMP1'],
                            ['C', 'TEMP2', 'TEMP2'],
                            ['PSI', 'PRESS1', 'PRESS1'],
                            ['DEG', 'ACCX1', 'ACC_X1'],
                            ['DEG', 'ACCY1', 'ACC_Y1'],
                            ['DEG', 'ACCZ1', 'ACC_Z1'],
                            ['DEG', 'ACCX2', 'ACC_X2'],
                            ['DEG', 'ACCY2', 'ACC_Y2'],
                            ['DEG', 'ACCZ2', 'ACC_Z2'],
                            ['', 'CCL', 'CCL'],
                            ['', 'GAMMA_RAY', 'TENSION'],
                            ['FT', 'DEPTH', 'DEPTH'],
                            ['', 'ENCODER', 'ENCODER'],
                            ['', 'TENSION', 'TENSION']
                          ]

        if nTracks == 6:
            self.titles = [
                            [units, metadata['index'], metadata['index']],
                            ['C', 'TEMP1', 'TEMP1'],
                            ['C', 'TEMP2', 'TEMP2'],
                            ['PSI', 'PRESS1', 'PRESS1'],
                            ['DEG', 'ACCX1', 'ACC_X1'],
                            ['DEG', 'ACCY1', 'ACC_Y1'],
                            ['DEG', 'ACCZ1', 'ACC_Z1'],
                            ['DEG', 'ACCX2', 'ACC_X2'],
                            ['DEG', 'ACCY2', 'ACC_Y2'],
                            ['DEG', 'ACCZ2', 'ACC_Z2'],
                            ['--', '--', '--'],
                            ['N', 'TENSION', 'TENSION'],
                            ['N', 'DEPTH', 'DEPTH'],
                            ['', 'ENCODER', 'ENCODER'],
                            ['', 'AVG_GR', 'AVG_GAMMA_RAY']
                          ]
        #if need any different number of tracks, add a new self.titles

    def genFile(self, dict_data, vals, tracks, filename, metadata):

        #look index parameter, minim and maxim value of this too
        self.vals = vals
        self.tracks = tracks
        self.makingHeader(metadata)
        name = filename + '.las'
        a = open(name, 'w')

        #write headers
        for hElem in self.mainHeader:
            element = hElem[0] + " " * 5 + hElem[1] + " " * 5 + hElem[2] + "\n"
            a.write(element)

        #write curv header
        for hElem in self.curvHeader:
            element = (
                       hElem[0] + " " * 10 + hElem[1] + " " * 10 +
                       hElem[2] + "\n"
                       )
            a.write(element)

        #write curve apis of every parameter, if need api add a list with code
        #apis an replace withe space between separators below
        tLine = ""
        for hEle in self.titles:
            element = (
                       hEle[1] + "." + hEle[0] + " " * 10 + '' + " " * 10 +
                       hEle[2] + "\n"
                       )
            a.write(element)

        #write parameter header
        for hElem in self.paramHeader:
            element = (
                        hElem[0] + " " * 10 + hElem[1] + " " * 10 +
                        hElem[2] + "\n"
                        )
            a.write(element)

        #write line with parameters,if need unitys just add date[0]
        tLine = "\n#"
        for date in self.titles:
            tLine = tLine + date[1] + "  "
        a.write(tLine + "\n")

        #write values from dict, first need length of any parameter
        dk = list(dict_data.keys())
        for i in range(len(dict_data[dk[1]])):
            tLine = "\n"
            for date in self.titles:
                try:
                    tLine = tLine + str(format(dict_data[date[1]][i], '.3f')) + " "
                except:
                    error = 3

            a.write(tLine)

        a.close()
        return name