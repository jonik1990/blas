# -*- coding: utf-8 -*-
#!/usr/bin/env python3
"""
Tests for reports module that exports .jdb files to pdf, las and excel

for more information, see docs/reports.md in main folder
"""

import unittest
import os

from __init__ import export_center


class TestReports(unittest.TestCase):
    """class to test listener, uses the hub simulator"""

    def setUp(self):
        """build pylistener tester"""
        self.files_to_export = [
            'dummy_data/field_01_2015-12-2_12-33-13_Down12-33-16.jdb',
            'dummy_data/field_01_2015-12-15_10-18-39_Down10-18-52.jdb',
            'dummy_data/field_01_2015-12-15_10-59-51_Down11-0-29.jdb',
            'dummy_data/field_01_2015-12-15_11-58-17_Down11-58-30.jdb']

        self.reporter = export_center()

    def test_to_las(self):
        """test las data export"""
        n = 0
        for f in self.files_to_export:
            #export .jdb file to .las file, only requires path of .jdb file
            #and saves .las file with the .jdb file name
            self.reporter.to_las(f)
            #can receive a name parameter that
            self.reporter.to_las(f, name='file{:}.las'.format(n))
            #verify if custom file exists
            self.assertTrue(os.path.exists('file{:}.las'.format(n)))
            n += 1

    def test_to_excel(self):
        """test excel data export"""
        n = 0
        for f in self.files_to_export:
            #export .jdb file to .xlsx file, only requires path of .jdb file
            #and saves .xlsx file with the .jdb file name
            self.reporter.to_excel(f)
            #can receive a name parameter that
            self.reporter.to_excel(f, name='file{:}.xlsx'.format(n))
            #verify if custom file exists
            self.assertTrue(os.path.exists('file{:}.xlsx'.format(n)))
            n += 1

    def test_to_pdf(self):
        """test pdf data export"""
        n = 0
        for f in self.files_to_export:
            #export .jdb file to .las file, only requires path of .jdb file
            #and saves .pdf file with the .jdb file name
            self.reporter.to_pdf(f)
            #can receive a name parameter that
            self.reporter.to_pdf(f, name='file{:}.pdf'.format(n))
            #verify if custom file exists
            self.assertTrue(os.path.exists('file{:}.pdf'.format(n)))
            n += 1

    def test_to_Elas(self):
        pass

    def test_to_Eexcel(self):
        pass

    def test_to_Epdf(self):
        pass


if __name__ == '__main__':
    unittest.main()
