# -*- coding: utf-8 -*-
import pandas


class Excel():
    """work with xlsx files, create and read strong relation with dicts and
       structures described below"""

    def __init__(self):
        super(Excel, self).__init__()

    def genFile(self, dictD, name):
        """create a xlsx file, need a dictionary and a name,
           to create need a dictoinary with next structure
        {'sheet1':{'tittle':[elem, elem], 'tittle2': [elem, elem]}, 'sheet2'"""

        #convert dictionary in dataTable of pandas
        table = pandas.DataFrame(dictD)

        #create a empty xls file ready to write
        name = name + '.xlsx'
        writer = pandas.ExcelWriter(name)

        #create a xls with the table and the empty xls file
        table.to_excel(writer, 'sheet')

        #save all changes in the empty file
        writer.save()

        return name