# -*- coding: utf-8 -*-

import svgwrite as svg
import re
import json
import cairosvg


class draw_param():
    """draw graph of a list of values,
       need list with index (label x), list of value (label y)
       and range to normalize function"""

    def __init__(self):
        super(draw_param, self).__init__()

    def graph(self, graph, blck, index, value, color, normal_to=[], dev=0):
        """do normalized graphs"""

        #calculate factor to normalize
        rangNorm = 100
        if normal_to == []:
            minim = min(value)
            maxim = max(value)

        else:
            minim = normal_to[0]
            maxim = normal_to[1]
        len_ind = len(index)

        #normalize index
        minIndex = min(index)
        normIndex = len_ind / (max(index) - min(index))

        #create a grid, step should be give by metadata
        #self.grid(graph, len_ind, 10)
        rangValue = maxim - minim
        try:
            factorNorm = rangNorm / float(rangValue)
        except:
            factorNorm = 1

        #create points
        points = []
        for x in range(len(value)):

            points.append(
                          (factorNorm * (value[x] - minim) + dev,
                          ((index[x] - minIndex) * normIndex))
                          )

        #adding points to svg image
        dt = blck.add(
                  graph.polyline(points, stroke=color, fill="none",
                  stroke_width='0.5', stroke_linejoin="round")
                  )
        return dt

    def grid(self, graph1, blck, index, step=10, dev=0):
        """create a grid, first need a svg image to write in it.
           also need limits to graphs and step of meassurement"""

        #create horizontal grid
        limits = len(index)
        nLinesY = limits
        for y in range(int(nLinesY / step) + 1):
            strk = '0.05'
            if y % 10 == 0:
                strk = '0.7'
            y = y * step
            blck.add(graph1.line(
                               (0 + dev, y), (100 + dev, y), stroke='black',
                               fill="none", stroke_width=strk))

        #create vertical grid
        for x in range(11):
            strk = '0.05'
            if x == 0 or x == 10:
                strk = '0.7'
            x = x * 10
            blck.add(graph1.line(
                               (x + dev, 0), (x + dev, limits), stroke='black',
                               fill="none", stroke_width=strk))
        return limits

    def blankSpace(self, blck, index, step):
        """create a blank space with same length than the others graphics
           also it contain the value of index in every strong line of grid"""

        #create horizontal grid
        limits = len(index)
        nLinesY = limits
        aspect = int(nLinesY / step)

        for y in range(aspect + 1):
            if y % 10 == 0:
                value = str(index[y * 10])
                text = svg.text.Text(value, dx='1', dy='1', font_size=5)
                #text.rotate(180, None)
                text.translate(110, (y * 10) + 3)
                blck.add(text)

    def headers(self, graph1, blck, way1, way2, part, dev=0):
        """create headers of inners signals, it contains signals by way and
           with its respective colors and ranges of operation"""
        #creating independent image
        #way1
        cont = 0
        if part == "0":
            cont = 1
            for elem in way1:
                blck.add(
                      graph1.polyline([(0, -15 * cont), (100, -15 * cont)],
                      stroke=elem[2], fill="none", stroke_width='0.5',
                       stroke_linejoin="round"))

                text = svg.text.Text(elem[0], dx='1', dy='1', font_size=4)
                    #text.rotate(180, None)
                text.translate(40, (-15 * cont) - 1)
                blck.add(text)

                text = svg.text.Text(format(elem[1][0], '.1f'), dx='1', dy='1',
                                                                   font_size=4)
                    #text.rotate(180, None)
                text.translate(0, (-15 * cont) + 3)
                blck.add(text)

                text = svg.text.Text(format(elem[1][1], '.1f'), dx='1', dy='1',
                                                                   font_size=4)
                    #text.rotate(180, None)
                text.translate(80, (-15 * cont) + 3)
                blck.add(text)

                cont += 1

            #way2
            cont = 1
            for elem in way2:
                blck.add(
                      graph1.polyline([(135, -15 * cont), (235, -15 * cont)],
                      stroke=elem[2], fill="none", stroke_width='0.5',
                       stroke_linejoin="round"))

                text = svg.text.Text(elem[0], dx='1', dy='1', font_size=4)
                    #text.rotate(180, None)
                text.translate(40 + 135, (-15 * cont) - 1)
                blck.add(text)

                text = svg.text.Text(format(elem[1][0], '.1f'), dx='1', dy='1',
                                                                   font_size=4)
                    #text.rotate(180, None)
                text.translate(135, (-15 * cont) + 3)
                blck.add(text)

                text = svg.text.Text(format(elem[1][1], '.1f'), dx='1', dy='1',
                                                                   font_size=4)
                    #text.rotate(180, None)
                text.translate(215, (-15 * cont) + 3)
                blck.add(text)

                cont += 1

            #draw square of header
            blck.add(
                      graph1.polyline([(0, -15 * cont), (235, -15 * cont)],
                      stroke='black', fill="none", stroke_width='0.5',
                       stroke_linejoin="round"))

            blck.add(
                      graph1.polyline([(0, 0), (235, 0)],
                      stroke='black', fill="none", stroke_width='0.5',
                       stroke_linejoin="round"))
            blck.add(
                      graph1.polyline([(0, 0), (0, -15 * cont)],
                      stroke='black', fill="none", stroke_width='0.5',
                       stroke_linejoin="round"))

            blck.add(
                      graph1.polyline([(100, 0), (100, -15 * cont)],
                      stroke='black', fill="none", stroke_width='0.5',
                       stroke_linejoin="round"))

            blck.add(
                      graph1.polyline([(135, 0), (135, -15 * cont)],
                      stroke='black', fill="none", stroke_width='0.5',
                       stroke_linejoin="round"))

            blck.add(
                      graph1.polyline([(235, 0), (235, -15 * cont)],
                      stroke='black', fill="none", stroke_width='0.5',
                       stroke_linejoin="round"))
        return 235, -15 * cont


class Pdf():

    def __init__(self):
        super(Pdf, self).__init__()
        self.colors = ['#ff0000', 'green', 'blue', 'yellow', 'orange', 'purple']
        self.size_h = re.compile('height="100%"')
        self.size_w = re.compile('width="100%"')
        self.temp_heigth = re.compile('viewBox="0 0 \d+.\d+ \d+.\d+"')
        self.t_heigth = re.compile('\d+.\d+"')
        self.t_width = re.compile(' \d+.\d+ ')

    def replaceHeader(self):
        """replace values of header template"""

        #load parameters from external file
        val = open("parameters_header", "r+")
        val = val.read()
        val_header = json.loads(val)
        values = list(val_header.keys())

        #open and replace values of header template
        s = open('template.svg', 'r+')
        head_txt = s.read()

        for n in range(len(values)):
            head_txt = head_txt.replace(">" + values[n] + "<", ">" +
                                                   val_header[values[n]] + "<")
        s.close()
        s = open('templateReplaced.svg', 'w')
        s.write(head_txt)
        s.close()

    def genFile(self, data, filename, metadata):
        """generate three svg objects, two with graphics and one blank space"""
        self.filename = filename
        keys = list(data.keys())

        #if need dont show any signal
        #print (keys)
        try:
            keys.remove('TIME')
            keys.remove('DEPTH')
            keys.remove('ENCODER')

        except:
            print ("some value dont exist")
        #also delete null signals
        realKeys = []
        for k in keys:
            minim = min(data[k])
            maxim = max(data[k])
            print (k, minim, maxim, minim == maxim)
            if minim != maxim:
                realKeys.append(k)
        print (realKeys)
        #check length of data and calculate dimentions of objects
        n_var = len(realKeys)

        #separate variables
        vrs1 = int(n_var / 2)
        V1, V2 = [], []
        way1, way2 = [], []
        cont = 0
        index = data[metadata['index']]

        for k in realKeys:
            cont += 1
            if cont <= vrs1:
                V1.append(k)
            else:
                V2.append(k)

        #create svg objects
        #graphs 1
        vals = []
        ranges = []
        for k in V1:
            vals.append(data[k])
            #limits
            minim = min(data[k])
            maxim = max(data[k])
            ranges.append([minim, maxim])
            way1.append([k, [minim, maxim]])

        #graphs 2
        vals2 = []
        ranges2 = []
        for k in V2:
            vals2.append(data[k])
            #limits
            minim = min(data[k])
            maxim = max(data[k])
            ranges2.append([minim, maxim])
            way2.append([k, [minim, maxim]])

        #do multiple graphs
        len_data = len(index)
        Index = []
        Vals, Vals2 = [], []
        parts = 1
        if len_data > 7000:
            exceed = len_data - 7000
            parts = round(exceed / 7000.0)

            #split data
            parts = parts + 1
            #to bypass first iterations not bigger than 1
            if parts == 1:
                parts = 2
            dim = int(len_data / parts)

            for n in range(parts):
                #spliting index
                Index.append(index[n * dim: (n + 1) * dim])

                #spliting values1
                v1, v2 = [], []
                for k in vals:
                    v1.append(k[n * dim: (n + 1) * dim])

                #spliting values1
                for k in vals2:
                    v2.append(k[n * dim: (n + 1) * dim])
                Vals.append(v1)
                Vals2.append(v2)


            for n in range(parts):
                #print (len(Index[n]), len(Vals[n]), len(Vals2[n]))
                self.mult_draw(Index[n], Vals[n], Vals2[n], ranges, ranges2, way1,
                                                                      way2, str(n))
            #ind += 1
        elif len_data <= 7000:
            self.mult_draw(index, vals, vals2, ranges, ranges2, way1, way2, "0")

        return "exported" + filename + ".pdf"

    def splitGraph(self, dimy, blck):
        """split a big image """
        #pg = svg.Drawing('graph1.svg', profile='full', size=(744,09449, 30000))
        ra = dimy / 30000.0

        if ra >= 1.6:
            ra = round(ra)
            n = dimy / float(ra)

            #create separated images
            count = 0
            for i in range(ra):
                pg = svg.Drawing('exported/graph' + str(count) + '.svg',
                                       profile='full', size=(744.09449, n))
                blck.translate(0, n * count)
                pg.add(blck)
                pg.save()
                count += 1

        elif ra < 1.6:
            pg = svg.Drawing('exported/graph.svg',
                                       profile='full', size=(744.09449, dimy))
            pg.add(blck)
            pg.save()

    def mult_draw(self, index, vals, vals2, ranges, ranges2, way1, way2, part):
        self.replaceHeader()
        #object1
        graph1 = svg.Drawing('exported/graph1' + part + '.svg', profile='full')

        #claculate size of image header
        s = open('template.svg', 'r+')
        head_im = s.read()
        head_im = self.temp_heigth.findall(head_im)
        head_im = head_im[0]

        length_im = self.t_heigth.findall(head_im)
        length_im = length_im[0][: len(length_im[0]) - 1]
        length_im = float(length_im)

        width_im = self.t_width.findall(head_im)
        width_im = width_im[0][1: len(width_im[0]) - 1]
        width_im = float(width_im)

        length_im = 0
        #put header just to first iteration
        if part == "0":
            #add image of header
            length_im = self.t_heigth.findall(head_im)
            length_im = length_im[0][: len(length_im[0]) - 1]
            length_im = float(length_im)
            im = graph1.image("templateReplaced.svg",
                                                    size=(width_im, length_im))
            graph1.add(im)

        blck = graph1.g()

        #grid1
        long_grid = draw_param().grid(graph1, blck, index)
        gr1 = list(zip(vals, ranges))

        #diferent colors
        col = 0
        n_col = len(self.colors)
        for o in gr1:
            if col == n_col:
                col = 0

            draw_param().graph(graph1, blck, index, o[0],
                                                        self.colors[col], o[1])
            way1[col].append(self.colors[col])
            col += 1
        #draw blank space
        draw_param().blankSpace(blck, index, 10)

        #grid 2
        draw_param().grid(graph1, blck, index, dev=135)
        gr2 = list(zip(vals2, ranges2))
        c = 0
        for o in gr2:
            if col == n_col:
                col = 0
            draw_param().graph(graph1, blck, index, o[0],
                                               self.colors[col], o[1], dev=135)
            way2[c].append(self.colors[col])
            col += 1
            c += 1
        #draw headers
        lhx, lhy = draw_param().headers(graph1, blck, way1, way2, part, dev=10)

        #scale graphic
        s_w = (708.65 / float(lhx))
        blck.scale(s_w, s_w)

        #if need translate graphic do it below, to add box of information (ex)
        despy = abs(lhy) + (length_im / s_w)
        blck.translate((17.71 / s_w), despy)

        graph1.add(blck)

        #dimentions
        dimx = str(744.094) + 'px"'
        dimy = str((s_w * (abs(lhy) + (long_grid))) + length_im) + 'px"'
        graph1.save()

        #create images in parts
        dy = (s_w * (abs(lhy) + (long_grid))) + length_im
        self.splitGraph(dy, blck)

        #create sheet
        s = open('exported/graph1' + part + '.svg', 'r+')
        table = s.read()
        s.close()

        table = self.size_h.sub('height="' + dimy, table)
        table = self.size_w.sub('width="' + dimx, table)
        table = table.replace("><", ">\n<")
        name = self.filename + part + '.svg'
        new = open(name, 'w')
        new.write(table)
        new.close()

        #export to pdf
        cairosvg.svg2pdf(url=name, write_to="exported/" + self.filename +
                                                                 part + '.pdf')


if __name__ == '__main__':
    grid = svg.Drawing('grid.svg', profile='tiny')
    text = svg.text.Text('hello', dx='10', dy='10')
    text.rotate(180, None)
    text.translate(30, )
    grid.add(text)
    grid.save()
