# -*- encoding: utf-8 -*-
'''
Emdyp.me
Revisado: Pedro Rivera
Script: Tipo A
(composicion de informacion para guardado de datos)
[Guardado]
'''
import json
from datetime import datetime
import os
from copy import copy
from shutil import copyfile


def load_db(filename):
    """
        Funcion que se encarga de cargar todos los datos de un previo registro
        en un archivo de texto plano .json
        @param filename Nombre del fichero .jdb donde esta la informacion
        almacenada
        @return options, data opciones de configuracion del servicio con que
        se hizo el  registro y los datos de dicho registro
    """
    control = 'radiobuttons'
    conf = 'config_options'
    logging_conf = 'logingmodedialog_options'
    db_file = open(filename, "r")
    line = db_file.readline()
    options = json.loads(line)

    raw_data = []
    #if analogic service
    if options[conf][control]['rad_lg_service']:
        #if 4 tracks saved data
        if options['config_options']['radiobuttons']['rad_lg_4_tracks']:
            data = {'index': [],
                    'ccl': [],
                    'gr': [],
                    'depth': [],
                    'enc': [],
                    'ten': []}
            datamap = ['index', 'ccl', 'gr', 'ten', 'depth']
            tool_configurated = True
        #if 3 tracks saved data
        if options[conf][control]['rad_lg_3_tracks']:
            data = {'index': [],
                    'ccl': [],
                    'gr': [],
                    'depth': [],
                    'enc': [],
                    'ten': [],
                    'avggr': []}
            datamap = ['index', 'ccl', 'gr', 'ten', 'depth', 'avggr']
            tool_configurated = True
    #if digital service
    elif options[conf][control]['rad_pt_service']:
        # variables in 5 tracks file
        datamap = ['index', 'temp1', 'temp2',
                   'pres', 'accx', 'accy',
                   'accz', 'res1', 'res2',
                   'res3', 'ccl', 'gr',
                   'depth', 'enc', 'ten']
        if options[conf][control]['rad_pt_6_tracks']:
            # if file has 6 tracks, append avggr variable
            datamap.append('avggr')
        #build data dict
        data = {}
        for item in datamap:
            data[item] = []
        tool_configurated = True
        #populate data
    if tool_configurated:
        while True:
            #read line
            line = db_file.readline()
            if not line:
                break
            d = json.loads(line)
            # data comes from normal file
            if 'Normal' in d:
                row = d['Normal']
                #build a data structure like data variable
                for i in range(len(datamap)):
                    if i == 0:
                        if row[i] != -1:
                            data[datamap[i]].append(row[i])
                    elif row[i] != -1:
                        data[datamap[i]].append((row[i], row[0]))
                raw_data.append(row)

            # data comes from mermory file
            elif 'Memory' in d:
                row = d['Memory']
                # get operation mode (depth or time)
                depth_opt = options[logging_conf][control]['rad_real_depth']
                time_opt = options[logging_conf][control]['rad_real_time']
                # build data array
                for a in range(1, len(datamap)):
                    if row[a] != -1:
                        if depth_opt:
                            data[datamap[a]].append((row[a], row[12]))
                        elif time_opt:
                            data[datamap[a]].append((row[a], row[0]))
            # save raw_data
            raw_data.append(row)
    db_file.close()
    print json.dumps(data)
    return options, data, raw_data


class DataBase():
    """
         Clase  que  representa  a  DataBase  encargada  de
         escribir  los  datos  en  la  base  de  datos  json
    """
    def __init__(self, filename, options=None, **kwargs):
        """
            Constructor de la clase DataBase
            @param filename Nombre del archivo (STRING)
            @param options Opciones de usuario ha configurado
            para el servcio que haya elegido
        """
        self.global_buffer = kwargs['globalBuffer']
        self.ls = self.global_buffer['ls']
        self.datetime = datetime.now()
        self.filename = filename
        self.filename2 = None
        self.filename3 = None
        self.file = None
        self.file2 = None
        self.file3 = None
        if options:
            self.metadata = options
            self.metadata['datetime'] = self.datetime.strftime('%d-%m-%y %H:%M:%S')
            self.metadata['filename'] = filename
            self.metadata['version'] = self.ls.VERSION

    def open_file_append(self):
        """@fn open_file_append
            Para abrir una base de datos, experimental
        """
        filename = self.ls.compose_name(self.filename)
        filename2 = self.ls.compose_name(self.filename2)
        self.file = open(filename, "a")
        self.file2 = open(filename2, "a")

    def json_write(self, data):
        """@fn json_write
            Funcion que se encarga de escribir datos en un archivo .json Down
            @param data Datos que van a ser escritos en archivo
        """
        json.dump(data, self.file)
        self.file.write('\n')

    def json_write2(self, data):
        """@fn json_write2
            Funcion que se encarga de escribir datos en un archivo .json Up
            @param data Datos que van a ser escritos en archivo
        """
        json.dump(data, self.file3)
        self.file3.write('\n')

    def json_write3(self, data):
        """@fn json_write3
            Funcion que se encarga de escribir datos en un archivo .json Memory
            @param data Datos que van a ser escritos en archivo
        """
        json.dump(data, self.file2)
        self.file2.write('\n')

    def write_header(self):
        """@fn write_header
            Funcion que se encarga de escribir una cabezera en el archivo .json
            en este caso la cabezaer son las opciones de configuracion del
            usuario, es lo que va ir primero en el archivo.
        """
        print
        filename = self.ls.compose_name(self.metadata['filename'] + '_Down')
        self.file = open(filename, "w")
#        filename2 = ls.db_file(self.metadata['filename'] + '_MEMORY')
#        self.filename2 = filename2
#        self.file2 = open(filename2, "w")

        self.json_write(self.metadata)
#        self.json_write3(self.metadata)

    def write_data(self, data):
        """@fn write_data
            Funcion que se encarga de escribir datos de una trama normal
            en el archivo .json Down
            @param data trama de datos
        """
        print 'data writing: {:}'.format(data)
        if self.file:
            self.json_write({'Normal': data})

    def write_memory_data(self, _id, data):
        """@fn write_memory_data
            Funcion que se encarga de escribir datos de una trama normal en el
            archivo .json Memory
            @param data trama de datos
        """
        if self.file:
            self.json_write3({'Memory': data})

    def write_data2(self, data):
        """@fn write_data2
            Funcion que se encarga de escribir datos de una trama normal en el
            archivo .json Down, antes era Up
            @param data trama de datos
        """
        if self.file:
            self.json_write({'Normal': data})

    def end_writing(self):
        """@fn end_writing
            Se encarga de cerrar los archivo .json para que no queden abiertos
            por el software
        """
        if self.file:
            self.file.close()
        if self.file2:
            self.file2.close()


class DataBase2():
    """
        @b Clase @b que @b representa @b a @b DataBase2 @b encargada @b de @b
        escribir @b los @b datos @b en @b la @b base @b de @b datos @b json
    """
    modes = ['_time', '_down', '_up']
    drive_mode = None

    def __init__(self, filename, drive_mode, count, options, **kwargs):
        """
            Constructor de la clase DataBase2
            @param filename Nombre del archivo (STRING)
            @param options Opciones de usuario ha configurado para el servcio
            que haya elegido
        """
        self.global_buffer = kwargs['globalBuffer']
        ls = self.global_buffer['ls']
        self.datetime = datetime.now()
        self.filename = filename
        self.filename3 = None
        self.file = None
        self.file3 = None
        self.drive_mode = drive_mode
        if options:
            self.metadata = options
            self.metadata['datetime'] = self.datetime.strftime('%d-%m-%y %H:%M:%S')
            self.metadata['filename'] = filename
            self.metadata['version'] = ls.VERSION
        self.count = count

    def open_file_append(self):
        """@fn open_file_append
            Para abrir una base de datos, experimental
        """
        filename = self.ls.compose_name(self.filename)
        self.file = open(filename, "a")

    def json_write(self, data, test_mode):
        """@fn json_write
            Funcion que se encarga de escribir datos en un archivo .json Down
            @param data Datos que van a ser escritos en archivo
        """
        if test_mode == 'time_drive':
            json.dump(data, self.file['_time'])
            self.file['_time'].write('\n')
        elif test_mode == 'depth_drive_down':
            json.dump(data, self.file['_down'])
            self.file['_down'].write('\n')
        elif test_mode == 'depth_drive_up':
            json.dump(data, self.file['_up'])
            self.file['_up'].write('\n')

    def write_header(self):
        """@fn write_header
            Funcion que se encarga de escribir una cabezera en el archivo
            .json en este caso la cabezaer son las
            opciones de configuracion del usuario, es lo que va ir primero en
            el archivo Down y Up.
        """
        eqiv = copy(self.modes)
        val = ['time_drive', 'depth_drive_down', 'depth_drive_up']
        tupl = zip(eqiv, val)
        md = dict(tupl)
        self.file = {}
        for m in self.modes:
            name = self.ls.compose_name(self.metadata['filename'] + m)
            self.file[m] = open(name, "w")
            self.json_write(self.metadata, md[m])

    def write_data(self, data):
        """@fn write_data
            Funcion que se encarga de escribir datos de una trama normal en
            el archivo .json Down
            @param data trama de datos
        """
        self.json_write({'Normal': data}, self.drive_mode)

    def end_writing(self):
        """@fn end_writing
            Se encarga de cerrar los archivo .json para que no queden abiertos
            por el software
        """
        #names of files generated
        names = []
        #check files and eliminate the ones that doesnt work
        for f in self.file:
            names.append(os.path.abspath(self.file[f].name))
            self.file[f].close()

            if names[-1].endswith('time.jdb') and self.drive_mode.startswith('time'):
                self.keep_file(names[-1])
            elif names[-1].endswith('_up.jdb') and self.drive_mode.endswith('_up'):
                self.keep_file(names[-1])
            elif names[-1].endswith('_down.jdb') and self.drive_mode.endswith('_down'):
                self.keep_file(names[-1])
            else:
                self.remove_file(names[-1])
        self.count += 1

    def remove_file(self, filename):
        """remove a file, needs absolute path of the file to remove"""
        try:
            os.remove(filename)
        except:
            print 'no existe ' + filename

    def keep_file(self, filename):
        """keeps a file adding a mark related to the sequence of data
        generation, needs an absolute path as input"""
        tmpl = '_({:}).jdb'
        copyfile(filename, filename.replace('.jdb', tmpl.format(self.count)))
        self.remove_file(filename)
