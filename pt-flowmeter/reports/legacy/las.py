#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
Emdyp.me
Revisado: Pedro Rivera
Script: Tipo A
(guardado y manipulacion de datos)
'''
import re
import json
import unittest
from datetime import datetime
from las_templates import *
import config
import os
import copy
#==============================================================================
# AUXILIAR FUNCTIONS
#==============================================================================


def organizeData(data, order):
    """build ordered string of data for write in las file"""
    for item in order:
        if item == order[0]:
            orderedData = '{:.2f}'.format(data[item])
        else:
            orderedData += ' {:.2f}'.format(data[item])
    orderedData += '\n'
    return orderedData

#==============================================================================
# PRINCIPAL LAS CLASS
#==============================================================================


class LasFile():
    """
         Clase  que  representa  la  Clase  LasFile
    """
    def __init__(self, metadata, data):
        """
            Constructor de la clase LasFile
            @param metada Contiene informacion de datos para configiracion
            de exportacion
            @param data Datos que van a ser exportados
        """
        a = open('a', 'w')
        m = json.dumps(data)
        a.write(m)
        a.close()

        self.metadata = metadata
        self.filename = config.las_file(os.path.splitext(os.path.basename(self.metadata['filename']))[0])
        self.file = None

        if metadata['index'] == "TIME":
            self.index_units = 'SECONDS'
            noindex = "DEPTH"
        if metadata['index'] == "DEPTH":
            self.index_units = 'FEET'
            noindex = "TIME"

        try:
            if self.metadata['down']:
                self.fixed_data = self.prepare_data(data, 'down')
            else:
                self.fixed_data = self.prepare_data(data, 'up')
        except:
            print 'error found during las exportw'

        b = open('b', 'w')
        m = json.dumps(data)
        b.write(m)
        b.close()

        self.file = open(self.filename, 'w')

        self.content = {'VERS_U': '',
                        'VERS_DA': '2.0',
                        'VERS_DE': 'CWLS LOG ASCII STANDARD -VERSION 2.0',
                        'WRAP_U': '',
                        'WRAP_DA': 'NO',
                        'WRAP_DE': 'ONE LINE PER DEPTH STEP',
                        'STRT_U': self.index_units,
                        'STRT_DA': self.fixed_data[0][0],
                        'STRT_DE': 'START ' + metadata['index'],
                        'STOP_U': self.index_units,
                        'STOP_DA': self.fixed_data[-1][0],
                        'STOP_DE': 'STOP ' + metadata['index'],
                        'STEP_U': self.index_units,
                        'STEP_DA': metadata['sampling_rate'],
                        'STEP_DE': 'STEP',
                        'NULL_U': '',
                        'NULL_DA': '-999.25',
                        'NULL_DE': 'NULL VALUE',
                        'COMP_U': '',
                        'COMP_DA': self.metadata['company'],
                        'COMP_DE': 'COMPANY',
                        'WELL_U': '',
                        'WELL_DA': self.metadata['well'],
                        'WELL_DE': 'WELL',
                        'FLD_U': '',
                        'FLD_DA': self.metadata['field'],
                        'FLD_DE': 'FIELD',
                        'LOC_U': '',
                        'LOC_DA': self.metadata['location'],
                        'LOC_DE': 'LOCATION',
                        'PROV_U': '',
                        'PROV_DA': '',
                        'PROV_DE': 'PROVINCE',
                        'CNTY_U': '',
                        'CNTY_DA': self.metadata['county'],
                        'CNTY_DE': 'COUNTY',
                        'STAT_U': '',
                        'STAT_DA': self.metadata['state'],
                        'STAT_DE': 'STATE',
                        'CTRY_U': '',
                        'CTRY_DA': self.metadata['country'],
                        'CTRY_DE': 'COUNTRY',
                        'SRVC_U': '',
                        'SRVC_DA': self.metadata['service_company'],
                        'SRVC_DE': 'SERVICE COMPANY',
                        'DATE_U': '',
                        'DATE_DA': self.metadata['datetime'],
                        'DATE_DE': 'LOG DATE',
                        'UWI_U': '',
                        'UWI_DA': self.metadata['well_id'],
                        'UWI_DE': 'UNIQUE WELL ID',
                        'LIC_U': '',
                        'LIC_DA': self.metadata['ercb'],
                        'LIC_DE': 'ERCB LICENCE NUMB',
                        'TAB1': ' ' * 5,
                        'TAB2': ' ' * 5}

        self.curve_content_lg4t = {'INDEX_U': self.index_units,
                                   'INDEX_DA': metadata['index'],
                                   'INDEX_DE': metadata['index'],
                                   'D1_U': '',
                                   'D1_DA': 'GAMMA_RAY',  # 2
                                   'D1_DE': 'GAMMA_RAY',
                                   'D2_U': '',
                                   'D2_DA': 'CCL',  # 1
                                   'D2_DE': 'CCL',
                                   'D3_U': 'N',
                                   'D3_DA': 'TENSION',  # 3
                                   'D3_DE': 'TENSION',
                                   'D4_U': 'FT',
                                   'D4_DA': noindex,  # 4
                                   'D4_DE': noindex,
                                   'TAB1': ' ' * 10,
                                   'TAB2': ' ' * 10,
                                   'TAB3': ''}

        self.curve_content_lg3t = self.curve_content_lg4t
        self.curve_content_lg3t['D5_U'] = ''
        self.curve_content_lg3t['D5_DA'] = 'AVG_GR'
        self.curve_content_lg3t['D5_DE'] = 'AVG_GAMMA_RAY'

        self.curve_content_pt5t = {'INDEX_U': self.index_units,
                                   'INDEX_DA': metadata['index'],
                                   'INDEX_DE': metadata['index'],
                                   'D1_U': '',
                                   'D1_DA': 'TEMP1',  # 11
                                   'D1_DE': 'TEMP1',
                                   'D2_U': 'C',
                                   'D2_DA': 'TEMP2',  # 10
                                   'D2_DE': 'TEMP2',
                                   'D3_U': 'C',
                                   'D3_DA': 'PRESS2',  # 1
                                   'D3_DE': 'PRESS2',
                                   'D4_U': 'PSI',
                                   'D4_DA': 'PRESS1',  # 2
                                   'D4_DE': 'PRESS1',
                                   'D5_U': 'PSI',
                                   'D5_DA': 'ACC_X1',  # 3
                                   'D5_DE': 'ACC_X1',
                                   'D6_U': 'DEG',
                                   'D6_DA': 'ACC_Y1',  # 7
                                   'D6_DE': 'ACC_Y1',
                                   'D7_U': 'DEG',
                                   'D7_DA': 'ACC_Z1',  # 8
                                   'D7_DE': 'ACC_Z1',
                                   'D8_U': 'DEG',
                                   'D8_DA': 'ACC_X2',  # 9
                                   'D8_DE': 'ACC_X2',
                                   'D9_U': 'DEG',
                                   'D9_DA': 'ACC_Y2',  # 4
                                   'D9_DE': 'ACC_Y2',
                                   'D10_U': 'DEG',
                                   'D10_DA': 'ACC_Z2',  # 5
                                   'D10_DE': 'ACC_Z2',
                                   'D11_U': 'DEG',
                                   'D11_DA': '--',  # 6
                                   'D11_DE': '--',
                                   'D12_U': '',
                                   'D12_DA': 'TENSION',  # 14
                                   'D12_DE': 'TENSION',
                                   'D13_U': 'FT',
                                   'D13_DA': 'DEPTH',  # 12
                                   'D13_DE': 'DEPTH',
                                   'D14_U': '',
                                   'D14_DA': 'ENCODER',  # 13
                                   'D14_DE': 'ENCODER',
                                   'TAB1': ' ' * 10,
                                   'TAB2': ' ' * 10,
                                   'TAB3': ''}

        self.curve_content_pt6t = self.curve_content_pt5t
        self.curve_content_pt6t['D15_U'] = ''
        self.curve_content_pt6t['D15_DA'] = 'AVG_GR'
        self.curve_content_pt6t['D15_DE'] = 'AVG_GAMMA_RAY'

        # write general headers
        self.write_metadata(header1 % self.content)

        if self.metadata['mode'] == 'lg3t':
            self.write_metadata(header_lg3t % self.curve_content_lg3t)
        if self.metadata['mode'] == 'lg4t':
            self.write_metadata(header_lg4t % self.curve_content_lg4t)
        if self.metadata['mode'] == 'pt5t':
            self.write_metadata(header_lg5t % self.curve_content_lg5t)
        if self.metadata['mode'] == 'pt6t':
            self.write_metadata(header_lg6t % self.curve_content_lg6t)
        for row in self.fixed_data:
            self.write_data(row, self.metadata['mode'])

        self.end_write()

    def write_metadata(self, metadata):
        """@fn write_metadata
            Escribe en el fichero la metadata del LAS
            @param metadata Datos meta
        """
        if self.file:
            self.file.write(metadata)

    def write_data(self, data, mode):
        """write ordered data in las file depending on the display mode
           numbers in order variable are the index of signals in data matrix"""
        # 4 tracks file
        if mode == 'lg4t':
            order = [0, 2, 1, 3, 4]
        # 3 tracks file
        elif mode == 'lgt3':
            order = [0, 2, 1, 3, 4, 5]
        # 5 tracks file
        elif mode == 'pt5t':
            order = [0, 11, 10, 1, 2, 3, 9, 7, 8, 4, 5, 14, 12]
        # 6 tracks file
        elif mode == 'pt6t':
            order = [0, 11, 10, 1, 2, 3, 9, 7, 8, 4, 5, 14, 12, 15]
        # write data in las file
        if self.file:
            self.file.write(organizeData(data, order))

    def end_write(self):
        """@fn end_write
            Finaliza la cerrada del fichero de exportacion LAS
        """
        if self.file:
            self.file.close()

    def prepare_data(self, data, mode):
        """@fn prepare_data_down
            Se encarga de organizar datos segun el samplingrate que tengan los
            datos para organizar bien la informacion en el archivo LAS
            @param data Datos a exportar
            mode puede ser up o down
        """
        #if mode is down, data must be sorted normal, otherwise must be sorted
        #in a reverse order, key parameter says data should be sorted by
        #first atribute, that is time in this data list
        if mode == 'down':
            fixed_data = sorted(data, key=lambda d: d[0])
        else:
            fixed_data = sorted(data, key=lambda d: d[0], reverse=True)
        return fixed_data
