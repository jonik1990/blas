# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
import json
import re


class export_center():
    """center of export, do all the relations for extract information of json
       database -- could be better, doing an asignattion automatic for tracks
       but it's encoded with another part of external code'"""

    def __init__(self):
        super(export_center, self).__init__()
        self.testData = ('test.jdb')

    def decompozed_hedaer(self, header):
        """decompoze a header, first line in a json datafile, here it looks,
           for number of tracks than have information about units"""

        #look for number of tracks and extract the value
        nT = re.compile('_[\d]+_tracks": true')
        numberTracks = nT.findall(header)
        n = numberTracks[0][1:(len(numberTracks) - 15)]
        n = int(n)

        #asign keys for every track

        if n == 4:
            #4 tracks
            self.listVar = ['index', 'ccl', 'gr', 'depth', 'enc', 'ten']

        if n == 3:
            #3 tracks
            self.listVar = [
                            'index', 'ccl', 'gr', 'depth', 'enc', 'ten', 'avggr'
                           ]

        if n == 5:
            #5 tracks
            self.listVar = ['index', 'temp1', 'temp2', 'pres', 'accx', 'accy',
                            'accz', 'res1', 'res2', 'res3', 'ccl', 'gr',
                            'depth', 'enc', 'ten']

        if n == 6:
            #6 tracks
            self.listVar = ['index', 'temp1', 'temp2', 'pres', 'accx', 'accy',
                           'accz', 'res1', 'res2', 'res3', 'ccl', 'gr',
                           'depth', 'enc', 'ten', 'avggr']

    def decompoze(self):
        """decomposed a json data in a dictionary, easy to read"""

        #converting
        dataDict = {}
        db_file = open(self.testData, "r")

        keyValues = []
        #name of all lines
        n = 0
        for line in db_file:
            if n == 0:
                #process header
                self.decompozed_hedaer(line)
                #create a k list, of k variables
                for k in self.listVar:
                    keyValues.append([])
            else:
                try:
                    #list of meassurements
                    data = list(json.loads(line).values())
                    data = data[0]

                    #add a new element in dictionary named in key list
                    for i in range(len(data)):
                        keyValues[i].append(data[i])
                except:
                    n += 1
            n += 1
        #save a ordened dict with values
        for i in range(len(self.listVar)):
            dataDict[self.listVar[i]] = keyValues[i]
        db_file.close()
        return dataDict


if __name__ == '__main__':

    data = export_center().decompoze()
    print (data)