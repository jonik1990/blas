"""
las_templates
contains various templates for export las files
"""
#=============================================================================
# GENERAL HEADER
#=============================================================================

header1 = """~VERSION metadata
VERS.%(VERS_U)s %(TAB1)s %(VERS_DA)s %(TAB2)s :%(VERS_DE)s
WRAP.%(WRAP_U)s %(TAB1)s %(WRAP_DA)s %(TAB2)s :%(WRAP_DE)s
~WELL
#MNEM.UNIT %(TAB1)s DATA %(TAB2)s DESCRIPTION
#---- ---- %(TAB1)s ---- %(TAB2)s -----------
STRT.%(STRT_U)s %(TAB1)s %(STRT_DA)s %(TAB2)s :%(STRT_DE)s
STOP.%(STOP_U)s %(TAB1)s %(STOP_DA)s %(TAB2)s :%(STOP_DE)s
STEP.%(STEP_U)s %(TAB1)s %(STEP_DA)s %(TAB2)s :%(STEP_DE)s
NULL.%(NULL_U)s %(TAB1)s %(NULL_DA)s %(TAB2)s :%(NULL_DE)s
COMP.%(COMP_U)s %(TAB1)s %(COMP_DA)s %(TAB2)s :%(COMP_DE)s
WELL.%(WELL_U)s %(TAB1)s %(WELL_DA)s %(TAB2)s :%(WELL_DE)s
FLD.%(FLD_U)s %(TAB1)s %(FLD_DA)s %(TAB2)s :%(FLD_DE)s
LOC.%(LOC_U)s %(TAB1)s %(LOC_DA)s %(TAB2)s :%(LOC_DE)s
PROV.%(PROV_U)s %(TAB1)s %(PROV_DA)s %(TAB2)s :%(PROV_DE)s
SRVC.%(SRVC_U)s %(TAB1)s %(SRVC_DA)s %(TAB2)s :%(SRVC_DE)s
DATE.%(DATE_U)s %(TAB1)s %(DATE_DA)s %(TAB2)s :%(DATE_DE)s
UWI.%(UWI_U)s %(TAB1)s %(UWI_DA)s %(TAB2)s :%(UWI_DE)s
LIC.%(VERS_U)s %(TAB1)s %(LIC_DA)s %(TAB2)s :%(LIC_DE)s
"""
#=============================================================================
# 4 TRACKS
#=============================================================================
header_lg4t = """~CURVE
#MNEM.UNIT %(TAB1)s API CODES %(TAB2)s CURVE DESCRIPTION
#---- ---- %(TAB1)s --------- %(TAB2)s -----------------
%(INDEX_DA)s.%(INDEX_U)s %(TAB2)s :%(INDEX_DE)s
%(D1_DA)s .%(D1_U)s  %(TAB2)s :%(D1_DE)s
%(D2_DA)s .%(D2_U)s  %(TAB2)s :%(D2_DE)s
%(D3_DA)s .%(D3_U)s  %(TAB2)s :%(D3_DE)s
%(D4_DA)s .%(D4_U)s  %(TAB2)s :%(D4_DE)s
~PARAMETER
#MNEM.UNIT %(TAB1)s VALUE %(TAB2)s DESCRIPTION
#---- ---- %(TAB1)s ----- %(TAB2)s -----------
~OTHER
#
#
~A
#%(INDEX_DA)s %(TAB3)s %(D1_DA)s %(TAB3)s %(D2_DA)s %(TAB3)s %(D3_DA)s %(TAB3)s %(D4_DA)s\n
"""
#=============================================================================
# 3 TRACKS
#=============================================================================
header_lg3t = """~CURVE
#MNEM.UNIT %(TAB1)s API CODES %(TAB2)s CURVE DESCRIPTION
#---- ---- %(TAB1)s --------- %(TAB2)s -----------------
%(INDEX_DA)s.%(INDEX_U)s %(TAB2)s :%(INDEX_DE)s
%(D1_DA)s .%(D1_U)s  %(TAB2)s :%(D1_DE)s
%(D2_DA)s .%(D2_U)s  %(TAB2)s :%(D2_DE)s
%(D3_DA)s .%(D3_U)s  %(TAB2)s :%(D3_DE)s
%(D4_DA)s .%(D4_U)s  %(TAB2)s :%(D4_DE)s
%(D5_DA)s .%(D5_U)s  %(TAB2)s :%(D5_DE)s
~PARAMETER
#MNEM.UNIT %(TAB1)s VALUE %(TAB2)s DESCRIPTION
#---- ---- %(TAB1)s ----- %(TAB2)s -----------
~OTHER
#
#
~A
#%(INDEX_DA)s %(TAB3)s %(D1_DA)s %(TAB3)s %(D2_DA)s %(TAB3)s %(D3_DA)s %(TAB3)s %(D4_DA)s %(D5_DA)s\n
"""
#=============================================================================
# 5 TRACKS
#=============================================================================
header_pt5t = """~CURVE
#MNEM.UNIT %(TAB1)s API CODES %(TAB2)s CURVE DESCRIPTION
#---- ---- %(TAB1)s --------- %(TAB2)s -----------------
%(INDEX_DA)s.%(INDEX_U)s %(TAB2)s :%(INDEX_DE)s
%(D1_DA)s .%(D1_U)s  %(TAB2)s :%(D1_DE)s
%(D2_DA)s .%(D2_U)s  %(TAB2)s :%(D2_DE)s
%(D3_DA)s .%(D3_U)s  %(TAB2)s :%(D3_DE)s
%(D4_DA)s .%(D4_U)s  %(TAB2)s :%(D4_DE)s
%(D5_DA)s .%(D5_U)s  %(TAB2)s :%(D5_DE)s
%(D6_DA)s .%(D6_U)s  %(TAB2)s :%(D6_DE)s
%(D7_DA)s .%(D7_U)s  %(TAB2)s :%(D7_DE)s
%(D8_DA)s .%(D8_U)s  %(TAB2)s :%(D8_DE)s
%(D9_DA)s .%(D9_U)s  %(TAB2)s :%(D9_DE)s
%(D10_DA)s .%(D10_U)s  %(TAB2)s :%(D10_DE)s
%(D11_DA)s .%(D11_U)s  %(TAB2)s :%(D11_DE)s
%(D12_DA)s .%(D12_U)s  %(TAB2)s :%(D12_DE)s
%(D13_DA)s .%(D13_U)s  %(TAB2)s :%(D13_DE)s
%(D14_DA)s .%(D14_U)s  %(TAB2)s :%(D14_DE)s
~PARAMETER
#MNEM.UNIT %(TAB1)s VALUE %(TAB2)s DESCRIPTION
#---- ---- %(TAB1)s ----- %(TAB2)s -----------
~OTHER
#
#
~A
#%(INDEX_DA)s %(TAB3)s %(D1_DA)s %(TAB3)s %(D2_DA)s %(TAB3)s %(D3_DA)s %(TAB3)s %(D4_DA)s %(TAB3)s %(D5_DA)s %(TAB3)s %(D6_DA)s %(TAB3)s %(D7_DA)s %(TAB3)s %(D8_DA)s %(TAB3)s %(D9_DA)s %(TAB3)s %(D10_DA)s %(TAB3)s %(D11_DA)s %(TAB3)s %(D12_DA)s %(TAB3)s %(D13_DA)s %(TAB3)s %(D14_DA)s\n
"""
#=============================================================================
# 6 TRACKS
#=============================================================================
header_pt6t = """~CURVE
#MNEM.UNIT %(TAB1)s API CODES %(TAB2)s CURVE DESCRIPTION
#---- ---- %(TAB1)s --------- %(TAB2)s -----------------
%(INDEX_DA)s.%(INDEX_U)s %(TAB2)s :%(INDEX_DE)s
%(D1_DA)s .%(D1_U)s  %(TAB2)s :%(D1_DE)s
%(D2_DA)s .%(D2_U)s  %(TAB2)s :%(D2_DE)s
%(D3_DA)s .%(D3_U)s  %(TAB2)s :%(D3_DE)s
%(D4_DA)s .%(D4_U)s  %(TAB2)s :%(D4_DE)s
%(D5_DA)s .%(D5_U)s  %(TAB2)s :%(D5_DE)s
%(D6_DA)s .%(D6_U)s  %(TAB2)s :%(D6_DE)s
%(D7_DA)s .%(D7_U)s  %(TAB2)s :%(D7_DE)s
%(D8_DA)s .%(D8_U)s  %(TAB2)s :%(D8_DE)s
%(D9_DA)s .%(D9_U)s  %(TAB2)s :%(D9_DE)s
%(D10_DA)s .%(D10_U)s  %(TAB2)s :%(D10_DE)s
%(D11_DA)s .%(D11_U)s  %(TAB2)s :%(D11_DE)s
%(D12_DA)s .%(D12_U)s  %(TAB2)s :%(D12_DE)s
%(D13_DA)s .%(D13_U)s  %(TAB2)s :%(D13_DE)s
%(D14_DA)s .%(D14_U)s  %(TAB2)s :%(D14_DE)s
%(D15_DA)s .%(D15_U)s  %(TAB2)s :%(D15_DE)s
~PARAMETER
#MNEM.UNIT %(TAB1)s VALUE %(TAB2)s DESCRIPTION
#---- ---- %(TAB1)s ----- %(TAB2)s -----------
~OTHER
#
#
~A
#%(INDEX_DA)s %(TAB3)s %(D1_DA)s %(TAB3)s %(D2_DA)s %(TAB3)s %(D3_DA)s %(TAB3)s %(D4_DA)s %(TAB3)s %(D5_DA)s %(TAB3)s %(D6_DA)s %(TAB3)s %(D7_DA)s %(TAB3)s %(D8_DA)s %(TAB3)s %(D9_DA)s %(TAB3)s %(D10_DA)s %(TAB3)s %(D11_DA)s %(TAB3)s %(D12_DA)s %(TAB3)s %(D13_DA)s %(TAB3)s %(D14_DA)s %(TAB3)s %(D15_DA)s\n
"""