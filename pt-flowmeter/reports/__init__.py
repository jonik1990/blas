# -*- coding: utf-8 -*-

import os
import json
import re
from .excel import Excel
#from pdf import Pdf
from .las import las


class decompozer():
    """Do all the relations for extract information of json
       database -- could be better, doing an asignattion automatic for tracks
       but it's encoded with another part of external code'"""

    def __init__(self, dataset):
        super(decompozer, self).__init__()
        self.testData = (dataset)
        self.tracks = 0
        self.nT = re.compile('_[\d]+_tracks": true')

    def decompozed_hedaer(self, header):
        """decompoze a header, first line in a json datafile, here it looks,
           for number of tracks than have information about units"""

        #this value is give by database
        #look for metadata info
        config = json.loads(header)

        #SAMPLING AND DEPTH
        #print (config['logingmodedialog_options']['comboboxes']['cb_sampling_rate'])
        #print (config['logingmodedialog_options']['spinbuttons']['sb_depth_zero'])

        #VARIABLES IN FILE
        #print (config['tracks_options']['comboboxes'])

        #VERSION
        #print (config['version'])

        self.filename = config['filename']
        if (config["logingmodedialog_options"]["radiobuttons"]
                                             ["rad_real_time"] is True):
            ind = 'TIME'
        else:
            ind = 'DEPTH'

        ##with metadata info, extracts information
        self.metadata = {
            'index': ind,
            'sampling_rate': config['logingmodedialog_options']
                                ['comboboxes']['cb_sampling_rate'][0],
            'company': config['config_options']['entries']
                                                       ['ent_company'],
            'well': config['config_options']['entries']['ent_well'],
            'field': config['config_options']['entries']['ent_field'],
            'location': config['config_options']['entries']
                                                      ['ent_location'],
            'county': config['config_options']['entries']['ent_county'],
            'state': config['config_options']['entries']['ent_state'],
            'country': config['config_options']['entries']
                                                       ['ent_country'],
            'service_company': config['config_options']['entries']
                                               ['ent_service_company'],
            'datetime': config['datetime'],
            'well_id': config['config_options']['entries']
                                                       ['ent_well_id'],
            'ercb': config['config_options']['entries']['ent_ercb']}

        if self.metadata['index'] == "TIME":
            units = 'SECONDS'
            noindex = "DEPTH"
        if self.metadata['index'] == "DEPTH":
            units = 'FEET'
            noindex = "TIME"

        #look for number of tracks and extract the value
        numberTracks = self.nT.findall(header)
        n = numberTracks[0][1:(len(numberTracks) - 15)]
        try:
            n = int(n)
        except:
            n = numberTracks[0][1:(len(numberTracks) - 16)]
            n = int(n)
        self.tracks = n

        #asign keys for every track

        if n == 4:
            #4 tracks
            self.listVar = [
                self.metadata['index'], 'CCL', 'GAMMA_RAY', 'DEPTH',
                'TENSION'
                ]

        if n == 3:
            #3 tracks
            self.listVar = [
                self.metadata['index'], 'CCL', 'GAMMA_RAY', noindex,
                'ENCODER', 'TENSION', 'AVG_GR'
               ]

        if n == 5:
            #5 tracks
            self.listVar = [
                self.metadata['index'], 'TEMP1', 'TEMP2', 'PRESS1',
                'ACCX1', 'ACCY1', 'ACCZ1', 'ACCX2', 'ACCY2',
                'ACCZ2', 'CCL', 'GAMMA_RAY',
                'DEPTH', 'ENCODER', 'TENSION']

        if n == 6:
            #6 tracks
            self.listVar = [
                self.metadata['index'], 'TEMP1', 'TEMP2', 'PRESS1',
               'ACCX1', 'ACCY1', 'ACCZ1', 'ACCX2', 'ACCY2', 'ACCZ2',
               'CCL', 'GAMMA_RAY', noindex, 'ENCODER', 'TENSION',
               'AVG_GR'
               ]

    def decompoze(self):
        """decomposed a json data in a dictionary, easy to read"""

        #converting
        dataDict = {}
        db_file = open(self.testData, "r")

        keyValues = []
        #name of all lines
        n = 0
        for line in db_file:
            if n == 0:
                #process header
                self.decompozed_hedaer(line)

                #create a k list, of k variables
                for k in self.listVar:
                    keyValues.append([])
            else:
                try:
                    #list of meassurements
                    data = list(json.loads(line).values())
                    data = data[0]
                    #add a new element in dictionary named in key list
                    for i in range(len(data)):
                        keyValues[i].append(data[i])
                except:
                    n += 1
            n += 1
        #save a ordened dict with values
        for i in range(len(self.listVar)):
            dataDict[self.listVar[i]] = keyValues[i]
        db_file.close()

        index = self.metadata['index']
        vals = [min(dataDict[index]), max(dataDict[index])]
        return dataDict, vals, self.tracks, self.filename, self.metadata
###############################################################################


class export_center():

    def set_data(self, dataset, saving_folder=None):
        super(export_center, self).__init__()
        self.data, self.vals, self.tracks, self.filename, self.metadata = (
            decompozer(dataset).decompoze()
            )
        #if not file path given, create one
        if not saving_folder:
            self.file_path = os.path.join(
                os.getenv('HOME'),
                self.filename
                )
        else:
            self.file_path = saving_folder

    def to_las(self):
        """create a las file with factory standar, requiere a dict with
           information and create las file returning path of file"""
        print (self.file_path)
        path = las().genFile(
            self.data,
            self.vals,
            self.tracks,
            self.file_path,
            self.metadata
            )
        return path

    def to_excel(self):
        """create a excel file with factory standar, requiere a dict with
           information and create excel file returning path of file"""
        print (self.file_path)
        path = Excel().genFile(
            self.data,
            self.file_path
            )
        return path

    def to_pdf(self):
        """create a pdf file with factory standar, requiere a dict with
           information and create pdf file returning path of file"""
        pass
        #path = Pdf().genFile(
            #self.data,
            #self.filename,
            #self.metadata
            #)
        #return path


#if __name__ == '__main__':

    #wk = export_center('test/2016_6_1__Down12-15-32.jdb')
    #wk.Elas()
    ##wk.Eexcel()
    #wk.Epdf()
#