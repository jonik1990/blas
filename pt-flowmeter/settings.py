# -*- coding: utf-8 -*-

###############################################################################
# LOGGING SETTINGS
###############################################################################

lg_settings = {
    'default': {
        'format': '[%(name)s-%(asctime)s] - %(levelname)s - %(message)s',
        'datefmt': '%m/%d/%Y_%H:%M:%S'
        }
    }
"""
contains settings for logging foratters
"""

###############################################################################
# DATA STORAGE
###############################################################################


jdb_data_layout = {
    'time_drive': {
        'pt': [
            'time', 'temp1', 'temp2', 'pres', 'accx', 'accy',
            'accz', 'res1', 'res2', 'res3', 'ccl', 'gr',
            'depth', 'enc', 'ten', 'avggr'
            ],
        'gr_ccl': [
            'time', 'ccl', 'gr', 'ten', 'depth', 'avggr'
            ],
        'flowmeter': [
            'time', 'temp1', 'pres1', 'x1', 'y1', 'z1', 'depth', 'ten'
            ],
        'flowmeter-pt': [],
        },

    'depth_drive_up': {
        'pt': [
            'depth', 'temp1', 'temp2', 'pres', 'accx', 'accy',
            'accz', 'res1', 'res2', 'res3', 'ccl', 'gr',
            'time', 'enc', 'ten', 'avggr'],
        'gr_ccl': [
            'depth', 'ccl', 'gr', 'ten', 'time', 'avggr'],
        'flowmeter': [
            'depth', 'temp1', 'pres1', 'x1', 'y1', 'z1', 'time', 'ten'
            ],
        'flowmeter-pt': [],
        },

    'depth_drive_down': {
        'pt': [
            'depth', 'temp1', 'temp2', 'pres', 'accx', 'accy',
            'accz', 'res1', 'res2', 'res3', 'ccl', 'gr',
            'time', 'enc', 'ten', 'avggr'],
        'gr_ccl': [
            'depth', 'ccl', 'gr', 'ten', 'time', 'avggr'],
        'flowmeter': [
            'depth', 'temp1', 'pres1', 'x1', 'y1', 'z1', 'time', 'ten'
            ],
        'flowmeter-pt': [],
        }
    }
"""
contains variables layout for all services and daq mode of blas
first level have the daq mode, can be:
    - depth_drive_up
    - depth_drive_down
    - time_drive
the second level have services, currently supported services are:
    - pt
    - gr_ccl
    - flowmeter
in the third level there are a list with variable names in order
"""

###############################################################################
# SERIAL SETTINGS
###############################################################################

#orders_to_panel = {
    ##configure pt
    #'st': [2, val.hsb, val.lsb],
    #'sf': [3, val.hsb, val.lsb],
    #'comp': [4, val.hsb, val.lsb],
    #'gn': [5, val.hsb, val.lsb],
    ##configure tool
    #'set_id': [6, value, 0],
    #'get_id': [7, 0, 0],
    ##configure daq mode
    #'listen_mode': [8, 1, value],
    #'rollover_mode': [8, 2, 0],
    #'pin_mode': [8, 3, value],
    ##ask for panel reading
    #'panel_data': ['c', 0, 0],
    ##configure gr_ccl
    #'gr_gain': [1, 1, value, 'sl'],
    #'ccl_gain': [0, 0, value, 'sl'],
    #'hpf': [0, 3, value, 'sl', 1, 0, value, 'sl', 1, 2, value],
    #'lpf': [1, 3, value, 'sl', 0, 2, value, 'sl', 0, 1, value],
    #}

#panel_answers = {
    #}