#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
script description
author: Pedro Rivera
to know more, read ...
"""

################################################################################
# IMPORTS
################################################################################
import os
from reports import export_center
from tkinter import filedialog, messagebox, Tk, Button  # Label, Button, Text
#from tkinter import StringVar, RAISED, INSERT, END


class exporterInterface(Tk):
    """gui of application to export blas data to different formats"""
    def __init__(self):
        """builder of application"""
        super(exporterInterface, self).__init__()
        #=================================================================
        # GENERAL
        #=================================================================
        self.direccion = os.path.dirname(__file__)
        self.statusTempl = 'Estatus: {msj}'
        self.busy = False
        #=================================================================
        # GUI
        #=================================================================
        self.title('BLAS DATA EXPORTATION')
        #need a button to pick file
        self.pick_file_button = Button(
            self,
            text='SELECT A FILE TO EXPORT',
            command=self.pick_file
            )
        self.pick_file_button.pack()
        #need a button to export data
        self.export_button = Button(
            self,
            text='EXPORT DATA',
            command=self.export_files
            )
        self.export_button.pack()
        #=================================================================
        # CORE
        #=================================================================
        self.reporter = export_center()
        #=================================================================
        # CONFIGURATIONS
        #=================================================================
        self.fi = None
        self.fo = None

    #=================================================================
    # BUTTON HANDLERS
    #=================================================================

    def pick_file(self):
        """pick a file"""
        #get the path of the file and its container folder
        self.fi = filedialog.askopenfilename()
        self.fo = os.path.dirname(self.fi)
        self.reporter.file_path = self.fo

    def export_files(self):
        """handles the button to export files"""
        print('exportig files')
        #if there is no path, tell user to provide and stop
        if self.fi is None:
            #send a warning to the user about not file picked
            messagebox.showwarning(
                title='no .jdb file picked',
                message='please pick a jdb file to export'
                )
        else:
            #if there is a path, export in the path folder in a new folder
            saving_folder = os.path.join(self.fo, 'reports', self.fi)
            if not os.path.exists(saving_folder):
                os.mkdir(saving_folder)
                os.chdir(saving_folder)
            self.reporter.set_data(self.fi, saving_folder=saving_folder)
            #do file exportation to each format
            self.reporter.to_excel()
            self.reporter.to_pdf()
            self.reporter.to_las()
            #notify file export finished
            messagebox.showinfo(
                title='operation completed',
                message='files succesfully exported at {:}'.format(
                    saving_folder
                    )
                )


if __name__ == '__main__':
    gui = exporterInterface()
    gui.mainloop()