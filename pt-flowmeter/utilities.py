# -*- coding: utf-8 -*-
"""
script for utilities, this script contains:

* jpt_logger: default logger for jpt software
* bit_parse: parse a number given in its lsb and hsb
* get_active_ports
* get_suitable_ports
* fake_serial

"""

#general imports
import os
import re
import gc
import serial
import logging as lg
from sys import version_info
from random import randrange

#local imports
from datetime import datetime
from settings import lg_settings

###############################################################################
# LOGGER
###############################################################################


class jpt_logger():
    """default logger class for jpt software"""
    def __init__(self, name='jpt', mode='normal'):
        """creates a logger, requires a name of the module of function that
        creates the logger in order to name it. valid_mode are = [
            'normal',
            'only_screen',
            'only_file',
            'quiet'
            ]"""
        self.cursor = lg.getLogger(name)
        self.cursor.setLevel(lg.DEBUG)
        # create formatter
        formatter = lg.Formatter(
            fmt=lg_settings['default']['format'],
            datefmt=lg_settings['default']['datefmt']
            )
        #create handlers depending on mode variables
        if mode in ['normal', 'only_screen']:
            # create console handler and set level to debug
            ch = lg.StreamHandler()
            ch.setLevel(lg.DEBUG)
            # add formatter to ch
            ch.setFormatter(formatter)
            # add ch to logger
            self.cursor.addHandler(ch)
        if mode in ['normal', 'only_file']:
            # create file handler and set level to debug
            tmpl_name = name + '_%Y%m%d_%H-%M-%S.log'
            self.filename = datetime.now().strftime(tmpl_name)
            fh = lg.FileHandler(self.filename)
            # add formatter to fh
            fh.setFormatter(formatter)
            # add fh to logger
            self.cursor.addHandler(fh)

    def disable(self):
        """disable logging of data if needed"""
        self.cursor.setLevel(lg.WARNING)


class bit_parse():
    """parse a numeric value into hsb-lsb and return it in object"""
    def __init__(self, value):
        """build object"""
        number = int(value)
        self.hsb = int(number / 256)
        self.lsb = int(number - (self.hsb * 256))

    def __str__(self):
        return 'hsb: {d.hsb}, lsb: {d.lsb}'.format(d=self)


def safe_byte_encode(value):
    """
    choose safety bytes encoding for python 2 and 3
    """
    value = int(value)
    if version_info.major == 3:
        return bytes([value])
    elif version_info.major == 2:
        return chr(value)

###############################################################################
# SERIAL UTILITIES
###############################################################################


# Escoger una implementacion de puerto serial dependiendo del sistema operativo
if os.name == 'nt':
    from serial.tools.list_ports_windows import *
elif os.name == 'posix':
    from serial.tools.list_ports_posix import *
else:
    raise ImportError(
        "no Serial implementation for your platform ('%s')" % (os.name,)
        )


def get_active_ports():
    """return a list of strings with ports without processing or validation
    """
    iterator = sorted(comports())
    all_ports = []
    for port, desc, hwid in iterator:
        all_ports.append(port)
    return all_ports


def get_suitable_ports(baud=19200, mode='normal'):
    """list suitable serial ports,
    in normal mode make a strict verification,
    in a quiet mode, make a ligth verification"""
    sysports = re.compile('(/dev/ttyS[\d]+)|([\d]+portsfound)')
    # Get ports list
    port_list = get_active_ports()
    port_list = [a for a in port_list if not sysports.match(a) and not a == '']
    # Check ports in list
    suitable_ports = list()
    for port in port_list:
        suitable = True
        try:
            j = serial.Serial(port=port, baudrate=baud, timeout=10)
            if mode != 'quiet':
                j.read()
            j.close()
        except Exception as e:
            return e
            suitable = False
        if suitable:
            suitable_ports.append(port)
    # Clear serial connections
    if suitable_ports != []:
        del j
    gc.collect()
    return suitable_ports


class fakeSerial():
    """fake a serial communication"""
    def write(self, data):
        """fake writing function"""
        print((data, type(data)))

    def read(self, data_len):
        """fake reading function"""
        data_array = [safe_byte_encode(randrange(10)) for i in range(data_len)]
        return b''.join(data_array)