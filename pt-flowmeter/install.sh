#!/bin/bash
echo ====================================
echo installing python management packages
echo ====================================
sudo apt-get install python-pip python3-pip
echo ====================================
echo installing blas dependencies
echo ====================================
echo ====================================
sudo pip install pyserial
echo ====================================
echo installing blas exporter dependencies
echo ====================================
sudo pip3 install pandas openpyxl
echo ====================================
echo adding $USER TO DIALOUT GROUP
echo ====================================
sudo adduser $USER dialout
echo ====================================
echo ====================================
echo restart user session or computer to start working with blas