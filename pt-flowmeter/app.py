#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
application to export blas data to different formats such as .las, pdf, excel
from a .jdb source file
author: Pedro Rivera
to know more, read reports/test.py
"""

################################################################################
# IMPORTS
################################################################################
import os
#import json
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from reports import export_center


class exporterInterface():
    """gui of application to export blas data to different formats"""
    def __init__(self):
        """builder of application"""
        #=================================================================
        # GENERAL
        #=================================================================
        self.direccion = os.path.dirname(__file__)
        self.statusTempl = 'Estatus: {msj}'
        self.busy = False
        #=================================================================
        # GUI
        #=================================================================
        # Crear el constructor de la interfaz
        interface = Gtk.Builder()
        # Construir la interfaz desde archivo
        interface.add_from_file(os.path.join(
            self.direccion,
            'gui_files',
            'blas_exporter.glade')
            )
        # Conectar señales de la interfaz con los metodos de la clase
        interface.connect_signals(self)
        # Extraer el widget window
        self.window = interface.get_object('m_window')
        # Extraer boton file chooser
        self.btn_pick_file = interface.get_object('btn_pick_file')
        # Extraer boton export file
        self.btn_export_action = interface.get_object('btn_export_action')
        #=================================================================
        # CORE
        #=================================================================
        self.reporter = export_center()
        #=================================================================
        # CONFIGURATIONS
        #=================================================================

    def gtk_main_quit(self, widget, *args):
        """stops application"""
        Gtk.main_quit(*args)

    #=================================================================
    # BUTTON HANDLERS
    #=================================================================

    def export_files(self, widget, *args):
        """handles the button to export files"""
        print('exportig files')
        #get the path of the file
        fo = self.btn_pick_file.get_current_folder()
        fi = self.btn_pick_file.get_uri()[7:]
        #if there is no path, tell user to provide and stop
        if fi is None:
            warning_dialog = Gtk.MessageDialog()
            warning_dialog.window.show_all()
        else:
            #if there is a path, export in the path folder in a new folder
            saving_folder = os.path.join(fo, 'reports')
            self.reporter.set_data(fi)
            if not os.path.exists(saving_folder):
                os.mkdir(saving_folder)
                os.chdir(saving_folder)
            self.reporter.to_excel()
            self.reporter.to_pdf()
            self.reporter.to_las()
            #os.chdir(self.direccion)


def app_main():
    """main app code"""
    i = exporterInterface()
    i.window.show_all()

if __name__ == '__main__':
    app_main()
    Gtk.main()