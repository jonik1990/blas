from gui.tracks import *
import time
import random


def launch_threaded(function, *args, **kwargs):
    """@fn launch_threaded
        Funcion para ejecutar el hilo de graficado aleatorio, solo para modo
        de prueba
    """
    t = threading.Thread(None, function, 'Tacks', args, kwargs)
    t.setDaemon(True)
    t.start()

"""
def run(t):
        current_time = 800
        t.set_autoscroll(False)
        while current_time > 0:
            current_time -= 1
            if current_time % 50==0:
                gobject.idle_add(t.set_data, 0,0,(66000,current_time))
            else:
                gobject.idle_add(t.set_data, 0,0,(random.randint(-1000,10000),current_time))
            time.sleep(0.1)
        t.set_autoscroll(False)
        print t.draw()

if __name__ == "__main__":

    import time
    import random

    def main():

        def on_scale(w, t):
            print w.get_value()
            t.set_track_options(0, {'size_request': (int(w.get_value()), -1)})
            t.set_line_options(0, 0, None, {'size_request': (int(w.get_value()), -1)})

        t = ComposeTrackWidget()

        t.add_wtrack(0, False)
        t.add_line(0,0)


        t.set_line_options(0, 0, {'span': (-1000, 10000), 'color': (1,0,0), 'wrapper': 1, 'down':False, },
                          {'size_request': (200, -1),'labels': (-1000, 10000), 'line_name': 'Temperature 1', 'line_name_color': (1,0,0)})

        default_textgraph_options = {'bg_color': "#000066",
                                     'size_request': (40, -1),
                                     'height_scale': 100,
                                     'step': 50,
                                     'text_color': (1,1,1)}
        t.add_ttrack(0) #falta poner opciones a este
        t.set_ttrack_options(0, default_textgraph_options)

        t.set_track_options(0, {'size_request': (200, -1)})

        gtk.gdk.threads_init() #@UndefinedVariable

        launch_threaded(run, t)

        window = gtk.Window()
        b = gtk.Button('Image!')
        s = gtk.HScale(gtk.Adjustment(value=800, lower=50, upper=900))
        s.connect("value-changed", on_scale, t)
        current_time = [-1]
        vbox = gtk.VBox()
        window.set_title("Compose Track Widget")
        vbox.pack_start(t)
        vbox.pack_start(b,False,False)
        vbox.pack_start(s,False,False)
        window.add(vbox)
        window.set_size_request(800, 600)
        window.connect("destroy", gtk.main_quit)
        window.show_all()
        gtk.main()
    main()

"""


def run(t):
        current_time = -1
        t.set_autoscroll(True)
        current_time = 10000
        while current_time > 0:
            print 'current time: {:}'.format(current_time)
            current_time -= 1
            if current_time % 50 == 0:
                gobject.idle_add(
                    t.set_data, 0, 0, (66000, current_time)
                    )
            else:
                gobject.idle_add(
                    t.set_data, 0, 0, (random.randint(-1000, 10000), current_time)
                    )

            gobject.idle_add(
                t.set_data, 0, 1, (random.randint(-1000, 2000), current_time)
                )
            gobject.idle_add(
                t.set_data, 0, 2, (random.randint(-1000, 2000), current_time)
                )
            gobject.idle_add(
                t.set_data, 1, 0, (random.randint(-1000, 3000), current_time)
                )
            gobject.idle_add(
                t.set_data, 1, 1, (random.randint(-1000, 4000), current_time)
                )
            gobject.idle_add(
                t.set_data, 1, 2, (random.randint(-1000, 2000), current_time)
                )
            gobject.idle_add(
                t.set_data, 2, 0, (random.randint(-1000, 5000), current_time)
                )
            gobject.idle_add(
                t.set_data, 2, 1, (random.randint(-1000, 6000), current_time)
                )
            gobject.idle_add(
                t.set_data, 3, 0, (random.randint(-1000, 6000), current_time)
                )
            gobject.idle_add(
                t.set_data, 3, 1, (random.randint(-4000, 10000), current_time)
                )
            gobject.idle_add(
                t.set_data, 4, 0, (random.randint(32400, 32600), current_time)
                )
            #gobject.idle_add(t.draw)
            time.sleep(0.1)
        t.set_autoscroll(False)
        print t.draw()


if __name__ == "__main__":

    def on_btn_image_clicked(button, t, window, ct):

        ct[0] += 1
        current_time = ct[0]
        t.set_autoscroll(False)
        t.set_data(0, 0, (random.randint(-1000, 1000), current_time))

        #p = window.get_size_request()
        adj = t.scrolled_window.get_vadjustment()
        window.set_size_request(-1, int(adj.upper) + 100)
        gobject.idle_add(t.print_image)
        #window.set_size_request(*p)

    def main():

        t = ComposeTrackWidget()

        t.add_wtrack(0)
        t.add_line(0, 0)
        t.add_line(0, 1)
        t.add_line(0, 2)

        t.set_line_options(
            0, 0,
            {
                'span': (-1000, 10000),
                'color': (1, 0, 0),
                'wrapper': 1,
                'max_data': 300,
                'down': False},
            {
                'labels': (-1000, 10000),
                'line_name': 'Temperature 1',
                'line_name_color': (1, 0, 0)}
            )

        t.set_line_options(
            0, 1,
            {
                'span': (-1000, 10000),
                'color': (0, 0, 1),
                'dashed': True,
                'max_data': 300,
                'down': False},
            {
                'labels': (-1000, 10000),
                'line_name': 'Temperature 2',
                'line_name_color': (0, 0, 1)}
            )
        t.set_line_options(
            0, 2,
            {
                'span': (-1000, 10000),
                'color': (0, 0, 1),
                'dashed': False,
                'max_data': 300,
                'down': False},
            {
                'labels': (-1000, 10000),
                'line_name': 'Press',
                'line_name_color': (0, 0, 1)}
            )

        default_textgraph_options = {
            'bg_color': "#000066",
            'size_request': (40, -1),
            'height_scale': 100,
            'step': 50,
            'text_color': (1, 1, 1)}
        t.add_ttrack(0)  # falta poner opciones a este
        t.set_ttrack_options(0, default_textgraph_options)

        t.add_wtrack(1)
        t.add_line(1, 0)
        t.add_line(1, 1)
        t.add_line(1, 2)
        t.set_line_options(
            1, 0,
            {
                'span': (-1000, 10000),
                'color': (.5, 0, .5),
                'wrapper': 0,
                'max_data': 300,
                'down': False},
            {
                'labels': (-1000, 10000),
                'line_name': 'ACC x',
                'line_name_color': (.5, 0, .5),
                'bg_color': '#ee0'}
            )
        t.set_line_options(
            1, 1,
            {
                'span': (-1000, 10000),
                'color': (0, .5, .5),
                'max_data': 300,
                'down': False},
            {
                'labels': (-1000, 10000),
                'line_name': 'ACC y',
                'line_name_color': (0, .5, .5),
                'bg_color': '#033',
                'labels_color': (1, 1, 1)}
            )
        t.set_line_options(
            1, 2,
            {
                'span': (-1000, 10000),
                'color': (0, .5, .5),
                'max_data': 300,
                'down': False},
            {
                'labels': (-1000, 10000),
                'line_name': 'ACC z',
                'line_name_color': (0, .5, .5),
                'bg_color': '#033',
                'labels_color': (1, 1, 1)}
            )

        t.add_wtrack(2)
        t.add_line(2, 0)
        t.add_line(2, 1)
        t.add_line(2, 2)
        t.set_line_options(
            2, 0,
            {
                'span': (-1000, 10000),
                'color': (.5, 1, 0),
                'wrapper': 0,
                'width': 1,
                'max_data': 300,
                'down': False},
            {
                'labels': (-1000, 10000),
                'line_name': 'Resisitivity 1',
                'line_name_color': (.5, 1, 0)}
            )
        t.set_line_options(
            2, 1,
            {
                'span': (-1000, 10000),
                'color': (1, .5, 0),
                'width': 3,
                'max_data': 300,
                'down': False},
            {
                'labels': (-1000, 10000),
                'line_name': 'Resisitivity 2',
                'line_name_color': (1, .5, 0)}
            )
        t.set_line_options(
            2, 2,
            {
                'span': (-1000, 10000),
                'color': (1, .5, 0),
                'width': 3,
                'max_data': 300,
                'down': False},
            {
                'labels': (-1000, 10000),
                'line_name': 'Resisitivity 3',
                'line_name_color': (1, .5, 0)}
            )

        op1 = {
            'bg_color': '#eee',
            'size_request': (-1, -1),
            'width_scale': 100,
            'height_scale': 100,
            'h_grids': 10,  # cada cuanto habra lineas horizontales
            'v_grids': 10,  # cada cuanto habra lineas verticales
            'grid_line_width': 1,
            'grid_dashed': True,
            'strong_grid': 10,  # cada cuanto se hace una linea vertical fuerte
            'grid_color': (0.5, 0.5, 0.5),
            'strong_grid_color': (0, 0, 0),
            'begin_scrolling': 40,  # cuanndo empieza a hacer scroll
            'pointer_label': False,
            'autoscroll': True}
        t.set_track_options(1, op1)

        op2 = {
            'bg_color': '#000',
            'size_request': (-1, -1),
            'width_scale': 100,
            'height_scale': 100,
            'h_grids': 20,  # cada cuanto habra lineas horizontales
            'v_grids': 10,  # cada cuanto habra lineas verticales
            'grid_line_width': 2,
            'grid_dashed': False,
            'strong_grid': 10,  # cada cuanto se hace una linea vertical fuerte
            'grid_color': (.4, .4, .4),
            'strong_grid_color': (1, 1, 1),
            'begin_scrolling': 40,  # cuanndo empieza a hacer scroll
            'pointer_label': False,
            'autoscroll': True}

        t.set_track_options(2, op2)

        t.add_wtrack(3)
        t.add_line(3, 0)
        t.add_line(3, 1)
        t.set_line_options(
            3, 0,
            {
                'span': (-1000, 10000),
                'color': (1, 0, 0),
                'wrapper': 0,
                'width': 1,
                'max_data': 300,
                'down': False},
            {
                'labels': (-1000, 10000),
                'line_name': 'CCL',
                'line_name_color': (1, 0, 0)}
            )
        t.set_line_options(
            3, 1,
            {
                'span': (-1000, 10000),
                'color': (0, 0, 1),
                'wrapper': 1,
                'width': 2,
                'max_data': 300,
                'down': False},
            {
                'labels': (-1000, 10000),
                'line_name': 'Gamma Ray',
                'line_name_color': (0, 0, 1)}
            )

        t.add_wtrack(4)
        t.add_line(4, 0)
        t.set_line_options(
            4, 0,
            {
                'span': (32400, 32600),
                'color': (1, 0, 0),
                'wrapper': 0,
                'width': 1,
                'last_value': True,
                'max_data': 300,
                'down': False},
            {
                'labels': (32400, 32600),
                'line_name': 'Encoder',
                'line_name_color': (1, 0, 0)}
            )

        t.add_ttrack(1)  # falta poner opciones a este

        gtk.gdk.threads_init()  # UndefinedVariable

        launch_threaded(run, t)

        window = gtk.Window()
        b = gtk.Button('Image!')
        current_time = [-1]
        b.connect("clicked", on_btn_image_clicked, t, window, current_time)
        vbox = gtk.VBox()
        window.set_title("Compose Track Widget")
        vbox.pack_start(t)
        vbox.pack_start(b, False, False)
        window.add(vbox)
        window.set_size_request(800, 600)
        window.connect("destroy", gtk.main_quit)
        window.show_all()
        gtk.main()

    main()
