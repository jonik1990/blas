# -*- coding: utf-8 -*-
"""
Created on Fri Jun 19 15:14:07 2015

@author: ging
"""
import os
import csv
import datetime
#matplotlib imports
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


class exportFile():
    """export data into different kind of file"""
    # indicates if data array has been created
    dataBuilt = False
    data = []

    def __init__(self, data, options, saveFolder):
        """builder method of exportFile class, needs the dictionary with data
        and the dictionary with options"""
        self.datainput = data
        self.opt = options
        self.saveFolder = saveFolder

    def getdata(self):
        """return data constructed in 2D list to write files"""
        #check if data arrays have been calculated previously
        if self.dataBuilt:
            return
        else:
            #build data in order to be consumed for the rest of the object
            #change state of dataBuilt to True
            self.dataBuilt = True

    def getFileName(self, extension):
        """return a string with a full direction of a file name given it's
        extension, supports .pdf, .csv and .xls"""
        name = datetime.datetime.now().strftime('%w_%b_%Y_%X')
        filename = os.path.join(self.saveFolder[extension],
                                '{:}.{:}'.format(name, extension))
        return filename

    def exportPDF(self):
        """export data to pdf file"""
        self.getdata()
        pdfFile = self.getFileName('pdf')
        plt.plot(self.getdata())
        plt.savefig(pdfFile)

    def exportExcel(self):
        """export data to MS Excel compatible format"""
        self.getdata()
        with open(self.getFileName('xls'), 'wb') as f:
            writer = csv.writer(f, dialect='excel-tab')
            writer.writerows(self.getdata())
            f.close()

    def exportCSV(self):
        """export data to CSV plain text file"""
        self.getdata()
        with open(self.getFileName('csv'), 'wb') as f:
            writer = csv.writer(f)
            writer.writerows(self.getdata())
            f.close()
