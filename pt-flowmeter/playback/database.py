# -*- encoding: utf-8 -*-
'''
Emdyp.me
Revisado: Pedro Rivera
Script: Tipo A
(composicion de informacion para guardado de datos)
[Guardado]
'''

import json
from datetime import datetime

import config
from utilities import jpt_logger


class DataBase():
    """
         Clase  que  representa  a  DataBase  encargada  de
         escribir  los  datos  en  la  base  de  datos  json
    """
    def __init__(self, filename, options=None):
        """
            Constructor de la clase DataBase
            @param filename Nombre del archivo (STRING)
            @param options Opciones de usuario ha configurado
            para el servcio que haya elegido
        """
        #added logging captabilities
        self.lg = jpt_logger(name='DataBase')
        self.log = self.lg.cursor
        self.lg.disable()

        self.datetime = datetime.now()
        self.filename = filename
        self.filename2 = None
        #self.filename3 = None
        self.file = None
        self.file2 = None
        #self.file3 = None
        if options:
            self.metadata = options
            self.metadata['datetime'] = self.datetime.strftime(
                '%d-%m-%y %H:%M:%S'
                )
            self.metadata['filename'] = filename
            self.metadata['version'] = config.VERSION

    def open_file_append(self):
        """@fn open_file_append
            Para abrir una base de datos, experimental
        """
        filename = config.db_file(self.filename)
        filename2 = config.db_file(self.filename2)
        self.file = open(filename, "a")
        self.file2 = open(filename2, "a")

    def json_write(self, data):
        """@fn json_write
            Funcion que se encarga de escribir datos en un archivo .json Down
            @param data Datos que van a ser escritos en archivo
        """
        json.dump(data, self.file)
        self.file.write('\n')

    def json_write2(self, data):
        """@fn json_write2
            Funcion que se encarga de escribir datos en un archivo .json Up
            @param data Datos que van a ser escritos en archivo
        """
        json.dump(data, self.file3)
        self.file3.write('\n')

    #def json_write3(self, data):
        #"""@fn json_write3
            #Funcion que se encarga de escribir datos en un archivo .json Memory
            #@param data Datos que van a ser escritos en archivo
        #"""
        #json.dump(data, self.file2)
        #self.file2.write('\n')

    def write_header(self):
        """@fn write_header
            Funcion que se encarga de escribir una cabezera en el archivo .json
            en este caso la cabezaer son las opciones de configuracion del
            usuario, es lo que va ir primero en el archivo.
        """
        filename = config.db_file(self.metadata['filename'] + '_Down')
        self.log.debug(
            'time_drive: jdb filename: %s, metadata_filename: %s',
            filename,
            self.metadata['filename'],
            )
        self.file = open(filename, "w")
        self.json_write(self.metadata)

    def write_data(self, data):
        """@fn write_data
            Funcion que se encarga de escribir datos de una trama normal
            en el archivo .json Down
            @param data trama de datos
        """
        self.log.debug(
            'data writing: %s',
            data
            )
        #raise Exception('im a jackass!!')
        for i in [0, 12]:
            data[i] = round(data[i], 4)
        if self.file:
            self.json_write({'Normal': data})

    #def write_memory_data(self, _id, data):
        #"""@fn write_memory_data
            #Funcion que se encarga de escribir datos de una trama normal en el
            #archivo .json Memory
            #@param data trama de datos
        #"""
        #if self.file:
            #self.json_write3({'Memory': data})

    def write_data2(self, data):
        """@fn write_data2
            Funcion que se encarga de escribir datos de una trama normal en el
            archivo .json Down, antes era Up
            @param data trama de datos
        """
        self.log.debug(
            'depth-drive-up: jdb filename: %s, data_filename: %s',
            self.filename,
            data
            )
        raise Exception('we have a bad guy :P')
        if self.file:
            self.json_write({'Normal': data})

    def end_writing(self):
        """@fn end_writing
            Se encarga de cerrar los archivo .json para que no queden abiertos
            por el software
        """
        if self.file:
            self.file.close()
        if self.file2:
            self.file2.close()

###############################################################################
# LOAD JDB FILE
###############################################################################


def load_db(filename):
    """
        Funcion que se encarga de cargar todos los datos de un previo registro
        en un archivo de texto plano .json
        @param filename Nombre del fichero .jdb donde esta la informacion
        almacenada
        @return options, data opciones de configuracion del servicio con que
        se hizo el  registro y los datos de dicho registro
    """
    db_file = open(filename, "r")
    line = db_file.readline()
    options = json.loads(line)

    raw_data = []

    if options['config_options']['radiobuttons']['rad_pt_service']:
        # variables in 5 tracks file
        variables = ['index', 'temp1', 'temp2',
                     'pres', 'accx', 'accy',
                     'accz', 'res1', 'res2',
                     'res3', 'ccl', 'gr',
                     'depth', 'enc', 'ten']
        if options['config_options']['radiobuttons']['rad_pt_6_tracks']:
            # if file has 6 tracks, append avggr variable
            variables.append('avggr')
        data = {}
        for variable in variables:
            data[variable] = []
        while True:
            line = db_file.readline()
            if not line:
                break
            d = json.loads(line)
            # data comes from normal file
            if 'Normal' in d:
                row = d['Normal']
                # build data array
                for item in range(len(variables)):
                    if row[item] != -1:
                        if item == 0:
                            data[variables[item]].append(row[item])
                        else:
                            data[variables[item]].append((row[item], row[0]))

            # data comes from mermory file
            elif 'Memory' in d:
                row = d['Memory']
                # get operation mode (depth or time)
                depth_opt = options['logingmodedialog_options']['radiobuttons']['rad_real_depth']
                time_opt = options['logingmodedialog_options']['radiobuttons']['rad_real_time']
                # build data array
                for a in range(1, len(variables)):
                    if row[a] != -1:
                        if depth_opt:
                            data[variables[a]].append((row[a], row[12]))
                        elif time_opt:
                            data[variables[a]].append((row[a], row[0]))
            # save raw_data
            raw_data.append(row)
    db_file.close()
    #print json.dumps(data)
    return options, data, raw_data
